<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 5:59 PM
 */
View::composer('views.layouts.header', function ($view)
{
    $articles = Article::all();

    $view->with('all_articles', $articles);
});
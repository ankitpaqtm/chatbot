<?php

function pr($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function paginationSize() {
    return 30;
}

function storeDateTime($date = '') {
    return $date != '' ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d H:i:s');
}

function startEndDate($date = '', $type = 'start') {
    $date = ($type == 'end' ? $date . ' 23:59:59' : $date);
    return $date != '' ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d H:i:s');
}

function dateFormat($date = '', $format = 'd-m-Y') {
    return $date != '' ? date($format, strtotime($date)) : date($format);
}

function todayDate($type = 'start') {
    $date = ($type == 'end' ? ' 23:59:59' : ' 00:00:00');
    return date('Y-m-d' . $date);
}

function makeFirstCapitalize($string = '') {
    $string = strtolower($string);
    // return ucfirst($string);
    $string = ucfirst($string);
    $string = preg_replace_callback('/[.!?].*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'), $string);
    return $string;
}

function statusVal($status, $type = '1') {
    $statusArray = statusArray($type);
    if ($status != '') {
        return $statusArray[$status];
    } else {
//        return $statusArray;
        return '';
    }
}

function statusClass($status) {
    $classArray = array('0' => 'danger', '1' => 'success', '2' => 'warning');
    if ($status != '') {
        return $classArray[$status];
    } else {
//        return $classArray;
        return '';
    }
}

function statusArray($type = '1') {
    if ($type == '1') {
        return array('0' => 'In-active', '1' => 'Active');
    } elseif ($type == '2') {
        return array('0' => 'Pending', '1' => 'Approved', '2' => 'Rejected');
    }
}

function folderArray($type = '', $path = '0') {
    $uploadFolder = public_path('uploads/');
    if ($path != '0') {
        $uploadFolder = '';
    }
    switch ($type) {
        case '1':
            return $uploadFolder . 'category/';
            break;
        case '2':
            return $uploadFolder . 'profile/';
            break;
        case '3':
            return $uploadFolder . 'seller/';
            break;
        case '4':
            return $uploadFolder . 'product/';
            break;
        case '5':
            return $uploadFolder . 'user/';
            break;

        default:
            return $uploadFolder . 'category/';
            break;
    }
}

function noImage_url() {
    return asset('/img/noImage.png');
}

//Function to get image Url from uploads folder
function imageUploadUrl($folder = '', $imgName = '') {
    if ($imgName != '' && check_image_exist($folder, $imgName)) {
        return uploadUrl() . folderArray($folder, $folder) . $imgName;
    } else {
        return noImage_url();
    }
}

//Function to check if image exist in folder
function check_image_exist($folder = '', $imgName = '') {
    $fileFolder = folderArray($folder) . $imgName;

    if (file_exists($fileFolder)) {
        return true;
    } else {
        return false;
    }
}

function checkIsset($postPara = '', $msgPara = '') {

    if (!isset($_REQUEST[$postPara]) || empty($_REQUEST[$postPara])) {
        $response['status'] = '0';
        // $response['error'] = 401;
        $response['message'] = makeFirstCapitalize($msgPara) . " is require.";
        echo json_encode($response);
        exit;
    } else {
        // return true;
        return $_REQUEST[$postPara];
    }
}

function encode($value) {
    return $value != '' ? base64_encode($value) : '';
}

function decode($value) {
    return $value != '' ? base64_decode($value) : '';
}

// function 
function array_filter_null_recursive($array) {
    if (count($array) > 0 && is_array($array)) {
        foreach ($array as $key => $value) {
            // pr($value);
            if (is_array($value) && empty($value)) {
                $value = array();
                $array[$key] = $value;
            } else if (is_array($value) && !empty($value)) {
                $array[$key] = $value;
            }
            if (is_null($value)) {
                $value = "";
                $array[$key] = $value;
            }
        }
    } else {
        if (is_null($array)) {
            $value = "";
            $array = $value;
        }
    }
    return $array;
}

function getdistance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}

function getAddressNew($latitude, $longitude) {
    if (!empty($latitude) && !empty($longitude)) {
        //Send request and receive json data by address
        $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '');
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        //Get address from json data
        $address = ($status == "OK") ? $output->results[1]->formatted_address : '';
        //Return address of the given latitude and longitude
        if (!empty($address)) {
            return $address;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getLatLong($address) {
    if (!empty($address)) {
        //Formatted address
        $formattedAddr = str_replace(' ', '+', $address);
        //Send request and receive json data by address
        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false');
        $output = json_decode($geocodeFromAddr);
        //Get latitude and longitute from json data
        $data['latitude'] = $output->results[0]->geometry->location->lat;
        $data['longitude'] = $output->results[0]->geometry->location->lng;
        //Return latitude and longitude of the given address
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function GetDrivingDistance($lat1, $long1, $lat2, $long2) {
    if (!empty($lat1) && !empty($long1) && !empty($lat2) && !empty($long2)) {
        $geocodeFromLatLong = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving");
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        //Get address from json data
//        pr($output);
//        $address = ($status == "OK") ? $output->rows[0]->elements[0]->duration->text : '';

        if (!empty($output->rows[0]->elements[0]->duration->text) && $status == "OK") {
            return $output->rows[0]->elements[0]->duration->text;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//get emp status by task
function getempStatus($emp_id = 0, $taskId = 0, $startDate = "") {
    $isBusy = 0;
    $recurring_type = DB::table('tbl_task')
            ->select("recurring_type")
            ->where('task_id', $taskId)
            ->first();

    if ($recurring_type->recurring_type == 0) {

        $isEmpBusy = DB::table('tbl_emp_task')
                ->select("et_duedate", "task_id", "started")
                ->where('task_id', "!=", $taskId)
                ->where('status', "!=", "2")
                ->where('status', "!=", "-1")
                ->whereDate('et_duedate', "=", date("Y-m-d", strtotime($startDate)))
                ->where('emp_id', $emp_id)
                ->orderBy('et_id', 'DESC')
                ->first();

        if (!empty($isEmpBusy)) {

            $durationArr = DB::table('tbl_task')
                    ->select("duration")
                    ->where('task_id', $isEmpBusy->task_id)
                    ->first();

            $duration = $durationArr->duration;

            $dueDt = date("Y-m-d H:i", strtotime($isEmpBusy->et_duedate . "+$duration minutes"));

            $et_duedate = strtotime($dueDt);

            $currDate = strtotime($startDate);



            if ($et_duedate >= $currDate) {
                $isBusy = 1;
            }
        } else {
            $isBusy = 0;
        }
    } else {

        $isEmpBusy = DB::table('tbl_emp_task')
                ->select("et_duedate", "task_id", "started")
                ->where('task_id', "!=", $taskId)
                ->where('status', "!=", "2")
                ->where('status', "!=", "-1")
                ->whereDate('et_duedate', "=", date("Y-m-d", strtotime($startDate)))
                ->where('emp_id', $emp_id)
                ->orderBy('et_id', 'DESC')
                ->get();

        if (!empty($isEmpBusy)) {

            foreach ($isEmpBusy as $key => $val) {

                $durationArr = DB::table('tbl_task')
                        ->select("duration")
                        ->where('task_id', $val->task_id)
                        ->first();

                $duration = $durationArr->duration;

                $dueDt = date("Y-m-d H:i", strtotime($val->et_duedate . "+$duration minutes"));

                $et_duedate = strtotime($dueDt);

                $currDate = strtotime($startDate);

                if ($et_duedate >= $currDate) {
                    $isBusy = 1;
                }
            }
        } else {
            $isBusy = 0;
        }
    }

    return $isBusy;
}

//get emp status
function getEmployeeStatus($emp_id = 0) {
    $isBusy = 0;
    $date = date("Y-m-d H:i");

    $isEmpBusy = DB::table('tbl_emp_task')
            ->select("et_duedate", "task_id", "started")
            ->where('status', "!=", "2")
            ->where('status', "!=", "-1")
            ->whereDate('et_duedate', "=", date("Y-m-d"))
            ->where('emp_id', $emp_id)
            ->orderBy('et_id', 'DESC')
            ->first();


    if (!empty($isEmpBusy)) {

        $durationArr = DB::table('tbl_task')
                ->select("duration")
                ->where('task_id', $isEmpBusy->task_id)
                ->first();

        $Onlyduedate = strtotime(date("Y-m-d", strtotime($isEmpBusy->et_duedate)));
        $OnlycurrDate = strtotime(date("Y-m-d"));

        $duration = $durationArr->duration;

        $dueDt = date("Y-m-d H:i", strtotime($isEmpBusy->et_duedate . "+$duration minutes"));

        $et_duedate = strtotime($dueDt);
        $currDate = strtotime($date);
        if ($Onlyduedate == $OnlycurrDate) {

            if ($et_duedate >= $currDate) {
                $isBusy = 1;
            }
        }
    }

    return $isBusy;
}

function getEmployeeStatusByTask($emp_id = 0, $task_id = 0, $et_id = 0) {
    $isBusy = 0;
    $date = date("Y-m-d H:i");

    $isEmpBusy = DB::table('tbl_emp_task')
            ->select("et_duedate", "task_id", "started")
            ->where('status', "!=", "2")
            ->where('status', "!=", "-1")
            ->where('emp_id', $emp_id)
            ->where('et_id', $et_id)
            ->where('task_id', $task_id)
            ->orderBy('et_id', 'DESC')
            ->first();


    if (!empty($isEmpBusy)) {

        $durationArr = DB::table('tbl_task')
                ->select("duration")
                ->where('task_id', $isEmpBusy->task_id)
                ->first();

        $duration = $durationArr->duration;

        $dueDt = date("Y-m-d H:i", strtotime($isEmpBusy->et_duedate . "+$duration minutes"));

        $et_duedate = strtotime($dueDt);
        $currDate = strtotime($date);
        
        if ($et_duedate >= $currDate) {
            $isBusy = 1;
        }
    }

    return $isBusy;
}

function employeeDropdown($companyId = 0, $emp_id = 0) {

    $role = Session::get('role') != '' ? Session::get('role') : '';
    if ($role == 2) {
        $companyId = Session::get('uid') != '' ? Session::get('uid') : '';
    } else {
        $companyId = Session::get('companyid') != '' ? Session::get('companyid') : '';
    }


    $empList = DB::table('tbl_employee')
            ->select("emp_id", "emp_name")
            ->where('emp_status', 1)
            ->where('companyid', $companyId)
            ->get();
    $html = "";
    foreach ($empList as $key => $val) {
        $selected = "";
        if ($emp_id == $val->emp_id) {
            $selected = "selected=selected";
        }
        $html .= "<option value=" . $val->emp_id . ">" . $val->emp_name . "</option>";
    }
    echo $html;
}

function get_task_location_detail($task_id = 0, $location_id = 0, $field = "") {
    $response = "0";
    if ($task_id && $location_id && $field) {
        $parner_data = DB::table("tbl_task_client_location")
                ->select("$field")
                ->where('location_id', $location_id)
                ->where('task_id', $task_id)
                ->first();
        if ($parner_data->$field != "") {
            $response = $parner_data->$field;
        }
    }
    echo $response;
}

//*****--------------PUSH NOTIFICATION---------------------******//
function iosPushnotification($iphoneData = array(), $deviceId = "") {
    error_reporting(0);
    $passphrase = '123456';
    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', public_path('pem-files/push.pem'));

    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

    //for send box : ssl://gateway.sandbox.push.apple.com:2195
    //for live : ssl://gateway.push.apple.com:2195

    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

    if (!$fp)
        exit("Failed to connect: $err $errstr" . PHP_EOL);
    $payload = json_encode($iphoneData);

    $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $deviceId)) . pack('n', strlen($payload)) . $payload;
    $results = fwrite($fp, $msg, strlen($msg));

    if (!$results)
        $result = 'Message not delivered' . PHP_EOL;
    else
        $result = 'Message successfully delivered:' . $msg . PHP_EOL;

    fclose($fp);
    return $results;
}

function androidPushnotification($fields = array()) {
    if (empty($fields)) {
        return FALSE;
    }
    $fields = json_encode($fields);
    $url = 'https://fcm.googleapis.com/fcm/send';
    $headers = array(
        'Authorization: key=AIzaSyDoyOvG-vbTgqbEjBKcEGovnlslu0hbYEM',
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

    $result = curl_exec($ch);
//            echo $result;
    curl_close($ch);
    return $result;
}

//*****--------------END PUSH NOTIFICATION---------------------******//
?>

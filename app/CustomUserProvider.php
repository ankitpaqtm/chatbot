<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 2:50 PM
 */
namespace App;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use DB;

class CustomUserProvider implements UserProvider {

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */

    public function retrieveById($identifier)
    {
        // TODO: Implement retrieveById() method.


        $qry = User::where('admin_id','=',$identifier);

        if($qry->count() >0)
        {
            $user = $qry->select('admin_id', 'username', 'first_name', 'last_name', 'email', 'password')->first();

            $attributes = array(
                'id' => $user->admin_id,
                'username' => $user->username,
                'password' => $user->password,
                'name' => $user->first_name . ' ' . $user->last_name,
            );

            return $user;
        }
        return null;
    }

    /**
     * Retrieve a user by by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        // TODO: Implement retrieveByToken() method.
        $qry = User::where('EmailAddress','=',$identifier)->where('remember_token','=',$token);

        if($qry->count() >0)
        {
            $user = $qry->select('Username','EmailAddress', 'Password')->first();

            $attributes = array(
                'Username' => $user->username,
                'Password' => $user->password,

            );

            return $user;
        }
        return null;



    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        // TODO: Implement updateRememberToken() method.
        $user->setRememberToken($token);

        $user->save();

    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        // TODO: Implement retrieveByCredentials() method.
        $qry = User::where('Username','=',$credentials['Username']);

        if($qry->count() >0)
        {
            $user = $qry->select('Username','EmailAddress','Password')->first();




            return $user;
        }
        return null;


    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        print_r($credentials);
        exit;
        // TODO: Implement validateCredentials() method.
        // we'll assume if a user was retrieved, it's good

//        if($user->Username == $credentials['Username'] && $user->getAuthPassword() == md5($credentials['Password'].\Config::get('constants.SALT')))
//        {
//
//            $user->last_login_time = Carbon::now();
//            $user->save();
//
//            return true;
//        }

        if(!empty($credentials)) {
            $decryptedpas = md5($credentials['password']);
            $email = $credentials['email'];
            $password = $credentials['password'];
            try {
                $partner_data = DB::table('partnerslist')->select(DB::raw('company_name,partnerid,email_id'))->where('email_id', $email)->where('admin_password', $decryptedpas)->get()->first();
            } catch (QueryException $ex) {
                dd($ex->getMessage());
                // Note any method of class PDOException can be called on $ex.
            }

            if (!empty($partner_data)) {


                print_r($user);
                exit;
            } else {


                try {
                    $supervisor_data = DB::table('tbl_supervisor')->select(DB::raw('sup_name,companyid,supervisor_id,email'))->where('email', $email)->where('password', $password)->get()->first();
                } catch (QueryException $ex) {
                    dd($ex->getMessage());
                    // Note any method of class PDOException can be called on $ex.
                }

                if (!empty($supervisor_data)) {

                } else {
                    try {
                        $superadmin_data = DB::table('superadmin')->select(DB::raw('username'))->where('username', $email)->where('password', $password)->get()->first();
                    } catch (QueryException $ex) {
                        dd($ex->getMessage());
                        // Note any method of class PDOException can be called on $ex.
                    }

                    if (!empty($superadmin_data)) {
                        print_r($superadmin_data);
                        exit;
                    } else {

                    }

                }
            }
        }
        return false;


    }
}
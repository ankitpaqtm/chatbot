<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;
//@include('../resources/assets/momom-assets/calendar/jdf.php');

class Supervisors extends Model
{
    //
    protected $table='tbl_supervisor';
    public function getSupervisorsByCompanyId($companyid)
    {
        try {
            $parner_data=DB::table($this->table)->where('companyid', $companyid)->where('deleteval',0)->orderBy('status', 'asc')->get();
            
            
            
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function add_supervisors($post=array())
    {
        //echo "<pre>";print_r($post);print_r($_FILES);exit;
        $language=Session::get('language')!='' ? Session::get('language') : '';
        
        $error = 'no';
        $pfilename = $_FILES['profileimage']['name'];
        if(!empty($pfilename)){
            $file_type = $_FILES['profileimage']['type']; //returns the mimetype

            $allowed = array("image/jpeg", "image/gif", "image/png");
            if (!in_array($file_type, $allowed)) {
                $error_message = 'Only jpg, gif, and png files are allowed.';
                $error = 'yes';
            }

            $file = $_FILES['profileimage'];
            $destination_path = public_path('uploads/supervisors/');
            $filename = str_random(10);
            $pfilename = $filename.'-'.$pfilename;
            move_uploaded_file($_FILES["profileimage"]["tmp_name"], $destination_path.$pfilename);
        }

        if ($error == 'no') {

            if(isset($post['supervisor_id']) && $post['supervisor_id'] > 0){
                $updateArray['sup_name'] = $post["sup_name"];
                if(!empty($pfilename)){
                    $updateArray['pfilename'] = $pfilename;
                }
                $updateArray['email'] = $post["email"];
                $updateArray['mobilenum'] = $post["mobilenum"];
                if(isset($post["password"]) && !empty($post["password"])){
                    $updateArray['password'] = $post["password"];
                }
                if(isset($post["status"])){
                    $updateArray['status'] = $post["status"];
                }
                $updateArray['companyid'] = $post["companyid"];
                $updateArray['designation'] = "";
                $updateArray['deleteval'] =0;
                return DB::table($this->table)->where('supervisor_id', $post['supervisor_id'])->update($updateArray);
            } else {
                $updateArray['sup_name'] = $post["sup_name"];
                if(!empty($pfilename)){
                    $updateArray['pfilename'] = $pfilename;
                }
                $updateArray['email'] = $post["email"];
                $updateArray['mobilenum'] = $post["mobilenum"];
                $updateArray['companyid'] = $post["companyid"];
                $updateArray['status'] = $post["status"];
                $updateArray['password'] = $post["password"];
                $updateArray['designation'] = "";
                $updateArray['deleteval'] =0;
                $updateArray['createdat'] = date('Y-m-d H:i:s');
                return DB::table($this->table)->insert($updateArray);
            }
        }
    }

    /*
    Function used to get partner details by id
    */
    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('supervisor_id', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }
    
    function statusPartner($id,$status){
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['status'] = $status;
        return DB::table($this->table)->where('supervisor_id', $id)->update($updateArray);
    }
    function destroyPartner($id){
        $updateArray['deleteval'] = 1;
        return DB::table($this->table)->where('supervisor_id', $id)->update($updateArray);
    }
}

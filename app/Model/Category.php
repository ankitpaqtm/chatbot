<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;

class Category extends Model
{
    //
    protected $table='task_categories';
    public function getEmployeesByCompanyId($companyid)
    {
        try {
            $parner_data=DB::table($this->table)->where('partner_id', $companyid)->orderBy('status', 'desc')->orderBy('Id', 'desc')->get();
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function getActiveEmployeesByCompanyId($companyid)
    {
        try {
            $parner_data=DB::table($this->table)->where('partner_id', $companyid)->where('status','1')->orderBy('Id', 'desc')->get();
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function add_category($post=array())
    {
        //echo "<pre>";print_r($post);print_r($_FILES);exit;
        $language=Session::get('language')!='' ? Session::get('language') : '';
        if(isset($post['Id']) && $post['Id'] > 0){
            $updateArray['name'] = $post["name"];
            return DB::table($this->table)->where('Id', $post['Id'])->update($updateArray);
        } else {
            $updateArray['name'] = $post["name"];
            $updateArray['partner_id'] = $post["partner_id"];
            return DB::table($this->table)->insert($updateArray);
        }
    }

    /*
    Function used to get partner details by id
    */
    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('Id', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }
    
    function statusPartner($id,$status){
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['status'] = $status;
        return DB::table($this->table)->where('Id', $id)->update($updateArray);
    }
    
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;

@include('../../resources/assets/momom-assets/calendar/jdf.php');

class Partner extends Model {

    //
    protected $table = 'partnerslist';

    public function get_partner_list() {
        $company_id = Session::get('companyid') != '' ? Session::get('companyid') : '';
        try {
            $parner_data = DB::table($this->table)->select(DB::raw('partnerid,email_id,company_name,address,mobile,status,is_report,is_review,is_signature'))->where('deleteval', 0)->orderBy('status', 'asc')->get();
            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function add_partner($post = array()) {
        $language = Session::get('language') != '' ? Session::get('language') : '';
        if (isset($post["due_date"]) && !empty($post["due_date"])) {
//            if ($language == "persian") {
//                $arrFrom = explode("-", $post['due_date']);
//                $duedate = jalali_to_gregorian($arrFrom[0], $arrFrom[1], $arrFrom[2], "-");
//            } else {
                $duedate = $post['due_date'];
//            }
        }
        $error = 'no';
        $pfilename = $_FILES['clientlogo']['name'];
        if (!empty($pfilename)) {
            $file_type = $_FILES['clientlogo']['type']; //returns the mimetype

            $allowed = array("image/jpeg", "image/gif", "image/png");
            if (!in_array($file_type, $allowed)) {
                $error_message = 'Only jpg, gif, and png files are allowed.';
                $error = 'yes';
            }

            $file = $_FILES['clientlogo'];
            $destination_path = public_path('uploads/logos/');
            $filename = str_random(10);
            $pfilename = $filename . '-' . $pfilename;
            move_uploaded_file($_FILES["clientlogo"]["tmp_name"], $destination_path . $pfilename);
        }

         
        
        if ($error == 'no') {

            if (isset($post['partnerid']) && $post['partnerid'] > 0) {
                $updateArray['company_name'] = $post["company_name"];
                $updateArray['address'] = $post["address"];
                if (!empty($pfilename)) {
                    $updateArray['logo'] = $pfilename;
                }
                $updateArray['email_id'] = $post["email_id"];
                $updateArray['admin_usernmae'] = $post["admin_username"];
                if (isset($post["admin_password"]) && !empty($post["admin_password"])) {
                    $updateArray['admin_password'] = md5($post["admin_password"]);
                }
                if (isset($post["taskcount"]) && !empty($post["taskcount"])) {
                    $updateArray['taskcount'] = $post["taskcount"];
                }
                if (isset($post["supervisorcount"]) && !empty($post["supervisorcount"])) {
                    $updateArray['supervisorcount'] = $post["supervisorcount"];
                }
                if (isset($post["employeecount"]) && !empty($post["employeecount"])) {
                    $updateArray['employeecount'] = $post["employeecount"];
                }
                if (isset($post["clinetscount"]) && !empty($post["clinetscount"])) {
                    $updateArray['clinetscount'] = $post["clinetscount"];
                }
                if (isset($post["due_date"]) && !empty($post["due_date"])) {
                    $updateArray['due_date'] = $post["due_date"];
                }
                $updateArray['mobile'] = $post["mobile"];
                $updateArray['latitude'] = $post["latitude"];
                $updateArray['longitude'] = $post["longitude"];

                $is_report = 0;
                if (isset($post["is_report"]) && $post["is_report"] == 1) {
                    $is_report = 1;
                }
                $is_review = 0;
                if (isset($post["is_review"]) && $post["is_review"] == 1) {
                    $is_review = 1;
                }
                $is_signature = 0;
                if (isset($post["is_signature"]) && $post["is_signature"] == 1) {
                    $is_signature = 1;
                }
                
                
                $updateArray['is_report'] = $is_report;
                $updateArray['is_review'] = $is_review;
                $updateArray['is_signature'] = $is_signature;
                 
                
                
                DB::table($this->table)->where('partnerid', $post['partnerid'])->update($updateArray);
                return true;
                 
            } else {
  
                $is_report = 0;
                if (isset($post["is_report"]) && $post["is_report"] == 1) {
                    $is_report = 1;
                }
                $is_review = 0;
                if (isset($post["is_review"]) && $post["is_review"] == 1) {
                    $is_review = 1;
                }
                $is_signature = 0;
                if (isset($post["is_signature"]) && $post["is_signature"] == 1) {
                    $is_signature = 1;
                }
                
             
                
                return DB::table($this->table)->insert(
                                ['company_name' => $post["company_name"], 'address' => $post["address"], 'logo' => $pfilename, 'email_id' => $post["email_id"], 'admin_usernmae' => $post["admin_username"], 'admin_password' => md5($post["admin_password"]), 'taskcount' => $post["taskcount"],
                                    'supervisorcount' => $post["supervisorcount"], 'contractperiod' => 1, 'deleteval' => 0, 'employeecount' => $post["employeecount"], 'mobile' => $post["mobile"], 'clinetscount' => $post["clinetscount"], 'due_date' => $duedate, 'latitude' => $post["latitude"], 'longitude' => $post["longitude"]
                                    , 'is_report' => $is_report, 'is_review' => $is_review, 'is_signature' => $is_signature
                                ]
                );
            }
        }
    }

    /*
      Function used to get partner details by id
     */

    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('partnerid', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }

    function destroyPartner($id) {
        $updateArray['deleteval'] = 1;
        return DB::table($this->table)->where('partnerid', $id)->update($updateArray);
    }

    function statusPartner($id, $status) {
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['status'] = $status;
        return DB::table($this->table)->where('partnerid', $id)->update($updateArray);
    }

}

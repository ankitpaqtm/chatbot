<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;
use DateTime;
use DateInterval;
use DatePeriod;

//@include('../resources/assets/momom-assets/calendar/jdf.php');

class Task extends Model {

    //
    protected $table = 'tbl_task';
    protected $emptable = 'tbl_emp_task';
    protected $emp_task_assign_table = 'tbl_assign_task_emp';
    protected $tbl_task_client_location = 'tbl_task_client_location';
    protected $tbl_clients_location = 'tbl_clients_location';

    public function getTaskByCompanyId($companyid, $supervisor_id, $role = 2) {
        try {

            if ($role == 2) {
                $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')->where($this->table . '.comp_id', $companyid)->where($this->table . '.is_recurring', 0)->where($this->table . '.task_delete', 0)->orderBy($this->emptable . '.et_id', 'desc')->get();
            } else if ($role == 3) {
                $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')->where($this->emptable . '.supervisor_id', $supervisor_id)->where($this->table . '.comp_id', $companyid)->where($this->table . '.is_recurring', 0)->where($this->table . '.task_delete', 0)->orderBy($this->emptable . '.et_id', 'desc')->get();
            }
            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function getRecTaskByCompanyId($companyid, $supervisor_id, $role = 2) {
        try {

            if ($role == 2) {
                $parner_data = DB::table($this->table)
                        ->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')
                        ->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')
                        ->where($this->table . '.comp_id', $companyid)
                        ->where($this->table . '.is_recurring', 1)
                        ->where($this->table . '.task_delete', 0)
                        ->groupBy($this->emptable . '.task_id')
                        ->orderBy($this->emptable . '.et_id', 'desc')
                        ->get();
            } else if ($role == 3) {
                $parner_data = DB::table($this->table)
                        ->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')
                        ->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')
                        ->where($this->emptable . '.supervisor_id', $supervisor_id)
                        ->where($this->table . '.comp_id', $companyid)
                        ->where($this->table . '.is_recurring', 1)
                        ->where($this->table . '.task_delete', 0)
                        ->groupBy($this->emptable . '.task_id')
                        ->orderBy($this->emptable . '.et_id', 'desc')
                        ->get();
            }
            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function getRecTaskByTaskId($taskId) {
        try {
            $parner_data = DB::table($this->table)
                    ->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')
                    ->select($this->table . '.*', $this->emptable . '.et_duedate', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')
                    ->where($this->table . '.is_recurring', 1)
                    ->where($this->table . '.task_delete', 0)
                    ->where($this->emptable . '.task_id', $taskId)
                    ->get();
            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function getTaskByTaskId($task_id) {
        try {
            $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')->where($this->table . '.task_id', $task_id)->where($this->table . '.task_delete', 0)->orderBy($this->emptable . '.et_id', 'desc')->get();

            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function getSingleTaskByTaskId($task_id) {
        try {
            $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status', $this->emptable . '.et_duedate', $this->emptable . '.et_hours', $this->emptable . '.et_mins', $this->emptable . '.et_id')->where($this->table . '.task_id', $task_id)->where($this->table . '.task_delete', 0)->orderBy($this->emptable . '.et_id', 'desc')->first();

            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function add_task($post = array()) {
//        echo "<pre>";print_r($post);

        $language = Session::get('language') != '' ? Session::get('language') : '';
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if (isset($post['task_id']) && $post['task_id'] > 0) {
            $task_id = $post['task_id'];
            $updateArray['task_cat'] = $post["categoryid"];
            $updateArray['task_name'] = $post["task_name"];
            $updateArray['additional_instruction'] = $post["additional_desc"];
            $updateArray['duration'] = $post['duration'];
            $updateArray['tasktype'] = $post['task_type'];
            $updateArray['clientid'] = $post['clientid'];
            $updateArray['is_recurring'] = $post['is_recurring_task'];
//            $updateArray['recurring_type'] = $post['recurring_type'];
            $updateArray['recurring_end_date'] = $post['recurring_end_date'];
            DB::table($this->table)->where('task_id', $post['task_id'])->update($updateArray);

            if (!empty($post["client_multi_location"])) {
               
                DB::table($this->tbl_task_client_location)
                        ->where('client_id', $post['clientid'])
                        ->where('task_id', $post['task_id'])
                        ->delete();
                
                
                for ($i=0;$i < count($post["client_multi_location"]);$i++) {
                    $insertArray1['location_id'] = $post["client_multi_location"][$i];
                    $insertArray1['sort_location'] = $post["sort_location"][$i];
                    $insertArray1['client_id'] = $post['clientid'];
                    $insertArray1['task_id'] = $post['task_id'];
                    $insertArray1['created_date'] = date('Y-m-d H:i:s');
                    $res = DB::table($this->tbl_task_client_location)->insert($insertArray1);
                }
            }
        } else {
            $updateArray['comp_id'] = $post["comp_id"];
            $updateArray['task_cat'] = $post["categoryid"];
            $updateArray['loc_id'] = "1";
            $updateArray['task_name'] = $post["task_name"];
            $updateArray['task_status'] = 1;
            $updateArray['additional_instruction'] = $post["additional_desc"];
            $updateArray['duration'] = $post['duration'];
            $updateArray['createdat'] = date('Y-m-d H:i:s');
            $updateArray['tasktype'] = $post['task_type'];
            $updateArray['companyid'] = '0';
            $updateArray['clientid'] = $post['clientid'];
            $updateArray['task_delete'] = '0';
            $updateArray['is_from_app'] = '0';
            $updateArray['is_recurring'] = $post['is_recurring_task'];
            $updateArray['recurring_type'] = $post['recurring_type'];
            $updateArray['recurring_end_date'] = $post['recurring_end_date'];

            $task_id = DB::table($this->table)->insertGetId($updateArray);

            if (!empty($post["client_multi_location"])) {
//                foreach ($post["client_multi_location"] as $val) {
//                    $insertArray1['location_id'] = $val;
//                    $insertArray1['client_id'] = $post['clientid'];
//                    $insertArray1['task_id'] = $task_id;
//                    $insertArray1['created_date'] = date('Y-m-d H:i:s');
//                    $res = DB::table($this->tbl_task_client_location)->insert($insertArray1);
//                }
                
                for ($i=0;$i < count($post["client_multi_location"]);$i++) {
                    $insertArray1['location_id'] = $post["client_multi_location"][$i];
                    $insertArray1['sort_location'] = $post["sort_location"][$i];
                    $insertArray1['client_id'] = $post['clientid'];
                    $insertArray1['task_id'] = $task_id;
                    $insertArray1['created_date'] = date('Y-m-d H:i:s');
                    $res = DB::table($this->tbl_task_client_location)->insert($insertArray1);
                }
                
            }
        }

        if ($task_id > 0) {

            if (isset($post["task_emp_id"]) && $post["task_emp_id"] > 0) {
                $dueDate = $post["due_date"] . " " . $post["starettime"] . ":" . $post["et_mins"] . ":00";

                $emp_task_id = $post["task_emp_id"];
                $taskArray['task_cat'] = $post["categoryid"];
                $taskArray['clientid'] = $post['clientid'];
                $taskArray['et_duedate'] = $dueDate;
                $taskArray['et_hours'] = $post["starettime"];
                $taskArray['et_mins'] = $post["et_mins"];
                $response = DB::table($this->emptable)->where('task_id', $task_id)->update($taskArray);
                $desc = 'Task Updated';
            } else {
                if (isset($post['is_recurring_task']) && $post['is_recurring_task'] > 0 && isset($post['recurring_type']) && $post['recurring_type'] > 0) {
                    $begin = new DateTime($post['due_date']);
                    $end = new DateTime($post['recurring_end_date']);
                    if ($post['recurring_type'] == 1) {
                        $end = $end->modify('+1 day');
                        $interval = new DateInterval('P1D');
                    } else if ($post['recurring_type'] == 2) {
                        $interval = new DateInterval('P7D');
                    } else {
                        $interval = new DateInterval('P1M');
                    }
                    $daterange = new DatePeriod($begin, $interval, $end);
                    foreach ($daterange as $date) {
                        //echo $date->format("Y-m-d") . "<br>";
                        $dueDate = $date->format("Y-m-d") . " " . $post["starettime"] . ":" . $post["et_mins"] . ":00";
                        $taskArray['comp_id'] = $post["comp_id"];
                        $taskArray['task_cat'] = $post["categoryid"];
                        $taskArray['loc_id'] = "1";
                        $taskArray['clientid'] = $post['clientid'];
                        $taskArray['task_id'] = $task_id;
                        $taskArray['et_duedate'] = $dueDate;
                        $taskArray['supervisor_id'] = $post['supervisor_id'];
                        $taskArray['logintype'] = $role;
                        $taskArray['et_hours'] = $post["starettime"];
                        $taskArray['et_mins'] = $post["et_mins"];
                        //---------------Unused Field-----------//
                        $taskArray['emp_id'] = '0';
                        //---------------End--------------------//
                        $emp_task_id = DB::table($this->emptable)->insertGetId($taskArray);
                        $desc = 'New Task Added';
                    }
                } else {
                    $dueDate = $post["due_date"] . " " . $post["starettime"] . ":" . $post["et_mins"] . ":00";
                    $taskArray['comp_id'] = $post["comp_id"];
                    $taskArray['task_cat'] = $post["categoryid"];
                    $taskArray['loc_id'] = "1";
                    $taskArray['clientid'] = $post['clientid'];
                    $taskArray['task_id'] = $task_id;
                    $taskArray['et_duedate'] = $dueDate;
                    $taskArray['supervisor_id'] = $post['supervisor_id'];
                    $taskArray['logintype'] = $role;
                    $taskArray['et_hours'] = $post["starettime"];
                    $taskArray['et_mins'] = $post["et_mins"];
                    //---------------Unused Field-----------//
                    $taskArray['emp_id'] = '0';
                    //---------------End--------------------//
                    $emp_task_id = DB::table($this->emptable)->insertGetId($taskArray);
                    $desc = 'New Task Added';
                }
            }
            if ($emp_task_id > 0) {
                $logArray['client_id'] = $post["comp_id"];
                $logArray['log_name_id'] = '5';
                $logArray['user_name'] = Session::get('current_user_email');
                $logArray['desc'] = $desc;
                $logArray['added_date'] = date('Y-m-d H:i:s');
                DB::table("logs")->insert($logArray);
                return $task_id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function taskAssignToEmp($post = array()) {

        if (isset($post['task_id']) && $post['task_id'] > 0) {
            $task_id = $post['task_id'];

            $isTastAssigned = DB::table($this->emptable)
                    ->select("emp_id", "supervisor_id", "comp_id")
                    ->where('task_id', $task_id)
                    ->first();


            if ($isTastAssigned->emp_id > 0) {
                $employeeName = DB::table('tbl_employee')
                        ->select("emp_name")
                        ->where('emp_id', $isTastAssigned->emp_id)
                        ->first();
                return array("flag" => 3, "emp_name" => $employeeName->emp_name);
            }



            if (!empty($post["multiassignEmpId"])) {
                $empArr = array();
                foreach ($post["multiassignEmpId"] as $k => $val) {
                    $empArr[] = $val;
                }
                DB::table($this->emp_task_assign_table)
                        ->whereNotIn('emp_id', $empArr)
                        ->where('task_id', $task_id)
                        ->delete();


                foreach ($post["multiassignEmpId"] as $key => $emp_id) {
                    $isExist = DB::table($this->emp_task_assign_table)
                            ->select("emp_id")
                            ->where('emp_id', $emp_id)
                            ->where('task_id', $task_id)
                            ->first();

                    if (empty($isExist)) {
                        if ($emp_id != "") {

                            $empDetail = DB::table('tbl_employee')
                                    ->select("emp_name", "device", "deviceId", "emp_number")
                                    ->where('emp_id', $emp_id)
                                    ->first();

                            $oTask = DB::table($this->table)
                                    ->select("task_name")
                                    ->where('task_id', $task_id)
                                    ->first();

                            $alert = $oTask->task_name . ' Assigned To ' . $empDetail->emp_name;


                            $is_push_send = 0;

                            if ($empDetail->device == 2 && strlen($empDetail->deviceId) > 8) {

                                $fields = array(
                                    'to' => $empDetail->deviceId,
                                    'notification' => array("title" => $alert, "body" => $alert, 'task_id' => $task_id, 'emp_id' => $emp_id, 'NT' => 1),
                                    'data' => array("message" => $alert),
                                );
                                $response = androidPushnotification($fields);


                                if ($response) {
                                    $is_push_send = 1;
                                }
                            }
                            if ($empDetail->device == 1 && strlen($empDetail->deviceId) > 8) {

                                $iphoneData = array("aps" => array(
                                        'alert' => $alert,
                                        "sound" => "Mowom.caf",
                                        'task_id' => $task_id,
                                        'emp_id' => $emp_id,
                                    ),
                                    "NT" => 1
                                );

                                $results = iosPushnotification($iphoneData, $empDetail->deviceId);

                                if ($results) {
                                    $is_push_send = 1;
                                }
                            }

                            $updateArray['task_id'] = $post["task_id"];
                            $updateArray['emp_id'] = $emp_id;
                            $updateArray['notification'] = $alert;
                            $updateArray['created_date'] = date("Y-m-d H:i:s");
                            $updateArray['updated_date'] = date("Y-m-d H:i:s");
                            $updateArray['is_push_send'] = $is_push_send;
                            $updateArray['comp_id'] = $isTastAssigned->comp_id;
                            $updateArray['supervisor_id'] = $isTastAssigned->supervisor_id;
                            DB::table($this->emp_task_assign_table)->insertGetId($updateArray);
                        }
                    }
                }

                return array("flag" => 1);
            } else {
                return array("flag" => 2);
            }
        } else {
            return false;
        }
    }

    /*
      Function used to get partner details by id
     */

    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('supervisor_id', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }

    function statusPartner($id, $status) {
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['status'] = $status;
        return DB::table($this->table)->where('supervisor_id', $id)->update($updateArray);
    }

    function destroyPartner($id) {
        $updateArray['task_delete'] = 1;
        return DB::table($this->table)->where('task_id', $id)->update($updateArray);
    }

    function getTotalTaskCount($comp_id, $supervisor_id) {
        if ($supervisor_id > 0) {
            return DB::table($this->table)
                            ->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')
                            ->where($this->emptable . '.supervisor_id', $supervisor_id)
                            ->where($this->table . '.comp_id', $comp_id)
                            ->where($this->table . '.task_delete', 0)
                            ->get()->count();
        } else {
            return DB::table("tbl_task")
                            ->where("comp_id", $comp_id)
                            ->where("task_delete", '0')
                            ->get()->count();
        }
    }

    function getTaskClientLocationByClientId($client_id, $task_id) {
        $parner_data = DB::table($this->tbl_task_client_location)
                ->select("location_id", "sort_location")
                ->where('client_id', $client_id)
                ->where('task_id', $task_id)
                ->get();

        $locationIds = array();
        if($parner_data->count() > 0){
        foreach ($parner_data as $key => $val) {
            $locationIds["location_ids"][$key] = $val->location_id;
            $locationIds["sort_locations"][$key] = $val->sort_location;
        }
        }
        return $locationIds;
    }
    
    function getTaskClientLocation($task_id){
        return DB::table($this->tbl_task_client_location)
            ->join($this->tbl_clients_location, $this->tbl_task_client_location . '.location_id', '=', $this->tbl_clients_location . '.id')
            ->where($this->tbl_task_client_location . '.task_id', $task_id)
            ->orderby($this->tbl_task_client_location . '.sort_location', 'asc')
            ->limit(1)->get()->first();
    }

}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
use Carbon\Carbon;

class Login extends Model
{
    protected $table = 'partnerslist';
    public $timestamps = false;
    public function login($credentials="")
    {


        if(!empty($credentials)) {
            $decryptedpas = md5($credentials['password']);
            $email = $credentials['email'];
            $password = $credentials['password'];
            try {
                $partner_data = DB::table('partnerslist')->select(DB::raw('company_name,partnerid,email_id'))->where('email_id', $email)->where('admin_password', $decryptedpas)->get()->first();

            } catch (QueryException $ex) {
                dd($ex->getMessage());
                // Note any method of class PDOException can be called on $ex.
            }

            if (!empty($partner_data)) {

                    Session::put('companyid', $partner_data->partnerid);
                    Session::put('company_name', $partner_data->company_name);
                    Session::put('uname', $partner_data->company_name);
                    Session::put('role', '2');
                    Session::put('current_user_email', $partner_data->email_id);
                    Session::put('uid', $partner_data->partnerid);
                    Session::save();

                    /*
                     * TODO::Add detail in log table
                     */
                        $companyid=$partner_data->partnerid;
                        $user_name=$partner_data->email_id;

                        try {


                            $date=Carbon::now()->format('Y-m-d H:i:s');


                            $results=DB::insert('insert into logs (`client_id`, `log_name_id`, `log_id`, `user_name`, `desc`, `added_date`) values (?,?,?,?,?,?)',[$companyid,7,$companyid,$user_name,'Partner Logged In',$date]);

                            return true;
                            }
                        catch (QueryException $ex) {
                            dd($ex->getMessage());
                            // Note any method of class PDOException can be called on $ex.
                            return false;
                        }
                    /*
                     * TODO::Add Detail in log table end
                     */
                    return true;

            } else {


                try {
                    $supervisor_data = DB::table('tbl_supervisor')->select(DB::raw('sup_name,companyid,supervisor_id,email'))->where('email', $email)->where('password', $password)->get()->first();
                } catch (QueryException $ex) {
                    dd($ex->getMessage());
                    // Note any method of class PDOException can be called on $ex.
                }

                if (!empty($supervisor_data)) {
                    Session::put('companyid', $supervisor_data->companyid);
                    Session::put('uname', $supervisor_data->sup_name);
                    Session::put('supervisor_id', $supervisor_data->supervisor_id);
                    Session::put('email', $supervisor_data->email);
                    Session::put('current_user_email', $supervisor_data->email);
                    //Session::put('uid', $supervisor_data->partnerid);
                    Session::put('uid', $supervisor_data->supervisor_id);
                    Session::put('role', '3');
                    Session::save();

                    /*
                     * TODO::Add detail in log table
                     */
                    $companyid=$supervisor_data->companyid;
                    $user_name=$supervisor_data->sup_name;
                    $sup_id=$supervisor_data->supervisor_id;
                    try {


                        $date=Carbon::now()->format('Y-m-d H:i:s');

                        $results=DB::insert('insert into logs (`client_id`, `log_name_id`, `log_id`, `user_name`, `desc`, `added_date`) values (?,?,?,?,?,?)',[$companyid, '3', $sup_id, $user_name, 'Supurvisor Logged In',$date]);

                        return true;
                    }
                    catch (QueryException $ex) {
                        dd($ex->getMessage());
                        // Note any method of class PDOException can be called on $ex.
                        return false;
                    }
                    /*
                     * TODO::Add Detail in log table end
                     */
                    return true;
                } else {
                    try {
                        $superadmin_data = DB::table('superadmin')->select(DB::raw('username,id'))->where('username', $email)->where('password', $password)->get()->first();
                    } catch (QueryException $ex) {
                        dd($ex->getMessage());
                        // Note any method of class PDOException can be called on $ex.
                    }

                    if (!empty($superadmin_data)) {

                        Session::put('username', $superadmin_data->username);
                        Session::put('uid', $superadmin_data->id);
                        Session::put('role', '100');
                        Session::put('superadmin', $superadmin_data->id);
                        Session::put('current_user_email', $superadmin_data->username);
                        Session::save();

                        /*
                     * TODO::Add detail in log table
                     */
                        $user_name=$superadmin_data->username;

                        try {


                            $date=Carbon::now()->format('Y-m-d H:i:s');

                            $results=DB::insert('insert into logs (`client_id`, `log_name_id`, `log_id`, `user_name`, `desc`, `added_date`) values (?,?,?,?,?,?)',['0', '1', '100', $user_name, 'Admin Logged In',$date]);
                            return true;
                        }
                        catch (QueryException $ex) {
                            dd($ex->getMessage());
                            // Note any method of class PDOException can be called on $ex.
                            return false;
                        }
                        /*
                         * TODO::Add Detail in log table end
                         */

                    }

                    else {
                        return false;
                    }

                }
            }
        }
    }
}

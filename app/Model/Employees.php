<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;
//@include('../resources/assets/momom-assets/calendar/jdf.php');

class Employees extends Model
{
    //
    protected $table='tbl_employee';
    public function getEmployeesByCompanyId($companyid = 0,$supervisor_id = 0,$role = 0)
    {
        try {
            if($role == 2)
            {
            $parner_data=DB::table($this->table)->where('companyid', $companyid)->orderBy('emp_status', 'desc')->get();
            }else{
            $parner_data=DB::table($this->table)->where('supervisor_id', $supervisor_id)->where('companyid', $companyid)->orderBy('emp_status', 'desc')->get();    
            }
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function getActiveEmployeesByCompanyId($companyid = 0,$supervisor_id = 0,$role = 0)
    {
        try {
            if($role == 2)
            {
            $parner_data=DB::table($this->table)->where('companyid', $companyid)->where('emp_status', '1')->where('delete_employee', '0')->orderBy('emp_id', 'desc')->get();
            }  else {
                $parner_data=DB::table($this->table)->where('supervisor_id', $supervisor_id)->where('companyid', $companyid)->where('emp_status', '1')->where('delete_employee', '0')->orderBy('emp_id', 'desc')->get();
            }
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function add_employees($post=array())
    {
        //echo "<pre>";print_r($post);print_r($_FILES);exit;
        $language=Session::get('language')!='' ? Session::get('language') : '';
        
        $error = 'no';
        $pfilename = $_FILES['clientlogo']['name'];
        if(!empty($pfilename)){
            $file_type = $_FILES['clientlogo']['type']; //returns the mimetype

            $allowed = array("image/jpeg", "image/gif", "image/png");
            if (!in_array($file_type, $allowed)) {
                $error_message = 'Only jpg, gif, and png files are allowed.';
                $error = 'yes';
            }

            $file = $_FILES['clientlogo'];
            $destination_path = public_path('uploads/employees/');
            $filename = str_random(10);
            $pfilename = $filename.'-'.$pfilename;
            move_uploaded_file($_FILES["clientlogo"]["tmp_name"], $destination_path.$pfilename);
        }

        if ($error == 'no') {

            if(isset($post['emp_id']) && $post['emp_id'] > 0){
                $updateArray['emp_name'] = $post["teamname"];
                $updateArray['address'] = $post["address"];
                if(!empty($pfilename)){
                    $updateArray['profileimage'] = $pfilename;
                }
                $updateArray['emp_email'] = $post["email_id"];
                if(!empty($post["password"])){
                    $updateArray['emp_pass'] = md5($post["password"]);
                }
                $updateArray['emp_number'] = $post["mobile"];
                $updateArray['lat'] = $post["latitude"];
                $updateArray['lang'] = $post["longitude"];
                $updateArray['companyid'] = $post["companyid"];
                $updateArray['emp_post'] = $post["teamdesignation"];
                $updateArray['emp_status'] = $post["cmbstat"];
                $updateArray['department'] = "";
//                $updateArray['can_create_task'] = $post["can_create_task"];
                $updateArray['can_create_task'] = 0;
                $updateArray['supervisor_id'] = $post["supervisor_id"];
                $updateArray['is_privacy'] = isset($post["is_privacy"]) ? 1 : 0;
                DB::table($this->table)->where('emp_id', $post['emp_id'])->update($updateArray);
                return true;
            } else {
                $updateArray['emp_name'] = $post["teamname"];
                $updateArray['address'] = $post["address"];
                if(!empty($pfilename)){
                    $updateArray['profileimage'] = $pfilename;
                }
                $updateArray['emp_email'] = $post["email_id"];
                $updateArray['emp_pass'] = md5($post["password"]);
                $updateArray['emp_number'] = $post["mobile"];
                $updateArray['lat'] = $post["latitude"];
                $updateArray['lang'] = $post["longitude"];
                $updateArray['companyid'] = $post["companyid"];
                $updateArray['emp_post'] = $post["teamdesignation"];
                $updateArray['emp_status'] = $post["cmbstat"];
                $updateArray['delete_employee'] = 0;
                $updateArray['department'] = "";
//                $updateArray['can_create_task'] = $post["can_create_task"];
                $updateArray['can_create_task'] = 0;
                $updateArray['supervisor_id'] = $post["supervisor_id"];
                $updateArray['is_privacy'] = isset($post["is_privacy"]) ? 1 : 0;
                //-------Unused Fields--------//
                $updateArray['comp_id'] = 1;
                $updateArray['employe_task_status'] = 0;
                $updateArray['emp_rate'] = "0.00";
                $updateArray['availability'] = 0;
                $updateArray['leavereason'] = "";
                $updateArray['deviceId'] = "";
                $updateArray['battery_status'] = "";
                $updateArray['emp_work_status'] = 0;
                $updateArray['work_date_time_update'] = date('Y-m-d H:i:s');
                $updateArray['work_status_updated_date'] = date('Y-m-d H:i:s');
                //----------------------------//
                return DB::table($this->table)->insert($updateArray);
            }
        }
    }

    /*
    Function used to get partner details by id
    */
    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('emp_id', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }
    
    function statusPartner($id,$status){
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['emp_status'] = $status;
        return DB::table($this->table)->where('emp_id', $id)->update($updateArray);
    }
    
    function destroyPartner($id){
        $updateArray['delete_employee'] = 1;
        return DB::table($this->table)->where('emp_id', $id)->update($updateArray);
    }
    function reactivePartner($id){
        $updateArray['delete_employee'] = 0;
        return DB::table($this->table)->where('emp_id', $id)->update($updateArray);
    }
}

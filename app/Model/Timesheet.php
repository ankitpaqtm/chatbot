<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;

//@include('../resources/assets/momom-assets/calendar/jdf.php');

class Timesheet extends Model {

    //
    protected $table = 'tbl_task';
    protected $emptable = 'tbl_emp_task';
    protected $tbl_employee = 'tbl_employee';
    protected $tbl_client = 'tbl_clients';

    public function getTimesheetByCompanyId($companyid, $supervisor_id, $role = 2, $post = array()) {
        try {
            
//                $fetchMode = DB::getFetchMode();
//                DB::setFetchMode(\PDO::FETCH_ASSOC);
                $sqry = "";
                 if (isset($post["emp_id"]) && $post["emp_id"] > 0) {
                    $sqry.=" and et.emp_id=" . $post['emp_id'];
                }

                if (isset($post["txtDateFrom"]) && $post["txtDateFrom"] != "") {
                    $txtDateFrom = $post["txtDateFrom"];
                    $sqry.=" and DATE(et.task_starttime) >= '$txtDateFrom'";
                }
                if (isset($post["txtDateTo"]) && $post["txtDateTo"] != "") {
                    $txtDateTo = $post["txtDateTo"];
                    $sqry.=" and DATE(et.task_endtime) <= '$txtDateTo'";
                }
            
            if ($role == 2) {
//            $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->join($this->tbl_employee, $this->tbl_employee . '.emp_id', '=', $this->emptable . '.emp_id')->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status', $this->tbl_employee . '.emp_name')->where($this->table . '.comp_id', $companyid)->where($this->table . '.task_delete', 0)->orderBy($this->emptable . '.task_id', 'desc')->get();

             $parner_data = DB::select("select  t.duration,et.emp_id,et.task_id, et.et_id,et.task_starttime,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, e.emp_name FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and c.companyid='$companyid' and t.task_delete=0 and et.status !=-1 and   1=1 " . $sqry . " order by et.task_id desc");
               
            } else if ($role == 3) {
//                $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->join($this->tbl_employee, $this->tbl_employee . '.emp_id', '=', $this->emptable . '.emp_id')->select($this->table . '.*', $this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status')->where($this->emptable . '.supervisor_id', $supervisor_id)->where($this->table . '.comp_id', $companyid)->where($this->table . '.task_delete', 0)->orderBy($this->emptable . '.task_id', 'desc')->get();
                $parner_data = DB::select("select t.duration,et.emp_id,et.task_id, et.et_id,et.task_starttime,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, e.emp_name FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and c.companyid='$companyid' and et.supervisor_id = '$supervisor_id' and t.task_delete=0 and et.status !=-1 and   1=1 " . $sqry . " order by et.task_id desc");
            }
//            DB::setFetchMode($fetchMode);
            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

    public function getSingleTimesheetByTaskId($task_id) {
        try {
            $parner_data = DB::table($this->table)->join($this->emptable, $this->table . '.task_id', '=', $this->emptable . '.task_id')->join($this->tbl_employee, $this->tbl_employee . '.emp_id', '=', $this->emptable . '.emp_id')->join($this->tbl_client, $this->tbl_client . '.clientid', '=', $this->table . '.clientid')->select($this->table . '.*', $this->tbl_client. '.client_name',$this->tbl_employee. '.emp_name',$this->emptable . '.task_starttime', $this->emptable . '.task_endtime', $this->emptable . '.emp_id', $this->emptable . '.started', $this->emptable . '.status', $this->emptable . '.et_duedate', $this->emptable . '.et_hours', $this->emptable . '.et_mins', $this->emptable . '.et_id')->where($this->table . '.task_id', $task_id)->where($this->table . '.task_delete', 0)->first();

            return $parner_data;
        } catch (QueryException $ex) {
            dd($ex->getMessage());
        }
    }

}

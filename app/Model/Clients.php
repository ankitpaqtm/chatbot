<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;
//@include('../resources/assets/momom-assets/calendar/jdf.php');

class Clients extends Model
{
    //
    protected $table='tbl_clients';
    protected $tbl_clients_location='tbl_clients_location';
    public function getClientsByCompanyId($companyid)
    {
        try {
            $parner_data=DB::table($this->table)->where('companyid', $companyid)->orderBy('status', 'asc')->get();
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function add_clients($post=array())
    {
        //echo "<pre>";print_r($post);print_r($_FILES);exit;
        $language=Session::get('language')!='' ? Session::get('language') : '';
        
        $error = 'no';
        $pfilename = isset($_FILES['clientlogo']['name']) ? $_FILES['clientlogo']['name'] : '';
        if(!empty($pfilename)){
            $file_type = $_FILES['clientlogo']['type']; //returns the mimetype

            $allowed = array("image/jpeg", "image/gif", "image/png");
            if (!in_array($file_type, $allowed)) {
                $error_message = 'Only jpg, gif, and png files are allowed.';
                $error = 'yes';
            }

            $file = $_FILES['clientlogo'];
            $destination_path = public_path('uploads/clients/');
            $filename = str_random(10);
            $pfilename = $filename.'-'.$pfilename;
            move_uploaded_file($_FILES["clientlogo"]["tmp_name"], $destination_path.$pfilename);
        }

        if ($error == 'no') {

            if(isset($post['clientid']) && $post['clientid'] > 0){
                $res = 0;
                $response = 0;
                for($i = 0; $i < count($post["address"]); $i++){
                
                
                if(isset($post["location_uid"][$i]) && $post["location_uid"][$i] > 0){
                $insertArray1['latitude'] = $post["latitude"][$i];
                $insertArray1['longitude'] = $post["longitude"][$i];
                $insertArray1['status'] = 1;
                $insertArray1['location'] = $post["address"][$i];
                $insertArray1['created_date'] = date('Y-m-d H:i:s');
                $res =  DB::table($this->tbl_clients_location)->where('id', $post["location_uid"][$i])->update($insertArray1);  
                }else{    
                $insertArray['latitude'] = $post["latitude"][$i];
                $insertArray['longitude'] = $post["longitude"][$i];
                $insertArray['status'] = 1;
                $insertArray['location'] = $post["address"][$i];
                $insertArray['client_id'] = $post['clientid'];
                $insertArray['created_date'] = date('Y-m-d H:i:s');
                $res = DB::table($this->tbl_clients_location)->insert($insertArray);
                }
                }
                
                 $updateArray['client_name'] = $post["client_name"];
                $updateArray['address'] = "";
                if(!empty($pfilename)){
                    $updateArray['logo'] = $pfilename;
                }
                $updateArray['clientemail'] = $post["email_id"];
                $updateArray['contract'] = $post["contact_person"];
                $updateArray['contactonsite'] = $post["mobile"];
                $updateArray['latitude'] = "0";
                $updateArray['longtitude'] = "0";
                $updateArray['companyid'] = $post["companyid"];
                
                $response = DB::table($this->table)->where('clientid', $post['clientid'])->update($updateArray);
                
                if($res || $response){
                    return true;
                }
                
            } else {
                $updateArray['client_name'] = $post["client_name"];
//                $updateArray['address'] = $post["address"];
                $updateArray['address'] = "";
                if(!empty($pfilename)){
                    $updateArray['logo'] = $pfilename;
                }
                $updateArray['clientemail'] = $post["email_id"];
                $updateArray['contract'] = $post["contact_person"];
                $updateArray['contactonsite'] = $post["mobile"];
                
//                $updateArray['latitude'] = $post["latitude"];
//                $updateArray['longtitude'] = $post["longitude"];
                
                $updateArray['latitude'] = "0";
                $updateArray['longtitude'] = "0";
                
                $updateArray['companyid'] = $post["companyid"];
                $updateArray['com_information'] = "Null";
                $updateArray['status'] = 1;
                $updateArray['createat'] = date('Y-m-d H:i:s');
                
                $lastClientId =  DB::table($this->table)->insertGetId($updateArray);
                
                for($i = 0; $i < count($post["address"]); $i++){
                $insertArray['latitude'] = $post["latitude"][$i];
                $insertArray['longitude'] = $post["longitude"][$i];
                $insertArray['status'] = 1;
                $insertArray['location'] = $post["address"][$i];
                $insertArray['client_id'] = $lastClientId;
                $insertArray['created_date'] = date('Y-m-d H:i:s');
                DB::table($this->tbl_clients_location)->insert($insertArray);
                }
                
                if($lastClientId){
                return true;
                }
                
            }
        }
    }

    /*
    Function used to get partner details by id
    */
    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('clientid', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }
    
    function getClientByEmailId($emailid) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('clientemail', $emailid)
                ->get()
                ->toArray();
        return $parner_data;
    }
    function getClientLocationByClientId($client_id) {
        $parner_data = DB::table($this->tbl_clients_location)
                ->select("*")
                ->where('client_id', $client_id)
                ->get();
                
        return $parner_data;
    }
    
    function statusPartner($id,$status){
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['status'] = $status;
        return DB::table($this->table)->where('clientid', $id)->update($updateArray);
    }
    function destroyLocation($id){
        return DB::table($this->tbl_clients_location)->where('id', $id)->delete();
    }
}

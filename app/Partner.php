<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Language;
@include('../resources/assets/momom-assets/calendar/jdf.php');

class Partner extends Model
{
    //
    protected $table='partnerslist';
    public function get_partner_list()
    {
        $company_id=Session::get('companyid')!='' ? Session::get('companyid') : '';
        try {
            $parner_data=DB::table($this->table)->select(DB::raw('partnerid,email_id,company_name,address,mobile,status'))->where('deleteval', 0)->orderBy('status', 'asc')->get();
            return $parner_data;
        }
        catch(QueryException $ex){
            dd($ex->getMessage());

        }

    }
    public function add_partner($post=array())
    {
        //echo "<pre>";print_r($post);print_r($_FILES);exit;
        $language=Session::get('language')!='' ? Session::get('language') : '';
//        if ($language == "persian") {
//            $arrFrom = explode("-", $post['due_date']);
//            $duedate = jalali_to_gregorian($arrFrom[0], $arrFrom[1], $arrFrom[2], "-");
//        } else {
            $duedate = $post['due_date'];
//        }
        $error = 'no';
        $pfilename = $_FILES['clientlogo']['name'];
        if(!empty($pfilename)){
            $file_type = $_FILES['clientlogo']['type']; //returns the mimetype

            $allowed = array("image/jpeg", "image/gif", "image/png");
            if (!in_array($file_type, $allowed)) {
                $error_message = 'Only jpg, gif, and png files are allowed.';
                $error = 'yes';
            }

            $file = $_FILES['clientlogo'];
            $destination_path = public_path('uploads/logos/');
            $filename = str_random(10);
            $pfilename = $filename.'-'.$pfilename;
            move_uploaded_file($_FILES["clientlogo"]["tmp_name"], $destination_path.$pfilename);
        }

        if ($error == 'no') {

            if(isset($post['partnerid']) && $post['partnerid'] > 0){
                $updateArray['company_name'] = $post["company_name"];
                $updateArray['address'] = $post["address"];
                if(!empty($pfilename)){
                    $updateArray['logo'] = $pfilename;
                }
                $updateArray['email_id'] = $post["email_id"];
                $updateArray['admin_usernmae'] = $post["admin_username"];
                if(!empty($post["admin_password"])){
                    $updateArray['admin_password'] = $post["admin_password"];
                }
                $updateArray['taskcount'] = $post["taskcount"];
                $updateArray['supervisorcount'] = $post["supervisorcount"];
                $updateArray['employeecount'] = $post["employeecount"];
                $updateArray['mobile'] = $post["mobile"];
                $updateArray['clinetscount'] = $post["clinetscount"];
                $updateArray['due_date'] = $post["due_date"];
                $updateArray['latitude'] = $post["latitude"];
                $updateArray['longitude'] = $post["longitude"];
                return DB::table($this->table)->where('partnerid', $post['partnerid'])->update($updateArray);
            } else {
                return DB::table($this->table)->insert(
                    ['company_name' => $post["company_name"], 'address' => $post["address"], 'logo' => $pfilename, 'email_id' => $post["email_id"], 'admin_usernmae' => $post["admin_username"], 'admin_password' => $post["admin_password"], 'taskcount' => $post["taskcount"],
                        'supervisorcount' => $post["supervisorcount"], 'employeecount' => $post["employeecount"], 'mobile' => $post["mobile"], 'clinetscount' => $post["clinetscount"], 'due_date' => $duedate, 'latitude' => $post["latitude"], 'longitude' => $post["longitude"]
                    ]
                );
            }
        }
    }

    /*
    Function used to get partner details by id
    */
    function getPartner($id) {
        $parner_data = DB::table($this->table)
                ->select("*")
                ->where('partnerid', $id)
                ->get()
                ->toArray();
        return $parner_data;
    }
    
    function destroyPartner($id){
        $updateArray['deleteval'] = 1;
        return DB::table($this->table)->where('partnerid', $id)->update($updateArray);
    }
    function statusPartner($id,$status){
        //echo "id=".$id.'<br>status='.$status;exit;
        $updateArray['status'] = $status;
        return DB::table($this->table)->where('partnerid', $id)->update($updateArray);
    }
}

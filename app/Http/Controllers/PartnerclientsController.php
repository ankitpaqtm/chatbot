<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Clients;
use Session;
use App\Model\Partner;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;

class PartnerclientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {

            
            /*
             * TODO::Get Partner List Code Start
             */
                $clients=new Clients();
                $clients_data=$clients->getClientsByCompanyId($uid);
                $Partner = new Partner();
                $Partner_data=$Partner->getPartner($uid);
            /*
             * TODO::Get Partner List Code End
             */
            return View('partnerClients.index')->with('client_data',$clients_data)->with('partner_data',$Partner_data)->with('urlPrifix',$urlPrifix);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        
        

        if(isset($uid) && $uid!='')
        {
            $clients=new Clients();
            $clients_data=$clients->getClientsByCompanyId($uid);

            $Partner = new Partner();
            $Partner_data=$Partner->getPartner($uid);

            $partnerClientCount = $Partner_data[0]->clinetscount;
            $currentClientCount = count($clients_data);
            if($currentClientCount < $partnerClientCount){
                return View('partnerClients.create');
            } else {
                return Redirect::to($urlPrifix.'/clients/')->with('msg',"Limit Riched! You can not add more client.")->with('alert','alert-danger');
            }
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $language_data=new Language();
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        
        //
        $tmp_input   = Input::all();
        $tmp_input['companyid'] = $uid;
        //echo "<pre>";print_r($tmp_input);exit;
        $clientid = 0;
        if(isset($tmp_input['clientid']) && $tmp_input['clientid'] > 0){
            $clientid = $tmp_input['clientid'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['client_name'] = 'required';
        $rules['address'] = 'required';
        if(!$clientid){
            $rules['clientlogo'] = 'required';
            $rules['email_id'] = 'required|email|unique:tbl_clients,clientemail';
        } else {
            $rules['email_id'] = 'required|email';
        }
        $rules['contact_person'] = 'required';
        $rules['mobile'] = 'required';
        $rules['latitude'] = 'required';
        $rules['longitude'] = 'required';

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails())
        {
            if($clientid > 0){
                return Redirect::to($urlPrifix.'/clients/update/'.$clientid)->withInput()->withErrors($validator);
            } else {
                
                return Redirect::to($urlPrifix.'/clients/create')->withInput()->withErrors($validator);
            }
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Clients();
        if($partner->add_clients($tmp_input)) {
            if($clientid > 0){
                $msg = $language_data->__('text_admin_success_msg');
            } else {
                $msg = $language_data->__('text_client_inserted');
            }
            return Redirect::to($urlPrifix."/clients")->with('msg',$msg)->with('alert','alert-success');
            //return Redirect::to("partner/clients");
        } else {
            return Redirect::to($urlPrifix."/clients")->with('msg',$language_data->__('text_we_are_sorry'))->with('alert','alert-danger');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
         $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $partner=new Clients();
        $partner_data = $partner->getPartner($id);
        
        
        $clientLocations = $partner->getClientLocationByClientId($id);
        
        return View('partnerClients.show')->with('partner_data',$partner_data)->with('urlPrifix',$urlPrifix)->with('clientLocations',$clientLocations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        
        $partner=new Clients();
        $partner_data = $partner->getPartner($id);
        
        $clientLocations = $partner->getClientLocationByClientId($id);
        return View('partnerClients.update')->with('partner_data',$partner_data)->with('clientLocations',$clientLocations)->with('urlPrifix',$urlPrifix);
    }

    
    public function status(request $request, $id)
    {
        $param = Input::all();
        if(isset($param['status'])){
            $partner=new Clients();
            $partner->statusPartner($id,$param['status']);
        }
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $urlPrifix = 'partner';
        } else {
            $urlPrifix = 'supervisor';
        }
        return Redirect::to($urlPrifix."/clients/index");
    }
    
    public function uploadexcel(){
        $language_data=new Language();
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        
        

        if(isset($uid) && $uid!='')
        {
            
            $clients=new Clients();
            $clients_data=$clients->getClientsByCompanyId($uid);

            $Partner = new Partner();
            $Partner_data=$Partner->getPartner($uid);

            $partnerClientCount = $Partner_data[0]->clinetscount;
            $currentClientCount = count($clients_data);
            
            $tmp_input   = Input::all();
            if(!empty($tmp_input)){
                if (isset($tmp_input['insert_product_details']) && !empty($tmp_input['xmlData'])) {
                    $xmlDatas = json_decode($tmp_input['xmlData'],true);
                    $partner=new Clients();
                    foreach ($xmlDatas as $xmlData){
                        if(empty($partner->getClientByEmailId($xmlData['client_email']))){
                            $insData['client_name'] = $xmlData['client_name'];
                            $insData['email_id'] = $xmlData['client_email'];
                            $insData['address'] = $xmlData['address'];
                            $insData['contact_person'] = $xmlData['contact_person'];
                            $insData['mobile'] = $xmlData['contact_number'];
                            $insData['latitude'] = $xmlData['latitude'];
                            $insData['longitude'] = $xmlData['longitude'];
                            $insData['companyid'] = $uid;
                            $partner->add_clients($insData);
                        }
                    }
                    return Redirect::to($urlPrifix."/clients")->with('msg',$language_data->__('text_client_inserted'))->with('alert','alert-success');
                }
                if(isset($tmp_input['upload_xls_data'])){
                    //pr($tmp_input);pr($_FILES);exit;
                    $type = explode(".", strrev($_FILES['upload_xls']['name']));
                    $type = strrev($type[0]);
                    if ($type == 'xls') {
                        $destination_path = "";
                        if ($_FILES['upload_xls']['error'] == '0') {
                            $filename = $_FILES['upload_xls']['name'];
                            if (is_uploaded_file($_FILES["upload_xls"]["tmp_name"])) {
                                $destination_path = public_path('client_xls/'.$filename);
                                move_uploaded_file($_FILES["upload_xls"]["tmp_name"], $destination_path);
                                chmod($destination_path, 0777);
                            }
                        }
                        $data = Excel::load($destination_path, function($reader) {})->get();
                        
                        $excelData = array();
                        if(!empty($data) && $data->count()){
                            $exDatas = $data->toArray();
                            //foreach ($data->toArray() as $key => $value) {
                                if(!empty($exDatas[0])){
                                    foreach ($exDatas[0] as $k=>$v) {
                                        $latLong = getLatLong($v['address']);
                                        $v['latitude'] = $latLong['latitude'] ? $latLong['latitude'] : '0';
                                        $v['longitude'] = $latLong['longitude'] ? $latLong['longitude'] : '0';
                                        $excelData[] = $v;
                                    }
                                    if(count($exDatas[0]) < $partnerClientCount){
                                        return View('partnerClients.uploadexcel')->with("excelData",$excelData)->with('msgLimit','');
                                    } else {
                                        return View('partnerClients.uploadexcel')->with("excelData",$excelData)->with('msgLimit',"Limit Riched! You can not add more then ".($partnerClientCount - $currentClientCount)." client.")->with('alert','alert-danger');
                                    }
                                }
                            //} 
			}
                        
                    } else {
                        return View('partnerClients.uploadexcel')->with('msg',$language_data->__('text_excel_file_validation'))->with('alert','alert-danger');
                    }
                }
            }
            if($currentClientCount < $partnerClientCount){
                return View('partnerClients.uploadexcel');
            } else {
                return Redirect::to('partner/clients/')->with('msg',"Limit Riched! You can not add more client.")->with('alert','alert-danger');
            }
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }
    public function destroyLocation($id,$cid)
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $partner=new Clients();
        $partner->destroyLocation($id);
        return Redirect::to($urlPrifix."/clients/update/".$cid);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Illuminate\Database\QueryException;
use App\Model\Login;
use App\CustomUserProvider;
use Session;
use Carbon\Carbon;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data_session=Session::all();

        if (isset($data_session['role']) && $data_session['role'] == "100") {
            return redirect()->action('PartnersController@index');
            exit;
        }
        else if (isset($data_session['role']) && $data_session['role'] == "2") {
            return redirect()->action('PartnerController@index');
            exit;
        }
        else if (isset($data_session['role']) && $data_session['role'] == "3") {
            return redirect()->action('SupervisorController@index');
            exit;
        }
        else {
            if (isset($request['email'])) {
                $credentials = $request->only('email', 'password');

                $login = new Login();
                $data = $login->login($credentials);

                if ($data) {
                    $data_session = Session::all();

                    if (isset($data_session['role']) && $data_session['role'] == "100") {
                        return redirect()->action('PartnersController@index');
                        exit;
                    } else if (isset($data_session['role']) && $data_session['role'] == "2") {
                        return redirect()->action('PartnerController@index');
                        exit;
                    } else if (isset($data_session['role']) && $data_session['role'] == "3") {
                        return redirect()->action('SupervisorController@index');
                        exit;
                    }
                } else {
                    return Redirect::to("login")->withInput()->with('msg',"Invalid Username and/or Password.")->with('alert','alert-danger');
                }


            }
            //
            return View('login.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //
        try {

            $date=Carbon::now()->format('Y-m-d H:i:s');

            $results=DB::insert('insert into logs (`client_id`, `log_name_id`, `log_id`, `user_name`, `desc`, `added_date`) values (?,?,?,?,?,?)',[1,1,1,1,1,$date]);
        }
        catch (QueryException $ex) {
            dd($ex->getMessage());
            // Note any method of class PDOException can be called on $ex.
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

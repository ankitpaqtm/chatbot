<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Task;
use Session;
use App\Model\Partner;
use App\Model\Clients;
use App\Model\Category;
use App\Model\Employees;
use Language;
use DB;
use DatePeriod;
use Illuminate\Support\Facades\Hash;

class PartnertaskController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        
    }

    public function index() {

        $role = Session::get('role') != '' ? Session::get('role') : '';
        $sup_id = 0;
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
            $sup_id = $supervisor_id;
        }
        $language_data = new Language();

        if (isset($uid) && $uid != '') {


            /*
             * TODO::Get Partner List Code Start
             */
            $task = new Task();
            $task_data = $task->getTaskByCompanyId($uid,$supervisor_id,$role);
            $rec_task_data = $task->getRecTaskByCompanyId($uid,$supervisor_id,$role);
            $currentTaskCount = $task->getTotalTaskCount($uid,$sup_id);
            //echo "<pre>";print_r($task_data);exit;
            $Partner = new Partner();
            $Partner_data = $Partner->getPartner($uid);

            /*
             * TODO::Get Partner List Code End
             */
            return View('partnerTask.index')->with('partner_data', $Partner_data)->with('task_data', $task_data)->with('urlPrifix',$urlPrifix)->with('currentTaskCount',$currentTaskCount)->with('rec_task_data',$rec_task_data);
        } else {
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $sup_id = 0;
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
            $sup_id = $supervisor_id;
        }

        if (isset($uid) && $uid != '') {

            $Partner = new Partner();
            $Partner_data = $Partner->getPartner($uid);

            $task = new Task();
            $task_data = $task->getTaskByCompanyId($uid,$supervisor_id,$role);
            $partnertaskCount = $Partner_data[0]->taskcount;
            //$currenttaskCount = count($task_data);
            $currenttaskCount = $task->getTotalTaskCount($uid,$sup_id);
            if ($currenttaskCount < $partnertaskCount) {

                $clients = new Clients();
                $clients_data = $clients->getClientsByCompanyId($uid);

                $category = new Category();
                $category_data = $category->getActiveEmployeesByCompanyId($uid);


                return View('partnerTask.create')->with('partner_data', $Partner_data)->with('task_data', $task_data)->with('clients_data', $clients_data)->with('category_data', $category_data)->with('urlPrifix', $urlPrifix)->with('currentTaskCount',$currenttaskCount);
            } else {
                return Redirect::to($urlPrifix.'/task/');
            }
        } else {
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data = new Language();
        //
        $tmp_input = Input::all();
        $tmp_input['comp_id'] = $uid;
        //echo "<pre>";print_r($tmp_input);exit;
        $task_id = 0;
        if (isset($tmp_input['task_id']) && $tmp_input['task_id'] > 0) {
            $task_id = $tmp_input['task_id'];
       }
       $tmp_input['supervisor_id'] = $supervisor_id;
        /*
         * TODO::Validate The Data
         */
        $rules['clientid'] = 'required';
        $rules['categoryid'] = 'required';
        $rules['task_name'] = 'required';
        $rules['task_type'] = 'required';
        $rules['due_date'] = 'required';
        $rules['starettime'] = 'required';
        $rules['et_mins'] = 'required';
        $rules['duration'] = 'required';
        $rules['client_multi_location'] = 'required';

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            if ($task_id > 0) {
                return Redirect::to($urlPrifix.'/task/update/' . $task_id)->withInput()->withErrors($validator);
            } else {
                return Redirect::to($urlPrifix.'/task/create')->withInput()->withErrors($validator);
            }
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner = new Task();
        $task_id = $partner->add_task($tmp_input);
        if ($task_id) {
            $language_data = new Language();
            if ($task_id > 0) {
                $msg = $language_data->__('text_well_done') . ' ' . $language_data->__('text_record_update_successfully');
            } else {
                $msg = $language_data->__('text_well_done') . ' ' . $language_data->__('text_task_inserted');
            }
            return Redirect::to($urlPrifix."/task/assign/" . $task_id)->with('msg', $msg)->with('alert', 'alert-success');
        } else {
            return Redirect::to($urlPrifix."/task")->with('msg', $language_data->__('text_we_are_sorry'))->with('alert', 'alert-danger');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $partner = new Task();
        $partner_data = $partner->getPartner($id);
        return View('partnerTask.show')->with('partner_data', $partner_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }

        $partner = new Task();
        $task_data = $partner->getSingleTaskByTaskId($id);
        
        $task_data->duedate = date("Y-m-d",  strtotime($task_data->et_duedate));
       
        
        $clients = new Clients();
        $clients_data = $clients->getClientsByCompanyId($uid);
        
        $category = new Category();
        $category_data = $category->getActiveEmployeesByCompanyId($uid);

        return View('partnerTask.update')->with('task_data', $task_data)->with('clients_data', $clients_data)->with('category_data', $category_data)->with('urlPrifix', $urlPrifix);
    }

    public function status(request $request, $id) {
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $param = Input::all();
        if (isset($param['status'])) {
            $partner = new Task();
            $partner->statusPartner($id, $param['status']);
        }
        return Redirect::to($urlPrifix."/task/index");
    }

    public function destroy($id) {
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $partner = new Task();
        $partner->destroyPartner($id);
        return Redirect::to($urlPrifix."/task/index");
    }

    public function assign($id) {
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        if ($uid > 0 && $id > 0) {
            $task = new Task();
            $task_data = $task->getTaskByTaskId($id);
            
            if (!empty($task_data)) {
                //$client = New Clients();
                //$clientlocation = $client->getPartner($task_data[0]->clientid);
                
                $tasklocation = New Task();
                $clientlocation = $tasklocation->getTaskClientLocation($task_data[0]->task_id);
                
            }
            $employees = New Employees();
            $employees_data = $employees->getActiveEmployeesByCompanyId($uid,$supervisor_id,$role);
            $empcurrentlocation_data = array();
            if (!empty($employees_data)) {
                foreach ($employees_data as $key => $data) {
                    $latvalue = $data->lat;
                    $langvalue = $data->lang;
                    $empcurrentlocation_data = DB::table('employeesurrentlocation')
                            ->select("*")
                            ->where('employeid', $data->emp_id)
                            ->get()
                            ->toArray();
                    if (!empty($empcurrentlocation_data)) {
                        $latvalue = $empcurrentlocation_data[0]->lan;
                        $langvalue = $empcurrentlocation_data[0]->long;
                    }
                    $getDustance = getdistance($latvalue, $langvalue, $clientlocation->latitude, $clientlocation->longitude, "K");
                   $getTime = GetDrivingDistance($latvalue, $langvalue, $clientlocation->latitude, $clientlocation->longitude);
                   
                   $employees_data[$key]->distance = round($getDustance);
                   $employees_data[$key]->estimated_time = $getTime;
                }
            }
            $collection = collect($employees_data);
            $employees_data = $collection->sortBy('distance');
            $employees_data = $employees_data->values()->all();
            //echo "<pre>";print_r($employees_data);exit;
            return View('partnerTask.assign')->with('task_data', $task_data)->with('employees_data', $employees_data)->with('empcurrentlocation_data', $empcurrentlocation_data)->with('urlPrifix', $urlPrifix);
        } else {
            return Redirect::to($urlPrifix.'/task/');
        }
    }

    public function store_assign(Request $request) {


        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }

        $language_data = new Language();
        $tmp_input = Input::all();
        $tmp_input['comp_id'] = $uid;
        $task_id = 0;
        if (isset($tmp_input['task_id']) && $tmp_input['task_id'] > 0) {
            $task_id = $tmp_input['task_id'];
        }
        
        
        $rules['multiassignEmpId'] = 'required';

        $message = array(
            'multiassignEmpId.required' => $language_data->__('text_alert_select_employee'),
        );

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            if ($task_id > 0) {
                return Redirect::to($urlPrifix.'/task/assign/' . $task_id)->withInput()->withErrors($validator)->with('validatorError', 'validatorError')->with('alert', 'alert-danger');
                ;
            } else {
                return Redirect::to($urlPrifix.'/task/create')->withInput()->withErrors($validator);
            }
        }

        $partner = new Task();
        $response = $partner->taskAssignToEmp($tmp_input);

        if ($response) {
            $language_data = new Language();
            if ($response["flag"] == 1) { // 1 = Assigned successfully
                $msg = $language_data->__('text_success_task_assigned');
            } else if ($response["flag"] == 2) { //2 = Select at least one employee
                $msg = $language_data->__('text_alert_select_employee');
            } else if ($response["flag"] == 3) { //3 = Task accepted by emp
                if ($response["emp_name"] != "") {
                    $msg = $language_data->__('text_task_accepted_by') . " " . $response["emp_name"];
                } else {
                    $msg = $language_data->__('text_somthing_went_wrong');
                }
            }
            return Redirect::to($urlPrifix."/task/assign/" . $task_id)->with('msg', $msg)->with('alert', 'alert-success');
        } else {
            return Redirect::to($urlPrifix."/task")->with('msg', "We are sorry! Something went wronge. please try again leter.")->with('alert', 'alert-danger');
        }
    }
    public function getClientMultilocation() {
        $clients = new Clients();
        $clientsLocationdata = $clients->getClientLocationByClientId($_POST["client_id"]);
        
        $taskLocationdata = array();
        $task_id = 0;
        if(isset($_POST["task_id"])){
        $task = new Task();
        $taskLocationdata = $task->getTaskClientLocationByClientId($_POST["client_id"],$_POST["task_id"]);
        $task_id = $_POST["task_id"];
        }
        echo View('partnerTask.client_location')->with('clientLocations', $clientsLocationdata)->with('taskLocationdata', $taskLocationdata)->with('task_id',$task_id);
        exit;
    }

}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Clients;
use App\Model\Partner;
use App\Model\Employees;
use App\Model\Task;
use Session;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;

class PartnertaskbankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {   
            return View('partnerTaskbank.taskbank')->with('urlPrifix',$urlPrifix)->with('uid',$uid)->with('supervisor_id',$supervisor_id);
        }
        else{
            Session::flush();
            return redirect()->action('LoginController@index');
            exit;
        }
    }
    public function insertdragtask(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $res = array();
        if(isset($_POST['isRecurring']) && $_POST['CurrentEmpId'] > 0 && $_POST['EmpId'] > 0 && $_POST['iHour'] > 0 && $_POST['taskId'] > 0 && $_POST['taskEmpId'] > 0){
            try {
                    $task_id = $_POST['taskId'];
                    $emp_id = $_POST['EmpId'];
                    $isRecurring = $_POST['isRecurring'];
                    $task_emp_data = DB::table('tbl_emp_task')->where('et_id', $_POST['taskEmpId'])->get()->first();
                    if(!empty($task_emp_data)){
                        $oTask = DB::table('tbl_task')
                                ->select("*")
                                ->where('task_id', $task_id)
                                ->first();
                        
                        $empDetail = DB::table('tbl_employee')
                                    ->select("emp_name", "device", "deviceId", "emp_number")
                                    ->where('emp_id', $emp_id)
                                    ->first();
                        
                        if($isRecurring==1){
                            $insertArray1['task_name'] = $oTask->task_name;
                            $insertArray1['comp_id'] = $oTask->comp_id;
                            $insertArray1['task_cat'] = $oTask->task_cat;
                            $insertArray1['loc_id'] = $oTask->loc_id;
                            $insertArray1['task_status'] = $oTask->task_status;
                            $insertArray1['additional_instruction'] = $oTask->additional_instruction;
                            $insertArray1['duration'] = $oTask->duration;
                            $insertArray1['tasktype'] = $oTask->tasktype;
                            $insertArray1['clientid'] = $oTask->clientid;
                            $insertArray1['task_delete'] = $oTask->task_delete;
                            $insertArray1['is_from_app'] = $oTask->is_from_app;
                            $insertArray1['is_recurring'] = '0';
                            $insertArray1['recurring_type'] = '0';
                            $insertArray1['recurring_end_date'] = '0000-00-00';
                            $insertArray1['createdat'] = date('Y-m-d H:i:s');
                            $new_task_id = DB::table('tbl_task')->insertGetId($insertArray1);
                            $updateArray['task_id'] = $new_task_id;
                        } 
                        
                        $newDeuDate = date_time_set(date_create($task_emp_data->et_duedate),$_POST['iHour'],$task_emp_data->et_mins);
                        $updateArray['et_duedate'] = date_format($newDeuDate, 'Y-m-d H:i:s');
                        $updateArray['emp_id'] = $_POST['EmpId'];
                        $updateArray['et_hours'] = $_POST['iHour'];
                        DB::table('tbl_emp_task')->where('et_id', $_POST['taskEmpId'])->update($updateArray);
                        
                        //echo "<pre>";print_r($oTask);exit;
                        

                        $alert = $oTask->task_name . ' Assigned To ' . $empDetail->emp_name;


                        $is_push_send = 0;

                        if ($empDetail->device == 2 && strlen($empDetail->deviceId) > 8) {

                            $fields = array(
                                'to' => $empDetail->deviceId,
                                'notification' => array("title" => $alert, "body" => $alert, 'task_id' => $task_id, 'emp_id' => $emp_id, 'NT' => 1),
                                'data' => array("message" => $alert),
                            );
                            $response = androidPushnotification($fields);


                            if ($response) {
                                $is_push_send = 1;
                            }
                        }
                        if ($empDetail->device == 1 && strlen($empDetail->deviceId) > 8) {

                            $iphoneData = array("aps" => array(
                                    'alert' => $alert,
                                    "sound" => "Mowom.caf",
                                    'task_id' => $task_id,
                                    'emp_id' => $emp_id,
                                ),
                                "NT" => 1
                            );

                            $results = iosPushnotification($iphoneData, $empDetail->deviceId);

                            if ($results) {
                                $is_push_send = 1;
                            }
                        }
                        $updateArray = array();
                        $updateArray['emp_id'] = $emp_id;
                        $updateArray['notification'] = $alert;
                        $updateArray['updated_date'] = date("Y-m-d H:i:s");
                        $updateArray['is_push_send'] = $is_push_send;
                        $updateArray['is_accepted'] = '0';
                        if($isRecurring==1){
                            $updateArray['task_id'] = $new_task_id;
                            $updateArray['comp_id'] = $uid;
                            $updateArray['supervisor_id'] = $supervisor_id;
                            DB::table('tbl_assign_task_emp')->insertGetId($updateArray);
                        } else {
                            DB::table('tbl_assign_task_emp')->where('emp_id', $_POST['CurrentEmpId'])->where('task_id', $_POST['taskId'])->update($updateArray);
                        }
                        $res['message'] = 'Task reassigned successfully';
                    }
            }
            catch(Exception $e) {
                    $res['error'] = true;
                    $res['message'] = $e->getMessage();
            }
        }
        echo json_encode($res);
    }

}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Supervisors;
use Session;
use App\Model\Partner;
use App\Model\Clients;
use App\Model\Task;
use App\Model\Employees;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;


class PartnersettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {
            $partner=new Partner();
            $partner_data = $partner->getPartner($uid);
            return View('partnerSetting.update')->with('partner_data',$partner_data);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();
        //
        $tmp_input   = Input::all();
        //echo "<pre>";print_r($tmp_input);exit;
        $supervisor_id = 0;
        if(isset($tmp_input['supervisor_id']) && $tmp_input['supervisor_id'] > 0){
            $supervisor_id = $tmp_input['supervisor_id'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['company_name'] = 'required';
        $rules['address'] = 'required';
        $rules['email_id'] = 'required|email';
        $rules['latitude'] = 'required';
        $rules['longitude'] = 'required';
        $rules['admin_username'] = 'required';
        $rules['mobile'] = 'required';
       
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('partner/setting')->withInput()->withErrors($validator);
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Partner();
        $partner->add_partner($tmp_input);
        $msg = $language_data->__('text_record_update_successfully');
        return Redirect::to("partner/setting")->with('msg',$msg)->with('alert','alert-success');;
    }

    public function changepassword(Request $request){
        $language_data=new Language();
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $tmp_input   = Input::all();
        $superadmin_data = DB::table('tbl_supervisor')->where('supervisor_id', $uid)->get()->first();
        $rules['admin_password'] = 'required';
        $rules['new_password'] = 'required';
        $rules['reenter_password'] = 'required|same:new_password';
        $message = array(
        'adminuname.required' => $language_data->__('text_username_validation_msg'),
        'admin_password.required' => $language_data->__('text_old_password_validation_msg'),
        'new_password.required' => $language_data->__('text_new_password_validation_msg'),
        'reenter_password.required' => $language_data->__('text_reenter_password_validation_msg'),
        'reenter_password.same' => $language_data->__('text_new_pwd_reenter_not_match')
        );
        $validator = Validator::make(Input::all(), $rules,$message);
        $validator->after(function($validator) use ($request) {
            $language_data=new Language();
            $SPA = DB::table('tbl_supervisor')->where('supervisor_id', Session::get('uid'))->get()->first();
            $check = $SPA->password == $request->admin_password ? true : false;
            if (!$check):
                $validator->errors()->add('admin_password',$language_data->__('text_old_pwd_not_match'));
            endif;
        });
        //echo "<pre>";        print_r($validator);exit;
        if ($validator->fails())
        {
            return Redirect::to('partner/setting')->with('passError','Error')->withInput()->withErrors($validator);
        } else {
            $updateArray['password'] = $request->new_password;
            DB::table('tbl_supervisor')->where('supervisor_id', $uid)->update($updateArray);
            $msg = $language_data->__('text_pwd_success');
            return Redirect::to('partner/setting')->with('msg',$msg)->with('alert','alert-success');
        }
    }
}

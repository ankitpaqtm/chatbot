<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Session;
use App\Model\Partner;
use Language;
use DB;
use Route;
use Illuminate\Support\Facades\Hash;


class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        $supervisor_id = 0;
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        
        
        
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {

            
            /*
             * TODO::Get Partner List Code Start
             */
                $partner=new Partner();
                $partner_data=$partner->get_partner_list();

            /*
             * TODO::Get Partner List Code End
             */
            return View('partnerDashboard.index')->with('partner_data',$partner_data)->with('uid',$uid)->with('supervisor_id',$supervisor_id)->with('urlPrifix',$urlPrifix);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }
}

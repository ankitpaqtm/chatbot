<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Clients;
use App\Model\Partner;
use App\Model\Employees;
use Session;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;
use Knp\Snappy\Pdf;

class PartnerreportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        return Redirect::to('partner/report/activeuser');
    }

    public function activeuser(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {   
            return View('partnerReports.activeusers')->with('urlPrifix',$urlPrifix)->with('uid',$uid)->with('supervisor_id',$supervisor_id);
        }
        else{
            Session::flush();
            return redirect()->action('LoginController@index');
            exit;
        }
    }
    
    public function workreport(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {   
            return View('partnerReports.workreport')->with('urlPrifix',$urlPrifix)->with('uid',$uid)->with('supervisor_id',$supervisor_id);
        }
        else{
            Session::flush();
            return redirect()->action('LoginController@index');
            exit;
        }
    }
    public function timereport(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {   
            return View('partnerReports.timereport')->with('urlPrifix',$urlPrifix)->with('uid',$uid)->with('supervisor_id',$supervisor_id);
        }
        else{
            Session::flush();
            return redirect()->action('LoginController@index');
            exit;
        }
    }
    public function logs(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {   
            return View('partnerReports.log')->with('urlPrifix',$urlPrifix)->with('uid',$uid)->with('supervisor_id',$supervisor_id);
        }
        else{
            Session::flush();
            return redirect()->action('LoginController@index');
            exit;
        }
    }
    public function surveyreport(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        $supervisor_id = 0;
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {   
            return View('partnerReports.surveyreport')->with('urlPrifix',$urlPrifix)->with('uid',$uid)->with('supervisor_id',$supervisor_id);
        }
        else{
            Session::flush();
            return redirect()->action('LoginController@index');
            exit;
        }
    }
    
    public function downloadExcel(Request $request)
    {
        $tmp_input   = Input::all();
        $data = json_decode($tmp_input['xlsData'],true);
        $filename = $tmp_input['filename'];
        //echo "<pre>";print_r($data);exit;
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        return Excel::create($filename, function($excel) use ($data,$filename) {
        $excel->sheet($filename, function($sheet) use ($data)
        {
            $sheet->fromArray($data, null, 'A1', true);
            // Set auto size for sheet
            $sheet->setAutoSize(true);
            $sheet->setWidth('A', 3);
            $sheet->setHeight(1, 20);
            // Freeze the first column
            $sheet->freezeFirstColumn();
            // Font family
            $sheet->setFontFamily('Comic Sans MS');

            // Font size
            //$sheet->setFontSize(5);

            // Font bold
            $sheet->setFontBold(true);
            // Sets all borders
            $sheet->setAllBorders('thin');

            // Set border for cells
            $sheet->setBorder('A1', 'thin');

            // Set black background
            $sheet->row(1, function($row) {
                // call cell manipulation methods
                $row->setBackground('#000000');
                $row->setFontColor('#ffffff');
            });
        });
        })->download('xls');            
    }
    public function downloadPDF(Request $request)
    {
        $tmp_input   = Input::all();
        $data = json_decode($tmp_input['PDFData'],true);
        $filename = $tmp_input['filename'];
        //echo "<pre>";print_r($data);exit;
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        return Excel::create($filename, function($excel) use ($data,$filename) {
        $excel->sheet($filename, function($sheet) use ($data)
        {
            $sheet->fromArray($data, null, 'A1', true);
            
            $sheet->setWidth('A', 3);
            $sheet->setHeight(1, 20);
            // Freeze the first column
            $sheet->freezeFirstColumn();
            // Font family
            $sheet->setFontFamily('Comic Sans MS');

            // Font size
            //$sheet->setFontSize(5);

            // Font bold
            $sheet->setFontBold(true);
            // Sets all borders
            $sheet->setAllBorders('thin');

            // Set border for cells
            $sheet->setBorder('A1', 'thin');

            // Set black background
            $sheet->row(1, function($row) {
                // call cell manipulation methods
                $row->setBackground('#000000');
                $row->setFontColor('#ffffff');
            });
            $sheet->cell('A1', function($cells) {

                $cells->setAlignment('center');

            });
        });
        })->download('pdf');          
    }

    public function downloadHTML($id){
        $name = 'MissionReport_'.time();
        $snappy = new Pdf(realpath('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));
        header('Content-Type: application/pdf');
        header("Content-Disposition: attachment; filename=$name.pdf");
        $html = View('partnerReports.downloadhtml')->with('et_id',$id);
        echo $snappy->getOutputFromHtml($html);
        exit;
    }
    
}

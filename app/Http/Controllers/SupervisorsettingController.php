<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Supervisors;
use Session;
use App\Model\Partner;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;


class SupervisorsettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {
            $supervisors = new Supervisors();
            $supervisors_data = $supervisors->getPartner($uid);
            return View('supervisorSetting.update')->with('supervisors_data',$supervisors_data);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comId = Session::get('companyid')!='' ? Session::get('companyid') : '';
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();
        //
        $tmp_input   = Input::all();
        $tmp_input['companyid'] = $comId;
        //echo "<pre>";print_r($tmp_input);exit;
        $supervisor_id = 0;
        if(isset($tmp_input['supervisor_id']) && $tmp_input['supervisor_id'] > 0){
            $supervisor_id = $tmp_input['supervisor_id'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['sup_name'] = 'required';
        $rules['email'] = 'required|email';
        $rules['mobilenum'] = 'required';
       
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('supervisor/setting')->withInput()->withErrors($validator);
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Supervisors();
        if($partner->add_supervisors($tmp_input)) {
            $msg = $language_data->__('text_record_update_successfully');
            return Redirect::to("supervisor/setting")->with('msg',$msg)->with('alert','alert-success');;
        }
    }

    public function changepassword(Request $request){
        $language_data=new Language();
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $tmp_input   = Input::all();
        $superadmin_data = DB::table('tbl_supervisor')->where('supervisor_id', $uid)->get()->first();
        $rules['admin_password'] = 'required';
        $rules['new_password'] = 'required';
        $rules['reenter_password'] = 'required|same:new_password';
        $message = array(
        'adminuname.required' => $language_data->__('text_username_validation_msg'),
        'admin_password.required' => $language_data->__('text_old_password_validation_msg'),
        'new_password.required' => $language_data->__('text_new_password_validation_msg'),
        'reenter_password.required' => $language_data->__('text_reenter_password_validation_msg'),
        'reenter_password.same' => $language_data->__('text_new_pwd_reenter_not_match')
        );
        $validator = Validator::make(Input::all(), $rules,$message);
        $validator->after(function($validator) use ($request) {
            $language_data=new Language();
            $SPA = DB::table('tbl_supervisor')->where('supervisor_id', Session::get('uid'))->get()->first();
            $check = $SPA->password == $request->admin_password ? true : false;
            if (!$check):
                $validator->errors()->add('admin_password',$language_data->__('text_old_pwd_not_match'));
            endif;
        });
        //echo "<pre>";        print_r($validator);exit;
        if ($validator->fails())
        {
            return Redirect::to('supervisor/setting')->with('passError','Error')->withInput()->withErrors($validator);
        } else {
            $updateArray['password'] = $request->new_password;
            DB::table('tbl_supervisor')->where('supervisor_id', $uid)->update($updateArray);
            $msg = $language_data->__('text_pwd_success');
            return Redirect::to('supervisor/setting')->with('msg',$msg)->with('alert','alert-success');
        }
    }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Clients;
use App\Model\Partner;
use App\Model\Employees;
use Session;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;


class PartneremployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        
        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {

            
            /*
             * TODO::Get Partner List Code Start
             */
                $clients=new Employees();
                $clients_data=$clients->getEmployeesByCompanyId($uid,$supervisor_id,$role);
                $Partner = new Partner();
                $Partner_data=$Partner->getPartner($uid);
            /*
             * TODO::Get Partner List Code End
             */
            return View('partnerEmployees.index')->with('employees_data',$clients_data)->with('partner_data',$Partner_data)->with('urlPrifix',$urlPrifix);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        
        

        if(isset($uid) && $uid!='')
        {
            $clients=new Employees();
            $clients_data=$clients->getEmployeesByCompanyId($uid);

            $Partner = new Partner();
            $Partner_data=$Partner->getPartner($uid);

            $partnerClientCount = $Partner_data[0]->employeecount;
            $currentClientCount = count($clients_data);
            if($currentClientCount < $partnerClientCount){
                return View('partnerEmployees.create');
            } else {
                return Redirect::to('partner/employees/');
            }
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $language_data=new Language();
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        
        //
        $tmp_input   = Input::all();
        $tmp_input['companyid'] = $uid;
        $tmp_input['supervisor_id'] = $supervisor_id;
        //echo "<pre>";print_r($tmp_input);exit;
        $clientid = 0;
        if(isset($tmp_input['emp_id']) && $tmp_input['emp_id'] > 0){
            $clientid = $tmp_input['emp_id'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['teamname'] = 'required';
        if(!$clientid){
            $rules['clientlogo'] = 'required';
            $rules['email_id'] = 'required|email|unique:tbl_employee,emp_email';
            $rules['password'] = 'required';
        } else {
            $rules['email_id'] = 'required|email';
        }
        $rules['teamdesignation'] = 'required';
        $rules['mobile'] = 'required';
        $rules['address'] = 'required';
        $rules['latitude'] = 'required';
        $rules['longitude'] = 'required';

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails())
        {
            if($clientid > 0){
                return Redirect::to($urlPrifix.'/employees/update/'.$clientid)->withInput()->withErrors($validator);
            } else {
                return Redirect::to($urlPrifix.'/employees/create')->withInput()->withErrors($validator);
            }
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Employees();
        if($partner->add_employees($tmp_input)) {
            if($clientid > 0){
                $msg = $language_data->__('text_record_update_successfully');
            } else {
                $msg = $language_data->__('text_record_insert_successfully');
            }
            return Redirect::to($urlPrifix."/employees")->with('msg',$msg)->with('alert','alert-success');
            //return Redirect::to("partner/clients");
        } else {
            return Redirect::to($urlPrifix."/employees")->with('msg',$language_data->__('text_we_are_sorry'))->with('alert','alert-danger');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $partner=new Employees();
        $partner_data = $partner->getPartner($id);
        //echo "<pre>";print_r($partner_data);exit;
        return View('partnerEmployees.update')->with('employee_data',$partner_data);
    }

    
    public function status(request $request, $id)
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $param = Input::all();
        if(isset($param['status'])){
            $partner=new Employees();
            $partner->statusPartner($id,$param['status']);
        }
        return Redirect::to($urlPrifix."/employees/index");
    }
    
    public function destroy($id)
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $partner=new Employees();
        $partner->destroyPartner($id);
        return Redirect::to($urlPrifix."/employees/index");
    }
    public function reactive($id)
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $supervisor_id = $uid;
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        $partner=new Employees();
        $partner->reactivePartner($id);
        return Redirect::to($urlPrifix."/employees/index");
    }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests\PartnersRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Session;
use App\Model\Partner;
use Language;
use DB;
use Route;
use Illuminate\Support\Facades\Hash;


class PartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {

            
            /*
             * TODO::Get Partner List Code Start
             */
                $partner=new Partner();
                $partner_data=$partner->get_partner_list();

            /*
             * TODO::Get Partner List Code End
             */
            return View('partner.index')->with('partner_data',$partner_data);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';

        if(isset($uid) && $uid!='')
        {
            return View('partner.create');
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();
        //
        $tmp_input   = Input::all();
        //echo "<pre>";print_r($tmp_input);exit;
        $partnerid = 0;
        if(isset($tmp_input['partnerid']) && $tmp_input['partnerid'] > 0){
            $partnerid = $tmp_input['partnerid'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['company_name'] = 'required';
        $rules['address'] = 'required';
        if(!$partnerid){
            $rules['clientlogo'] = 'required';
            $rules['email_id'] = 'required|email|unique:partnerslist,email_id';
        } else {
            $rules['email_id'] = 'required|email';
        }
        $rules['admin_username'] = 'required';

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            if($partnerid > 0){
                return Redirect::to('partners/update/'.$partnerid)->withInput()->withErrors($validator);
            } else {
                return Redirect::to('partners/create')->withInput()->withErrors($validator);
            }
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Partner();
        if($partner->add_partner($tmp_input)) {
            return Redirect::to("partners/index");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $partner=new Partner();
        $partner_data = $partner->getPartner($id);
        return View('partner.show')->with('partner_data',$partner_data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $partner=new Partner();
        $partner_data = $partner->getPartner($id);
        return View('partner.update')->with('partner_data',$partner_data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner=new Partner();
        $partner->destroyPartner($id);
        return Redirect::to("partners/index");
    }
    
    public function status(request $request, $id)
    {
        $param = Input::all();
        if(isset($param['status'])){
            $partner=new Partner();
            $partner->statusPartner($id,$param['status']);
        }
        return Redirect::to("partners/index");
    }
    
    public function setting(){
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $superadmin_data = DB::table('superadmin')->select(DB::raw('username,id,password'))->where('id', $uid)->get()->first();
        if(isset($uid) && $uid!='')
        {
            return View('partner.setting')->with('superadmin_data',$superadmin_data); 
        } else {
            return Redirect::to("partners/index");
        }
        
    }
    
    public function storesetting(Request $request){
        $language_data=new Language();
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $tmp_input   = Input::all();
        $superadmin_data = DB::table('superadmin')->select(DB::raw('username,id,password'))->where('id', $uid)->get()->first();
        $rules['adminuname'] = 'required';
        $rules['admin_password'] = 'required';
        $rules['adminpass'] = 'required';
        $rules['reenter_password'] = 'required|same:adminpass';
        $message = array(
        'adminuname.required' => $language_data->__('text_username_validation_msg'),
        'admin_password.required' => $language_data->__('text_old_password_validation_msg'),
        'adminpass.required' => $language_data->__('text_new_password_validation_msg'),
        'reenter_password.required' => $language_data->__('text_reenter_password_validation_msg'),
        'reenter_password.same' => $language_data->__('text_new_pwd_reenter_not_match')
        );
        $validator = Validator::make(Input::all(), $rules,$message);
        $validator->after(function($validator) use ($request) {
            $language_data=new Language();
            $SPA = DB::table('superadmin')->select(DB::raw('username,id,password'))->where('id', Session::get('uid'))->get()->first();
            $check = $SPA->password == $request->admin_password ? true : false;
            if (!$check):
                $validator->errors()->add('admin_password',$language_data->__('text_old_pwd_not_match'));
            endif;
        });
        //echo "<pre>";        print_r($validator);exit;
        if ($validator->fails())
        {
            return Redirect::to('partners/setting')->withInput()->withErrors($validator);
        } else {
            $updateArray['username'] = $request->adminuname;
            $updateArray['password'] = md5($request->adminpass);
            DB::table('superadmin')->where('id', $uid)->update($updateArray);
            DB::insert("insert into `logs` (`client_id`, `log_name_id`, `log_id`, `user_name`, `desc`, `added_date`) values ('0', '1', '100', '".$request->adminuname."', 'Admin Settings Changed', now())");
            return Redirect::to('partners/setting');
        }
    }
    
    public function partnermode($id){
        if($id > 0){
            $superadmin=Session::get('superadmin')!='' ? Session::get('superadmin') : '';
            $role=Session::get('role')!='' ? Session::get('role') : '';
            $partner=new Partner();
            $partner_data = $partner->getPartner($id);
            //pr($partner_data);exit;
            if(!empty($partner_data) && $superadmin > 0 && $role=='100'){
                $partner_data = $partner_data[0];
                Session::put('companyid', $partner_data->partnerid);
                Session::put('company_name', $partner_data->company_name);
                Session::put('uname', $partner_data->company_name);
                Session::put('role', '2');
                Session::put('current_user_email', $partner_data->email_id);
                Session::put('uid', $partner_data->partnerid);
                Session::save();
                return Redirect::to("partner/index");
            } else {
                return Redirect::to("partners/index");
            }
        } else {
            return Redirect::to("partners/index");
        }
    }
    public function adminmode($id){
        if($id > 0){
            $superadmin=Session::get('superadmin')!='' ? Session::get('superadmin') : '';
            $role=Session::get('role')!='' ? Session::get('role') : '';
            $partner=new Partner();
            $superadmin_data = DB::table('superadmin')->select(DB::raw('username,id'))->where('id', $id)->get()->first();
            //pr($partner_data);exit;
            if(!empty($superadmin_data) && $superadmin > 0 && $role=='2'){
                Session::put('username', $superadmin_data->username);
                Session::put('uid', $superadmin_data->id);
                Session::put('role', '100');
                Session::put('superadmin', $superadmin_data->id);
                Session::save();
                return Redirect::to("partners/index");
            } else {
                return Redirect::to("partner/index");
            }
        } else {
            return Redirect::to("partner/index");
        }
    }
}

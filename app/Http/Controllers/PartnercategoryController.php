<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Model\Partner;
use App\Model\Category;
use Session;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;


class PartnercategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        $language_data=new Language();
        
        if(isset($uid) && $uid!='')
        {

            
            /*
             * TODO::Get Partner List Code Start
             */
                $clients=new Category();
                $clients_data=$clients->getEmployeesByCompanyId($uid);
            /*
             * TODO::Get Partner List Code End
             */
            return View('partnerCategory.index')->with('employees_data',$clients_data)->with('urlPrifix',$urlPrifix);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        if(isset($uid) && $uid!='')
        {
            return View('partnerCategory.create');
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $language_data=new Language();
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        
        //
        $tmp_input   = Input::all();
        $tmp_input['partner_id'] = $uid;
        //echo "<pre>";print_r($tmp_input);exit;
        $clientid = 0;
        if(isset($tmp_input['Id']) && $tmp_input['Id'] > 0){
            $clientid = $tmp_input['Id'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['name'] = 'required';
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails())
        {
            if($clientid > 0){
                return Redirect::to($urlPrifix.'/category/update/'.$clientid)->withInput()->withErrors($validator);
            } else {
                return Redirect::to($urlPrifix.'/category/create')->withInput()->withErrors($validator);
            }
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Category();
        if($partner->add_category($tmp_input)) {
            if($clientid > 0){
                $msg = $language_data->__('text_record_update_successfully');
            } else {
                $msg = $language_data->__('text_record_insert_successfully');
            }
            return Redirect::to($urlPrifix."/category")->with('msg',$msg)->with('alert','alert-success');
            //return Redirect::to("partner/clients");
        } else {
            return Redirect::to($urlPrifix."/category")->with('msg',$language_data->__('text_we_are_sorry'))->with('alert','alert-danger');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $partner=new Category();
        $partner_data = $partner->getPartner($id);
        //echo "<pre>";print_r($partner_data);exit;
        return View('partnerCategory.update')->with('employee_data',$partner_data);
    }

    
    public function status(request $request, $id)
    {
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid=Session::get('uid')!='' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid=Session::get('companyid')!='' ? Session::get('companyid') : '';
            $urlPrifix = 'supervisor';
        }
        $param = Input::all();
        if(isset($param['status'])){
            $partner=new Category();
            $partner->statusPartner($id,$param['status']);
        }
        return Redirect::to($urlPrifix."/category");
    }
    
}

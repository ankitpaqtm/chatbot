<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Supervisors;
use Session;
use App\Model\Partner;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;


class PartnersupervisorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();

        if(isset($uid) && $uid!='')
        {

            
            /*
             * TODO::Get Partner List Code Start
             */
                $supervisors=new Supervisors();
                $supervisors_data=$supervisors->getSupervisorsByCompanyId($uid);
                $Partner = new Partner();
                $Partner_data=$Partner->getPartner($uid);

            /*
             * TODO::Get Partner List Code End
             */
            return View('partnerSupervisors.index')->with('partner_data',$Partner_data)->with('supervisors_data',$supervisors_data);
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';

        if(isset($uid) && $uid!='')
        {
            $supervisors=new Supervisors();
            $supervisors_data=$supervisors->getSupervisorsByCompanyId($uid);
            
            $Partner = new Partner();
            $Partner_data=$Partner->getPartner($uid);

            $partnerClientCount = $Partner_data[0]->supervisorcount;
            $currentsupervisorsCount = count($supervisors_data);
            if($currentsupervisorsCount < $partnerClientCount){
                return View('partnerSupervisors.create');
            } else {
                return Redirect::to('partner/supervisors/');
            }
            
            
        }
        else{
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        $language_data=new Language();
        //
        $tmp_input   = Input::all();
        $tmp_input['companyid'] = $uid;
        //echo "<pre>";print_r($tmp_input);exit;
        $supervisor_id = 0;
        if(isset($tmp_input['supervisor_id']) && $tmp_input['supervisor_id'] > 0){
            $supervisor_id = $tmp_input['supervisor_id'];
        }

        /*
         * TODO::Validate The Data
         */
        $rules['sup_name'] = 'required';
        if(!$supervisor_id){
            $rules['profileimage'] = 'required';
            $rules['email'] = 'required|email|unique:tbl_supervisor,email';
        } else {
            $rules['email'] = 'required|email';
        }
        $rules['password'] = 'required';
        $rules['mobilenum'] = 'required';
       
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            if($supervisor_id > 0){
                return Redirect::to('partner/supervisors/update/'.$supervisor_id)->withInput()->withErrors($validator);
            } else {
                return Redirect::to('partner/supervisors/create')->withInput()->withErrors($validator);
            }
        }
        /*
         * TODO::Validate The Data code end
         */
        $partner=new Supervisors();
        if($partner->add_supervisors($tmp_input)) {
            return Redirect::to("partner/supervisors");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $partner=new Supervisors();
        $partner_data = $partner->getPartner($id);
        return View('partnerSupervisors.show')->with('partner_data',$partner_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $partner=new Supervisors();
        $partner_data = $partner->getPartner($id);
        return View('partnerSupervisors.update')->with('partner_data',$partner_data);
    }

    
    public function status(request $request, $id)
    {
        $param = Input::all();
        if(isset($param['status'])){
            $partner=new Supervisors();
            $partner->statusPartner($id,$param['status']);
        }
        return Redirect::to("partner/supervisors/index");
    }
     public function destroy($id)
    {
        $partner=new Supervisors();
        $partner->destroyPartner($id);
        return Redirect::to("partner/supervisors/index");
    }
}

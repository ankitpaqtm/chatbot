<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Clients;
use Session;
use App\Model\Partner;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function getnotification(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            
            $uid = Session::get('uid')!='' ? Session::get('uid') : '';
            $rows = DB::select("SELECT count(id) as  countid FROM tbl_assign_task_emp where is_noti_count =0 AND is_accepted !=0 AND `comp_id`='$uid' order by created_date desc");
            $nCount = $rows[0]->countid;
            
            $query = "SELECT ate.updated_date,ate.is_accepted,e.emp_name,e.profileimage,t.task_name FROM tbl_assign_task_emp ate ";
            $query.= "LEFT JOIN tbl_employee e ON ate.emp_id = e.emp_id ";
            $query.= "LEFT JOIN tbl_task t ON ate.task_id = t.task_id ";
            $query.= "WHERE ate.is_accepted !=0 ";
            $query.= "AND ate.comp_id='$uid' ";
            $query.= "order by ate.updated_date desc";
            //echo $query;
            $notiDatas = DB::select($query);
        } else {
            
            $uid = Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';
            $rows = DB::select("SELECT count(id) as  countid FROM tbl_assign_task_emp where is_noti_count =0 AND is_accepted !=0 AND `comp_id`='$uid'  and supervisor_id='$supervisor_id' order by created_date desc");
            $nCount = $rows[0]->countid;
            
            $query = "SELECT ate.updated_date,ate.is_accepted,e.emp_name,e.profileimage,t.task_name FROM tbl_assign_task_emp ate ";
            $query.= "LEFT JOIN tbl_employee e ON ate.emp_id = e.emp_id ";
            $query.= "LEFT JOIN tbl_task t ON ate.task_id = t.task_id ";
            $query.= "WHERE ate.is_accepted !=0 ";
            $query.= "AND ate.comp_id='$uid' ";
            $query.= "AND ate.supervisor_id='$supervisor_id' ";
            $query.= "order by ate.updated_date desc";
            $notiDatas = DB::select($query);
        }
        echo View('notification.getnotification')->with('nCount',$nCount)->with('notiDatas',$notiDatas);
        exit;
    }
    public function clearnotificationcount(){
        $role = Session::get('role')!='' ? Session::get('role') : '';
        if($role==2){
            $uid = Session::get('uid')!='' ? Session::get('uid') : '';
            DB::table('tbl_assign_task_emp')
            ->where('is_noti_count', '0')
            ->where('comp_id', $uid)
            ->update(['is_noti_count' => '1']);
        } else {
            $uid = Session::get('companyid')!='' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid')!='' ? Session::get('uid') : '';            
            DB::table('tbl_assign_task_emp')
            ->where('is_noti_count', '0')
            ->where('comp_id', $uid)
            ->where('supervisor_id', $supervisor_id)
            ->update(['is_noti_count' => '1']);
        }
        echo 0;
        exit;
    }
}

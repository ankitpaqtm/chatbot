<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class LogoutController extends Controller
{
    public function __construct()
    {
        $this->index();
    }
    public function index()
    {
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';

        if(isset($uid) && $uid!='')
        {
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }
    }
}

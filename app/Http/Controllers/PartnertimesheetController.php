<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Model\Task;
use Session;
use App\Model\Partner;
use App\Model\Clients;
use App\Model\Category;
use App\Model\Employees;
use App\Model\Timesheet;
use Language;
use DB;
use Illuminate\Support\Facades\Hash;

class PartnertimesheetController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        
    }

    public function index() {

        $role = Session::get('role') != '' ? Session::get('role') : '';
        if ($role == 2) {
            $uid = Session::get('uid') != '' ? Session::get('uid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'partner';
        } else {
            $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
            $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
            $urlPrifix = 'supervisor';
        }
        if (isset($uid) && $uid != '') {
            
            $tmp_input   = Input::all();
            
            $task = new Timesheet();
            
            if(!empty($tmp_input)){
            $task_data = $task->getTimesheetByCompanyId($uid,$supervisor_id,$role,$tmp_input);
            }  else {
            $task_data = $task->getTimesheetByCompanyId($uid,$supervisor_id,$role,"");    
            }
            
//            pr($task_data);
//            exit;
            
            return View('partnerTimesheet.index')->with('task_data', $task_data)->with('urlPrifix',$urlPrifix)->with($tmp_input);
        } else {
            Session::flush();

            return redirect()->action('LoginController@index');
            exit;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $task = new Timesheet();
        $task_data = $task->getSingleTimesheetByTaskId($id);
        return View('partnerTimesheet.show')->with('task_data', $task_data);
    }

}

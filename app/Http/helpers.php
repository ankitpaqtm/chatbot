<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 16/1/17
 * Time: 4:50 PM
 */
use App\Model\Task;
Class Helper{
    public static function getusername(){
        return self::getLoggedInUserData('name');
    }
    public static function getuserlogo(){
        return self::getLoggedInUserData('logo');
    }

    public static function getLoggedInUserData($arg){
       $role=Session::get('role')!='' ? Session::get('role') : '';
        $uid=Session::get('uid')!='' ? Session::get('uid') : '';
        if(isset($role) && $role==100)
        {
            try {
                $superadmin_data = DB::table('superadmin')->select(DB::raw('username,id'))->where('id', $uid)->get()->first();
            }
            catch (QueryException $ex) {
                dd($ex->getMessage());
                // Note any method of class PDOException can be called on $ex.
            }
            if(!empty($superadmin_data))
            {
                $userArg=$superadmin_data->username;
                if($arg=='logo'){
                    //$userArg='http://api.randomuser.me/portraits/thumb/men/10.jpg';
                    $userArg = asset('resources/assets/momom-assets/img/mowomlogo.png');
                }
                return $userArg;
            }

        }
        if(isset($role) && ($role==2))
        {
            try {
                $partner_data = DB::table('partnerslist')->where('partnerid', $uid)->get()->first();
            }
            catch (QueryException $ex) {
                dd($ex->getMessage());
                // Note any method of class PDOException can be called on $ex.
            }
            if(!empty($partner_data))
            {
                $userArg=$partner_data->company_name;
                if($arg=='logo'){
                    $userArg= url('public/uploads/logos/').'/'.$partner_data->logo;
                }
                return $userArg;
            }
        } 
        if(isset($role) && ($role==3))
        {
            try {
                $supervisor_data = DB::table('tbl_supervisor')
                        ->select('sup_name', 'pfilename')
                        ->where('supervisor_id', $uid)
                        ->get()
                        ->first();
            }
            catch (QueryException $ex) {
                dd($ex->getMessage());
                // Note any method of class PDOException can be called on $ex.
            }
            if(!empty($supervisor_data))
            {
                $userArg=$supervisor_data->sup_name;
                if($arg=='logo'){
                    $userArg= url('public/uploads/supervisors').'/'.$supervisor_data->pfilename;
                }
                return $userArg;
            }
        } 
    }
    
    public static function changeDateFormate($date,$date_format){
        /*
         * Example
         * {{ Helper::changeDateFormate(date('Y-m-d H:i:s'),'d-m-Y')  }}
         */
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format($date_format);
    }
    public static function getRecTaskByTaskId($taskid){
        $task = new task();
        return $task->getRecTaskByTaskId($taskid);
    }
}

<?php 
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Response;
class PartnersRequest extends FormRequest {


	//protected $redirect = 'user/create';
	 public function rules()
    {
        return [
           'event_name' => 'required',
           'location' => 'required',
           'price' => 'required|numeric',
           
        ];
    }
    public function authorize()
    {
        // Only allow logged in users
       return \Auth::check();
        // Allows all users in
       // return true;
    }
    
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','IndexController@index');
Route::post('/login','LoginController@index');
Route::get('/login','LoginController@index');
Route::get('login/create','LoginController@create');
Route::any('getnotification','NotificationController@getnotification');
Route::any('clearnotificationcount','NotificationController@clearnotificationcount');

//----- Super Admin ------//
Route::any('partners','PartnersController@index');
Route::any('partners/index','PartnersController@index');
Route::any('partners/create','PartnersController@create');
Route::any('partners/store','PartnersController@store');
Route::get('partners/show/{id}','PartnersController@show');
Route::any('partners/update/{id}','PartnersController@update');
Route::get('partners/destroy/{id}','PartnersController@destroy');
Route::get('partners/status/{id}','PartnersController@status');
Route::get('partners/partnermode/{id}','PartnersController@partnermode');
Route::get('partners/adminmode/{id}','PartnersController@adminmode');
Route::any('partners/setting','PartnersController@setting');
Route::any('partners/storesetting','PartnersController@storesetting');
//----- End of Super Admin ------//

//----- Patner ------//

//--- Dashboard---//
Route::any('partner','PartnerController@index');
Route::any('partner/index','PartnerController@index');
Route::any('partner/employeemap','PartnerController@employeemap');
//--- Dashboard End---//

//--- Clients---//
Route::any('partner/clients','PartnerclientsController@index');
Route::any('partner/clients/index','PartnerclientsController@index');
Route::any('partner/clients/create','PartnerclientsController@create');
Route::any('partner/clients/import','PartnerclientsController@import');
Route::any('partner/clients/store','PartnerclientsController@store');
Route::any('partner/clients/uploadexcel','PartnerclientsController@uploadexcel');
Route::any('partner/clients/update/{id}','PartnerclientsController@update');
Route::get('partner/clients/show/{id}','PartnerclientsController@show');
Route::get('partner/clients/status/{id}','PartnerclientsController@status');
Route::get('partner/clients/destroyLocation/{id}/{cid}','PartnerclientsController@destroyLocation');
//--- Clients End---//

//--- Supervisor---//
Route::any('partner/supervisors','PartnersupervisorsController@index');
Route::any('partner/supervisors/index','PartnersupervisorsController@index');
Route::any('partner/supervisors/create','PartnersupervisorsController@create');
Route::any('partner/supervisors/store','PartnersupervisorsController@store');
Route::any('partner/supervisors/update/{id}','PartnersupervisorsController@update');
Route::get('partner/supervisors/show/{id}','PartnersupervisorsController@show');
Route::get('partner/supervisors/status/{id}','PartnersupervisorsController@status');
Route::get('partner/supervisors/destroy/{id}','PartnersupervisorsController@destroy');
//--- Supervisor End---//

//--- Employees---//
Route::any('partner/employees','PartneremployeesController@index');
Route::any('partner/employees/index','PartneremployeesController@index');
Route::any('partner/employees/create','PartneremployeesController@create');
Route::any('partner/employees/store','PartneremployeesController@store');
Route::any('partner/employees/update/{id}','PartneremployeesController@update');
Route::get('partner/employees/show/{id}','PartneremployeesController@show');
Route::get('partner/employees/status/{id}','PartneremployeesController@status');
Route::get('partner/employees/destroy/{id}','PartneremployeesController@destroy');
Route::get('partner/employees/reactive/{id}','PartneremployeesController@reactive');
//--- Employees End---//

//---Category---//
Route::any('partner/category','PartnercategoryController@index');
Route::any('partner/category/index','PartnercategoryController@index');
Route::any('partner/category/create','PartnercategoryController@create');
Route::any('partner/category/store','PartnercategoryController@store');
Route::any('partner/category/update/{id}','PartnercategoryController@update');
Route::get('partner/category/show/{id}','PartnercategoryController@show');
Route::get('partner/category/status/{id}','PartnercategoryController@status');
Route::get('partner/category/destroy/{id}','PartnercategoryController@destroy');
//--- Category End---//

//Timesheet
Route::any('partner/timesheet','PartnertimesheetController@index');
Route::any('partner/timesheet/show/{id}','PartnertimesheetController@show');
//Timesheet END

//---Task---//
Route::any('partner/task','PartnertaskController@index');
Route::any('partner/task/index','PartnertaskController@index');
Route::any('partner/task/create','PartnertaskController@create');
Route::any('partner/task/store','PartnertaskController@store');
Route::any('partner/task/update/{id}','PartnertaskController@update');
Route::get('partner/task/show/{id}','PartnertaskController@show');
Route::get('partner/task/status/{id}','PartnertaskController@status');
Route::get('partner/task/destroy/{id}','PartnertaskController@destroy');
Route::get('partner/task/assign/{id}','PartnertaskController@assign');
Route::any('partner/task/store_assign/','PartnertaskController@store_assign');
Route::any('getclientmultilocation','PartnertaskController@getClientMultilocation');
//--- Task End---//

//---Setting---//
Route::any('partner/setting','PartnersettingController@index');
Route::any('partner/setting/index','PartnersettingController@index');
Route::any('partner/setting/store','PartnersettingController@store');
Route::any('partner/setting/changepassword','PartnersettingController@changepassword');
//--- Setting End---//

//---Report---//
Route::any('partner/report','PartnerreportController@index');
Route::any('partner/report/index','PartnerreportController@index');
Route::any('partner/report/activeuser','PartnerreportController@activeuser');
Route::any('partner/report/workreport','PartnerreportController@workreport');
Route::any('partner/report/timereport','PartnerreportController@timereport');
Route::any('partner/report/surveyreport','PartnerreportController@surveyreport');
Route::any('partner/report/logs','PartnerreportController@logs');
Route::any('partner/report/downloadExcel','PartnerreportController@downloadExcel');
Route::any('partner/report/downloadPDF','PartnerreportController@downloadPDF');
Route::get('partner/report/downloadHTML/{id}','PartnerreportController@downloadHTML');
//--- Report End---//

//---Taskbank---//
Route::any('partner/taskbank','PartnertaskbankController@index');
Route::any('partner/taskbank/index','PartnertaskbankController@index');
Route::any('partner/taskbank/insertdragtask','PartnertaskbankController@insertdragtask');
//--- Taskbank End---//


//----- End of Patner ------//


//----- Supervisor Controller ------//
Route::any('supervisor','SupervisorController@index');
Route::any('supervisor/index','SupervisorController@index');

Route::any('supervisor/task','PartnertaskController@index');
Route::any('supervisor/task/index','PartnertaskController@index');
Route::any('supervisor/task/create','PartnertaskController@create');
Route::any('supervisor/task/store','PartnertaskController@store');
Route::any('supervisor/task/update/{id}','PartnertaskController@update');
Route::get('supervisor/task/show/{id}','PartnertaskController@show');
Route::get('supervisor/task/status/{id}','PartnertaskController@status');
Route::get('supervisor/task/destroy/{id}','PartnertaskController@destroy');
Route::get('supervisor/task/assign/{id}','PartnertaskController@assign');
Route::any('supervisor/task/store_assign/','PartnertaskController@store_assign');

//--- Clients---//
Route::any('supervisor/clients','PartnerclientsController@index');
Route::any('supervisor/clients/index','PartnerclientsController@index');
Route::any('supervisor/clients/create','PartnerclientsController@create');
Route::any('supervisor/clients/import','PartnerclientsController@import');
Route::any('supervisor/clients/store','PartnerclientsController@store');
Route::any('supervisor/clients/update/{id}','PartnerclientsController@update');
Route::get('supervisor/clients/show/{id}','PartnerclientsController@show');
Route::get('supervisor/clients/status/{id}','PartnerclientsController@status');
Route::get('supervisor/clients/destroyLocation/{id}/{cid}','PartnerclientsController@destroyLocation');
//--- Clients End---//

//--- Employees---//
Route::any('supervisor/employees','PartneremployeesController@index');
Route::any('supervisor/employees/index','PartneremployeesController@index');
Route::any('supervisor/employees/create','PartneremployeesController@create');
Route::any('supervisor/employees/store','PartneremployeesController@store');
Route::any('supervisor/employees/update/{id}','PartneremployeesController@update');
Route::get('supervisor/employees/show/{id}','PartneremployeesController@show');
Route::get('supervisor/employees/status/{id}','PartneremployeesController@status');
Route::get('supervisor/employees/destroy/{id}','PartneremployeesController@destroy');
Route::get('supervisor/employees/reactive/{id}','PartneremployeesController@reactive');
//--- Employees End---//

//---Category---//
Route::any('supervisor/category','PartnercategoryController@index');
Route::any('supervisor/category/index','PartnercategoryController@index');
Route::any('supervisor/category/create','PartnercategoryController@create');
Route::any('supervisor/category/store','PartnercategoryController@store');
Route::any('supervisor/category/update/{id}','PartnercategoryController@update');
Route::get('supervisor/category/show/{id}','PartnercategoryController@show');
Route::get('supervisor/category/status/{id}','PartnercategoryController@status');
Route::get('supervisor/category/destroy/{id}','PartnercategoryController@destroy');
//--- Category End---//


//Timesheet
Route::any('supervisor/timesheet/index','PartnertimesheetController@index');
Route::any('supervisor/timesheet/show/{id}','PartnertimesheetController@show');
//Timesheet END

//---Setting---//
Route::any('supervisor/setting','SupervisorsettingController@index');
Route::any('supervisor/setting/index','SupervisorsettingController@index');
Route::any('supervisor/setting/store','SupervisorsettingController@store');
Route::any('supervisor/setting/changepassword','SupervisorsettingController@changepassword');
//--- Setting End---//

//---Report---//
Route::any('supervisor/report','PartnerreportController@index');
Route::any('supervisor/report/index','PartnerreportController@index');
Route::any('supervisor/report/activeuser','PartnerreportController@activeuser');
Route::any('supervisor/report/workreport','PartnerreportController@workreport');
Route::any('supervisor/report/timereport','PartnerreportController@timereport');
Route::any('supervisor/report/downloadExcel','PartnerreportController@downloadExcel');
Route::any('supervisor/report/downloadPDF','PartnerreportController@downloadPDF');
Route::get('supervisor/report/downloadHTML/{id}','PartnerreportController@downloadHTML');
//--- Report End---//

//---Taskbank---//
Route::any('supervisor/taskbank','PartnertaskbankController@index');
Route::any('supervisor/taskbank/index','PartnertaskbankController@index');
Route::any('supervisor/taskbank/insertdragtask','PartnertaskbankController@insertdragtask');
//--- Taskbank End---//
//----- End of Client ------//
Route::get('logout/','LogoutController@index');
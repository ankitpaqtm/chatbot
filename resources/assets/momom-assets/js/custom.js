/**
 * Created by avadh-latitude on 17/1/17.
 */

$(document).ready(function () {
    /*
     TODO::GOOGLE MAP INITIALIZATION FOR PARTNER CODE START
     */
    google.maps.event.addDomListener(window, 'load', initialize);
    $("#address").keyup(function () {

        initialize1($(this).val());
    });



    function resizeMap() {
        if (typeof map == "undefined")
            return;
        setTimeout(function () {
            resizingMap();
        }, 400);
    }

    function resizingMap() {
        if (typeof map == "undefined")
            return;
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    }

    $('#addressPopup').on('show.bs.modal', function () {

        resizeMap();


        $(document).on('click', '.addAddress', function (e) {
            var location_uid = $("#location_uid").val();
            var address = $(".address").val();
            var latitude = $("#latitude").val();
            var longitude = $("#longitude").val();
            var html = "";

            var addressCount = $(".removeaddress").length + 1;


            if (location_uid > 0) {

                $("#location-" + location_uid).val(address);
                $("#latitude-" + location_uid).val(latitude);
                $("#longitude-" + location_uid).val(longitude);
                $("#location-lable-" + location_uid).text(address);
                $(".address_input1").val(address);

            } else {

                if (latitude && longitude && address) {


                    html += '<div class="addressSection-' + addressCount + '">';
                    html += '<br><input type="hidden" name="latitude[]" value="' + latitude + '" >';
                    html += '<input type="hidden" name="location_uid[]" value="" >';
                    html += '<input type="hidden" name="longitude[]" value="' + longitude + '" >';
                    html += '<input type="hidden" class="address_input" name="address[]" value="' + address + '" >';
                    html += "<span  style='color:#45B6B0;font-size:15px;' class='icon-location'>&nbsp;&nbsp;" + address + "</span>";
                    html += "&nbsp;&nbsp;&nbsp;<a data-rid=" + addressCount + " href='javascript:;'  class='removeaddress' ><span style='font-size:15px;'  class='entypo-erase'></span></a></div>";
                    $(".appdendAddress").append(html);
                    $(".address_input1").val(address);

                    $("#latitude").val("");
                    $("#longitude").val("");
                    $(".address").val("");

                }
            }
            $('#addressPopup').modal("hide");

        });


    })

    $(document).on('click', '.removeaddress', function (e) {
//        if (confirm($(this).data("msg")) == false) {
//            return false;
//        }
        $(this).parent().remove();
    });
    

    $('.editaddress').on('click', function () {

        var locationId = $(this).data("lid");
        var address = $("#location-" + locationId).val();
        var latitude = $("#latitude-" + locationId).val();
        var longitude = $("#longitude-" + locationId).val();
        if (latitude && longitude && address) {

            $("#location_uid").val(locationId);
            $("#latitude").val(latitude);
            $("#longitude").val(longitude);
            $(".address").val(address);

        }
        setTimeout(function () {
            resizeMap();
            $("#address").trigger("keyup");
        }, 400);


    })


    /*
     TODO::GOOGLE MAP INITIALIZATION FOR PARTNER CODE START
     */

    /*
     TODO::On language change change a data code
     */

    /*$(document).on('change', '.dd-lang', function (e) {
     var lang = $(this).val();
     $(".changelang").val(lang);
     $(".changelangform").trigger('submit');
     
     });*/
    $(document).on('click', '.dd-lang', function (e) {
        console.log("call");
        var lang = $(this).attr('data-id');
        $(".changelang").val(lang);
        $(".changelangform").trigger('submit');

    });

    /*
     TODO::On language change change a data code
     */



    $(function () {
        $('.footable-res').footable();
    });

    $(function () {
        $('#footable-res2').footable().bind('footable_filtering', function (e) {
            var selected = $('.filter-status').find(':selected').text();
            if (selected && selected.length > 0) {
                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                e.clear = !e.filter;
            }
        });

        $('.clear-filter').click(function (e) {
            e.preventDefault();
            $('.filter-status').val('');
            $('table.demo').trigger('footable_clear_filter');
        });

        $('.filter-status').change(function (e) {
            e.preventDefault();
            $('table.demo').trigger('footable_filter', {
                filter: $('#filter').val()
            });
        });

        $('.filter-api').click(function (e) {
            e.preventDefault();

            //get the footable filter object
            var footableFilter = $('table').data('footable-filter');

            alert('about to filter table by "tech"');
            //filter by 'tech'
            footableFilter.filter('tech');

            //clear the filter
            if (confirm('clear filter now?')) {
                footableFilter.clearFilter();
            }
        });
    });

    $(function () {
        $(".submitPartner").click(function () {

        })
    })

    $("#wizard-tab").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "none",
        enableFinishButton: false,
        enablePagination: false,
        enableAllSteps: true,
        titleTemplate: "#title#",
        cssClass: "tabcontrol"
    });

    $('#downloadXLSFile').on('click', function () {
        $('.downloadXls').submit();
    });
    $('#downloadPDFFile').on('click', function () {
        $('.downloadPDF').submit();
    });


if($('#language').val() == "persian"){
//    var monthNames = ["January", "February", "March", "April", "May", "June",
//    "July", "August", "September", "October", "November", "December"
//];
//
//var monthNames1 = ['Janvier','Février','Mars','Avril','Mai','Juin',
//    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'];

setTimeout(function(){
        var mnthArr = [];
        mnthArr["January"] = "Janvier";
        mnthArr["February"] = "Février";
        mnthArr["March"] = "Mars";
        mnthArr["April"] = "Avril";
        mnthArr["May"] = "Mai";
        mnthArr["June"] = "Juin";
        mnthArr["July"] = "Juillet";
        mnthArr["August"] = "Août";
        mnthArr["September"] = "Septembre";
        mnthArr["October"] = "Octobre";
        mnthArr["November"] = "Novembre";
        mnthArr["December"] = "Décembre";
        var datArr = [];
        datArr["Sun"] = "Dom";
        datArr["Mon"] = "Lun";
        datArr["Tue"] = "Mar";
        datArr["Wed"] = "Mie";
        datArr["Thu"] = "Jue";
        datArr["Fri"] = "Vie";
        datArr["Sat"] = "Sab";
        
        
 $(".monthname").text(" "+mnthArr[$.trim($(".monthname").text())]);
 
 
 var daysname = $.trim($(".daysname").text());
 
 var daysname1 = daysname.replace(/^,|,$/g,'');
 
 $(".daysname").text(datArr[daysname1]+", ");
 }, 2000);
}


$(".paper-wrap .content-wrap").after('<div class="footer-space"></div><div id="footer"><div class="devider-footer-left"></div><div class="time"><p id="spanDate"></p><p id="clock"></p></div><div class="copyright">Copyright © synapsis ks morocco</div><div class="devider-footer"></div></div>');

});



var map;
var marker;
var gmarkers = [];
var zoomlevel = 3;
if ($("#latitude").val() != '' && $("#longitude").val() != '') {
    var myLatlng = new google.maps.LatLng($("#latitude").val(), $("#longitude").val());
    zoomlevel = 13;
} else {
    var myLatlng = new google.maps.LatLng('52.500556', '13.398889');
}

if (myLatlng) {

    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();
    function initialize() {
        var mapOptions = {
            zoom: zoomlevel,
            center: myLatlng,
        };

        map = new google.maps.Map(document.getElementById("myMap1"), mapOptions);

        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            draggable: true
        });
        gmarkers.push(marker);

        geocoder.geocode({'latLng': myLatlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });

        google.maps.event.addListener(marker, 'dragend', function () {
            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });

    }
}

function initialize1(address) {
    geocoder.geocode({'address': address},
    function (results, status) {
        for (i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                map.setCenter(results[0].geometry.location);
                map.setZoom(13);
                var marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    position: results[0].geometry.location
                });
                gmarkers.push(marker);
                $('#latitude').val(marker.getPosition().lat());
                $('#longitude').val(marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
                google.maps.event.addListener(marker, 'dragend', function () {
                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                $('#latitude').val(marker.getPosition().lat());
                                $('#longitude').val(marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
            }
        }
    });

}


var sLang = "<?php echo (isset($_SESSION['language']) ? $_SESSION['language'] : 'english'); ?>";
if (sLang != 'persian') {
    $(".alpha-only").keypress(function (event) {

        var inputValue = event.charCode;
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
    $(".allowNumber").keydown(function (e) {

        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }

                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
}
//    });

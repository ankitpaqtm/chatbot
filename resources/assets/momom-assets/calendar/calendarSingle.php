<!--calendar.start-->


<!-- import the Jalali Date Class script -->
<script type="text/javascript" src="calendar/jalali.js"></script>

<!-- import the calendar script -->
<script type="text/javascript" src="calendar/calendar.js"></script>

<!-- import the calendar script -->
<script type="text/javascript" src="calendar/calendar-setup.js"></script>

<!-- import the language module -->


<link rel="stylesheet" type="text/css" media="all" href="calendar/css/theme.css" title="Aqua" />
<?php if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") { ?>
    <script type="text/javascript" src="calendar/lang/calendar-fa.js"></script>
    <script type="text/javascript">
        Calendar.setup({
            inputField: "jalali_english_calendar", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
    </script>
    <style type="text/css">
        .calendar {
            direction: rtl;
        }

        #flat_calendar_1, #flat_calendar_2{
            width: 200px;
        }

        #flat_calendar_3{
            width: 230px;
        }
        .display_area {
            background-color: #FFFF88
        }
    </style>
<?php } else { ?>
    <script type="text/javascript" src="calendar/lang/calendar-en.js"></script>
    <script type="text/javascript">
        Calendar.setup({
            inputField: "jalali_english_calendar", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            weekNumbers: false,
            
        });
    </script>
<?php } ?>

<!--calendar.end-->
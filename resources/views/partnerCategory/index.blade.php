<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */

?>

@include('layouts.header')
@php
    $language_data=new Language();
@endphp
<body>



<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_categories') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_list_of_categories') }} </h6>
                            </label>
                            
                            
                        </div>
                        <div class="body-nest" id="Filtering">
                            @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                            @endif
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-sm-4">
                                    <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-6">
                                    <a href="{{ url($urlPrifix.'/category/create/') }}" style="margin-left:10px;" class="pull-right btn btn-info " title="{{ $language_data->__('text_add_category') }}">{{ $language_data->__('text_add_category') }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="body-nest" id="Footable">


                            <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter" data-filter-text-only="true">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo $language_data->__('text_category_name'); ?></th>
                                    <th><?php echo $language_data->__('text_status'); ?></th>
                                    <th width="15%"><?php echo $language_data->__('text_action'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>

                                @foreach($employees_data as $partnerdata)
                                    <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td>{{ $partnerdata->name }}</td>
                                        <td class="text-center"><?= $partnerdata->status=='1' ? '<span class="status-metro status-active" title="'.$language_data->__('text_active').'">'.$language_data->__('text_active').'</span>' : '<span class="status-metro status-disabled" title="'.$language_data->__('text_inactive').'">'.$language_data->__('text_inactive').'</span>' ?> </td>
                                        <td>
                                            <a class="btn btn-small btn-info" href="{{ url($urlPrifix.'/category/update/'.$partnerdata->Id)}}"
                                                title="{{ $language_data->__('text_edit_partner') }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-warning" href="<?= $partnerdata->status=='1' ? url($urlPrifix.'/category/status/'.$partnerdata->Id.'?status=0') : url($urlPrifix.'/category/status/'.$partnerdata->Id.'?status=1')?>" title="{{ $partnerdata->status=='1' ? $language_data->__('text_suspend_category') : $language_data->__('text_activate_category') }}">
                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="pagination pagination-centered"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');


<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 24/1/17
 * Time: 3:02 PM
 */
?>
@php
    $language_data=new Language();
@endphp
<script type="text/javascript">
    $(document).ready(function(){
        $(".submitEmp").click(function () {
            $(".error").html("");
            $(".employeecount").css("border","1px solid #e2e2e4");


            if ($("#name").val() == "") {
                $("#name_error").html('<?php echo $language_data->__('text_category_valid_msg'); ?>');
                return false;
            }
        });
        
    })
</script>

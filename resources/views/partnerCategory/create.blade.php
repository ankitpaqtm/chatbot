<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_categories') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-2">
                                <h6>{{ $language_data->__('text_add_category') }} </h6>
                            </label>
                        </div>


                        <div class="body-nest" id="element">
                            <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/category/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_category_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="name" name="name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_category_name'); ?>">
                                            <label for="name" id="name_error" generated="true" class="error">{{ $errors->first('name') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-info submitEmp" name="add_client" id="add_client" type="submit">{{ $language_data->__('text_submit') }}</button>
                                        <a class="btn btn-warning" href="{{ url('/partner/category') }}">{{ $language_data->__('text_cancel') }}</a>
                                    </div>
                                {!! Form::close() !!}
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partnerCategory.validation');





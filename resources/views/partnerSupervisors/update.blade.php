<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_supervisor') }}
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">


                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->

            <div class="content-wrap">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_edit_supervisor') }} </h6>
                                </label>
                            </div>
                            <div class="body-nest" id="element">
                                <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/supervisors/store']) !!}
                                    <div class="form-group">
                                        <input type="hidden" name="supervisor_id" value="{{ $partner_data[0]->supervisor_id }}" />
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" tabindex="1" value="{{ $partner_data[0]->sup_name }}" id="sup_name" name="sup_name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_name'); ?>">
                                            <label for="sup_name" id="sup_name_error" generated="true" class="error">{{ $errors->first('sup_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" tabindex="2"     id="email"  pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$" value="{{ $partner_data[0]->email }}"  name="email"  class="form-control placeholder-no-fix emailId" placeholder="<?php echo $language_data->__('text_email'); ?>">
                                            <label for="email" id="email_error" generated="true" class="error">{{ $errors->first('email') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_password') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="password" tabindex="3"  maxlength="16"      name="password" id="password"   autocomplete="off" value="{{ $partner_data[0]->password }}" class="form-control placeholder-no-fix contactPerson" placeholder="<?php echo $language_data->__('text_password'); ?>">
                                            <label for="contact_person" id="password_error" generated="true" class="error">{{ $errors->first('password') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="4"  maxlength="16"  style="margin-bottom:10px;"       name="mobilenum" id="mobilenum" value="{{ $partner_data[0]->mobilenum }}" value=""  placeholder="<?php echo $language_data->__('text_contact'); ?>" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="mobilenum_error" id="mobilenum_error" generated="true" class="error">{{ $errors->first('mobilenum') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_profile_image') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <img src="{{ url('public/uploads/supervisors/').'/'.$partner_data[0]->pfilename}}" alt="" height="100" width="100" />
                                            <input type="file"  tabindex="5" placeholder="" autocomplete="off" id="profileimage"  name="profileimage"  class="form-control round-input imgLogo">
                                            <label for="profileimage" id="profileimage_error" generated="true" class="error">{{ $errors->first('profileimage') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_status') }}</label>
                                        <div class="col-sm-6">
                                            <div>
                                                <select name="status" class="form-control" id="cmbstat" tabindex="6" >
                                                    <option value="1">{{ $language_data->__('text_active') }}</option>
                                                    <option value="0">{{ $language_data->__('text_inactive') }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <div class="col-md-12 text-center">
                                        <button class="btn btn-info submitPartner" tabindex="7" name="add_supervisor" id="add_supervisor" type="submit">{{ $language_data->__('text_update') }}</button>
                                        <a class="btn btn-warning" href="{{ url('/partner/supervisors') }}">{{ $language_data->__('text_cancel') }}</a>
                                    </div>
                                    
                                    {!! Form::close() !!}
                                </div>

                            </div>

                        </div>


                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partnerSupervisors.validation');





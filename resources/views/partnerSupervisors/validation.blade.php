<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 24/1/17
 * Time: 3:02 PM
 */
?>
@php
    $language_data=new Language();
@endphp
<script type="text/javascript">
    $(document).ready(function(){
        $(".submitPartner").click(function () {
            $(".error").html("");
            $(".employeecount").css("border","1px solid #e2e2e4");


            if ($("#sup_name").val() == "") {
                $("#sup_name_error").html('<?php echo $language_data->__('text_name_validation_msg'); ?>');
                return false;
            }
            
            var language=$("#language").val();
            if (language != 'persian') {
            
            if ($("#email").val() == "") {
                $("#email_error").html('<?php echo $language_data->__('text_email_validation_msg'); ?>');
                return false;
            }

            var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            if (!filter.test($("#email").val())) {
                $("#email_error").html('<?php echo $language_data->__('error_invalid_email'); ?>');
                return false;
            }
            }
            if ($('#password').val() == "") {
                $("#password_error").html('<?php echo $language_data->__('text_password_validation_msg'); ?>');
                return false;
            }
            
            if ($('#mobilenum').val() == "") {
                $("#mobilenum_error").html('<?php echo $language_data->__('text_valid_contact_msg'); ?>');
                return false;
            }
            
            if ($('#mobilenum').val() != "" && $("#mobilenum").val().length < 10) {
                $("#mobilenum_error").html('<?php echo $language_data->__('text_delete_partner_valid_contact_msg'); ?>');
                return false;
            }

            if ($('#profileimage').val() == "") {
                $("#profileimage_error").html('<?php echo $language_data->__('text_profile_image_valid_msg'); ?>');
                return false;
            }

            var ext = $('#profileimage').val().split('.').pop().toLowerCase();
            if ($('#profileimage').val() != "" && $.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                $("#profileimage_error").html('<?php echo $language_data->__('text_images_ext_allowed'); ?>');
                return false;
            }

        });
    })
</script>

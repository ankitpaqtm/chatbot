<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_client') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_import_from_excel') }} </h6>
                            </label>
                            <label class="col-sm-3 pull-right">
                                <h6 style="left: inherit;top: inherit;padding: 0;right: 15px;">
                                    <a href="{{ url('public/client_xls/example.xls') }}" type="button" class="btn btn-sm btn-info">
                                        <span class="glyphicon glyphicon-download"></span>&nbsp;<?php echo $language_data->__('text_download_sample'); ?>
                                    </a>
                                </h6>
                            </label>
                        </div>


                        <div class="body-nest" id="element">
                            @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                            @endif
                            <div class="panel-body">
                                {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/clients/uploadexcel']) !!}

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">{{ $language_data->__('text_upload_xls') }} <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="file"  tabindex="3" required id="upload_xls" accept=".xls" name="upload_xls"  class="form-control round-input">
                                        <label for="upload_xls" id="upload_xls_error" generated="true" class="error">{{ $errors->first('upload_xls') }}</label>
                                    </div>
                                </div>
                                <div class="col-md-7 text-center">
                                    <button class="btn btn-info upload_xls_data" name="upload_xls_data" id="add_client" type="submit">{{ $language_data->__('text_submit') }}</button>
                                </div>
                                {!! Form::close() !!}
                                
                            </div>
                            <div class="body-nest" id="Footable">
                                <?php
                                if(!empty($excelData)){ 
                                ?>
                                {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/clients/uploadexcel']) !!}
                                <table class="table-striped footable-res footable metro-blue">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_client_name') }}
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_client_email') }}
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_address') }}
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_contact_person') }}
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_contact_number') }}
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_latitude') }}
                                        </th>
                                        <th>
                                            {{ $language_data->__('text_longitude') }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $i = 0; 
                                    foreach($excelData as $exData)
                                    {
                                        
                                    ?>   
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td><?=$exData['client_name']?></td>
                                            <td><?=$exData['client_email']?></td>
                                            <td><?=$exData['address']?></td>
                                            <td><?=$exData['contact_person']?></td>
                                            <td><?=$exData['contact_number']?></td>
                                            <td><?=$exData['latitude']?></td>
                                            <td><?=$exData['longitude']?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        
                                        <tr>
                                            <td colspan="8" align="left">
                                                <?php
                                                if(empty($msgLimit)){
                                                    $xlsJsonData = json_encode($excelData);
                                                ?>
                                                    <input type="hidden" name="xmlData" value='<?=$xlsJsonData;?>'/>
                                                    <input type="submit" name="insert_product_details" value="<?php echo $language_data->__('text_insert_clients'); ?>" class="btn btn-sm btn-primary" />
                                                <?php
                                                } else {
                                                ?>    
                                                    <div class="alert {!!$alert!!}">
                                                        <a class="close" data-dismiss="alert">×</a>
                                                        <strong>{!!$msgLimit!!}</strong> 
                                                    </div>
                                                <?php    
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        
                                    </tfoot>
                                </table>
                                {!! Form::close() !!}
                                <?php
                                }
                                ?>
                            </div>    
                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partnerClients.validation');





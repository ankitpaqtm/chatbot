<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_client') }}
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">


                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-2">
                                    <h6>{{ $language_data->__('text_add_client') }} </h6>
                                </label>
                            </div>


                            <div class="body-nest" id="element">
                                <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/clients/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_client_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="client_name" name="client_name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_name'); ?>">
                                            <label for="client_name" id="client_name_error" generated="true" class="error">{{ $errors->first('client_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_location_address') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            
                                            <!--<textarea name="address" tabindex="2" id="address" required class="form-control address" ></textarea>-->
                                            <a  href="#addressPopup" data-toggle="modal" class=" btn btn-success">Add Address</a>
                                            
                                            
                                            <div class="appdendAddress">
                                               
                                            </div>
                                            <input type="hidden" class="address_input1" name="address_input" value="" >
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>
                                            <!--<label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>-->
                                            <!--<input type="hidden" id="latitude" name="latitude[]" value="" >-->
                                            <!--<input type="hidden" id="longitude" name="longitude" value="" >-->
                                        </div>

                                    </div>

                                    <div aria-hidden="true" aria-labelledby="myModalLabel" style="overflow:auto;" role="dialog" tabindex="-1" id="addressPopup" class="modal fade">

                                        <div class="modal-dialog">

                                            <div class="modal-content">

                                                <div class="modal-header">

                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                    <h4 class="modal-title"><?php echo $language_data->__('text_address'); ?></h4>

                                                </div>

                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">{{ $language_data->__('text_location_address') }} <span class="required">*</span></label>
                                                        <div class="col-sm-8">
                                                            <textarea name="addressPop" tabindex="2" id="address" class="form-control address" ></textarea>
                                                            
                                                            <input type="hidden" id="latitude" name="" value="" >
                                                            <input type="hidden" id="longitude" name="" value="" >
                                                            <input type="hidden" id="location_uid" name="" value="" >
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div id="myMap1" style="width: 100%;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="clear"></div>
                                                    <div class="modal-footer">
                                                        <button tabindex="4" class="btn btn-success addAddress" name="addAddress"
                                                                type="button"><?php echo $language_data->__('text_submit'); ?></button>
                                                        <button tabindex="5" data-dismiss="modal" class="btn btn-default"
                                                                type="button"><?php echo $language_data->__('text_cancel'); ?></button>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_logo') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="file"  tabindex="3" style="margin-bottom:10px;height:auto;"  required    placeholder="" autocomplete="off" id="clientlogo"  name="clientlogo"  class="form-control round-input imgLogo">
                                            <label for="clientlogo" id="clientlogo_error" generated="true" class="error">{{ $errors->first('clientlogo') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" tabindex="4" required  value="" id="email_id" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$"   name="email_id"  class="form-control placeholder-no-fix emailId" placeholder="<?php echo $language_data->__('text_email'); ?>">
                                            <label for="email_id" id="email_id_error" generated="true" class="error">{{ $errors->first('email_id') }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_person') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="7"  maxlength="16"  style="margin-bottom:10px;" required      name="contact_person" id="contact_person"  value=""  autocomplete="off" class="form-control placeholder-no-fix contactPerson" placeholder="<?php echo $language_data->__('text_designation'); ?>">
                                            <label for="contact_person" id="contact_person_error" generated="true" class="error">{{ $errors->first('contact_person') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="7"  maxlength="16"  style="margin-bottom:10px;" required      name="mobile" id="mobile"  value=""  placeholder="<?php echo $language_data->__('text_contact'); ?>" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="contact_number_error" id="contact_number_error" generated="true" class="error">{{ $errors->first('mobile') }}</label>
                                        </div>
                                    </div>



                                    <button class="btn btn-info submitPartner" name="add_client" id="add_client" type="submit">{{ $language_data->__('text_submit') }}</button>
                                    {!! Form::close() !!}
                                </div>

                            </div>

                        </div>


                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partnerClients.validation');





<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */

?>

@include('layouts.header')
@php
    $language_data=new Language();
@endphp
<body>



<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_client') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-2">
                                <h6>{{ $language_data->__('text_list_of_clients') }} </h6>
                            </label>
                            
                            <?php
                            $partnerClientCount = $partner_data[0]->clinetscount;
                            $currentClientCount = count($client_data);
                            ?>
                            <label class="col-sm-2 pull-right">
                            <h6 style="color: #DA0F0F;font-weight: bold !important;float: none;text-align: right;margin-right: 10px;">
                                <?php echo $language_data->__('text_used'); ?> <?php echo $language_data->digits($currentClientCount) ?> / <?php echo $language_data->digits($partnerClientCount) ?>
                            </h6>
                            </label>
                            
                        </div>
                        <div class="body-nest" id="Filtering">
                            @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                            @endif
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-sm-4">
                                    <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-6">
                                <?php
                                if($currentClientCount < $partnerClientCount){
                                ?>
                                    <a href="{{ url('/'.$urlPrifix.'/clients/create') }}" style="margin-left:10px;" class="pull-right btn btn-info " title="clear filter">{{ $language_data->__('text_add_client') }}</a>
                                    <a href="{{ url('/'.$urlPrifix.'/clients/uploadexcel') }}" style="margin-left:10px;" class="pull-right btn btn-success" title="clear filter">{{ $language_data->__('text_import_from_excel') }}</a>
                                <?php
                                }
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="body-nest" id="Footable">


                            <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter" data-filter-text-only="true">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_client_name') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_client_email') }}
                                    </th>
<!--                                    <th>
                                        {{ $language_data->__('text_address') }}
                                    </th>-->
                                    <th>
                                        {{ $language_data->__('text_contact_person') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_contact_number') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_status') }}
                                    </th>
                                    <th width="15%">
                                        {{ $language_data->__('text_action') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>

                                @foreach($client_data as $partnerdata)
                                    <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td>{{ $partnerdata->client_name }}</td>
                                        <td>{{ $partnerdata->clientemail }}</td>
                                        <!--<td>{{ $partnerdata->address }}</td>-->
                                        <td>{{ $partnerdata->contract }}</td>
                                        <td>{{ $partnerdata->contactonsite }}</td>
                                        <td class="text-center"><?= $partnerdata->status=='1' ? '<span class="status-metro status-active" title="'.$language_data->__('text_active').'">'.$language_data->__('text_active').'</span>' : '<span class="status-metro status-disabled" title="'.$language_data->__('text_inactive').'">'.$language_data->__('text_inactive').'</span>' ?> </td>
                                        <td>
                                            <a class="btn btn-small btn-info" href="{{ url($urlPrifix.'/clients/update/'.$partnerdata->clientid)}}" title="{{ $language_data->__('text_edit_client') }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-danger" href="<?= $partnerdata->status=='1' ? url($urlPrifix.'/clients/status/'.$partnerdata->clientid.'?status=0') : url($urlPrifix.'/clients/status/'.$partnerdata->clientid.'?status=1')?>" onclick="return confirm('<?= $partnerdata->status=='1' ? $language_data->__('text_delete_client_confirm_inactive') : $language_data->__('text_delete_client_confirm_active') ?>')" title="{{ $partnerdata->status=='1' ? $language_data->__('text_suspend_client') : $language_data->__('text_activate_client') }}">
                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-warning" href="{{ url($urlPrifix.'/clients/show/'.$partnerdata->clientid)}}" title="{{ $language_data->__('text_view_client') }}">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="8">
                                        <div class="pagination pagination-centered"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');


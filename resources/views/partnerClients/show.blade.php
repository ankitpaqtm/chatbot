<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_client') }}
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">


                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-2">
                                    <h6>{{ $partner_data[0]->client_name }} </h6>
                                </label>
                            </div>


                            <div class="body-nest" id="element">
                                <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => $urlPrifix.'/clients/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_client_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="hidden" name="clientid" value="{{ $partner_data[0]->clientid }}" />
                                            <input type="text" class="form-control" id="client_name" name="client_name" value="{{ $partner_data[0]->client_name }}" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_name'); ?>">
                                            <label for="client_name" id="client_name_error" generated="true" class="error">{{ $errors->first('client_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_location_address') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
<!--                                            <textarea name="address" tabindex="2" id="address" required    style="margin-bottom:10px;" class="form-control address" >{{ $partner_data[0]->address }}</textarea>
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>
                                            <input type="hidden" id="latitude" name="latitude" value="{{ $partner_data[0]->latitude }}" >
                                            <input type="hidden" id="longitude" name="longitude" value="{{ $partner_data[0]->longtitude }}" >-->


                                            

                                            <div class="appdendAddress">
                                                <?php $i = 1; ?>
                                                @foreach($clientLocations as $location)
                                                <div class="addressSection-{{ $i }}">
                                                    
                                                    <input type="hidden" name="location_uid[]"  value="{{ $location->id }}" >
                                                    <input type="hidden" name="latitude[]" id="latitude-{{ $location->id }}" value="{{ $location->latitude }}">
                                                    <input type="hidden" name="longitude[]" id="longitude-{{ $location->id }}" value="{{ $location->longitude }}">
                                                    <input type="hidden" class="address_input" name="address[]" id="location-{{ $location->id }}" value="{{ $location->location }}">
                                                    <span style="color:#45B6B0;font-size:15px;" class="icon-location">&nbsp;&nbsp;<span id="location-lable-{{ $location->id }}"> {{ $location->location }}</span></span>
                                                    
                                                </div>
                                                <?php $i++; ?>
                                                
                                                @endforeach
                                            </div>
                                            
                                            <input type="hidden" class="address_input1" name="address_input" value="<?= $clientLocations->count() ?> " >
                                            
                                            
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>


                                        </div>

                                    </div>

                                    
                                 

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_logo') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <img src="{{ url('public/uploads/clients/').'/'.$partner_data[0]->logo}}" alt="" height="100" width="100" />
                                            
                                            <label for="clientlogo" id="clientlogo_error" generated="true" class="error">{{ $errors->first('clientlogo') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" tabindex="4" required  value="{{ $partner_data[0]->clientemail }}" id="email_id" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$"   name="email_id"  class="form-control placeholder-no-fix emailId" placeholder="<?php echo $language_data->__('text_email'); ?>">
                                            <label for="email_id" id="email_id_error" generated="true" class="error">{{ $errors->first('email_id') }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_person') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="7"  maxlength="16"  style="margin-bottom:10px;" required      name="contact_person" id="contact_person"  value="{{ $partner_data[0]->contract }}"  autocomplete="off" class="form-control placeholder-no-fix contactPerson" placeholder="<?php echo $language_data->__('text_designation'); ?>">
                                            <label for="contact_person" id="contact_person_error" generated="true" class="error">{{ $errors->first('contact_person') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="7"  maxlength="16"  style="margin-bottom:10px;" required      name="mobile" id="mobile"  value="{{ $partner_data[0]->contactonsite }}"  placeholder="<?php echo $language_data->__('text_contact'); ?>" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="contact_number_error" id="contact_number_error" generated="true" class="error">{{ $errors->first('mobile') }}</label>
                                        </div>
                                    </div>



                                  <a class="btn btn-info" href="{{ url($urlPrifix.'/clients/')}}" title="{{ $language_data->__('text_cancel') }}">
                                        {{ $language_data->__('text_cancel') }}
                                    </a>
                                    {!! Form::close() !!}
                                </div>

                            </div>

                        </div>


                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
    
    
</body>
@include('layouts.footer');
@include('partnerClients.validation');
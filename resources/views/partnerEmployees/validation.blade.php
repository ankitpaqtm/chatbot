<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 24/1/17
 * Time: 3:02 PM
 */
?>
@php
    $language_data=new Language();
@endphp
<script type="text/javascript">
    $(document).ready(function(){
        $(".submitEmp").click(function () {
            $(".error").html("");
            $(".employeecount").css("border","1px solid #e2e2e4");


            if ($("#teamname").val() == "") {
                $("#teamname_error").html('<?php echo $language_data->__('text_name_validation_msg'); ?>');
                return false;
            }
            
            if ($("#email_id").val() == "") {
                $("#email_id_error").html('<?php echo $language_data->__('text_email_validation_msg'); ?>');
                return false;
            }
            var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            if (!filter.test($("#email_id").val())) {
                $("#email_id_error").html('<?php echo $language_data->__('error_invalid_email'); ?>');
                return false;
            }
            
            if ($("#password").val() == "") {
                $("#password_error").html('<?php echo $language_data->__('text_password_validation_msg'); ?>');
                return false;
            }
            
            if ($("#teamdesignation").val() == "") {
                $("#teamdesignation_error").html('<?php echo $language_data->__('text_designation_validation_msg'); ?>');
                return false;
            }
            
            if ($("#mobile").val() == "") {
                $("#contact_number_error").html('<?php echo $language_data->__('text_valid_contact_msg'); ?>');
                return false;
            }

            if ($("#mobile").val() != "" && $("#mobile").val().length < 8) {
                $("#contact_number_error").html('<?php echo $language_data->__('text_delete_partner_valid_contact_msg'); ?>');
                return false;
            }
            
            if ($("#address").val() == "") {
                $("#address_error").html('<?php echo $language_data->__('text_location_add_validation_msg'); ?>');
                return false;
            }
            
            if ($('#clientlogo').val() == "") {
                $("#clientlogo_error").html('<?php echo $language_data->__('text_logo_validation_msg'); ?>');
                return false;
            }

            var ext = $('#clientlogo').val().split('.').pop().toLowerCase();
            if ($('#clientlogo').val() != "" && $.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                $("#clientlogo_error").html('<?php echo $language_data->__('text_images_ext_allowed'); ?>');
                return false;
            }

            

            

            var language=$("#language").val();
            if (language != 'persian') {
                if ($("#email_id").val().charAt(0) == ".") {
                    $("#email_id_error").html('<?php echo $language_data->__('error_invalid_email'); ?>');
                    return false;
                }
            }
            
            if ($("#contact_person").val() == "") {
                $("#contact_person_error").html('<?php echo $language_data->__('text_contact_person_validation_msg'); ?>');
                return false;
            }
            
            

        });
        
    })
</script>

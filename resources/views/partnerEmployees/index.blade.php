<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */

?>

@include('layouts.header')
@php
    $language_data=new Language();
@endphp
<script type="text/javascript">
function updateBatteryDisplay(battery, id) {
    var level = battery;
    var batteryLevel = $('#empid-' + id + ' .battery .battery-level');

    batteryLevel.css('width', level + '%');

    if (battery.charging) {
        batteryLevel.addClass('charging');
        batteryLevel.removeClass('high');
        batteryLevel.removeClass('medium');
        batteryLevel.removeClass('low');
    } else if (level > 50) {
        batteryLevel.addClass('high');
        batteryLevel.removeClass('charging');
        batteryLevel.removeClass('medium');
        batteryLevel.removeClass('low');
    } else if (level >= 25) {
        batteryLevel.addClass('medium');
        batteryLevel.removeClass('charging');
        batteryLevel.removeClass('high');
        batteryLevel.removeClass('low');
    } else {
        batteryLevel.addClass('low');
        batteryLevel.removeClass('charging');
        batteryLevel.removeClass('high');
        batteryLevel.removeClass('medium');
    }
}
</script>
<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_employees') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_list_of_employees') }} </h6>
                            </label>
                            
                            <?php
                            $partnerClientCount = $partner_data[0]->employeecount;
                            $currentClientCount = count($employees_data);
                            ?>
                            <label class="col-sm-2 pull-right">
                            <h6 style="color: #DA0F0F;font-weight: bold !important;float: none;text-align: right;margin-right: 10px;">
                                <?php echo $language_data->__('text_used'); ?> <?php echo $language_data->digits($currentClientCount) ?> / <?php echo $language_data->digits($partnerClientCount) ?>
                            </h6>
                            </label>
                        </div>
                        <div class="body-nest" id="Filtering">
                            @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                            @endif
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-sm-4">
                                    <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-6">
                                <?php
                                if($currentClientCount < $partnerClientCount){
                                ?>
                                    <a href="{{ url($urlPrifix.'/employees/create/') }}" style="margin-left:10px;" class="pull-right btn btn-info " title="{{ $language_data->__('text_add_team') }}">{{ $language_data->__('text_add_team') }}</a>
                                <?php
                                }
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="body-nest" id="Footable">


                            <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter" data-filter-text-only="true">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo $language_data->__('text_name'); ?></th>
                                    <th><?php echo $language_data->__('text_profile_image'); ?></th>
                                    <th><?php echo $language_data->__('text_email'); ?></th>
                                    <th><?php echo $language_data->__('text_designation'); ?></th>
                                    <th><?php echo $language_data->__('text_status'); ?></th>
                                    <th><?php echo $language_data->__('text_work_status'); ?></th>
                                    <th><?php echo $language_data->__('text_battery_status'); ?></th>
                                    <!--<th><?php echo $language_data->__('text_tasks'); ?></th>-->
                                    <th width="15%"><?php echo $language_data->__('text_action'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>

                                @foreach($employees_data as $partnerdata)
                                    <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td>{{ $partnerdata->emp_name }}</td>
                                        <td>
                                            <?php if (!empty($partnerdata->profileimage)) { ?>
                                                <img src="{{ url('public/uploads/employees/').'/'.$partnerdata->profileimage }}" height="40px"
                                                     width="40px">

                                            <?php } else { ?>
                                                <img src="images/employees/icon-user.png" height="40px" width="40px">
                                            <?php } ?>
                                        </td>
                                        <td>{{ $partnerdata->emp_email }}</td>
                                        <td>{{ $partnerdata->emp_post }}</td>
                                        <td style="text-align:center !important">

                                            <?php if ($partnerdata->delete_employee == 1) { ?>
                                                <span class="status-metro status-disabled" title="<?php echo $language_data->__('text_deleted_user'); ?>"><?php echo $language_data->__('text_deleted_user'); ?></span>
                                            <?php } elseif ($partnerdata->emp_status == 1) { ?>
                                                <span class="status-metro status-active" title="<?php echo $language_data->__('text_activate_user'); ?>"><?php echo $language_data->__('text_activate_user'); ?></span>
                                            <?php } else { ?>
                                                <span class="status-metro status-disabled" title="<?php echo $language_data->__('text_suspend_user'); ?>"><?php echo $language_data->__('text_suspend_user'); ?></span>
                                            <?php } ?>
                                        </td>
                                        <td style="text-align: center !important;">
                                            <?php
                                            //$empopentask123 = isEmpBusy($dt);
                                            $empopentask123 = getEmployeeStatus($partnerdata->emp_id);
                                            ?>  
                                            <?php if ($partnerdata->delete_employee == 1) { ?>
                                                <a class="btn btn-danger"> <?php echo $language_data->__('text_deleted'); ?></a>
                                               <?php } else if ($partnerdata->availability == 2) { ?>
                                                <a class="btn btn-danger"> <?php echo $language_data->__('text_leave'); ?></a>
                                               <?php } else if ($empopentask123 == 0) { ?>
                                                <a class="btn btn-success"> <?php echo $language_data->__('text_available'); ?> </a>

                                            <?php } else if ($empopentask123 == 1) { ?>
                                                <a class="btn btn-warning" ><?php echo $language_data->__('text_on_duty'); ?></a>
                                            <?php } ?>

                                        </td>
                                        <td class="text-center">
                                            <?php
                                            $bettery = (int) $partnerdata->battery_status;
                                            if ($bettery > -1) {
                                                $betteryStatus = "";
                                                if ($bettery == 0) {
                                                    $betteryStatus = "3% 3%";
                                                } else if ($bettery > 0 && $bettery <= 20) {
                                                    $betteryStatus = "3% 23%";
                                                } else if ($bettery > 20 && $bettery <= 40) {
                                                    $betteryStatus = "17% 23%";
                                                } else if ($bettery > 40 && $bettery <= 60) {
                                                    $betteryStatus = "30% 23%";
                                                } else if ($bettery > 60 && $bettery <= 80) {
                                                    $betteryStatus = "43% 23%";
                                                } else if ($bettery > 80 && $bettery <= 100) {
                                                    $betteryStatus = "43% 23%";
                                                }
                                                ?>

                                                <div id="empid-<?php echo $partnerdata->emp_id; ?>">
                                                    <div class="battery">
                                                        <div class="battery-level" style="position: relative;"></div> 
                                                        <span style="position: absolute;top: 0px;left: 0px;right: 0;text-align: center;color: #000;">
                                                            <?php
                                                            echo $language_data->digits($bettery) . "%";
                                                            ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <script>updateBatteryDisplay(<?php echo $bettery; ?>,<?php echo $partnerdata->emp_id; ?>);</script>
                                                <?php 
                                            }
                                            ?>
                                        </td>
                                        <!--<td class="text-center">task</td>-->
                                        <td>
                                            <?php if ($partnerdata->delete_employee == 1) { ?>
                                            <a class="btn btn-small btn-info" href="{{ url($urlPrifix.'/employees/reactive/'.$partnerdata->emp_id)}}" title="{{ $language_data->__('text_Reactivate_user') }}" onclick="return confirm('<?= $language_data->__('text_reactive_deleted_confirmation') ?>')">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </a>
                                            <?php } else { ?>
                                                <?php if ($partnerdata->emp_status == 1) { ?>
                                                <a class="btn btn-small btn-info" href="{{ url($urlPrifix.'/employees/update/'.$partnerdata->emp_id)}}" title="{{ $language_data->__('text_edit_client') }}">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                                <?php }?>
                                            <a class="btn btn-small btn-warning" href="<?= $partnerdata->emp_status=='1' ? url($urlPrifix.'/employees/status/'.$partnerdata->emp_id.'?status=0') : url($urlPrifix.'/employees/status/'.$partnerdata->emp_id.'?status=1')?>" onclick="return confirm('<?= $partnerdata->emp_status=='1' ? $language_data->__('text_delete_client_confirm_inactive') : $language_data->__('text_delete_client_confirm_active') ?>')" title="{{ $partnerdata->emp_status=='1' ? $language_data->__('text_suspend_client') : $language_data->__('text_activate_client') }}">
                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                            </a>
                                            
                                            <a class="btn btn-small btn-danger" href="{{ url($urlPrifix.'/employees/destroy/'.$partnerdata->emp_id)}}" title="{{ $language_data->__('text_delete_user') }}" onclick="return confirm('<?= $language_data->__('text_delete_confirmation') ?>')">
                                                <i class="fa fa-remove" aria-hidden="true"></i>
                                            </a>
                                            <?php }?>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="10">
                                        <div class="pagination pagination-centered"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');


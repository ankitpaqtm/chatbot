<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_client') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <h6>{{$partner_data[0]->client_name}} </h6>
                            

                        </div>
                       
                        <div class="body-nest" id="element">
                            <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partners/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="company_name" name="company_name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" value="{{$partner_data[0]->client_name}}">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_location_address') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea name="address" tabindex="2" id="address" required    style="margin-bottom:10px;" class="form-control address">
                                                {{$partner_data[0]->address}}
                                            </textarea>
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>
                                            <input type="hidden" id="latitude" name="latitude" value=" {{$partner_data[0]->latitude}}" >
                                            <input type="hidden" id="longitude" name="longitude" value=" {{$partner_data[0]->longtitude}}" >
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 pull-left">
                                        <div id="myMap1"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_logo') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <img height="100px" width="100px" src="{{ url('public/uploads/clients/').'/'.$partner_data[0]->logo}}" alt="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" tabindex="4" required placeholder="" id="email_id" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$"   name="email_id"  class="form-control placeholder-no-fix emailId" value="{{$partner_data[0]->clientemail}}">
                                            <label for="address" id="email_id_error" generated="true" class="error">{{ $errors->first('email_id') }}</label>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="7"  maxlength="16"  style="margin-bottom:10px;" required      name="mobile" id="mobile" placeholder="" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber" 
                                            value="{{$partner_data[0]->contactonsite}}"
                                            >
                                            <label for="contact_number" id="contact_number_error" generated="true" class="error"></label>
                                        </div>
                                    </div>
                                    <a class="btn btn-info" href="{{ url('partner/clients/')}}" title="{{ $language_data->__('text_cancel') }}">
                                        {{ $language_data->__('text_cancel') }}
                                    </a>
                                {!! Form::close() !!}
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partner.validation');





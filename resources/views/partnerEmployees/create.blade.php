<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_employees') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-2">
                                <h6>{{ $language_data->__('text_add_team') }} </h6>
                            </label>
                        </div>


                        <div class="body-nest" id="element">
                            <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/employees/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_staff_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="teamname" name="teamname" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_name'); ?>">
                                            <label for="teamname" id="teamname_error" generated="true" class="error">{{ $errors->first('teamname') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" required  value="" id="email_id" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$"   name="email_id"  class="form-control placeholder-no-fix emailId" placeholder="<?php echo $language_data->__('text_email'); ?>">
                                            <label for="email_id" id="email_id_error" generated="true" class="error">{{ $errors->first('email_id') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_password') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="password" style="margin-bottom:10px;" required  name="password" id="password" value="" placeholder="<?php echo $language_data->__('text_password'); ?>" autocomplete="off" class="form-control placeholder-no-fix">
                                            <label for="password" id="password_error" generated="true" class="error">{{ $errors->first('password') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_designation') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="teamdesignation" name="teamdesignation" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_designation'); ?>">
                                            <label for="teamdesignation" id="teamdesignation_error" generated="true" class="error">{{ $errors->first('teamdesignation') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_mobile') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" maxlength="16"  style="margin-bottom:10px;" required      name="mobile" id="mobile"  value=""  placeholder="<?php echo $language_data->__('text_mobile'); ?>" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="contact_number_error" id="contact_number_error" generated="true" class="error">{{ $errors->first('mobile') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_address') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea name="address" id="address" required    style="margin-bottom:10px;" class="form-control address" ></textarea>
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>
                                            <input type="hidden" id="latitude" name="latitude" value="" >
                                            <input type="hidden" id="longitude" name="longitude" value="" >
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 pull-left">
                                        <div id="myMap1"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_profile_image') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="file"  style="margin-bottom:10px;height:auto;"  required    placeholder="" autocomplete="off" id="clientlogo"  name="clientlogo"  class="form-control round-input imgLogo">
                                            <label for="clientlogo" id="clientlogo_error" generated="true" class="error">{{ $errors->first('clientlogo') }}</label>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo $language_data->__('text_status'); ?></label>
                                        <div class="col-sm-6">
                                            <select name="cmbstat" style="margin-bottom:10px;" class="form-control round-input" id="cmbstat">
                                                <option value="1"><?php echo $language_data->__('text_active'); ?></option>
                                                <option value="0"><?php echo $language_data->__('text_inactive'); ?></option>
                                            </select>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php //echo $language_data->__('text_form_feedback_label'); ?></label>
                                        <div class="col-sm-6">
                                            <select name="department" class="form-control department" id="department">
                                                <option value=""><?php //echo $language_data->__('text_select'); ?></option>
                                                <option value="sales"><?php //echo $language_data->__('text_sales'); ?></option>
                                                <option value="survey"><?php //echo $language_data->__('text_survey'); ?></option>
                                                <option value="general"><?php //echo $language_data->__('text_general'); ?></option>
                                                <option value="support"><?php //echo $language_data->__('text_support'); ?></option>
                                                <option value="delivery"><?php //echo $language_data->__('text_delivery'); ?></option>
                                            </select>
                                        </div>
                                    </div>-->
                                    
<!--                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php //echo $language_data->__('text_create_task'); ?></label>
                                        <div class="col-sm-6">
                                            <label class="radio-inline">
                                                <input type="radio" checked="" name="can_create_task"  value="Yes"><?php //echo $language_data->__('text_create_task_yes'); ?>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="can_create_task"  value="No"><?php //echo $language_data->__('text_create_task_no'); ?>
                                            </label>
                                        </div>
                                    </div>-->
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo $language_data->__('text_is_privacy'); ?></label>
                                        <div class="col-sm-6">
                                            <input type="checkbox" name="is_privacy"  value="1">
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-info submitEmp" name="add_client" id="add_client" type="submit">{{ $language_data->__('text_submit') }}</button>
                                        <a class="btn btn-warning" href="{{ url('/partner/employees') }}">{{ $language_data->__('text_cancel') }}</a>
                                    </div>
                                {!! Form::close() !!}
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partnerEmployees.validation');





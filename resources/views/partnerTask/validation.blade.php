<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 24/1/17
 * Time: 3:02 PM
 */
?>
@php
$language_data=new Language();
@endphp
<script type="text/javascript">
    $(document).ready(function () {
        $(".tastHoursLimit").keyup(function (e) {
            if ($(this).val() > 23) {
                $(this).val("");
                return false;
            }
        });
        $(".tastMinLimit").keyup(function (e) {
            if ($(this).val() > 59) {
                $(this).val("");
                return false;
            }
        });
        $(".tastDurLimit").keyup(function (e) {
            if ($(this).val() < 1 || $(this).val() > 300) {
                $(this).val("");
                return false;
            }
        });
        $(".submitPartner").click(function () {
            $(".error").html("");
            $(".employeecount").css("border", "1px solid #e2e2e4");

            var language = $("#language").val();

            if ($("#clientid").val() == "") {
                $("#clientid_error").html('<?php echo $language_data->__('text_alert_select_client'); ?>');
                return false;
            } else {
                if ($('input[name="client_multi_location[]"]:checked').length == 0) {
                    $("#client_multi_location_error").html('<?php echo $language_data->__('text_client_address_msg'); ?>');
                    return false;
                }

            }


            if ($("#categoryid").val() == "") {
                $("#categoryid_error").html('<?php echo $language_data->__('text_alert_select_category'); ?>');
                return false;
            }
            if ($("#task_name").val() == "") {
                $("#task_name_error").html('<?php echo $language_data->__('text_name_validation_msg'); ?>');
                return false;
            }
            if ($("#task_type").val() == "") {
                $("#task_type_error").html('<?php echo $language_data->__('text_alert_enter_type'); ?>');
                return false;
            }

            if ($("#jalali_english_calendar").val() == "") {
                $("#jalali_english_calendar_error").html('<?php echo $language_data->__('text_alert_enter_date'); ?>');
                return false;
            }


            if (language != 'persian') {
                var currentDate = new Date('<?php echo date("Y-m-d") ?>');
                var selectedDate = new Date($("#jalali_english_calendar").val());
                if (currentDate <= selectedDate)
                {

                } else {
                    $("#jalali_english_calendar_error").html("<?php echo $language_data->__('text_prev_due_date_validation_msg'); ?>");
                    return false;
                }
            }

            if ($("#starettime").val() == "") {
                $("#starettime_error").html('<?php echo $language_data->__('text_alert_enter_correct_time'); ?>');
                return false;
            }
            if ($("#et_mins").val() == "") {
                $("#et_mins_error").html('<?php echo $language_data->__('text_min_alert'); ?>');
                return false;
            }
            if ($("#duration").val() == "") {
                $("#duration_error").html('<?php echo $language_data->__('text_alert_enter_correct_duration'); ?>');
                return false;
            }
            if ($('#is_recurring_task').val() == '1') {
                if ($("#recurring_type").val() == "") {
                    $("#recurring_type_error").html('<?php echo $language_data->__('text_alert_select_recurring_type'); ?>');
                    return false;
                }
                if ($("#recurring_end_date").val() == "") {
                    $("#recurring_end_date_error").html('<?php echo $language_data->__('text_alert_enter_recurring_end_date'); ?>');
                    return false;
                }
                var startDate = new Date($("#jalali_english_calendar").val());
                var endDate = new Date($("#recurring_end_date").val());
                if (endDate <= startDate) {
                    $("#recurring_end_date_error").html("<?php echo $language_data->__('text_alert_recurring_end_date_validation_msg'); ?>");
                    return false;
                }
            }
        });


    })
</script>

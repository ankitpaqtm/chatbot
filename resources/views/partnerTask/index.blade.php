<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>



    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_task') }}</span>
                        </h2>
                    </div>

                    <div class="col-sm-7"></div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_list_of_task') }} </h6>
                                </label>
                                <?php
                                $partnerTaskCount = $partner_data[0]->taskcount;
                                ?>
                                <label class="col-sm-2 pull-right">
                                    <h6 style="color: #DA0F0F;font-weight: bold !important;float: none;text-align: right;margin-right: 10px;">
                                        <?php echo $language_data->__('text_used'); ?> <?php echo $language_data->digits($currentTaskCount) ?> / <?php echo $language_data->digits($partnerTaskCount) ?>
                                    </h6>
                                </label> 
                            </div>
                            <div class="body-nest" id="Filtering">
                                @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-6">
                                    <?php if(!empty($task_data) && isset($task_data[0]->task_id) && $task_data[0]->task_id > 0){ ?>
                                    
                                        <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                    
                                    <?php } ?>
                                        </div>
                                    <div class="col-sm-6">
                                        <a href="{{ url($urlPrifix.'/task/create') }}" style="margin-left:10px;" class="pull-right btn btn-info " title="clear filter">{{ $language_data->__('text_add_new_task') }}</a>
                                    </div>
                                </div>
                            </div>

                            <div class="body-nest" id="Footable">
                                <?php
                                                             
                                if(!empty($task_data) && isset($task_data[0]->task_id) && $task_data[0]->task_id > 0){ ?>
                                
                                <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter" data-filter-text-only="true">
                                    <thead>
                                        <tr>
                                            <th>
                                                #
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_task_name') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_client_name') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_address') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_task_start_time') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_task_end_time') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_staff') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_status') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_task_status') }}
                                            </th>
                                            <th width="15%">
                                                {{ $language_data->__('text_task_action') }}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 0; ?>

                                        @foreach($task_data as $partnerdata)
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>{{ $partnerdata->task_name }}</td>
                                            <td>
                                             <?php 
                                                $clientdetails = DB::table('tbl_clients')
                                                        ->select("client_name","address")
                                                        ->where('clientid', $partnerdata->clientid)
                                                        ->first();
                                                echo $clientdetails->client_name;
                                             ?>   
                                                
                                                
                                            </td>
                                            <td> <?php 
                                                echo $clientdetails->address;
                                             ?> </td>
                                            <td><?php
                                                if (strtotime($partnerdata->task_starttime) != '0' && $partnerdata->task_starttime != "0000-00-00 00:00:00") {

                                                    $task_starttime = date("Y-m-d H:i:s", strtotime($partnerdata->task_starttime));

                                                    echo $language_data->convertedDates($task_starttime, "fulldatetime");
                                                } else {
                                                    print "-";
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                $newDate = date("Y-m-d H:i:s", strtotime($partnerdata->task_endtime));

                                                if ($partnerdata->emp_id > 0 && (strtotime($partnerdata->task_endtime) != '0' && $partnerdata->task_endtime != "0000-00-00 00:00:00")) {
                                                    echo $language_data->convertedDates($newDate, "fulldatetime");
                                                } else {
                                                    echo "-";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $taskId = $partnerdata->task_id;

                                                $isEmpAssign = DB::table('tbl_assign_task_emp')
                                                        ->select("emp_id")
                                                        ->where('task_id', $taskId)
                                                        ->orderBy('id', 'DESC')
                                                        ->first();
                                                
                                                $AllEmptaskRejectedList = DB::table('tbl_assign_task_emp')
                                                        ->select("emp_id")
                                                        ->where('task_id', $taskId)
                                                        ->where('is_accepted', "!=", "2")
                                                        ->orderBy('id', 'DESC')
                                                        ->first();
                                                

                                                $employeeId = 0;

                                                if ($partnerdata->emp_id > 0) {
                                                    $employeeId = $partnerdata->emp_id;
                                                    if ($employeeId > 0) {
                                                        $tasksupdetails = DB::table('tbl_employee')
                                                        ->select("emp_name")
                                                        ->where('emp_id', $employeeId)
                                                        ->first();
                                                        echo $tasksupdetails->emp_name;
                                                    }
                                                } else {
                                                    if (empty($AllEmptaskRejectedList)) {
                                                        $EmptaskRejectedIDArr = DB::table('tbl_assign_task_emp')
                                                        ->join("tbl_employee",'tbl_assign_task_emp.emp_id', '=','tbl_employee.emp_id')
                                                        ->select("tbl_employee.emp_name","tbl_assign_task_emp.emp_id")
                                                        ->where('tbl_assign_task_emp.task_id', $taskId)
                                                        ->where('tbl_assign_task_emp.is_accepted', 2)
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
                                                        
                                                        $emprecjetedids = "";
                                                        if (!empty($EmptaskRejectedIDArr)) {
                                                            foreach ($EmptaskRejectedIDArr as $val) {
                                                                if ($val->emp_name) {
                                                                    $emprecjetedids .= " " . $val->emp_name . ",";
                                                                }
                                                            }

                                                            $empIds = trim($emprecjetedids, ',');
                                                            echo $empIds;
                                                        }
                                                    }
                                                }
                                                ?>

                                            </td>
                                            <td style="text-align:center !important;">
                                                <?php
                                                if ($partnerdata->task_status == 1 or $partnerdata->task_status == -1) {
                                                    echo '<span class="status-metro status-active" title="' . $language_data->__('text_active') . '">' . $language_data->__('text_active') . '</span>';
                                                } else {
                                                    echo '<span class="status-metro status-disabled" title="' . $language_data->__('text_inactive') . '">' . $language_data->__('text_inactive') . '</span>';
                                                }
                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                if (empty($AllEmptaskRejectedList) && $partnerdata->emp_id == 0 && !empty($isEmpAssign)) {
                                                    print $language_data->__('text_rejected');
                                                } else if ($partnerdata->started == 1) {
                                                    print $language_data->__('text_started');
                                                } elseif ($partnerdata->status == 2) {
                                                    print $language_data->__('text_completed');
                                                } else if ($partnerdata->status == 0) {
                                                    print $language_data->__('text_accept_pending');
                                                } else if ($partnerdata->status == 1) {
                                                    print $language_data->__('text_pending');
                                                }
                                                ?>
                                            </td>

                                            <td>
                                                <a class="btn btn-small btn-info" href="{{ url('partner/task/update/'.$partnerdata->task_id)}}" title="{{ $language_data->__('text_edit_task') }}">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>

                                                <a class="btn btn-small btn-danger" onclick="return confirm('{{ $language_data->__('text_delete_confirmation') }}')" href="{{ url('partner/task/destroy/'.$partnerdata->task_id)}}" title="{{ $language_data->__('text_delete_task') }}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                                <a class="btn btn-small btn-info" href="{{ url('partner/task/assign/'.$partnerdata->task_id)}}" title="{{ $language_data->__('text_assign') }}">
                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                </a>
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <div class="pagination pagination-centered"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php }else{ ?>
                                <div class="nottaskFound"><?= $language_data->__('text_task_not_found'); ?></div>
                                <?php } ?>
                            </div>
                            
                            
                        </div>

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-4">
                                    <h6>{{ $language_data->__('text_list_of_recurring_task') }} </h6>
                                </label>
                            </div>
                            <div class="body-nest" id="Filtering">
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                    <?php if(!empty($rec_task_data) && isset($rec_task_data[0]->task_id) && $rec_task_data[0]->task_id > 0){ ?>
                                    
                                        <input class="form-control" id="filter1" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                    
                                    <?php } ?>
                                        </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </div>

                            <div class="body-nest" id="Footable">
                                <?php
                                                             
                                if(!empty($rec_task_data) && isset($rec_task_data[0]->task_id) && $rec_task_data[0]->task_id > 0){ ?>
                                
                                <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter1" data-filter-text-only="true">
                                    <thead>
                                        <tr>
                                            <th>
                                                #
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_task_name') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_client_name') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_address') }}
                                            </th>
                                           
                                            <th>
                                                {{ $language_data->__('text_staff') }}
                                            </th>
                                            <th>
                                                {{ $language_data->__('text_status') }}
                                            </th>
                                            
                                            <th>
                                                {{ $language_data->__('text_view_recurring_task') }}
                                            </th>
                                            <th width="15%">
                                                {{ $language_data->__('text_task_action') }}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 0; ?>

                                        @foreach($rec_task_data as $partnerdata)
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>{{ $partnerdata->task_name }}</td>
                                            <td>
                                             <?php 
                                                $clientdetails = DB::table('tbl_clients')
                                                        ->select("client_name","address")
                                                        ->where('clientid', $partnerdata->clientid)
                                                        ->first();
                                                echo $clientdetails->client_name;
                                             ?>   
                                                
                                                
                                            </td>
                                            <td> <?php 
                                                echo $clientdetails->address;
                                             ?> </td>
                                            
                                            <td>
                                                <?php
                                                $taskId = $partnerdata->task_id;

                                                $isEmpAssign = DB::table('tbl_assign_task_emp')
                                                        ->select("emp_id")
                                                        ->where('task_id', $taskId)
                                                        ->orderBy('id', 'DESC')
                                                        ->first();
                                                
                                                $AllEmptaskRejectedList = DB::table('tbl_assign_task_emp')
                                                        ->select("emp_id")
                                                        ->where('task_id', $taskId)
                                                        ->where('is_accepted', "!=", "2")
                                                        ->orderBy('id', 'DESC')
                                                        ->first();
                                                

                                                $employeeId = 0;

                                                if ($partnerdata->emp_id > 0) {
                                                    $employeeId = $partnerdata->emp_id;
                                                    if ($employeeId > 0) {
                                                        $tasksupdetails = DB::table('tbl_employee')
                                                        ->select("emp_name")
                                                        ->where('emp_id', $employeeId)
                                                        ->first();
                                                        echo $tasksupdetails->emp_name;
                                                    }
                                                } else {
                                                    if (empty($AllEmptaskRejectedList)) {
                                                        $EmptaskRejectedIDArr = DB::table('tbl_assign_task_emp')
                                                        ->join("tbl_employee",'tbl_assign_task_emp.emp_id', '=','tbl_employee.emp_id')
                                                        ->select("tbl_employee.emp_name","tbl_assign_task_emp.emp_id")
                                                        ->where('tbl_assign_task_emp.task_id', $taskId)
                                                        ->where('tbl_assign_task_emp.is_accepted', 2)
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
                                                        
                                                        $emprecjetedids = "";
                                                        if (!empty($EmptaskRejectedIDArr)) {
                                                            foreach ($EmptaskRejectedIDArr as $val) {
                                                                if ($val->emp_name) {
                                                                    $emprecjetedids .= " " . $val->emp_name . ",";
                                                                }
                                                            }

                                                            $empIds = trim($emprecjetedids, ',');
                                                            echo $empIds;
                                                        }
                                                    }
                                                }
                                                ?>

                                            </td>
                                            <td style="text-align:center !important;">
                                                <?php
                                                if ($partnerdata->task_status == 1 or $partnerdata->task_status == -1) {
                                                    echo '<span class="status-metro status-active" title="' . $language_data->__('text_active') . '">' . $language_data->__('text_active') . '</span>';
                                                } else {
                                                    echo '<span class="status-metro status-disabled" title="' . $language_data->__('text_inactive') . '">' . $language_data->__('text_inactive') . '</span>';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-small btn-success" href="#viewTask<?php echo $partnerdata->task_id; ?>"  data-toggle="modal" title="{{ $language_data->__('text_view_recurring_task') }}">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>
                                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="viewTask<?php echo $partnerdata->task_id; ?>" class="modal fade">
                                                    <div class="modal-dialog" style="width: 900px;">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title"><?php echo $language_data->__('text_task_details').": ".$partnerdata->task_name; ?></h4>
                                                            </div>
                                                            <div style="clear:both"></div>
                                                            <div class="modal-body" style="overflow: auto;height: 450px;">
                                                                <table class="display table table-bordered table-striped" id="dynamic-table1" style="margin-top: 20px;">
                                                                    <thead>
                                                                        <tr class="info">
                                                                            <th style="text-align: center;">#</th>
                                                                            <th><?php echo $language_data->__('text_date_time'); ?></th>
                                                                            <th><?php echo $language_data->__('text_task_start_time'); ?></th>
                                                                            <th><?php echo $language_data->__('text_task_end_time'); ?></th>
                                                                            <th><?php echo $language_data->__('text_task_status'); ?></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php
                                                                    //$Taskmodel = new Task();
                                                                    $RecTaskDatas = Helper::getRecTaskByTaskId($partnerdata->task_id);
                                                                    if(!empty($RecTaskDatas)){
                                                                        $r = 0;
                                                                        foreach ($RecTaskDatas as $partnerdataR){
                                                                        $r++;    
                                                                    ?>
                                                                        <tr>
                                                                            <td><?=$r;?></td>
                                                                            <td><?=$partnerdataR->et_duedate;?></td>
                                                                            <td><?php
                                                                                if (strtotime($partnerdataR->task_starttime) != '0' && $partnerdataR->task_starttime != "0000-00-00 00:00:00") {
                                                                                    $task_starttime = date("Y-m-d H:i:s", strtotime($partnerdataR->task_starttime));
                                                                                    echo $language_data->convertedDates($task_starttime, "fulldatetime");
                                                                                } else {
                                                                                    print "-";
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                            <td><?php
                                                                                $newDate = date("Y-m-d H:i:s", strtotime($partnerdataR->task_endtime));
                                                                                if ($partnerdataR->emp_id > 0 && (strtotime($partnerdataR->task_endtime) != '0' && $partnerdataR->task_endtime != "0000-00-00 00:00:00")) {
                                                                                    echo $language_data->convertedDates($newDate, "fulldatetime");
                                                                                } else {
                                                                                    echo "-";
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php
                                                                                if (empty($AllEmptaskRejectedList) && $partnerdataR->emp_id == 0 && !empty($isEmpAssign)) {
                                                                                    print $language_data->__('text_rejected');
                                                                                } else if ($partnerdataR->started == 1) {
                                                                                    print $language_data->__('text_started');
                                                                                } elseif ($partnerdataR->status == 2) {
                                                                                    print $language_data->__('text_completed');
                                                                                } else if ($partnerdataR->status == 0) {
                                                                                    print $language_data->__('text_accept_pending');
                                                                                } else if ($partnerdataR->status == 1) {
                                                                                    print $language_data->__('text_pending');
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php  
                                                                        }
                                                                    }
                                                                    ?>
                                                                    </tbody>
                                                                </table>    
                                                            </div>
                                                            <div style="clear:both"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a class="btn btn-small btn-info" href="{{ url('partner/task/update/'.$partnerdata->task_id)}}" title="{{ $language_data->__('text_edit_task') }}">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>

                                                <a class="btn btn-small btn-danger" onclick="return confirm('{{ $language_data->__('text_delete_confirmation') }}')" href="{{ url('partner/task/destroy/'.$partnerdata->task_id)}}" title="{{ $language_data->__('text_delete_task') }}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                                <a class="btn btn-small btn-info" href="{{ url('partner/task/assign/'.$partnerdata->task_id)}}" title="{{ $language_data->__('text_assign') }}">
                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                </a>
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <div class="pagination pagination-centered"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php }else{ ?>
                                <div class="nottaskFound"><?= $language_data->__('text_task_not_found'); ?></div>
                                <?php } ?>
                            </div>
                            
                            
                        </div>
                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');


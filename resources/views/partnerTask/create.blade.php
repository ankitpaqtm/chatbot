<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_tasks_add_screen') }}
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">


                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->

            <div class="content-wrap">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_add_new_task') }} </h6>
                                </label>

                                <?php
                                $partnerTaskCount = $partner_data[0]->taskcount;
                                //$currentTaskCount = count($task_data);
                                $language = Session::get('language') != '' ? Session::get('language') : '';
                                if (isset($language) && $language != '' && $language == 'persian') {
                                    ?>
                                    <label class="col-sm-2 pull-right">
                                        <h6 style="color: #DA0F0F;font-weight: bold !important;float: none;left:0;">
                                            <?php echo $language_data->__('text_used'); ?> <?php echo $language_data->digits($currentTaskCount) ?> / <?php echo $language_data->digits($partnerTaskCount) ?>
                                        </h6>
                                    </label>    
                                    <?php
                                } else {
                                    ?>
                                    <label class="col-sm-2 pull-right">
                                        <h6 style="color: #DA0F0F;font-weight: bold !important;float: none;text-align: right;margin-right: 10px;">
                                            <?php echo $language_data->__('text_used'); ?> <?php echo $language_data->digits($currentTaskCount) ?> / <?php echo $language_data->digits($partnerTaskCount) ?>
                                        </h6>
                                    </label>    
                                    <?php
                                }
                                ?>

                            </div>

                            <div class="body-nest" id="element">
                                <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => $urlPrifix.'/task/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_select_client') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <div>
                                                <select name="clientid" class="form-control" id="clientid" >
                                                    <option value="">{{ $language_data->__('text_select_client') }}</option>
                                                    @foreach($clients_data as $client)
                                                    <option value="{{ $client->clientid }}">{{ $client->client_name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="clientid" id="clientid_error" generated="true" class="error">{{ $errors->first('clientid') }}</label>
                                                <div class="multilocation"></div>
                                                <label for="client_multi_location" id="client_multi_location_error" generated="true" class="error">{{ $errors->first('client_multi_location') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_select_category') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <div>
                                                <select name="categoryid" class="form-control" id="categoryid" >
                                                    <option value="">{{ $language_data->__('text_select_category') }}</option>
                                                    @foreach($category_data as $category)
                                                    <option value="{{ $category->Id }}">{{ $category->name }}</option>
                                                    @endforeach

                                                </select>
                                                <label for="categoryid" id="categoryid_error" generated="true" class="error">{{ $errors->first('categoryid') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_task_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="task_name" name="task_name" novalidate  autocomplete="off" class="form-control placeholder-no-fix alpha-only" placeholder="<?php echo $language_data->__('text_task_name'); ?>">
                                            <label for="task_name" id="task_name_error" generated="true" class="error">{{ $errors->first('task_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_type') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <div>
                                                <select name="task_type" class="form-control" id="task_type" >
                                                    <option value="">{{ $language_data->__('text_select') }}</option>
                                                    <option value="1"><?php echo $language_data->__('text_normal'); ?></option>                  
                                                    <option value="2"><?php echo $language_data->__('text_special'); ?></option> 
                                                </select>
                                                <label for="task_type" id="task_type_error" generated="true" class="error">{{ $errors->first('task_type') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 30px;">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_additional_instruction') }} </label>
                                        <div class="col-sm-6">
                                            <textarea name="additional_desc" id="additional_desc" class="form-control placeholder-no-fix" ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_date') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" id="jalali_english_calendar"  required   name="due_date" readonly=""  value=""  placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
                                            <label for="jalali_english_calendar" id="jalali_english_calendar_error" generated="true" class="error"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_time_24_hrs') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" id="starettime" name="starettime"  required autocomplete="off" class="form-control placeholder-no-fix allowNumber tastHoursLimit" placeholder="<?php echo $language_data->__('text_alert_enter_correct_hrs'); ?>">
                                            <label for="starettime" id="starettime_error" generated="true" class="error">{{ $errors->first('starettime') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_min') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" id="et_mins" name="et_mins" required  autocomplete="off" class="form-control placeholder-no-fix allowNumber tastMinLimit" placeholder="<?php echo $language_data->__('text_min_alert'); ?>">
                                            <label for="et_mins" id="et_mins_error" generated="true" class="error">{{ $errors->first('et_mins') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_duration_min') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" id="duration" name="duration" required  autocomplete="off" class="form-control placeholder-no-fix allowNumber tastDurLimit" placeholder="<?php echo $language_data->__('text_alert_enter_correct_duration'); ?>">
                                            <label for="duration" id="duration_error" generated="true" class="error">{{ $errors->first('text_alert_enter_correct_duration') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_is_recurring_task') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6 skin skin-flat">
                                            <ul class="list" style="padding: 0;">
                                                <li>
                                                    <input type="radio" value="1" id="square-radio-1" name="is_recurring_task_radio" />
                                                    <label for="square-radio-1">{{ $language_data->__('text_create_task_yes') }}</label>
                                                    <input type="radio" value="0" id="square-radio-2" name="is_recurring_task_radio" checked />
                                                    <label for="square-radio-2">{{ $language_data->__('text_create_task_no') }}</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <input type="hidden" name="is_recurring_task" id="is_recurring_task" value="0"/>
                                    </div>
                                    <div id="IsRecurringDiv" style="display: none;">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">{{ $language_data->__('text_recurring_type') }} <span class="required">*</span></label>
                                            <div class="col-sm-6">
                                                <div>
                                                    <select name="recurring_type" class="form-control" id="recurring_type" >
                                                        <option value="">{{ $language_data->__('text_select') }}</option>
                                                        <option value="1"><?php echo $language_data->__('text_recurring_type_daily'); ?></option>                  
                                                        <option value="2"><?php echo $language_data->__('text_recurring_type_weekly'); ?></option> 
                                                        <option value="3"><?php echo $language_data->__('text_recurring_type_monthly'); ?></option> 
                                                    </select>
                                                    <label for="recurring_type" id="recurring_type_error" generated="true" class="error">{{ $errors->first('recurring_type') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">{{ $language_data->__('text_recurring_end_date') }} <span class="required">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text" id="recurring_end_date" name="recurring_end_date" readonly=""  value=""  placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
                                                <label for="recurring_end_date" id="recurring_end_date_error" generated="true" class="error"></label>
                                            </div>
                                        </div>
                                    </div>    
                                    <button class="btn btn-info submitPartner" tabindex="7" name="add_supervisor" id="add_supervisor" type="submit">{{ $language_data->__('text_submit') }}</button>
                                    {!! Form::close() !!}
                                </div>

                            </div>

                        </div>


                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer')
@include('partnerTask.validation')
<script type="text/javascript">
    $('.skin-flat input').iCheck({
        radioClass: 'iradio_flat-blue',
        checkboxClass: 'icheckbox_flat-red'
    });

    $('.skin-flat input').on('ifChecked', function (event) {
        var val = $(this).val();
        $('#is_recurring_task').val(val);
        if (val === '1') {
            $('#IsRecurringDiv').show();
        } else {
            $('#IsRecurringDiv').hide();
        }
    });

    var currentTime = new Date();
    // First Date Of the month 
    var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
    $('#jalali_english_calendar').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        startDate: currentTime
    });
    $('#recurring_end_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        startDate: currentTime
    });
    /*
     TODO::Calender js code start
     */
    /*Calendar.setup({
        inputField: "jalali_english_calendar", // id of the input field
        button: "date_btn_9", // trigger for the calendar (button ID)
        ifFormat: "%Y-%m-%d", // format of the input field
        showsTime: false,
        langNumbers: true,
        weekNumbers: false
    });

    Calendar.setup({
        inputField: "recurring_end_date", // id of the input field
        button: "date_btn_9", // trigger for the calendar (button ID)
        ifFormat: "%Y-%m-%d", // format of the input field
        showsTime: false,
        langNumbers: true,
        weekNumbers: false
    });*/



    /*
     TODO::Calender js code end
     */
    $(document).on('change', '#clientid', function (e) {
           var client_id = $(this).val();
            $.ajax({
                url: "<?= url("getclientmultilocation") ?>",
                type: "POST",
                data: "_token=<?= csrf_token() ?>&client_id="+client_id,
                success: function (msg) {
                    $(".multilocation").html(msg);
                }
            });

        });
    
</script>




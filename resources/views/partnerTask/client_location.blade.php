@php
$language_data=new Language();
@endphp
<?php if(!empty($clientLocations)){ ?>
<label class="pull-right">{{ $language_data->__('text_client_location_map_point') }}&nbsp;&nbsp;<span class="icon icon-information btn2" style="color:red;"  data-toggle="tooltip" data-placement="top" title="{{ $language_data->__('text_client_location_map_point_info') }}"></span></label>
<?php } ?>
<?php
$i = 1;

foreach ($clientLocations as $location) {

    $checked = "";

    if (!empty($taskLocationdata["location_ids"])) {
        if (in_array($location->id, $taskLocationdata["location_ids"])) {
            $checked = "checked=true";
        }
    }
    ?>
    <div class="col-sm-12 addressSection-{{ $i }}">
        <div class="skin skin-flat1">
            <ul class="" style="padding: 0;list-style: none;">
                <li>
                    <div class="col-sm-1">
                        <input class="multiLocationSectionm"  type="checkbox" <?= $checked; ?> name="client_multi_location[]" value="{{ $location->id }}" id="flat-checkbox-{{ $location->id }}">&nbsp;&nbsp;
                    </div>
                    <div class="col-sm-8">
                        <label for="flat-checkbox-{{ $location->id }}"><span style="color:#2489c5;font-size:15px;" class="icon-location">&nbsp;<span id="location-lable-{{ $location->id }}">{{ $location->location }}</span></span></label>
                    </div>
                    <div class="col-sm-3">
                        <?php if ($checked != "") {
                            ?>
                            <input type="text" id="sort-{{ $location->id }}" class="form-control" placeholder="" name="sort_location[]" value="{{ get_task_location_detail($task_id, $location->id,"sort_location") }}">
                        <?php } else { ?>
                            <input type="text" id="sort-{{ $location->id }}" disabled="disabled" class="form-control" placeholder="" name="sort_location[]" value="">
                        <?php } ?>
                    </div>
                </li>
            </ul>

        </div>
    </div>
    <?php
    $i++;
}
?>

<script type="text/javascript">
    $('.skin-flat1 input').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
    });

    $('.btn2').tooltip();

    $('.skin-flat1 input').on('ifChecked', function (event) {

        var locId = $(this).val();

        if ($(this).is(":checked")) {
            $("#sort-" + locId).removeAttr("disabled");
        }

    });
    $('.skin-flat1 input').on('ifUnchecked', function (event) {

        var locId = $(this).val();


        $("#sort-" + locId).attr("disabled", true);


    });


</script>
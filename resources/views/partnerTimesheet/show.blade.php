<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>

<style>
    #lightbox .modal-content {
    display: inline-block;
    text-align: center;   
}

#lightbox .close {
    opacity: 1;
    color: rgb(255, 255, 255);
    background-color: rgb(25, 25, 25);
    padding: 5px 10px;
    border-radius: 84px;
    border: 2px solid rgb(255, 255, 255);
    position: absolute;
    top: -15px;
    right: -78px;
    
    z-index:1032;
}
</style>

    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_time_sheet') }}</span>
                        </h2>
                    </div>

                    <div class="col-sm-7"></div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->

            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_time_sheet') }} </h6>
                                </label>
                            </div>
                            <div class="body-nest" id="Filtering">
                                @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                                @endif
                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-sm-4">
                                        <?php if (!empty($task_data)) { ?>

                                            <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>

                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-2"></div>

                                </div>
                            </div>

                            <div class="body-nest">
                                <?php if (!empty($task_data)) { ?>

                                    <table class=" footable-res ">
                                        <thead>
                                            <tr>

                                                <th>
                                                    {{ $language_data->__('text_employee_name') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_task') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_date_time') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_client') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_duration_min') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_total_time') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_route_map') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_task_status') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>{{ $task_data->emp_name }}</td>
                                                <td>
                                                    <a  class="hclass" href="#taskDetail"  data-toggle="modal" >{{ $task_data->task_name }}</a>
                                                </td>


                                                <td>
                                                    <?php
                                                    if ($task_data->et_duedate != '0000-00-00 00:00:00') {

                                                        //echo $newDate = $language_data->digits(date("d-m-Y H:i:s", strtotime($data['et_duedate'])));
                                                        echo $language_data->convertedDates($task_data->et_duedate, "fulldatetime");
                                                    } else {
                                                        print "-";
                                                    }
                                                    ?>
                                                </td>
                                                <td>{{ $task_data->client_name }}</td>
                                                <td>{{ $task_data->duration }}</td>
                                                <td>
                                                    <?php
                                                    if ($task_data->status == 2) {

                                                        if ($task_data->emp_id > 0 && (strtotime($task_data->task_starttime) != '0' && $task_data->task_starttime != "0000-00-00 00:00:00") && (strtotime($task_data->task_endtime) != '0' && $task_data->task_endtime != "0000-00-00 00:00:00")) {

                                                            $datetime1 = strtotime($task_data->task_starttime);
                                                            $datetime2 = strtotime($task_data->task_endtime);
                                                            $interval = abs($datetime2 - $datetime1);
                                                            $minutes = round($interval / 60);

                                                            if ($language_data->digits($task_data->duration) < $language_data->digits($minutes)) {
                                                                echo $language_data->digits($minutes);
                                                            } else {
                                                                echo $language_data->digits($minutes);
                                                            }
                                                        }
                                                    } else {
                                                        print "-";
                                                    }
                                                    ?>

                                                </td>
                                                <td align="center" style="font-size:25px;"><a href="javascript:;" class="text-center"><i class="fa fa-map-marker"></i></a></td>
                                                <td>
                                                    <?php
                                                    if ($task_data->started == 1) {


                                                        print $language_data->__('text_started');
                                                    } else if ($task_data->status == 2) {
                                                        print $language_data->__('text_completed');
                                                    } else if ($task_data->status == 0) {
                                                        print $language_data->__('text_accept_pending');
                                                    } else if ($task_data->status == 1) {
                                                        print $language_data->__('text_pending');
                                                    }
                                                    ?>
                                                </td>



                                            </tr>


                                        </tbody>

                                    </table>
                                <?php } else { ?>
                                    <div class="nottaskFound"><?= $language_data->__('text_no_records'); ?></div>
                                <?php } ?>

                            </div>
                            <div class="body-nest col-sm-12">
                                <div id="dvMap" style="width:100%;height:500px;"></div>
                                <br>
                                <div class="text-center">
                                    <span style="padding:5px 15px 5px 15px;background:blue;">&nbsp;</span> <span style="font-weight:bold;"><?php echo $language_data->__('text_actual_route'); ?></span>
                                    <span style="margin:2px;"></span>
                                    <span style="padding:5px 15px 5px 15px;background:green;">&nbsp;</span> <span style="font-weight:bold;"><?php echo $language_data->__('text_route_taken'); ?></span>
                                </div>
                            </div>

                            <!--map view .start-->
                            <?php
                            $fetchMode = DB::getFetchMode();
                            DB::setFetchMode(\PDO::FETCH_ASSOC);

                            $id = $task_data->task_id;

                            $taskdetails = collect(\DB::select("SELECT clientid,latvaule,longvalue FROM  `tbl_emp_task`  where task_id='$id'"))->first();

                            $clientid = $taskdetails['clientid'];

                            $latvalue = $taskdetails['latvaule'];
                            $langvalue = $taskdetails['longvalue'];
                            
                            
                            $taskClientLocations = DB::select("SELECT location_id FROM `tbl_task_client_location` WHERE client_id='$clientid' AND task_id='$id' order by sort_location asc");
                            
                            foreach ($taskClientLocations as $key=>$val){
                                
                                $clientcureentlocation = collect(\DB::select("SELECT latitude,longitude,location FROM `tbl_clients_location` WHERE id='".$val["location_id"]."' AND client_id='$clientid'"))->first();
                                $locations[] = $clientcureentlocation;
                                
                            }
                            $clatvalue = "";
                            $clangvalue = "";
                            if(!empty($locations)){
                            $destination = end($locations);
                            $clatvalue = $destination['latitude'];
                            $clangvalue = $destination['longitude'];
                            }
//                            $clientcureentlocation = collect(\DB::select("SELECT latitude,longtitude,address FROM `tbl_clients` WHERE clientid='$clientid' order by id desc"))->first();
//
//                            $clatvalue = $clientcureentlocation['latitude'];
//                            $clangvalue = $clientcureentlocation['longtitude'];

                            $tasknewdtails = DB::select("SELECT lang,lat,address,route_id,added_date FROM `taskrealroute` where task_id='$id' ORDER BY route_id   ASC  ");
                            ?>
                            <script type="text/javascript">

                                $(document).ready(function () {
                                    loadMap();
                                });

                                var directionsDisplay;
                                var directionsService = new google.maps.DirectionsService();
                                var map;
                                function loadMap() {
                                    var myOptions = {
                                        zoom: 12,
                                        center: new google.maps.LatLng(18.9750, 72.8258),
                                        //            maxZoom: 13,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    map = new google.maps.Map(document.getElementById('dvMap'), myOptions);
                                }

                                google.maps.event.addDomListener(window, 'load', function () {
//                                    new google.maps.places.SearchBox(document.getElementById('txtSource'));
//                                    new google.maps.places.SearchBox(document.getElementById('txtDestination'));
                                    GetRoute();
                                    // directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
                                });

                                function GetRoute() {

                                    // directionsDisplay.setMap(map);
                                    // directionsDisplay.setPanel(document.getElementById('dvPanel'));

                                    //*********DIRECTIONS AND ROUTE**********************//
                                    // var source = '<?php //echo getaddress($latvalue, $langvalue);         ?>';
                                    // var destination = '<?php ///echo getaddress($clatvalue, $clangvalue);         ?>';

                                    var requestArray = [];
                                    var requestArray1 = [];
                                    var renderArray = [];
                                    // var origins = [];
                                    // var destinations = [];
                                    var colors = {
                                        actual: 'blue',
                                        taken: 'green'
                                    }
                                    // if(source && destination) {  
                                    var source = new google.maps.LatLng('<?php echo $latvalue; ?>', '<?php echo $langvalue; ?>');
                                    var destination = new google.maps.LatLng('<?php echo $clatvalue; ?>', '<?php echo $clangvalue; ?>');




                                    requestArray.push({
                                        routeType: 'actual',
                                        request: {
                                            origin: source,
                                            destination: destination,
                                            waypoints: [
                                                {location: source, stopover: false},
                                                {location: destination, stopover: false}
                                            ],
                                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                                        }
                                    });

                                   
                                    var markers = [
                                    ['<b>Task Location :</b><br/> <?php echo getAddressNew($latvalue, $langvalue); ?>', '<?php echo $latvalue; ?>', '<?php echo $langvalue; ?>',0],
                                    <?php if(!empty($locations)){ $i=1; foreach ($locations as $v){ ?>
                                    ["<b>Client Location :</b> <br/> <?php echo trim(preg_replace('/\s\s+/', ' ',$v["location"])); ?>", '<?php echo $v["latitude"]; ?>', '<?php echo $v["longitude"]; ?>', '<?php echo $i; ?>'],
                                    <?php $i++; } }?>    
                                    ];

                                        var infowindow1 = new google.maps.InfoWindow();

                                        var lat_lng = new Array();
                                        var latlngbounds1 = new google.maps.LatLngBounds();

                                        var color1 = "blue";
                                        var marker, i1;
                                        for (i1 = 0; i1 < markers.length; i1++) {
                                            //            var data = markers1[i1]
                                            var myLatlng = new google.maps.LatLng(markers[i1][1], markers[i1][2]);
                                            lat_lng.push(myLatlng);

                                            var temp = i1 + 1;

                                            if (i1 == 0) {
                                                marker = new google.maps.Marker({
                                                    position: myLatlng,
                                                    map: map,
                                                    icon: "{{ url('resources/assets/momom-assets/img/') }}/" + color1 + "_MarkerA.png"
                                                });
                                            } else if (temp == markers.length) {
                                                marker = new google.maps.Marker({
                                                    position: myLatlng,
                                                    map: map,
                                                     icon: "{{ url('resources/assets/momom-assets/img/') }}/" + color1 + "_MarkerB.png"
                                                });
                                            } else {
                                                marker = new google.maps.Marker({
                                                    position: myLatlng,
                                                    map: map,
                                                    icon: "{{ url('resources/assets/momom-assets/img/map-icon/') }}/number_" + i1 + ".png"
                                                });
                                            }


                                            latlngbounds1.extend(marker.position);

                                            google.maps.event.addListener(marker, 'click', (function (marker, i1) {
                                                return function () {
                                                    infowindow1.setContent(markers[i1][0]);
                                                    infowindow1.open(map, marker);
                                                }
                                            })(marker, i1));
                                        }
                                        map.setCenter(latlngbounds1.getCenter());
                                        map.fitBounds(latlngbounds1);

                                        var directionsDisplay1 = new google.maps.DirectionsRenderer;

                                        //***********ROUTING****************//

                                        //Initialize the Path Array
                                        var path = new google.maps.MVCArray();

                                        //Initialize the Direction Service
                                        //var service = new google.maps.DirectionsService();

                                        //Set the Path Stroke Color
                                        var poly = new google.maps.Polyline({map: map, strokeColor: color1});

                                        //Loop and Draw Path Route between the Points on MAP
                                        for (var i = 0; i < lat_lng.length; i++) {
                                            //            if ((i + 1) < lat_lng.length) {
                                            var src = lat_lng[i];
                                            var des = lat_lng[i + 1];
                                            path.push(src);
                                            poly.setPath(path);
                                            directionsService.route({
                                                origin: src,
                                                destination: des,
                                                //                    waypoints: [
                                                //                        {location: src,stopover: true},
                                                //                        {location: des,stopover: true}
                                                //                    ],
                                                //                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                                                travelMode: 'DRIVING'
                                            }, function (result, status) {
                                                if (status == google.maps.DirectionsStatus.OK) {
                                                    //                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                                    //                            path.push(result.routes[0].overview_path[i]);
                                                    //                        }
                                                    directionsDisplay1.setDirections(result);
                                                }
                                            });
                                        }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
//                                    $.each(requestArray, function (i, arr) {
//                                        
//                                        directionsService.route(arr.request, function (response, status) {
//                                            if (status == google.maps.DirectionsStatus.OK) {
//                                                var color = colors[arr.routeType];
//                                                renderArray[i] = new google.maps.DirectionsRenderer();
//                                                renderArray[i].setMap(map);
//                                                renderArray[i].setOptions({
//                                                    preserveViewport: true,
//                                                    suppressInfoWindows: true,
//                                                    polylineOptions: {
//                                                        strokeWeight: 4,
//                                                        strokeOpacity: 1,
//                                                        strokeColor: color
//                                                    },
//                                                    suppressMarkers: true
//                                                });
//                                                renderArray[i].setDirections(response);
//                                                var aSteps = response.routes[0].legs[0].steps;
//                                                $.each(markers, function (i, sChr) {
//                                                    
//                                                    var position = i == 0 ? aSteps[i].start_point : aSteps[aSteps.length - 1].end_point;
//                                                    var marker = new google.maps.Marker({
//                                                        position: position,
//                                                        map: map,
////                                                        icon: "{{ url('resources/assets/momom-assets/img/') }}/" + color + "_Marker" + sChr[1] + ".png"
//                                                        icon: "{{ url('resources/assets/momom-assets/img/map-icon/') }}/number_" + i + ".png"
//                                                    });
//                                                    
//                                                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
//                                                        return function () {
//                                                            infowindow.setContent(sChr[i][0]);
//                                                            infowindow.open(map, marker);
//                                                        }
//                                                    })(marker, i));
//
//                                                });
//
//                                                var bnd = response.routes[0].bounds;
//                                                if (i == 0)
//                                                    bounds = bnd;
//                                                else
//                                                    bounds.union(bnd);
//                                                map.fitBounds(bounds);
//                                            }
//                                        });
//                                    });


<?php if (!empty($tasknewdtails)) { 
    
    ?>

                                        var markers1 = [
    <?php
    $i = 1;
    foreach ($tasknewdtails as $first) {
        if ($first["lat"] != "" && $first["lang"] != "") {
            ?>
                                                    ['<?php echo $first["address"] . "<br>" . "<center>(" . $i . ") - " . $first["added_date"] . "</center>"; ?>', '<?php echo $first["lat"]; ?>', '<?php echo $first["lang"]; ?>', '<?php echo $i; ?>'],
            <?php
            $i++;
        }
    }
    ?>
                                        ];

                                        var infowindow = new google.maps.InfoWindow();

                                        var lat_lng = new Array();
                                        var latlngbounds = new google.maps.LatLngBounds();

                                        var color1 = "green";
                                        var marker, i1;
                                        for (i1 = 0; i1 < markers1.length; i1++) {
                                            //            var data = markers1[i1]
                                            var myLatlng = new google.maps.LatLng(markers1[i1][1], markers1[i1][2]);
                                            lat_lng.push(myLatlng);

                                            var temp = i1 + 1;

                                            if (i1 == 0) {
                                                marker = new google.maps.Marker({
                                                    position: myLatlng,
                                                    map: map,
                                                    icon: "{{ url('resources/assets/momom-assets/img/') }}/" + color1 + "_MarkerA.png"
                                                });
                                            } else if (temp == markers1.length) {
                                                marker = new google.maps.Marker({
                                                    position: myLatlng,
                                                    map: map,
                                                    icon: "{{ url('resources/assets/momom-assets/img/') }}/" + color1 + "_MarkerB.png"
                                                });
                                            } else {
                                                marker = new google.maps.Marker({
                                                    position: myLatlng,
                                                    map: map,
                                                    //                        icon: "images/" + color1 + "_Marker" + markers1[i1][3] + ".png"
                                                });
                                            }


                                            latlngbounds.extend(marker.position);

                                            google.maps.event.addListener(marker, 'click', (function (marker, i1) {
                                                return function () {
                                                    infowindow.setContent(markers1[i1][0]);
                                                    infowindow.open(map, marker);
                                                }
                                            })(marker, i1));
                                        }
                                        map.setCenter(latlngbounds.getCenter());
                                        map.fitBounds(latlngbounds);

                                        var directionsDisplay = new google.maps.DirectionsRenderer;

                                        //***********ROUTING****************//

                                        //Initialize the Path Array
                                        var path = new google.maps.MVCArray();

                                        //Initialize the Direction Service
                                        //var service = new google.maps.DirectionsService();

                                        //Set the Path Stroke Color
                                        var poly = new google.maps.Polyline({map: map, strokeColor: color1});

                                        //Loop and Draw Path Route between the Points on MAP
                                        for (var i = 0; i < lat_lng.length; i++) {
                                            //            if ((i + 1) < lat_lng.length) {
                                            var src = lat_lng[i];
                                            var des = lat_lng[i + 1];
                                            path.push(src);
                                            poly.setPath(path);
                                            directionsService.route({
                                                origin: src,
                                                destination: des,
                                                //                    waypoints: [
                                                //                        {location: src,stopover: true},
                                                //                        {location: des,stopover: true}
                                                //                    ],
                                                //                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                                                travelMode: 'DRIVING'
                                            }, function (result, status) {
                                                if (status == google.maps.DirectionsStatus.OK) {
                                                    //                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                                    //                            path.push(result.routes[0].overview_path[i]);
                                                    //                        }
                                                    directionsDisplay.setDirections(result);
                                                }
                                            });
                                        }

<?php } ?>
                                }
                            </script>


                            <!--map view .end-->                            



                        </div>


                    </div>

                </div>
            </div>



        </div>
    </div>

    <?php if (isset($task_data->et_id) && $task_data->et_id > 0) { ?>
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="taskDetail" class="modal fade">

            <div class="modal-dialog" style="width: 900px;">
                <?php
                $id = $task_data->et_id;

                $task_details = DB::select("select t.task_id, t.description,et.rating,et.clientsign,t.additional_instruction,et.screenshot, t.task_name,et.started, et.task_starttime, et.task_endtime, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, et.report, et.feedback, et.status, c.client_name, c.address, c.contract, c.contactonsite, e.emp_name from tbl_emp_task et, tbl_task t, tbl_clients c, tbl_employee e where et.et_id= $id and et.task_id = t.task_id and et.clientid= c.clientid and e.emp_id = et.emp_id");
                $start_date = $task_details[0]['task_starttime'];
                $end_date = $task_details[0]['task_endtime'];
                ?>

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?php echo $language_data->__('text_task_details'); ?></h4>
                    </div>
                    <div style="clear:both"></div>
                    <div class="modal-body" style="overflow: auto;height: 450px;">
                        <!--                                                                        <div class="col-sm-12" >
                                                                                                    <button type="button" style="float: right;" class="btn btn-success" onclick="printDiv('printpage-<?php //echo $data['et_id'];  ?>')"><?php //echo $language_data->__('text_print');  ?></button>
                                                                                                </div>-->
                        <div  style="clear:both"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_task_name'); ?></strong>
                        </div>
                        <div class="col-md-5">


                            <?php print $task_details[0]['task_name']; ?>
                        </div>
                        <div  style="clear:both"></div>
                        <div style="margin-bottom:30px"></div>
                        <div  style="clear:both"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_additional_instruction'); ?></strong>
                        </div>
                        <div class="col-md-5">
                            <?php print $task_details[0]['additional_instruction']; ?>
                        </div>


                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>





                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_employee_name'); ?></strong>
                        </div>
                        <div class="col-md-5">
                            <?php print $task_details[0]['emp_name']; ?>
                        </div>


                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_client_name'); ?></strong>
                        </div>
                        <div class="col-md-5">
                            <?php print $task_details[0]['client_name']; ?>
                        </div>


                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_phone_number'); ?></strong>
                        </div>
                        <div class="col-md-5">
                            <?php print $language_data->digits($task_details[0]['contactonsite']); ?>
                        </div>


                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_client_address'); ?></strong>
                        </div>
                        <div class="col-md-5">
                            <?php print $task_details[0]['address']; ?>
                        </div>


                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_contact_person'); ?></strong>
                        </div>
                        <div class="col-md-5">
                            <?php print $task_details[0]['contract']; ?>
                        </div>


                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>
                        <?php if ($task_details[0]['clientsign'] !== "") { ?>
                            <div class="col-sm-4">
                                <strong><?php echo $language_data->__('text_client_sign'); ?></strong>
                            </div>
                            <div class="col-md-5">
                                <?php if (!empty($task_details[0]['clientsign'])) { ?>
                                    <img width="80" src="<?= url('public/uploads/taskimages/' . $task_details[0]['clientsign']) ?>">

                                <?php } ?>
                            </div>

                            <div  style="clear:both"></div>


                            <div style="margin-bottom:30px"></div>

                        <?php } ?>

                        <?php if ($task_details[0]['rating'] !== "0.0") { ?>
                            <div class="col-sm-4">
                                <strong><?php echo $language_data->__('text_rating'); ?></strong>
                            </div>
                            <div class="col-md-5">
                                <?php print $language_data->digits($task_details[0]['rating']); ?>
                            </div>

                            <div  style="clear:both"></div>


                            <div style="margin-bottom:30px"></div>
                        <?php } ?>
                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_task_start_time'); ?></strong>
                        </div>
                        <div class="col-sm-5">
                            <?php
                            if ($task_details[0]['task_starttime'] != "0000-00-00 00:00:00") {

                                echo $language_data->convertedDates($start_date, "fulldatetime");
                            } else {
                                print "-";
                            }
                            ?>
                        </div>
                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_task_end_time'); ?></strong>
                        </div>
                        <div class="col-sm-5">
                            <?php
                            if ($task_details[0]['task_endtime'] != "0000-00-00 00:00:00") {
                                echo $language_data->convertedDates($end_date, "fulldatetime");
                            } else {
                                print "-";
                            }
                            ?>
                        </div>
                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">
                            <strong><?php echo $language_data->__('text_travel_time'); ?></strong>
                        </div>
                        <div class="col-sm-5">
                            <?php
                            if ($task_details[0]['status'] == 2) {
                                print $language_data->digits($task_details[0]['work_hours']) . " " . $language_data->__('text_hours');
                            } else {
                                print "-";
                            }
                            ?>
                        </div>
                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>

                        <div class="col-sm-4">  
                            <strong><?php echo $language_data->__('text_task_images'); ?></strong>
                        </div>
                        <div class="col-sm-5">
                            <?php
                            $imglist = $task_details[0]['screenshot'];
                            $showimg = explode(',', $imglist);
                            $ttcount = count($showimg);

                            if (!empty($imglist)) {
                                for ($i = 0; $i < $ttcount; $i++) {
                                    ?>
                                    <div class="col-lg-6 col-md-6 col-xs-6 thumb">
                                        <a class="thumbnail surveAnchorImg" href="javascript:;" data-toggle="modal" data-target="#lightbox">
                                            <img class="img-responsive" src="<?= url('public/uploads/taskimages/' . $showimg[$i]) ?>" alt="<?php echo $showimg[$i]; ?>">
                                        </a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div  style="clear:both"></div>


                        <div style="margin-bottom:30px"></div>
                        <?php if ($task_details[0]['report'] !== "") { ?>
                            <div class="col-sm-4">  
                                <strong><?php echo $language_data->__('text_task_report'); ?></strong>
                            </div>
                            <div class="col-sm-5">
                                <?php
                                if (!empty($task_details[0]['report'])) {
                                    print $task_details[0]['report'];
                                } else {
                                    print "-";
                                }
                                ?>
                            </div>
                            <div  style="clear:both"></div>


                            <div style="margin-bottom:30px"></div>
                            <?php
                        }

                        if ($task_details[0]['feedback'] !== "") {
                            ?>

                            <div class="col-sm-4">  
                                <strong><?php echo $language_data->__('text_customer_feedback'); ?></strong>
                            </div>
                            <div class="col-sm-5">
                                <?php
                                if (!empty($task_details[0]['feedback'])) {
                                    print $task_details[0]['feedback'];
                                } else {
                                    print "-";
                                }
                                ?>
                            </div>
                            <div  style="clear:both"></div>


                            <div style="margin-bottom:30px"></div>
                        <?php } ?>
                        <div class="col-sm-4">  
                            <strong><?php echo $language_data->__('text_status'); ?></strong>
                        </div>
                        <div class="col-sm-5">
                            <?php
                            if ($task_details[0]['started'] == 1) {
                                print $language_data->__('text_started');
                            } elseif ($task_details[0]['status'] == 2) {
                                print $language_data->__('text_completed');
                            } else if ($task_details[0]['status'] == 0) {

                                print $language_data->__('text_pending');
                            }
                            ?>
                        </div>

                    </div>
                    <div style="clear:both"></div>
                </div>

            </div>
            <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>
<?php DB::setFetchMode($fetchMode); ?>
@include('layouts.footer');
<script type="text/javascript">
$(document).ready(function() {
    var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});
    
</script>
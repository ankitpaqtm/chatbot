<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>



    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_time_sheet') }}</span>
                        </h2>
                    </div>

                    <div class="col-sm-7"></div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_time_sheet') }} </h6>
                                </label>
                            </div>
                            <div class="body-nest" id="Filtering">
                                @if(Session::has('msg'))
                                <div class="alert {!!Session::get('alert')!!}">
                                    <a class="close" data-dismiss="alert">×</a>
                                    <strong>{!!Session::get('msg')!!}</strong> 
                                </div>
                                @endif
                                <div class="row" style="margin-bottom:10px;">
                                    
                                    
                                       {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => $urlPrifix.'/timesheet']) !!} 
                                       <div class="col-sm-3">
                                            <select class='form-control pull-left' name='emp_id'>
                                                <option value="">{{ $language_data->__('text_select_team') }}</option>
                                                {{ employeeDropdown() }}
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <input class="form-control pull-left" id="txtDateFrom" value="" name="txtDateFrom" placeholder="{{ $language_data->__('text_date_from') }}" type="text"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <input class="form-control" name="txtDateTo" id="txtDateTo" value="" placeholder="{{ $language_data->__('text_date_to') }}" type="text"/>
                                        </div>
                                       <div class="col-sm-3">
                                       <button class="btn btn-info"  name="filter" id="filter" type="submit">{{ $language_data->__('text_filter') }}</button>
                                       </div>
                                       {!! Form::close() !!} 
                                    

                                </div>
                            </div>

                            <div class="body-nest" id="Footable">
                                <?php if (!empty($task_data) && isset($task_data[0]->task_id) && $task_data[0]->task_id > 0) { ?>

                                    <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter" data-filter-text-only="true">
                                        <thead>
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_employee_name') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_task') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_punch_in') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_punch_out') }}
                                                </th>
                                                <th>
                                                    {{ $language_data->__('text_duration_min') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
    <?php $i = 0; ?>

                                            @foreach($task_data as $partnerdata)
                                            <tr>
                                                <td><?php echo ++$i; ?></td>
                                                <td>{{ $partnerdata->emp_name }}</td>
                                                <td><a href="{{ url($urlPrifix.'/timesheet/show/'.$partnerdata->task_id) }}"> {{ $partnerdata->task_name }}</a></td>
                                                <td><?php
    if ($partnerdata->emp_id > 0 && strtotime($partnerdata->task_starttime) != '0' && $partnerdata->task_starttime != "0000-00-00 00:00:00") {

        $task_starttime = date("Y-m-d H:i:s", strtotime($partnerdata->task_starttime));

        echo $language_data->convertedDates($task_starttime, "fulldatetime");
    } else {
        print "-";
    }
    ?>
                                                </td>
                                                <td><?php
                                                $newDate = date("Y-m-d H:i:s", strtotime($partnerdata->task_endtime));

                                                if ($partnerdata->emp_id > 0 && (strtotime($partnerdata->task_endtime) != '0' && $partnerdata->task_endtime != "0000-00-00 00:00:00")) {
                                                    echo $language_data->convertedDates($newDate, "fulldatetime");
                                                } else {
                                                    echo "-";
                                                }
    ?>
                                                </td>
                                                <td>
    <?php
    if ($partnerdata->status == 2) {

        if ($partnerdata->emp_id > 0 && (strtotime($partnerdata->task_starttime) != '0' && $partnerdata->task_starttime != "0000-00-00 00:00:00") && (strtotime($partnerdata->task_endtime) != '0' && $partnerdata->task_endtime != "0000-00-00 00:00:00")) {

            $datetime1 = strtotime($partnerdata->task_starttime);
            $datetime2 = strtotime($partnerdata->task_endtime);
            $interval = abs($datetime2 - $datetime1);
            $minutes = round($interval / 60);

            if ($language_data->digits($partnerdata->duration) < $language_data->digits($minutes)) {
                echo "<span style='color:red;'>" . $language_data->digits($minutes) . "</span>";
            } else {
                echo "<span style='color:green;'>" . $language_data->digits($minutes) . "</span>";
            }
        }
    } else {
        print "-";
    }
    ?>

                                                </td>


                                            </tr>
                                            @endforeach

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="10">
                                                    <div class="pagination pagination-centered"></div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
<?php } else { ?>
                                    <div class="nottaskFound"><?= $language_data->__('text_no_records'); ?></div>
                                <?php } ?>
                            </div>

                        </div>


                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>

<script type="text/javascript">
var language=$("#language").val();
    if(language=='english')
    {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
         Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
    }
    else{
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
        
        
    }
</script>

@include('layouts.footer');


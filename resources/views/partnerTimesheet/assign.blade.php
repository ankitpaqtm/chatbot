<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>



    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_task') }}</span>
                        </h2>
                    </div>

                    <div class="col-sm-7"></div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_list_of_employees') }} </h6>
                                </label>

                            </div>

                            <?php
                            $isTastAssigned = DB::table('tbl_emp_task')
                                    ->select("*")
                                    ->where('task_id', $task_data[0]->task_id)
                                    ->get()
                                    ->toArray();

                            if ($isTastAssigned[0]->emp_id == 0) {
                                $company_id = $isTastAssigned[0]->comp_id;
                                $clientId = $isTastAssigned[0]->clientid;
                                $et_mins = $isTastAssigned[0]->et_mins;
                                $hours = $isTastAssigned[0]->et_hours;
                                $duration = $task_data[0]->duration;
                                $taskId = $task_data[0]->task_id;

                                $startDate = $isTastAssigned[0]->et_duedate;
                                ?>

                                <div class="body-nest" id="Filtering">
                                    @if(Session::has('msg'))
                                    <div class="alert {!!Session::get('alert')!!}">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <strong>{!!Session::get('msg')!!}</strong> 
                                    </div>
                                    @endif
                                    @if(Session::has('validatorError'))
                                    <div class="alert {!!Session::get('alert')!!}">
                                        <a class="close" data-dismiss="alert">×</a>
                                        <strong>{{ $errors->first('multiassignEmpId') }}</strong> 
                                    </div>
                                    @endif

                                </div>

                                <div class="body-nest" id="Footable">

                                    <div id="wizard-tab">
                                        <h2>{{ $language_data->__('text_list_view') }}</h2>
                                        <section>

                                            <div class="panel-body">
                                                <div class="row" style="margin-bottom:10px;">
                                                    <div class="col-sm-4">
                                                        <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                                    </div>
                                                    <div class="col-sm-2"></div>

                                                </div>
                                                {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => $urlPrifix.'/task/store_assign']) !!}
                                                <table class="table-striped footable-res footable metro-blue" data-page-size="6" data-filter="#filter" data-filter-text-only="true">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                #
                                                            </th>
                                                            <th>
                                                                {{ $language_data->__('text_name') }}
                                                            </th>
                                                            <th>
                                                                {{ $language_data->__('text_current_location') }}
                                                            </th>
                                                            <th>
                                                                {{ $language_data->__('text_designation') }}
                                                            </th>
                                                            <th>
                                                                {{ $language_data->__('text_kilometer') }}
                                                            </th>
                                                            <th>
                                                                {{ $language_data->__('text_status') }}
                                                            </th>
                                                            <th>
                                                                {{ $language_data->__('text_assign') }}
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 0; ?>

                                                        @foreach($employees_data as $partnerdata)

                                                        <?php
                                                        $emp_id = $partnerdata->emp_id;
                                                        //+++++++++++++++++ Employee.status +++++++++++++++++++++++
                                                        $isBusy = getempStatus($emp_id, $taskId, $startDate);
                                                        //++++++++++++++++++++Employee.status END+++++++++++++++++++++++++++++
                                                        ?>


                                                        <tr>
                                                            <td><?php echo ++$i; ?></td>
                                                            <td>{{ $partnerdata->emp_name }}</td>
                                                            <td>
                                                                <?php
                                                                $latvalue = $partnerdata->lat;
                                                                $langvalue = $partnerdata->lang;
                                                                if (!empty($empcurrentlocation_data)) {
                                                                    $latvalue = $empcurrentlocation_data[0]->lan;
                                                                    $langvalue = $empcurrentlocation_data[0]->long;
                                                                }
                                                                echo getAddressNew($latvalue, $langvalue);
                                                                ?>
                                                            </td>
                                                            <td><?= $partnerdata->emp_post; ?></td>
                                                            <td>
                                                                <?php
                                                                if ($partnerdata->distance) {
                                                                    echo $partnerdata->distance . " km";
                                                                } else {
                                                                    echo "-";
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($partnerdata->availability == 2) {
                                                                    ?>
                                                                    <a class="btn btn-danger"> <?= $language_data->__('text_leave'); ?></a>
                                                                <?php } else if ($isBusy == 0) { ?>
                                                                    <a class="btn btn-success" ><?= $language_data->__('text_available'); ?></a>
                                                                <?php } else if ($isBusy == 1) { ?>
                                                                    <a class="btn btn-warning"><?= $language_data->__('text_on_duty'); ?></a>

                                                                <?php } ?>
                                                            </td>
                                                            <td>

                                                                <?php
                                                                if ($isBusy == 0) {
                                                                    $isExist = DB::table('tbl_assign_task_emp')
                                                                            ->select("id")
                                                                            ->where('emp_id', $emp_id)
                                                                            ->where('task_id', $taskId)
                                                                            ->first();

                                                                    $checked = "";
                                                                    if (!empty($isExist)) {
                                                                        $checked = "checked=true";
                                                                    }
                                                                    ?>

                                                                    <input type="checkbox" name="multiassignEmpId[]" <?php echo $checked; ?> id="multiassignEmpId" class="multiassignEmpId" value="<?php echo $emp_id; ?>"> 
                                                                    <input type="hidden" name="task_id"  value="<?= $taskId; ?>">

                                                                <?php } else {
                                                                    ?>
                                                                    <b style="color: #ff6c60;font-weight: bold;"><?= $language_data->__('text_busy') ?></b>
                                                                <?php }
                                                                ?>   

                                                            </td>

                                                        </tr>
                                                        @endforeach

                                                    </tbody>
                                                </table>
                                                <br>
                                                <div class="pull-right">
                                                    <a class="btn btn-warning" href="{{ url($urlPrifix.'/task')}}">{{ $language_data->__('text_task_back_button') }}</a>
                                                    <button class="btn btn-info assignTask" name="assignTask" id="assignTask" type="submit">{{ $language_data->__('text_submit') }}</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </section>

                                        <h2>{{ $language_data->__('text_map_view') }}</h2>
                                        <section>
                                            <div id="map2" style="width:900px; height: 300px;"></div>
<!--                                            {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => $urlPrifix.'/task/store_assign']) !!}

                                            <br>
                                            <div class="pull-right">
                                                <a class="btn btn-warning" href="{{ url($urlPrifix.'/task')}}">{{ $language_data->__('text_task_back_button') }}</a>
                                                <button class="btn btn-info assignTask" name="assignTask" id="assignTask" type="submit">{{ $language_data->__('text_submit') }}</button>
                                                <button class="btn btn-info assignTaskMapview" name="assignTask" id="assignTask" type="button">{{ $language_data->__('text_submit') }}</button>
                                            </div>
                                            {!! Form::close() !!}-->
                                        </section>

                                    </div>

                                </div>
                                <?php
                            } else {
                                $empName = DB::table('tbl_employee')
                                        ->select("emp_name")
                                        ->where('emp_id', $isTastAssigned[0]->emp_id)
                                        ->first()
                                ?>
                                <div class="modal-body text-center">
                                    <?= $language_data->__('text_task_accepted_by') . " " . $empName->emp_name; ?> 
                                </div>   
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>


<script type="text/javascript">

    $(document).on('click', '.empAssignMapview', function (e) {
        if ($(this).is(':checked')) {
            $(this).attr('checked', true);
        } else {
            $(this).attr('checked', false);
        }
    });

    $(document).on('click', '#wizard-tab-t-1', function (e) {
        mapviewEmp()
    });



    window.onload = function () {
        mapviewEmp()
    };
    function mapviewEmp() {

        var map = new google.maps.Map(document.getElementById('map2'), {
            zoom: 4,
            center: new google.maps.LatLng("19.1796181", "73.022982"),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        var bounds = new google.maps.LatLngBounds();
        var locations = [
<?php
//echo "<form "
$taskId = $task_data[0]->task_id;
    $startDate = $isTastAssigned[0]->et_duedate;
foreach ($employees_data as $partnerdata) {
    
    $emp_id = $partnerdata->emp_id;
    
    $isBusy = getempStatus($emp_id, $taskId, $startDate);
    $empcurrentlocation1 = DB::table('employeesurrentlocation')
            ->select("lan", "long")
            ->where('employeid', $emp_id)
            ->first();

    $latvalue = $partnerdata->lat;
    $langvalue = $partnerdata->lang;

    if (!empty($empcurrentlocation1)) {
        $latvalue = $empcurrentlocation1->lan;
        $langvalue = $empcurrentlocation1->long;
    }

    if ($isBusy == 0 || $isBusy == 3) {
        $isExist = DB::table('tbl_assign_task_emp')
                ->select("id")
                ->where('emp_id', $emp_id)
                ->where('task_id', $taskId)
                ->first();
        $checked = "";
        if (!empty($isExist)) {
            $checked = "checked=true";
        }
        ?>

                    ['<?php
        echo '<h5 class="mapviewLable">';
        echo '<input type="hidden" name="task_id"  value="' . $taskId . '">';

//        echo'<strong style="color:#000;">' . $partnerdata->emp_name . ' </strong><input type="checkbox" ' . $checked . ' id="multiassignEmpId" class="multiassignEmpId empAssignMapview"  name="multiassignEmpId[]" value="' . $emp_id . '"><div style="clear:both"></div>"';
        echo'<strong style="color:#000;">' . $partnerdata->emp_name . ' </strong><div style="clear:both"></div>"';

        if ($partnerdata->availability == 2) {
            echo $language_data->__('text_map_view_leave_reason');
            echo $partnerdata->leavereason;
        } else {
            echo $address1 = getAddressNew($latvalue, $langvalue);
        }

        echo '"</h5>';
        ?>',
        <?php echo $latvalue; ?>, <?php echo $langvalue;
        ?>, 4],
    <?php } else { ?>
                    ['<?php
        echo '<h5 class="mapviewLable">';

        echo '<strong style="color:#000;">' . $partnerdata->emp_name . '</strong><br><b class="mapviewBusyLable">' . $language_data->__('text_busy') . '</b><div style="clear:both"></div>';

        if ($partnerdata->availability == 2) {
            echo $language_data->__('text_map_view_leave_reason');
            echo $partnerdata->leavereason;
        } else {
            echo $address1 = getAddressNew($latvalue, $langvalue);
        }

        echo '</h5>';
        ?>',
        <?php echo $latvalue; ?>, <?php echo $langvalue;
        ?>, 4],
        <?php
    }
}
?>

        ]

<?php
$i1 = 0;
foreach ($employees_data as $partnerdata) {

    $image = URL::to('/') . "/public/uploads/employees/" . $partnerdata->profileimage;

    if ($partnerdata->profileimage == "") {
        $image = URL::to('/') . "/resources/assets/momom-assets/img/user.png";
    }
    ?>
            var i =<?php echo $i1; ?>;
            var lat = locations[<?php echo $i1; ?>][1];
            var lng = locations[<?php echo $i1; ?>][2];
            var icon = {
                url: "<?php echo $image; ?>",
                scaledSize: new google.maps.Size(37, 37),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                icon: icon
            });

            bounds.extend(new google.maps.LatLng(lat, lng));
            map.fitBounds(bounds);

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
    <?php $i1++;
}
?>
    }
</script>  



@include('layouts.footer');


<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_supervisor') }}
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">


                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->

            <div class="content-wrap">
                <div class="row">

                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-3">
                                    <h6>{{ $language_data->__('text_settings') }} </h6>
                                </label>
                            </div>
                            <div class="body-nest" id="element">
                                <div class="panel-body">
                                    @if(Session::has('msg'))
                                        <div class="alert {!!Session::get('alert')!!}">
                                            <a class="close" data-dismiss="alert">×</a>
                                            <strong>{!!Session::get('msg')!!}</strong> 
                                        </div>
                                    @endif
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'supervisor/setting/store']) !!}
                                    <div class="form-group">
                                        <input type="hidden" name="supervisor_id" value="{{ $supervisors_data[0]->supervisor_id }}" />
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" value="{{ $supervisors_data[0]->sup_name }}" id="sup_name" name="sup_name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_name'); ?>">
                                            <label for="sup_name" id="sup_name_error" generated="true" class="error">{{ $errors->first('sup_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email"  id="email"  pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$" value="{{ $supervisors_data[0]->email }}"  name="email"  class="form-control placeholder-no-fix emailId" placeholder="<?php echo $language_data->__('text_email'); ?>">
                                            <label for="email" id="email_error" generated="true" class="error">{{ $errors->first('email') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_password') }} </label>
                                        <div class="col-sm-6">
                                            <button class="btn btn-success" href="#changepassword" id="changepassbtn" data-toggle="modal" type="button">{{ $language_data->__('text_change_pwd') }}</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" maxlength="16" name="mobilenum" id="mobilenum" value="{{ $supervisors_data[0]->mobilenum }}" value=""  placeholder="<?php echo $language_data->__('text_contact'); ?>" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="mobilenum_error" id="mobilenum_error" generated="true" class="error">{{ $errors->first('mobilenum') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_profile_image') }} </label>
                                        <div class="col-sm-6">
                                            <img src="{{ url('public/uploads/supervisors/').'/'.$supervisors_data[0]->pfilename}}" alt="" height="100" width="100" />
                                            <input type="file" placeholder="" autocomplete="off" id="profileimage"  name="profileimage"  class="form-control round-input imgLogo">
                                            <label for="profileimage" id="profileimage_error" generated="true" class="error">{{ $errors->first('profileimage') }}</label>
                                        </div>
                                    </div>
                                    <!--<div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_status') }}</label>
                                        <div class="col-sm-6">
                                            <div>
                                                <select name="status" class="form-control" id="cmbstat" tabindex="6" >
                                                    <option value="1">{{ $language_data->__('text_active') }}</option>
                                                    <option value="0">{{ $language_data->__('text_inactive') }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-12 text-center">
                                    <button class="btn btn-info updateSupervisor"  name="add_supervisor" id="add_supervisor" type="submit">{{ $language_data->__('text_update') }}</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>

                        </div>


                    </div>

                </div>
            </div>

            <div aria-hidden="true" aria-labelledby="myModalLabel" style="overflow:auto;" role="dialog" tabindex="-1" id="changepassword" class="modal fade">

                <div class="modal-dialog">
                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'supervisor/setting/changepassword']) !!}
                        <div class="modal-content">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>


                                <h4 class="modal-title"><?php echo $language_data->__('text_change_pwd'); ?></h4>

                            </div>

                            <div class="modal-body">

                                <div class="col-sm-4">

                                    <label><?php echo $language_data->__('text_old_pwd'); ?></label></div>

                                <div class="col-sm-8">

                                    <input type="password" style="margin-bottom:10px;" tabindex="1" required  name="admin_password"   value="{{ old('admin_password') }}"      placeholder="<?php echo $language_data->__('text_old_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix admin_password">
                                    <label for="admin_password" id="admin_password_error" generated="true" class="error">{{ $errors->first('admin_password') }}</label>
                                </div>  <div class="clearfix"></div>


                                <div class="col-sm-4">

                                    <label><?php echo $language_data->__('text_new_pwd'); ?></label>
                                </div>

                                <div class="col-sm-8">
                                    <input type="password" style="margin-bottom:10px;" required  tabindex="2" name="new_password"   value="{{ old('new_password') }}"  placeholder="<?php echo $language_data->__('text_new_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix new_password">
                                    <label for="new_password" id="new_password_error" generated="true" class="error">{{ $errors->first('new_password') }}</label>
                                </div>  

                                <div class="clearfix"></div>


                                <div class="col-sm-4">

                                    <label><?php echo $language_data->__('text_re_enter_pwd'); ?></label>
                                </div>

                                <div class="col-sm-8">
                                    <input type="password" style="margin-bottom:10px;" required  tabindex="3" name="reenter_password"   value="{{ old('reenter_password') }}"      placeholder="<?php echo $language_data->__('text_re_enter_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix reenter_password">
                                    <label for="reenter_password" id="reenter_password_error" generated="true" class="error">{{ $errors->first('reenter_password') }}</label>
                                </div>  

                                <div class="clearfix"></div>
                                <div class="clear"></div>


                                <div class="modal-footer">



                                    <button tabindex="4" class="btn btn-success changespass" name="changepasswordsubmit"
                                            type="submit"><?php echo $language_data->__('text_submit'); ?></button>
                                     <button tabindex="5" data-dismiss="modal" class="btn btn-default"
                                            type="button"><?php echo $language_data->__('text_cancel'); ?></button>

                                </div>

                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('supervisorSetting.validation');
<script type="text/javascript">
    $(document).ready(function () {
        google.maps.event.addDomListener(window, 'load', initialize);
        $("#address").keyup(function () {

            initialize1($(this).val());
        });
        @if(Session::has('passError'))
            $('#changepassbtn').trigger('click');
        @endif    
        $('#changepassword').on('show.bs.modal', function () {


        });
    });
</script>    




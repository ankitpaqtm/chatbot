<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>



    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_dashboard') }}</span>
                        </h2>
                    </div>

                    <div class="col-sm-7"></div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
            <!--/ TITLE -->
            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="javascript:;" title="{{ $language_data->__('text_dashboard') }}">{{ $language_data->__('text_dashboard') }}</a>
                </li>

                <li class="pull-right">
                    <div class="input-group input-widget">

                        <!--<input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">-->
                        {!! Form::open(["id"=>"searchForm"]) !!}
                        <div class="col-sm-4 no-padding" style="margin-right: 10px;">
                            <input type="hidden" name="clientid" value="<?= $uid ?>"/>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="mapRlati" name="mapRlati" value=""/>
                            <input type="hidden" id="mapRlongi" name="mapRlongi" value=""/>
                            <input name="keyword" type="text" placeholder="<?php echo $language_data->__('text_search_emp'); ?>" class="form-control searchEmp">
                        </div>
                        <div class="col-sm-3 no-padding" style="margin-right: 10px;">
                            <select name="employee_status" id="employee_status" class="form-control employee_status">
                                <option value=""><?php echo $language_data->__('text_all_label'); ?></option>
                                <option value="1"><?php echo $language_data->__('text_active'); ?></option>
                                <option value="0"><?php echo $language_data->__('text_inactive'); ?></option>
                            </select>
                        </div>
                        <div class="col-sm-2 no-padding" style="margin-right: 15px;">    
                            <button type="button" class="btn btn-small btn-info searchEmpButton">
                                <?php echo $language_data->__('text_search'); ?>
                            </button>
                        </div>
                        <div class="col-sm-2 text-right no-padding">    
                            <button type="button" class="btn btn-small btn-success refresh-map">
                                <i class="fontawesome-refresh"></i> &nbsp;&nbsp; <?php echo $language_data->__('text_refresh'); ?>
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </li>
                <li class="pull-right">
                    <div class="titleClose loadingImg" style="display: none;">
                        <img src="<?= asset('resources/assets/momom-assets/img/loading.gif'); ?>" />
                    </div>
                </li>
            </ul>
            <div id="paper-middle">
                <div id="getlatestmap" style="width: 100%;height: 400px;position: absolute;"></div>
            </div>
            <!-- END OF BREADCRUMB -->
            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <div class="row" style="margin-top: 40px;">

                    <div class="col-sm-3">
                        <div class="profit" id="profitClose">
                            <a href="{{ url($urlPrifix.'/')  }}/clients">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="icon-user-group"></i>&#160;<?php echo $language_data->__('text_client'); ?>
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span style="top:-10px;"><i class="fa fa-users"></i>
                                    </span>
                                    <b id="speed">
                                        <?php
                                        $clntCount = DB::table("tbl_clients")
                                                        ->where("companyid", $uid)
                                                        ->where("status", '1')
                                                        ->get()->count();
                                        echo $clntCount;
                                        ?>
                                    </b>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="order" id="orderClose">
                            <a href="{{ url($urlPrifix.'/')  }}/employees">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="icon-user-group"></i>&#160;<?php echo $language_data->__('text_teams'); ?>
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span><i class="fa fa-users fa-2x"></i>
                                    </span>
                                    <b id="speed">
                                        <?php
                                        if ($supervisor_id > 0) {
                                        $empCount = DB::table("tbl_employee")
                                                        ->where("companyid", $uid)
                                                        ->where("supervisor_id", $supervisor_id)
                                                        ->where("delete_employee", '0')
                                                        ->get()->count();
                                        }  else {
                                           $empCount = DB::table("tbl_employee")
                                                        ->where("companyid", $uid)
                                                        ->where("delete_employee", '0')
                                                        ->get()->count(); 
                                        }
                                        echo $empCount;
                                        ?>
                                    </b>
                                </div>
                            </a>    
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="order member" id="memberClose">
                            <a href="{{ url($urlPrifix.'/')  }}/task">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="icon-document-new"></i>
                                            &#160;<?php echo $language_data->__('text_total_tasks'); ?>
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span><i class="icon icon-document"></i>
                                    </span>
                                    <?php
                                    if ($supervisor_id > 0) {
                                    $taskCount = DB::table('tbl_emp_task')
                                                        ->join('tbl_task', 'tbl_emp_task.task_id', '=', 'tbl_task.task_id')
                                                        ->select('tbl_emp_task.et_id')
                                                        ->where('tbl_emp_task.comp_id', $uid)
                                                        ->where('tbl_emp_task.supervisor_id', $supervisor_id)
                                                        ->where('tbl_task.task_delete', 0)
                                                        ->groupBy('tbl_emp_task.task_id')
                                                        ->get()->count();
                                    }  else {
//                                        $taskCount = DB::table("tbl_task")
//                                                    ->where("comp_id", $uid)
//                                                    ->where("task_delete", '0')
//                                                    ->get()->count();
                                        
                                        $taskCount = DB::table('tbl_emp_task')
                                                        ->join('tbl_task', 'tbl_emp_task.task_id', '=', 'tbl_task.task_id')
                                                        ->select('tbl_emp_task.task_id')
                                                        ->where('tbl_emp_task.comp_id', $uid)
                                                        ->where('tbl_task.task_delete', 0)
                                                        ->groupBy('tbl_emp_task.task_id')
                                                        ->get()->count();
                                        
                                    }
                                    echo $taskCount;
                                    ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="revenue" id="revenueClose">
                            <a href="{{ url($urlPrifix.'/')  }}/report/workreport">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="icon icon-document-edit"></i>&#160;<?php echo $language_data->__('text_completed_tasks'); ?>
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span><i class="entypo-newspaper"></i>
                                    </span>
                                    <?php
                                    if ($supervisor_id > 0) {
                                        $total_task = DB::table('tbl_emp_task')
                                                        ->join('tbl_task', 'tbl_emp_task.task_id', '=', 'tbl_task.task_id')
                                                        ->join('tbl_clients', 'tbl_task.clientid', '=', 'tbl_clients.clientid')
                                                        ->join('tbl_employee', 'tbl_emp_task.emp_id', '=', 'tbl_employee.emp_id')
                                                        ->select('tbl_emp_task.et_id')
                                                        ->where('tbl_clients.companyid', $uid)
                                                        ->where('tbl_emp_task.supervisor_id', $supervisor_id)
                                                        ->where('tbl_emp_task.status', 2)
                                                        ->where('tbl_emp_task.started', 0)
                                                        ->where('tbl_task.task_delete', 0)
                                                        ->get()->count();
                                    } else {
                                        $total_task = DB::table('tbl_emp_task')
                                                        ->join('tbl_task', 'tbl_emp_task.task_id', '=', 'tbl_task.task_id')
                                                        ->join('tbl_clients', 'tbl_task.clientid', '=', 'tbl_clients.clientid')
                                                        ->join('tbl_employee', 'tbl_emp_task.emp_id', '=', 'tbl_employee.emp_id')
                                                        ->select('tbl_emp_task.et_id')
                                                        ->where('tbl_clients.companyid', $uid)
                                                        ->where('tbl_emp_task.status', 2)
                                                        ->where('tbl_emp_task.started', 0)
                                                        ->where('tbl_task.task_delete', 0)
                                                        ->get()->count();
                                    }
                                    echo $total_task;
                                    ?>
                                </div>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
            <!--  / DEVICE MANAGER -->

            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-8">
                        <div id="siteClose" class="nest">
                            <div class="title-alt">
                                <h6>
                                    <span class="entypo-newspaper"></span>&nbsp;<?php echo $language_data->__('text_work_status'); ?></h6>
                            </div>
                            <div id="site11" class="body-nest">
                                <div class="table-responsive">
                                    <table class="table">

                                        <tbody>

                                            <?php
                                            if ($supervisor_id > 0) {
                                            $runningTaskArr = DB::table('tbl_emp_task')
                                                    ->select("tbl_emp_task.et_duedate", "tbl_emp_task.task_id", "tbl_emp_task.et_id", "started", "duration", "emp_id", "task_starttime", "task_name")
                                                    ->join('tbl_task', 'tbl_task.task_id', '=', 'tbl_emp_task.task_id')
                                                    ->where('started', "1")
                                                    ->where('tbl_emp_task.status', "1")
                                                    ->where('emp_id', "!=", 0)
                                                    ->where('task_starttime', "!=", "0000-00-00 00:00:00")
                                                    ->where('tbl_emp_task.comp_id', $uid)
                                                    ->where('tbl_emp_task.supervisor_id', $supervisor_id)
                                                    ->orderBy('et_id', 'DESC')
                                                    ->get();
                                            }else{
                                                $runningTaskArr = DB::table('tbl_emp_task')
                                                    ->select("tbl_emp_task.et_duedate", "tbl_emp_task.task_id", "tbl_emp_task.et_id", "started", "duration", "emp_id", "task_starttime", "task_name")
                                                    ->join('tbl_task', 'tbl_task.task_id', '=', 'tbl_emp_task.task_id')
                                                    ->where('started', "1")
                                                    ->where('tbl_emp_task.status', "1")
                                                    ->where('emp_id', "!=", 0)
                                                    ->where('task_starttime', "!=", "0000-00-00 00:00:00")
                                                    ->where('tbl_emp_task.comp_id', $uid)
                                                    ->orderBy('et_id', 'DESC')
                                                    ->get();
                                            }
                                            ?>
                                            <?php
                                            if ($runningTaskArr->count() > 0) {



                                                foreach ($runningTaskArr as $key => $val) {
                                                    if ($supervisor_id > 0) {
                                                    $empList = DB::table('tbl_employee')
                                                            ->select("emp_id", "emp_name", "profileimage", "availability")
                                                            ->where('emp_status', 1)
                                                            ->where('companyid', $uid)
                                                            ->where('supervisor_id', $supervisor_id)
                                                            ->where('emp_id', $val->emp_id)
                                                            ->limit(5)
                                                            ->first();
                                                    }else{
                                                     $empList = DB::table('tbl_employee')
                                                            ->select("emp_id", "emp_name", "profileimage", "availability")
                                                            ->where('emp_status', 1)
                                                            ->where('companyid', $uid)
                                                            ->where('emp_id', $val->emp_id)
                                                            ->limit(5)
                                                            ->first();   
                                                    }

                                                    $profileimage = url('public/uploads/employees/' . $empList->profileimage);
                                                    if ($empList->profileimage == "")
                                                        $profileimage = asset('resources/assets/momom-assets/img/user.png');

                                                    $taskEndTime = date("Y-m-d H:i", strtotime($val->task_starttime . "+$val->duration minutes"));
                                                    $taskStartTime = date("Y-m-d H:i", strtotime($val->task_starttime));



//                                                    $taskStartTime = '2017-03-03 01:00';
//                                                    $taskEndTime = '2017-03-03 02:00';
//                                                    $currentTime = '2017-03-03 02:20';
//                                                    $date1 = strtotime($taskStartTime);
//                                                    $date2 = strtotime($taskEndTime);
//                                                    $today = strtotime($currentTime);
//
//                                                    $dateDiff = ($date2 - $date1) / 60;
//                                                    echo "datediff=" . $dateDiff . "</br>";
//                                                    $dateDiffForToday = ($today - $date1) / 60;
//                                                    echo "datediff=" . $dateDiffForToday . "</br>";
//                                                    echo $dateDiffForToday * 100 / $dateDiff;

                                                    
//                                                    echo "taskStartTime ->". $taskStartTime;
//                                                    echo "<br>";
//                                                    echo "taskEndTime ->". $taskEndTime;
//                                                    echo "<br>";
//                                                    echo "today ->". date("Y-m-d H:i");
//                                                    echo "<br>";
                                                    $stime = strtotime($taskStartTime);
                                                    $etime = strtotime($taskEndTime);
                                                    $today = strtotime(date("Y-m-d H:i"));

                                                    $dateDiff = ($etime - $stime) / 60;
                                                    $dateDiffForToday = ($today - $stime) / 60;
                                                   $percentage = $dateDiffForToday * 100 / $dateDiff;
                                                   
                                                    $Spendtime1 = round($percentage);

                                                    if ($Spendtime1 <= 100) {

                                                        $per = $Spendtime1;
                                                    } else {
                                                        $per = 100;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td class="armada-devider">
                                                            <div class="armada">
                                                                <?php
//                                                                $is_busy = getEmployeeStatus($val->emp_id);
                                                                $is_busy = getEmployeeStatusByTask($val->emp_id, $val->task_id, $val->et_id);
                                                                ?>
                                                                <?php if ($empList->availability == 2) { ?>
                                                                    <span style="background:#FF6B6B"><?= $language_data->__('text_leave'); ?></span>
                                                                <?php } else if ($is_busy == 0) { ?>
                                                                    <span style="background:#45B6B0"> <?= $language_data->__('text_available'); ?> </span>
                                                                <?php } else if ($is_busy == 1) { ?>
                                                                    <span style="background:#f0ad4e">  <?= $language_data->__('text_on_duty'); ?> </span>
                                                                <?php } ?>
                                                                <p>
                                                                    <span class="icon icon-document-edit"></span>&nbsp; {{$val->task_name}}
                                                                </p>
                                                            </div>
                                                        </td>
                                                        <td class="driver-devider">
                                                            <img class="armada-pic img-circle" style="height:52px;" alt="" src="<?= $profileimage; ?>">
                                                            <h3><?= $empList->emp_name ?></h3>

                                                        </td>
                                                        <td class="progress-devider">
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= $per; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $per; ?>%">
                                                                    <span class=""><?= $per; ?>% <?php echo $language_data->__('text_completed'); ?> </span>
                                                                </div>
                                                            </div>
                                                            <span class="label pull-left"><?= $taskStartTime; ?></span>
                                                            <span class="label pull-right"><?= $taskEndTime; ?></span>

                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                echo $language_data->__('text_no_records');
                                            }
                                            ?>        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div id="RealTimeClose" class="nest">
                            <div class="title-alt">
                                <h6>
                                    <span class="entypo-newspaper"></span>&nbsp;<?php echo $language_data->__('text_completed_tasks'); ?></h6>
                            </div>
                            <div id="RealTime" style="padding-top:20px;" class="body-nest">
                                <ul class="direction">
                                    <?php
                                    
                                    if ($supervisor_id > 0) {
                                    $completed_tasklist = DB::select("SELECT "
                                                    . "tek.task_name, "
                                                    . "tk.emp_id, "
                                                    . "tk.task_starttime, "
                                                    . "c.client_name "
                                                    . "FROM tbl_task as tek "
                                                    . "inner join tbl_emp_task tk on tk.task_id = tek.task_id "
                                                    . "left join tbl_clients c on tek.clientid = c.clientid "
                                                    . "where tk.task_starttime > '0000-00-00 00:00:00' "
                                                    . "AND tek.comp_id = $uid "
                                                    . "AND tk.supervisor_id = $supervisor_id "
                                                    . "AND tek.task_delete = '0' "
                                                    . "AND tk.status = 2 "
                                                    . "limit 5");
                                        
                                    }  else {
                                    $completed_tasklist = DB::select("SELECT "
                                                    . "tek.task_name, "
                                                    . "tk.emp_id, "
                                                    . "tk.task_starttime, "
                                                    . "c.client_name "
                                                    . "FROM tbl_task as tek "
                                                    . "inner join tbl_emp_task tk on tk.task_id = tek.task_id "
                                                    . "left join tbl_clients c on tek.clientid = c.clientid "
                                                    . "where tk.task_starttime > '0000-00-00 00:00:00' "
                                                    . "AND tek.comp_id = $uid "
                                                    . "AND tek.task_delete = '0' "
                                                    . "AND tk.status = 2 "
                                                    . "limit 5");
                                    }
//echo "<pre>";print_r($logs);
                                    if (!empty($completed_tasklist)) {
                                        $i = 0;
                                        foreach ($completed_tasklist as $task) {
                                            $i++;
                                            $emp_id = $task->emp_id;
                                            $completed_emp_name = DB::select("select emp_name from tbl_employee where emp_id = $emp_id");
                                            ?>
                                            <li>
                                                <span class="direction-icon" style="background:#45B6B0"><?= $i ?></span>
                                                <h3><span><?= $task->client_name ?></span></h3>
                                                <p><i class="fa fa-arrow-circle-right"></i>&nbsp;&nbsp;<?= $task->task_name ?></p>
                                                <p><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?= $language_data->convertedDates($task->task_starttime, "fulldatetime"); ?></p>
                                                <p><i class="fa fa-user-circle"></i>&nbsp;&nbsp;<?= $completed_emp_name[0]->emp_name ?></p>
                                            </li>
                                            <li class="divider"></li>
                                            <?php
                                        }
                                        ?>
                                        <li class="text-right"><a href="{{ url($urlPrifix.'/')  }}/report/workreport"><?= $language_data->__('text_more'); ?></a></li>
                                        <li class="divider"></li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><?= '<p style="text-align:center">' . $language_data->__('text_no_data') . '</p>'; ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <?php /* ?><div class="content-wrap">
              <div class="row">
              <div class="col-sm-12">
              <div id="RealTimeClose" class="nest">
              <div class="title-alt">
              <h6>
              <span class="icon icon-location"></span>&nbsp;<?php echo $language_data->__('text_current_locations'); ?>
              </h6>

              {!! Form::open(["id"=>"searchForm",]) !!}
              <div class="titleToggle" style="margin: 10px 10px 0 0;">
              <button type="button" class="btn btn-small btn-success refresh-map">
              <i class="fontawesome-refresh"></i> &nbsp;&nbsp; <?php echo $language_data->__('text_refresh'); ?>
              </button>
              </div>
              <div class="titleToggle" style="margin: 10px 10px 0 0;">
              <button type="button" class="btn btn-small btn-info searchEmpButton">
              <?php echo $language_data->__('text_search'); ?>
              </button>
              </div>
              <div class="titleClose" style="margin: 17px -22px 0 0;">
              <div class="input-group input-widget">
              <input type="hidden" name="clientid" value="<?= $uid ?>"/>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="col-sm-7">
              <input name="keyword" type="text" placeholder="<?php echo $language_data->__('text_search_emp'); ?>" class="form-control searchEmp">
              </div>
              <div class="col-sm-5">
              <select name="employee_status" id="employee_status" class="form-control employee_status">
              <option value=""><?php echo $language_data->__('text_all_label'); ?></option>
              <option value="1"><?php echo $language_data->__('text_active'); ?></option>
              <option value="0"><?php echo $language_data->__('text_inactive'); ?></option>
              </select>
              </div>
              </div>
              </div>
              {!! Form::close() !!}
              <div class="titleClose loadingImg" style="display: none;">
              <img src="<?= asset('resources/assets/momom-assets/img/loading.gif'); ?>" />
              </div>
              </div>
              <div id="getlatestmap" style="padding: 20px 35px;"></div>
              </div>
              </div>
              </div>
              </div><?php */ ?>

            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-6">
                        <div id="RealTimeClose" class="nest">
                            <div class="title-alt">
                                <h6>
                                    <span class="icon icon-user-group"></span>&nbsp;<?php echo $language_data->__('text_employee_status'); ?></h6>
                            </div>
                            <div id="RealTime" style="padding-top:20px;" class="body-nest">
                                <ul class="direction">
                                    <?php
                                    if ($supervisor_id > 0) {
                                        $EmployeeArr = DB::select("SELECT "
                                                    . " emp_name, "
                                                    . " emp_id, "
                                                    . " availability "
                                                    . " FROM "
                                                    . " tbl_employee "
                                                    . " where "
                                                    . " delete_employee = 0 AND companyid=" . $uid . " AND supervisor_id=" . $supervisor_id . ""
                                                    . " order by emp_id asc limit 5");
                                    }  else {
                                    $EmployeeArr = DB::select("SELECT "
                                                    . " emp_name, "
                                                    . " emp_id, "
                                                    . " availability "
                                                    . " FROM "
                                                    . " tbl_employee "
                                                    . " where "
                                                    . " delete_employee = 0 AND companyid=" . $uid . ""
                                                    . " order by emp_id asc limit 5");
                                    }
                                    if (!empty($EmployeeArr)) {
                                        $i = 0;
                                        foreach ($EmployeeArr as $log) {
                                            $i++;
                                            $is_busy = getEmployeeStatus($log->emp_id);
                                            ?>
                                            <li>
                                                <span class="direction-icon" style="background:#65C3DF"><?= $i ?></span>
                                                <p><i class="fa fa-user-circle"></i>&nbsp;&nbsp;<?= $log->emp_name ?></p>
                                                <?php if ($log->availability == 2) { ?>
                                                    <i class="icon icon-forward"></i>&nbsp;&nbsp;<span class="label label-important"> <?= $language_data->__('text_leave'); ?></span>
                                                <?php } else if ($is_busy == 0) { ?>
                                                    <i class="icon icon-forward"></i>&nbsp;&nbsp;<span class="label label-success"> <?= $language_data->__('text_available'); ?> </span>
                                                <?php } else if ($is_busy == 1) { ?>
                                                    <i class="icon icon-forward"></i>&nbsp;&nbsp;<span class="label label-warning"> <?= $language_data->__('text_on_duty'); ?> </span>
                                                <?php } ?>
                                                <p>&nbsp;&nbsp;</p>
                                            </li>
                                            <li class="divider"></li>
                                            <?php
                                        }
                                        ?>
                                        <li class="text-right"><a href="{{ url($urlPrifix.'/')  }}/employees"><?= $language_data->__('text_more'); ?></a></li>
                                        <li class="divider"></li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><?= '<p style="text-align:center">' . $language_data->__('text_no_data') . '</p>'; ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="RealTimeClose" class="nest">
                            <div class="title-alt">
                                <h6>
                                    <span class="icon icon-clock"></span>&nbsp;<?php echo $language_data->__('text_recent_logs'); ?></h6>
                            </div>
                            <div id="RealTime" style="padding-top:20px;" class="body-nest">
                                <ul class="direction">
                                    <?php
                                    
                                    if ($supervisor_id > 0) {
                                    $EmployeeArr = DB::select("select et.task_id, et.et_id,et.task_starttime,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, e.emp_name FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where et.task_starttime > '0000-00-00 00:00:00' AND t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and t.task_delete=0  and c.companyid='$uid' and et.supervisor_id='$supervisor_id' and et.status !=-1 order by et.task_starttime desc limit 5");    
                                    }  else {
                                     $EmployeeArr = DB::select("select et.task_id, et.et_id,et.task_starttime,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, e.emp_name FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where et.task_starttime > '0000-00-00 00:00:00' AND t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and t.task_delete=0  and c.companyid='$uid' and et.status !=-1 order by et.task_starttime desc limit 5");   
                                    }
                                    
                                    
                                    if (!empty($EmployeeArr)) {
                                        $i = 0;
                                        foreach ($EmployeeArr as $log) {
                                            $i++;
                                            ?>
                                            <li>
                                                <span class="direction-icon" style="background:#65C3DF"><?= $i ?></span>

                                                <h3>
                                                    <span><?= $log->client_name . ' - ' . $log->task_name ?></span>
                                                </h3>
                                                <p><i>Time </i>:&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?= $language_data->convertedDates($log->task_starttime, "fulldatetime") ?></p>
                                                <p><i class="fa fa-user-circle"></i>&nbsp;&nbsp;<?= $log->emp_name ?></p>
                                            </li>
                                            <li class="divider"></li>
                                            <?php
                                        }
                                        ?>
                                        <li class="text-right"><a href="{{ url($urlPrifix.'/')  }}/report/workreport"><?= $language_data->__('text_more'); ?></a></li>
                                        <li class="divider"></li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><?= '<p style="text-align:center">' . $language_data->__('text_no_data') . '</p>'; ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-6">
                        <div id="RealTimeClose" class="nest">
                            <div class="title-alt">
                                <h6>
                                    <span class="fa fa-clock-o"></span>&nbsp;<?php echo $language_data->__('text_task_summary'); ?></h6>
                            </div>
                            <div id="RealTime" style="padding-top:20px;" class="body-nest">
                                <div id="graph_bar" style="width:100%; height:300px;"></div>
                                <?php
                                $fetchMode = DB::getFetchMode();
// set mode to custom
                                DB::setFetchMode(\PDO::FETCH_ASSOC);
                                $sQuery = "SELECT DATE(tk.task_endtime) AS dt,COUNT(DISTINCT te.task_id) AS totjob";
                                $sQuery .= " FROM tbl_emp_task tk";
                                $sQuery .= " INNER JOIN tbl_task AS te ON tk.task_id = te.task_id";
                                $sQuery .= " INNER JOIN tbl_clients AS c ON te.clientid =c.clientid";
                                $sQuery .= " INNER JOIN tbl_employee AS e ON e.emp_id = tk.emp_id";
                                $sQuery .= " WHERE c.companyid=$uid AND tk.status !=-1 AND tk.comp_id = " . $uid . " AND tk.status = '2' AND te.task_delete = 0";
                                $sQuery .= " AND DATE(tk.task_endtime) BETWEEN '" . date('Y-m-d', strtotime('-6 days')) . "' AND '" . date('Y-m-d') . "'";
                                if ($supervisor_id > 0){
                                    $sQuery .= " AND tk.supervisor_id = " . $supervisor_id;
                                }
                                $sQuery .= " GROUP BY dt";
                                $sQuery .= " ORDER BY dt";
                                $sQuery .= " LIMIT 7";

                                $aResultSet = DB::select($sQuery);
                                $aTasks = array();
                                $aRows = array();
                                foreach ($aResultSet as $aRow) {

                                    $perdate = date('Y-m-d', strtotime($aRow['dt']));

                                    if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") {
                                        $aRow['dt'] = $l->convertedDates($perdate, "Y-m-d");
                                    }

                                    $aRows[$aRow['dt']] = $aRow['totjob'];
                                }
                                $iDate = time();
                                for ($i = 0; $i < 7; $i++) {
                                    $date = date('Y-m-d', strtotime('-' . $i . ' Day', $iDate));

                                    if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") {
                                        $date = $l->convertedDates($date, "Y-m-d");
                                    }
                                    $aTasks[$date] = isset($aRows[$date]) ? $aRows[$date] : 0;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="RealTimeClose" class="nest">
                            <div class="title-alt">
                                <h6>
                                    <span class="entypo-newspaper"></span>&nbsp;<?php echo $language_data->__('text_work_efficiency'); ?></h6>
                            </div>
                            <div id="RealTime" style="padding-top:20px;" class="body-nest">
                                <div id="graph_line" style="width:100%; height:300px;"></div>
                                <?php
                                $sQuery = "SELECT t.task_id,t.task_name,DATE(et.task_starttime) as dt,et.task_starttime,et.task_endtime,et.et_hours,et.et_mins,t.duration";
//$sQuery .= ",SUM(t.duration) AS duration,SUM((et.et_hours * 60) + et.et_mins)/60 AS est_time,SUM(TIME_TO_SEC(TIMEDIFF(et.task_endtime,et.task_starttime))/60) AS work_hours";
                                $sQuery .= " FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et";
                                $sQuery .= " WHERE et.task_starttime > '0000-00-00 00:00:00' AND t.clientid =c.clientid AND e.emp_id = et.emp_id";
                                $sQuery .= " AND t.task_id = et.task_id AND c.companyid = " . $uid;
                                $sQuery .= " AND et.status = '2' AND t.task_delete = 0 AND t.is_from_app=0";
                                if ($supervisor_id > 0)
                                    $sQuery .= " AND et.supervisor_id = " . $supervisor_id;
//$sQuery .= " GROUP BY t.task_id";
                                $sQuery .= " ORDER BY t.task_id DESC";
                                $sQuery .= " LIMIT 7";

                                $aTime = DB::select($sQuery);
                                if (!empty($aTime)) {
                                    foreach ($aTime as $key => $aT) {
                                        $est = (($aT['et_hours'] * 60) + $aT['et_mins']) / 60;
                                        $estime = number_format($est / 3600, 3, '.', '');
                                        $aTime[$key]['est_time'] = $estime;

                                        $datetime1 = strtotime($aT['task_starttime']);
                                        $datetime2 = strtotime($aT['task_endtime']);
                                        $intervalCount = abs($datetime2 - $datetime1);
                                        $hoursTotal = number_format($intervalCount / 3600, 3, '.', '');
                                        //$aT[$key]['work_hours'] = $hoursTotal;
                                        $aTime[$key]['work_hours'] = $hoursTotal;
                                    }
                                }
//pr($aTime);exit;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
            <?php DB::setFetchMode($fetchMode); ?>
            <!-- /END OF CONTENT -->


            


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer')

<script type="text/javascript">
    $(window).load(function () {
        $('.refresh-map').trigger('click');
        // Tasks Summary Chart
        var aDigits = {
            english: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'],
            persian: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
        }
        var aData = JSON.parse('<?php echo json_encode($aTasks); ?>');

        var data = [];
        $.each(aData, function (i, val) {
            data.push({
                date: i, geekbench: parseInt(val),
            });
        });

        Morris.Bar({
            element: 'graph_bar',
            data: data,
            xkey: 'date',
            ykeys: ['geekbench'],
            labels: [''],
            barRatio: 0.4,
            barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
            xLabelAngle: 35,
            hideHover: 'auto'
        });

        // Work Efficiency Chart
        var aData = JSON.parse('<?php echo json_encode($aTime); ?>');
        var sKeys = {'duration': '<?php echo $language_data->__('text_duration'); ?>', 'work_hours': '<?php echo $language_data->__('text_actual_time_spent'); ?>'};
        var aSeries = [];



        var points = [];
        var tname = "";

        $.each(aData, function (j, obj) {

            if (obj.task_name.length > 10) {
                tname = obj.task_name.substr(0, 10) + "..";
            } else {
                tname = obj.task_name;
            }
            if (tname) {
                points.push({
                    label: tname,
                    duration: parseFloat(obj["duration"]),
                    workHours: parseFloat(obj["work_hours"])
                });
            }
        });

        Morris.Bar({
            element: 'graph_line',
            data: points,
            xkey: 'label',
            ykeys: ['duration', 'workHours'],
            labels: ['<?php echo $language_data->__('text_duration'); ?>', '<?php echo $language_data->__('text_actual_time_spent'); ?>'],
            barRatio: 0.4,
            barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
            xLabelAngle: 35,
            hideHover: 'auto'
        });
    });
    function getlatestmap() {
        $(".loadingImg").show();
        var data = $('#searchForm').serialize();
        $.ajax({
            url: "<?= url("partner/employeemap") ?>",
            type: "POST",
            data: data + "&urlPrifix=partner",
            async: false,
            success: function (msg) {
                $("#getlatestmap").html(msg);
                $(".loadingImg").hide();
            }
        });
    }
    $(document).on('click', '.refresh-map', function (e) {
        $(".searchEmp").val("");
        $(".employee_status option").prop("selected", false);
        getlatestmap();
        return false;
    });
    $(document).on('click', '.searchEmpButton', function (e) {
        getlatestmap();
        return false;
    });

    //setInterval(function () {
    getlatestmap();

    //}, 30000);

</script>

<?php
$language_data = new Language();
$keyword = str_replace("'", "", $_REQUEST['keyword']);
// get original model
$fetchMode = DB::getFetchMode();
// set mode to custom
DB::setFetchMode(\PDO::FETCH_ASSOC);
// get data
$where = "";
if ($keyword != "" && strlen($keyword) > 1) {
    $where = ' AND  REPLACE( REPLACE( emp_name, "-", "" ) , "\'", "" )  LIKE "' . trim($keyword) . '%" ';
}

if (isset($_REQUEST['employee_status']) && $_REQUEST['employee_status'] != "") {
    $where .= ' AND  emp_status=' . $_REQUEST['employee_status'];
}

$role = Session::get('role') != '' ? Session::get('role') : '';
if ($role == 2) {
    $uid = Session::get('uid') != '' ? Session::get('uid') : '';
    $empculo = DB::select("SELECT * from  tbl_employee where delete_employee='0' and companyid='$uid' " . $where . " order by emp_id desc");
} else {
    $supervisor_id = Session::get('uid') != '' ? Session::get('uid') : '';
    $uid = Session::get('companyid') != '' ? Session::get('companyid') : '';
    $empculo = DB::select("SELECT * from  tbl_employee where delete_employee='0' and companyid='$uid' AND supervisor_id='$supervisor_id' " . $where . " order by emp_id desc");
}



if (isset($_REQUEST['autorefresh']) && $_REQUEST['autorefresh'] == 'true') {
    $res = array();
    if (!empty($empculo)) {
        foreach ($empculo as $key => $data) {
            $eid = $data['emp_id'];
            $empcurrentlocation1 = DB::select("SELECT * FROM  `employeesurrentlocation` where employeid='$eid' order by id desc");
            $latvalue = $data['lat'];
            $langvalue = $data['lang'];
            if (!empty($empcurrentlocation1)) {
                $empcurrentlocation1 = $empcurrentlocation1[0];
                $latvalue = $empcurrentlocation1['lan'];
                $langvalue = $empcurrentlocation1['long'];
            }
            $empnane = DB::select("SELECT * from  tbl_employee  where emp_id=$eid AND delete_employee = 0 order by emp_id desc ");
            $empnane = $empnane[0];

            $title = '<h5 style="width: 210px; font-family: abel !important;text-align: center;font-size: 17px; ">';
            $title.= '<strong style="color:#000;">' . $empnane['emp_name'] . '</strong><div style="clear:both"></div>"';

            if ($data['availability'] == 2) {
                $title.= "Leave Reason:";
                $title.= $data['leavereason'];
            } else {
                $title.= getAddressNew($latvalue, $langvalue);
            }
            $title.= '"</h5>';
            $image = url('public/uploads/employees/' . $data['profileimage']);
            if ($data['profileimage'] == "" || !file_exists(public_path('uploads/employees/' . $data['profileimage']))) {
                $image = asset('resources/assets/momom-assets/img/user.png');
            }
            $res[$key]['title'] = $title;
            $res[$key]['imgUrl'] = $image;
            $res[$key]['latvalue'] = $latvalue;
            $res[$key]['langvalue'] = $langvalue;
        }
    }

    echo json_encode($res);
    exit;
}

$centerMapLat = "";
$centerMapLong = "";
if (!empty($empculo)) {
    foreach ($empculo as $key => $data) {

        $eid = $data['emp_id'];
        $empcurrentlocation1 = DB::select("SELECT * FROM  `employeesurrentlocation` where employeid='$eid' order by id desc");
        $latvalue = $data['lat'];
        $langvalue = $data['lang'];
        if (!empty($empcurrentlocation1)) {
            $empcurrentlocation1 = $empcurrentlocation1[0];
            $latvalue = $empcurrentlocation1['lan'];
            $langvalue = $empcurrentlocation1['long'];
        }
        if (!empty($latvalue) && !empty($langvalue)) {
            $centerMapLat = $latvalue;
            $centerMapLong = $langvalue;
            break;
        } else {
            $centerMapLat = $data['lat'];
            $centerMapLong = $data['lang'];
        }
    }
}
?>  

<?php if (empty($empculo) && !empty($keyword)) { ?>
    <div class="alert alert-danger fade in">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="fa fa-times"></i>
        </button>
        <strong><?php echo $language_data->__('text_alert'); ?></strong> <?php echo $language_data->__('text_emp_not_found_alert'); ?>
    </div>    

    <?php
}
?>

<div id="map" style="width: 100%; height: 100%;position: relative;"></div>

<script type="text/javascript">
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 3,
        center: new google.maps.LatLng('<?php echo $centerMapLat ?>', '<?php echo $centerMapLong ?>'),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    });
    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    var markersArray = [];

    function getmarker() {
        $(".loadingImg").show();
        var data = $('#searchForm').serialize();
        $.ajax({
            url: "<?= url($_REQUEST['urlPrifix'] . "/employeemap") ?>",
            type: "POST",
            data: data + "&autorefresh=true",
            success: function (markerData) {
                $(".loadingImg").hide();
                if (markerData !== '') {
                    for (i = 0; i < markersArray.length; i++) {
                        markersArray[i].setMap(null);
                    }
                    var markers = $.parseJSON(markerData);
                    //console.log(markers);
                    for (var i = 0; i < markers.length; i++) {
                        var lat = markers[i]['latvalue'];
                        var lng = markers[i]['langvalue'];
                        var icon = {
                            url: markers[i]['imgUrl'],
                            scaledSize: new google.maps.Size(37, 37),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(0, 0)
                        };
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat, lng),
                            map: map,
                            icon: icon
                        });
                        markersArray.push(marker);
                        //bounds.extend(new google.maps.LatLng(lat, lng));
                        //map.fitBounds(bounds);
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(markers[i]['title']);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                    }
                }
            }
        });
    }
    google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
     if (this.getZoom()){
     this.setZoom(8);
     }
     });
    getmarker();
    setInterval(function () {
        getmarker();
    }, 30000);

</script>
<?php
DB::setFetchMode($fetchMode);
?>

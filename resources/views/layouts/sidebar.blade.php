<div class="side-bar">
    <ul class="topnav menu-left-nest">
        <?php
        $role=Session::get('role')!='' ? Session::get('role') : '';
        $language_data=new Language();
        if($role=='100'){
        ?>
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('partners/*') && !Request::is('partners/setting') ?'active':'' }}" href="{{ url('/')  }}/partners/index" title="{{ $language_data->__('text_partners')  }}">
                <i class="icon-document-edit"></i>
                <span>{{ $language_data->__('text_partners')  }}</span>
            </a>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('partners/setting') ?'active':'' }}" href="{{ url('/')  }}/partners/setting" title="{{ $language_data->__('text_settings')  }}">
                <i class="icon-feed"></i>
                <span>{{ $language_data->__('text_settings')  }}</span>
            </a>
        </li>
        <?php
        }
        ?>
        <?php
        if($role=='2'){
        ?>
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('partner') || Request::is('partner/index') ?'active':'' }}" href="{{ url('/')  }}/partner/index" title="{{ $language_data->__('text_dashboard')  }}">
                <i class="icon-window"></i>
                <span>{{ $language_data->__('text_dashboard')  }}</span>
            </a>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('partner/clients') || Request::is('partner/clients/*')?'active':'' }}" href="{{ url('/')  }}/partner/clients" title="{{ $language_data->__('text_client')  }}">
                <i class="icon-user"></i>
                <span>{{ $language_data->__('text_client')  }}</span>
            </a>
        </li>
        
        
        
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('partner/supervisors') || Request::is('partner/supervisors/*')?'active':'' }}" href="{{ url('/')  }}/partner/supervisors" title="{{ $language_data->__('text_supervisors')  }}">
                <i class="icon-user"></i>
                <span>{{ $language_data->__('text_supervisors')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('partner/employees') || Request::is('partner/employees/*')?'active':'' }}" href="{{ url('/')  }}/partner/employees" title="{{ $language_data->__('text_employees')  }}">
                <i class="icon-user-group"></i>
                <span>{{ $language_data->__('text_employees')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('partner/category') || Request::is('partner/category/*'))?'active':'' }}" href="{{ url('/')  }}/partner/category" title="{{ $language_data->__('text_categories')  }}">
                <i class="icon-tag"></i>
                <span>{{ $language_data->__('text_categories')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('partner/task') || Request::is('partner/task/*'))?'active':'' }}" href="{{ url('/')  }}/partner/task" title="{{ $language_data->__('text_task_list')  }}">
                <i class="icon-document-new"></i>
                <span>{{ $language_data->__('text_task_list')  }}</span>
            </a>
        </li>
        
        
<!--        <li>
            <a class="tooltip-tip ajax-load" href="{{ url('/')  }}/partner/task/create" title="{{ $language_data->__('text_new_task')  }}">
                <i class="icon-document-new"></i>
                <span>{{ $language_data->__('text_new_task')  }}</span>
            </a>
        </li>-->
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('partner/timesheet') || Request::is('partner/timesheet/*'))?'active':'' }}" href="{{ url('/')  }}/partner/timesheet" title="{{ $language_data->__('text_task_list')  }}">
                <i class="icon-user"></i>
                <span>{{ $language_data->__('text_time_sheet')  }}</span>
            </a>
        </li>
        
        
        <li>
            <a class="tooltip-tip {{ (Request::is('partner/report/*'))?'active':'' }}" href="#" title="{{ $language_data->__('text_reports')  }}">
                <i class="icon-document"></i>
                <span>{{ $language_data->__('text_reports')  }}</span>
            </a>
            <ul>
                 <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('supervisor/report/activeuser'))?'active':'' }}" title="{{ $language_data->__('text_active_users') }}" href="{{ url('/')  }}/supervisor/report/activeuser"><span><?php echo $language_data->__('text_active_users'); ?></span></a>
                </li>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('supervisor/report/workreport'))?'active':'' }}" title="{{ $language_data->__('text_work_reports') }}" href="{{ url('/')  }}/supervisor/report/workreport"><span><?php echo $language_data->__('text_work_reports'); ?></span></a>
                </li>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('supervisor/report/timereport'))?'active':'' }}" title="{{ $language_data->__('text_time_report') }}" href="{{ url('/')  }}/supervisor/report/timereport"><span><?php echo $language_data->__('text_time_report'); ?></span></a>
                </li>
                <?php
                if(!empty(Session::get('superadmin')) && Session::get('superadmin') > 0){
                ?>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('partner/report/surveyreport'))?'active':'' }}" title="{{ $language_data->__('text_survey_report') }}" href="{{ url('/')  }}/partner/report/surveyreport"><span><?php echo $language_data->__('text_survey_report'); ?></span></a>
                </li>
                <?php
                }
                ?>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('partner/report/logs'))?'active':'' }}" title="{{ $language_data->__('text_logs') }}" href="{{ url('/')  }}/partner/report/logs"><span><?php echo $language_data->__('text_logs'); ?></span></a>
                </li>
            </ul>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('partner/setting') || Request::is('partner/setting/*'))?'active':'' }}" href="{{ url('/')  }}/partner/setting" title="{{ $language_data->__('text_settings')  }}">
                <i class="fontawesome-cogs"></i>
                <span>{{ $language_data->__('text_settings')  }}</span>
            </a>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('partner/taskbank') || Request::is('partner/taskbank/*'))?'active':'' }}" href="{{ url('/')  }}/partner/taskbank" title="{{ $language_data->__('text_task_bank')  }}">
                <i class="entypo-progress-3"></i>
                <span>{{ $language_data->__('text_task_bank')  }}</span>
            </a>
        </li>
        <?php
        }
        ?>
        <?php
        if($role=='3'){
        ?>
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('supervisor') || Request::is('supervisor/index') ?'active':'' }}" href="{{ url('/')  }}/supervisor/index" title="{{ $language_data->__('text_dashboard')  }}">
                <i class="icon-window"></i>
                <span>{{ $language_data->__('text_dashboard')  }}</span>
            </a>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('supervisor/clients') || Request::is('supervisor/clients/*')?'active':'' }}" href="{{ url('/')  }}/supervisor/clients" title="{{ $language_data->__('text_client')  }}">
                <i class="icon-user"></i>
                <span>{{ $language_data->__('text_client')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip ajax-load {{ Request::is('supervisor/employees') || Request::is('supervisor/employees/*')?'active':'' }}" href="{{ url('/')  }}/supervisor/employees" title="{{ $language_data->__('text_employees')  }}">
                <i class="icon-user-group"></i>
                <span>{{ $language_data->__('text_employees')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('supervisor/category') || Request::is('supervisor/category/*'))?'active':'' }}" href="{{ url('/')  }}/supervisor/category" title="{{ $language_data->__('text_categories')  }}">
                <i class="icon-tag"></i>
                <span>{{ $language_data->__('text_categories')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('supervisor/task') || Request::is('supervisor/task/*'))?'active':'' }}" href="{{ url('/')  }}/supervisor/task" title="{{ $language_data->__('text_task_list')  }}">
                <i class="icon-user"></i>
                <span>{{ $language_data->__('text_task_list')  }}</span>
            </a>
        </li>
        
        
<!--        <li>
            <a class="tooltip-tip ajax-load" href="{{ url('/')  }}/partner/task/create" title="{{ $language_data->__('text_new_task')  }}">
                <i class="icon-document-new"></i>
                <span>{{ $language_data->__('text_new_task')  }}</span>
            </a>
        </li>-->
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('partner/timesheet') || Request::is('partner/timesheet/*'))?'active':'' }}" href="{{ url('/')  }}/partner/timesheet" title="{{ $language_data->__('text_task_list')  }}">
                <i class="icon-user"></i>
                <span>{{ $language_data->__('text_time_sheet')  }}</span>
            </a>
        </li>
        
        <li>
            <a class="tooltip-tip {{ (Request::is('supervisor/report/*'))?'active':'' }}" href="#" title="{{ $language_data->__('text_reports') }}">
                <i class="icon-document"></i>
                <span>{{ $language_data->__('text_reports')  }}</span>
            </a>
            <ul>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('supervisor/report/activeuser'))?'active':'' }}" title="{{ $language_data->__('text_active_users') }}" href="{{ url('/')  }}/supervisor/report/activeuser"><span><?php echo $language_data->__('text_active_users'); ?></span></a>
                </li>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('supervisor/report/workreport'))?'active':'' }}" title="{{ $language_data->__('text_work_reports') }}" href="{{ url('/')  }}/supervisor/report/workreport"><span><?php echo $language_data->__('text_work_reports'); ?></span></a>
                </li>
                <li>
                    <a class="tooltip-tip2 ajax-load {{ (Request::is('supervisor/report/timereport'))?'active':'' }}" title="{{ $language_data->__('text_time_report') }}" href="{{ url('/')  }}/supervisor/report/timereport"><span><?php echo $language_data->__('text_time_report'); ?></span></a>
                </li>
            </ul>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('supervisor/setting') || Request::is('supervisor/setting/*'))?'active':'' }}" href="{{ url('/')  }}/supervisor/setting" title="{{ $language_data->__('text_settings')  }}">
                <i class="fontawesome-cogs"></i>
                <span>{{ $language_data->__('text_settings')  }}</span>
            </a>
        </li>
        <li>
            <a class="tooltip-tip ajax-load {{ (Request::is('supervisor/taskbank') || Request::is('supervisor/taskbank/*'))?'active':'' }}" href="{{ url('/')  }}/supervisor/taskbank" title="{{ $language_data->__('text_task_bank')  }}">
                <i class="entypo-progress-3"></i>
                <span>{{ $language_data->__('text_task_bank')  }}</span>
            </a>
        </li>
        <?php
        }
        ?>
        <?php
        /*
        ?>
        @if(isset($sidebar_data))
            @foreach($sidebar_data as $sidebardata)
                <li>
                    <a class="tooltip-tip ajax-load" href="{{ url('/')  }}{{ $sidebardata['link'] }}" title="Blog App">
                        <i class="{{ $sidebardata['icon']  }}"></i>
                        <span>{{ $sidebardata['label']  }}</span>

                    </a>
                </li>
            @endforeach
        @endif
        <?php
        */
        ?>
    </ul>
    <div id="g1" style="height:180px" class="gauge hide"></div>
</div>

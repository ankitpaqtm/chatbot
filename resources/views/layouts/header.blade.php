<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 11/1/17
 * Time: 2:15 PM
 */

?>
{{--TODO::set language value in session code start--}}
<?php
if(isset($_POST['language']))
{
    $language=$_POST['language'];
    Session::put('language',$language);
    $language_data=new Language();
}
else{
    $language=Session::get('language')!='' ? Session::get('language') : '';
    if (isset($language) && $language!='') {
        $language_data=new Language();
    }
    else{
        $language='persian';
        Session::put('language',$language);
        $language_data=new Language();
    }
}
$language_data=new Language();
?>
{{--TODO::set language value in session code End--}}
<!DOCTYPE html>
<html lang="en">
<head>
    @php
    $language=Session::get('language')!='' ? Session::get('language') : 'english';
    @endphp
    <meta charset="utf-8">
    <title>{{ $language_data->__('text_neerukaar') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Le styles -->
    <script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/css/loader-style.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/css/signin.css') }}">

    <link href="{{ asset('resources/assets/momom-assets/js/footable/css/footable.core.css?v=2-0-1') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('resources/assets/momom-assets/js/footable/css/footable.standalone.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('resources/assets/momom-assets/js/footable/css/footable-demos.css') }}" rel="stylesheet"
          type="text/css"/>

    <link rel="stylesheet"
          href="{{ asset('resources/assets/momom-assets/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css') }}"/>
    <link rel="stylesheet"
          href="{{ asset('resources/assets/momom-assets/js/dataTable/css/datatables.responsive.css') }}"/>

    <link rel="stylesheet" type="text/css"
          href="{{ asset('resources/assets/momom-assets/js/progress-bar/number-pb.css') }}">

    <link type="text/css" href="{{ asset('resources/assets/momom-assets/js/validate/validate.css') }}" rel="stylesheet" />
    {{--TODO::Style sheet for persian language--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/momom-assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/momom-assets/css/custom.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/momom-assets/js/wizard/css/jquery.steps.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/momom-assets/js/wizard/css/jquery.steps.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/momom-assets/js/wizard/jquery.stepy.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/assets/momom-assets/js/tabs/acc-wizard.min.css')}}" />
    
    <link href="{{ asset('resources/assets/momom-assets/js/iCheck/flat/all.css') }}" rel="stylesheet" />
    <link href="{{ asset('resources/assets/momom-assets/js/iCheck/line/all.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/js/timepicker/bootstrap-timepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/js/datepicker/clockface.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/momom-assets/js/datepicker/bootstrap-datetimepicker.min.css') }}">
    
    {{--TODO::Style sheet for persian language end--}}
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="{{ asset('resources/assets/momom-assets/ico/minus.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC7pr3lK9tR2pAUmJZ4DzVE289W-LB98qQ&language={{ ($language=='persian'?'fr' : 'en') }}" type="text/javascript"></script>
</head>

{!! Form::open(['class'=>'changelangform']) !!}
    <input type="hidden" id="language" class="changelang" name="language" value="{{ ($language=='persian'?'persian' : 'english') }}" />
{!! Form::close() !!}

<?php $uid=Session::get('uid')!='' ? Session::get('uid') : ''; ?>

@if(isset($uid) && $uid!='')

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<input type="hidden" name="uri"  id="uri" value="{{ url("/")}}" >
<!-- TOP NAVBAR -->
<nav role="navigation" class="navbar navbar-static-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle"
                    type="button">
                <span class="entypo-menu"></span>
            </button>
            <div id="logo-mobile" class="visible-xs">
                <h1>
                    <img class="logo_img" src="{{ Helper::getuserlogo() }}" alt="mowom logo" />
                </h1>
            </div>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            
            <div id="nt-title-container" class="navbar-left running-text visible-lg">
                <ul class="date-top">
                    <li class="entypo-calendar" style="margin-right:5px"></li>
                    <li id="Date"></li>
                </ul>
                <ul id="digital-clock" class="digital">
                    <li class="entypo-clock" style="margin-right:5px"></li>
                    <li class="hour"></li>
                    <li>:</li>
                    <li class="min"></li>
                    <li>:</li>
                    <li class="sec"></li>
                    <li class="meridiem"></li>
                </ul>
            </div>
            <ul class="nav navbar-nav">
                <?php
                if(Session::get('role')!="100"){
                ?>
                <li class="dropdown" id="ajaxGetNotification"></li>
                <script type="text/javascript">
                $('#notificationCountBellId').click(function(){
                    console.log("click");
                });    
                function addNotification() {
                    $.ajax({
                        url: "<?= url("getnotification") ?>",
                        type: "POST",
                        data: "_token=<?=csrf_token()?>",
                        success: function (msg) {
                            $("#ajaxGetNotification").html(msg);
                        }
                    });
                }
                addNotification();
                setInterval(function () {
                    //addNotification();
                }, 30000);
                </script>
                
                <?php
                }
                if(!empty(Session::get('superadmin')) && Session::get('superadmin') > 0 && Session::get('role')=="2"){
                ?>
                <li>
                    <div class="headeralert">
                        <div>
                            <strong>
                                <?php
                                    $userName = str_replace(" ","",substr(Helper::getusername(),0,4));
                                    echo sprintf($language_data->__('text_partner_login'),'"'.$userName.'.."');
                                ?>
                            </strong>
                            <a class="btn btn-small btn-success" href="{{ url('partners/adminmode/'.Session::get('superadmin'))}}" title="{{ $language_data->__('text_super_admin_login') }}">
                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
            <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                <!--<li>
                    <select class="dd-lang">
                        <option value="english" {{ $language=='english'?'selected=selected' : '' }}>English</option>
                        <option value="persian"  {{ $language=='persian'?'selected=selected' : '' }}>فارسی</option>
                    </select>
                </li>-->
                
                <li>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" class="admin-pic img-circle"
                             src="{{ asset('resources/assets/momom-assets/img/user.png') }}">{{ $language_data->__('text_hi') }}, {{ Helper::getusername() }}
                        <b class="caret"></b>
                    </a>
                    <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                        <!--<li>
                            <a href="#">
                                <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('/logout') }}">
                                <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                        </li>-->
                        <li>
                            <a href="{{ url('/logout') }}">
                                <span class="entypo-logout"></span>&#160;&#160;{{ $language_data->__('text_logout') }}</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-language"></i>  {{ $language=='english'?'English' : 'French' }}<b class="caret"></b></a>
                    <ul role="menu" class="dropdown-setting dropdown-menu">
                        <li class="theme-bg">
                            <a href="javascript:;" class="dd-lang" data-id="english"><span class="{{ $language=='english'?'fa fa-check' : '' }}"></span>&#160;&#160;English</a>
                        </li>
                        <li class="theme-bg">
                            <a href="javascript:;" class="dd-lang" data-id="persian"><span class="{{ $language=='persian'?'fa fa-check' : '' }}"></span>&#160;&#160;French</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!-- /END OF TOP NAVBAR -->
<!-- SIDE MENU -->
<div id="skin-select">
    <div id="logo">
        <h1 style="text-align: center;"><img style="width: auto;height: 100px;" class="logo_img" src="{{ Helper::getuserlogo() }}" alt="mowom logo">
        </h1>
    </div>
    <a id="toggle">
        <span class="entypo-menu"></span>
    </a>
    <div class="dark">
    </div>
    <div class="search-hover">
        <form id="demo-2">
            <input type="search" placeholder="{{ $language_data->__('text_search_menu')  }}" class="id_search">
        </form>
    </div>
    <div class="skin-part">
        <div id="tree-wrap">
            @include('layouts.sidebar')
        </div>
    </div>
</div>

@else

<nav role="navigation" class="navbar navbar-static-top">
    <div class="container-fluid">
       
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            
            <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                 <li>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-language"></i>  {{ $language=='english'?'English' : 'French' }}<b class="caret"></b></a>
                    <ul role="menu" class="dropdown-setting dropdown-menu">
                        <li class="theme-bg">
                            <a href="javascript:;" class="dd-lang" data-id="english"><span class="{{ $language=='english'?'fa fa-check' : '' }}"></span>&#160;&#160;English</a>
                        </li>
                        <li class="theme-bg">
                            <a href="javascript:;" class="dd-lang" data-id="persian"><span class="{{ $language=='persian'?'fa fa-check' : '' }}"></span>&#160;&#160;French</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

@endif
<!-- END OF SIDE MENU -->
<body>

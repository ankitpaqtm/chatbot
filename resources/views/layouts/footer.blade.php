@php
    $language_data=new Language();
    $language=Session::get('language')!='' ? Session::get('language') : 'english';
@endphp
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.core.min.js" integrity="sha256-Wlf9b6CzHOO4n//P7c9i9hTybRVXYrvs2SkWHEEZLi8=" crossorigin="anonymous"></script>
<script src="{{ asset('resources/assets/momom-assets/js/progress-bar/src/jquery.velocity.min.js') }}"></script>
<script src="{{ asset('resources/assets/momom-assets/js/progress-bar/number-pb.js') }}"></script>
<script src="{{ asset('resources/assets/momom-assets/js/progress-bar/progress-app.js') }}"></script>

<!-- MAIN EFFECT -->
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/preloader.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/load.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/main.js') }}"></script>

<!-- /MAIN EFFECT -->
<!-- GAGE -->
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/toggle_close.js') }}"></script>
<script src="{{ asset('resources/assets/momom-assets/js/footable/js/footable.js?v=2-0-1') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/footable/js/footable.sort.js?v=2-0-1') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/footable/js/footable.filter.js?v=2-0-1') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/footable/js/footable.paginate.js?v=2-0-1') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/footable/js/footable.paginate.js?v=2-0-1') }}" type="text/javascript"></script>

<!-- Moris Chart -->
<script src="{{ asset('resources/assets/momom-assets/js/moris/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/moris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/moris/example.js') }}" type="text/javascript"></script>
<!-- End Moris Chart -->

<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/calendar/jalali.js') }}"></script>

<!-- import the calendar script -->
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/calendar/calendar.js') }}"></script>

<!-- import the calendar script -->
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/calendar/calendar-setup.js') }}"></script>

<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/iCheck/jquery.icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/switch/bootstrap-switch.js') }}"></script>

<!-- import the language module -->

<link rel="stylesheet" type="text/css" media="all" href="{{ asset('resources/assets/momom-assets/calendar/css/theme.css') }}" title="Aqua" />
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/map/gmap3.js') }}"></script>
<!--<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/calendar/lang/calendar-'.($language=='persian'?'fa' : 'en').'.js') }}"></script>-->
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/calendar/lang/calendar-en.js') }}"></script>
{{--TODO::Jalali and english Calender Code End--}}
<script src="{{ asset('resources/assets/momom-assets/js/custom.js?v=2-0-2') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/wizard/build/jquery.steps.js') }}" type="text/javascript"></script>
<script src="{{ asset('resources/assets/momom-assets/js/wizard/jquery.stepy.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/timepicker/bootstrap-timepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/datepicker/clockface.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/datepicker/bootstrap-datetimepicker.js') }}"></script>

<ul id="breadcrumb">
    <li>
        <a href="{{ url($urlPrifix.'/index') }}" title="{{ $language_data->__('text_dashboard') }}">
            <span class="entypo-home"></span>
        </a>    
    </li>
    <?php
    $currentPathText = Request::path();
    if(!empty($currentPathText)){
    ?>
        <li><i class="fa fa-lg fa-angle-right"></i></li>
        <?php
        $currentPathDatas = explode('/', $currentPathText);
        //pr($currentPathDatas);
        $i=0;
        foreach ($currentPathDatas as $currentPathData){
        $i++;
            if($currentPathData != $urlPrifix){
            ?>
                <li>
                    <a href="javascript:;" title="{{ $currentPathData }}">
                        {{ $currentPathData }}
                    </a>
                </li>
                <?php
                if($i != count($currentPathDatas)){
                ?>
                    <li><i class="fa fa-lg fa-angle-right"></i></li>
                <?php
                }
            }
        }
    }
    ?>
</ul>
<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */

?>

@include('layouts.header')
@php
    $language_data=new Language();
@endphp
<body>



<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_partners') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <h6>
                                {{ $language_data->__('text_list_of_partners') }}
                                </h6>
                            <div class="titleClose">
                                <a class="gone" href="#FootableClose">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#Footable">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>

                        </div>
                        <div class="body-nest" id="Filtering">
                            <div class="row" style="margin-bottom:10px;">
                                <div class="col-sm-4">
                                    <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-6">
                                    <a href="{{ url('/partners/create/') }}" style="margin-left:10px;" class="pull-right btn btn-info " title="clear filter">{{ $language_data->__('text_add_partner') }}</a>
                                </div>
                            </div>
                        </div>

                        <div class="body-nest" id="Footable">


                            <table class="table-striped footable-res footable metro-blue" data-page-size="<?=paginationSize()?>" data-filter="#filter" data-filter-text-only="true">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_client_name') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_email') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_address') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_contact') }}
                                    </th>
                                    <th>
                                        {{ $language_data->__('text_status') }}
                                    </th>
                                    <th width="23%">
                                        {{ $language_data->__('text_action') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>

                                @foreach($partner_data as $partnerdata)
                                    <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td>{{ $partnerdata->company_name }}</td>
                                        <td>{{ $partnerdata->email_id }}</td>
                                        <td>{{ $partnerdata->address }}</td>
                                        <td>{{ $partnerdata->mobile }}</td>
                                        <td class="text-center"><?= $partnerdata->status=='1' ? '<span class="status-metro status-active" title="'.$language_data->__('text_active').'">'.$language_data->__('text_active').'</span>' : '<span class="status-metro status-disabled" title="'.$language_data->__('text_inactive').'">'.$language_data->__('text_inactive').'</span>' ?> </td>
                                        <td>
                                            <a class="btn btn-small btn-info"
                                                href="{{ url('partners/update/'.$partnerdata->partnerid)}}"
                                                title="{{ $language_data->__('text_edit_partner') }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-warning" href="<?= $partnerdata->status=='1' ? url('partners/status/'.$partnerdata->partnerid.'?status=0') : url('partners/status/'.$partnerdata->partnerid.'?status=1')?>" onclick="return confirm('<?= $partnerdata->status=='1' ? $language_data->__('text_delete_partner_confirm_inactive') : $language_data->__('text_delete_partner_confirm_active') ?>')" title="{{ $language_data->__('text_inactive_partner') }}">
                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-default"
                                                href="{{ url('partners/show/'.$partnerdata->partnerid)}}"
                                                title="{{ $language_data->__('text_view_partner') }}">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-success" href="{{ url('partners/partnermode/'.$partnerdata->partnerid)}}" title="{{ $language_data->__('text_enter_partner_mode') }}">
                                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                                            </a>
                                            <a class="btn btn-small btn-danger" onclick="return confirm('{{ $language_data->__('text_delete_partner_confirm') }}')" href="{{ url('partners/destroy/'.$partnerdata->partnerid)}}" title="{{ $language_data->__('text_delete_partner') }}">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>

                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="pagination pagination-centered"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');


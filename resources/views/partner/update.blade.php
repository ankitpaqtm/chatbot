<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_partners') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <h6>
                                {{ $language_data->__('text_add_partner') }} </h6>
                            <div class="titleClose">
                                <a class="gone" href="#FootableClose">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#Footable">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>

                        </div>


                        <div class="body-nest" id="element">
                            <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partners/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                        <input type="hidden" name="partnerid" value="{{ $partner_data[0]->partnerid }}" />
                                            <input type="text" class="form-control" id="company_name" name="company_name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" value="{{ $partner_data[0]->company_name }}">
                                            <label for="company_name" id="company_name_error" generated="true" class="error">{{ $errors->first('company_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_location_address') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea name="address" tabindex="2" id="address" required    style="margin-bottom:10px;" class="form-control address" >{{ $partner_data[0]->address }}</textarea>
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>
                                            <input type="hidden" id="latitude" name="latitude" value="" >
                                            <input type="hidden" id="longitude" name="longitude" value="" >

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 pull-left">
                                        <div id="myMap1"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_logo') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <img src="{{ url('public/uploads/logos/').'/'.$partner_data[0]->logo}}" alt="" height="100" width="100" />
                                            <input type="file"  tabindex="3" style="margin-bottom:10px;height:auto;"    placeholder="" autocomplete="off" name="clientlogo"  class="form-control round-input imgLogo" >
                                            <label for="address" id="clientlogo_error" generated="true" class="error">{{ $errors->first('clientlogo') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" tabindex="4" required placeholder="" value="{{$partner_data[0]->email_id}}" id="email_id" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$"   name="email_id"  class="form-control placeholder-no-fix emailId">
                                            <label for="address" id="email_id_error" generated="true" class="error">{{ $errors->first('email_id') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_admin_username') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="5" style="margin-bottom:10px;" required  value="{{$partner_data[0]->admin_usernmae}}"    id="admin_username"   name="admin_username"   placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
                                            <label for="address" id="admin_username_error" generated="true" class="error">{{ $errors->first('admin_username') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_admin_password') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input tabindex="6" type="password" style="margin-bottom:10px;" name="admin_password" id="admin_password"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
                                            <label for="address" id="admin_password_error" generated="true" class="error"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="7"  maxlength="16"  style="margin-bottom:10px;" required      name="mobile" id="mobile"  value="{{$partner_data[0]->mobile}}"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="contact_number" id="contact_number_error" generated="true" class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_employees_allowed') }} *</span></label>
                                        <div class="col-sm-6">
                                            <input type="number" tabindex="8" min="0" style="margin-bottom:10px;" required   name="employeecount" id="employeecount"  value="{{$partner_data[0]->employeecount}}"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix onlyNumberPositive employeecount">
                                            <label for="employeecount" id="employeecount_error" generated="true" class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_supervisors_allowed') }} *</span></label>
                                        <div class="col-sm-6">
                                            <input type="number" tabindex="9" min="0" style="margin-bottom:10px;"required  pattern="[0-9]" id="supervisorcount"   name="supervisorcount"  value="{{$partner_data[0]->supervisorcount}}"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix onlyNumberPositive">
                                            <label for="supervisorscount" id="supervisorcount_error" generated="true" class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_tasks_allowed') }} *</span></label>
                                        <div class="col-sm-6">
                                            <input type="number" tabindex="10" min="0"  style="margin-bottom:10px;"required   name="taskcount"  value="{{$partner_data[0]->taskcount}}"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix onlyNumberPositive" id="taskcount">
                                            <label for="taskcount" id="taskcount_error" generated="true" class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_clients_allowed') }} *</span></label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0"  tabindex="11" style="margin-bottom:10px;"required   name="clinetscount"  value="{{$partner_data[0]->clinetscount}}"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix onlyNumberPositive" id="clinetscount">
                                            <label for="clinetscount" id="clinetscount_error" generated="true" class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_duedate') }} *</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" tabindex="12" id="jalali_english_calendar"  style="margin-bottom:10px;" required   name="due_date" readonly=""  value="{{$partner_data[0]->due_date}}"  placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
                                            <label for="jalali_english_calendar" id="jalali_english_calendar_error" generated="true" class="error"></label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-top:0px;" for="flat-checkbox-report">{{ $language_data->__('text_is_report') }}</label>
                                        <div class="col-sm-6">
                                            <div class="skin skin-report">
                                                <ul class="" style="padding: 0;list-style: none;">
                                                    <li>
                                                        
                                                            <input class="multiLocationSectionm" <?= ($partner_data[0]->is_report == 1)? "checked='checked'":"" ?>  type="checkbox" name="is_report" value="1" id="flat-checkbox-report">
                                                        
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-top:0px;" for="flat-checkbox-review">{{ $language_data->__('text_is_review') }}</label>
                                        <div class="col-sm-6">
                                            <div class="skin skin-review">
                                                <ul class="" style="padding: 0;list-style: none;">
                                                    <li>
                                                            <input class="multiLocationSectionm" <?= ($partner_data[0]->is_review == 1)? "checked='checked'":"" ?>  type="checkbox" name="is_review" value="1" id="flat-checkbox-review">
                                                        
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-top:0px;" for="flat-checkbox-signature">{{ $language_data->__('text_is_signature') }}</label>
                                        <div class="col-sm-6">
                                            <div class="skin skin-signature">
                                                <ul class="" style="padding: 0;list-style: none;">
                                                    <li>
                                                            <input class="multiLocationSectionm"  type="checkbox" <?= ($partner_data[0]->is_signature == 1)? "checked='checked'":"" ?> name="is_signature" value="1" id="flat-checkbox-signature">
                                                        
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>

                                    <button class="btn btn-info submitPartner" name="add_partner" id="add_partner" type="submit">{{ $language_data->__('text_submit') }}</button>
                                {!! Form::close() !!}
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partner.validation');
<script type="text/javascript">
    /*
     TODO::Calender js code start
     */
    var language=$("#language").val();
    if(language=='english')
    {
        Calendar.setup({
            inputField: "jalali_english_calendar", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
    }
    else{
        Calendar.setup({
            inputField: "jalali_english_calendar", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
    }

    /*
     TODO::Calender js code end
     */
    $('.skin-report input').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
    });
    $('.skin-review input').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
    });
    $('.skin-signature input').iCheck({
        checkboxClass: 'icheckbox_flat-blue'
    });
</script>





<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_settings') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-2">
                                <h6>{{ $language_data->__('text_edit_settings') }} </h6>
                            </label>
                        </div>


                        <div class="body-nest" id="element">
                            <div class="panel-body">
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partners/storesetting']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_user_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" value="<?= $superadmin_data->username;?>" id="adminuname" name="adminuname" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" placeholder="<?php echo $language_data->__('text_user_name'); ?>">
                                            <label for="adminuname" id="adminuname_error" generated="true" class="error">{{ $errors->first('adminuname') }}</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_old_pwd') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="password" required id="admin_password" name="admin_password" placeholder="<?php echo $language_data->__('text_old_pwd'); ?>" autocomplete="off"  class="form-control placeholder-no-fix admin_password">
                                            <label for="admin_password" id="admin_password_error" generated="true" class="error">{{ $errors->first('admin_password') }}</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_new_pwd') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="password" required  name="adminpass" id="adminpass" placeholder="<?php echo $language_data->__('text_new_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix new_password">
                                            <label for="address" id="adminpass_error" generated="true" class="error">{{ $errors->first('adminpass') }}</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_re_enter_pwd') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="password" required  name="reenter_password" id="reenter_password" placeholder="<?php echo $language_data->__('text_re_enter_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix reenter_password">
                                            <label for="reenter_password" id="reenter_password_error" generated="true" class="error">{{ $errors->first('reenter_password') }}</label>
                                        </div>
                                    </div>
                                    
                                    <button class="btn btn-info submitPartner" name="add_partner" id="add_partner" type="submit">{{ $language_data->__('text_submit') }}</button>
                                {!! Form::close() !!}
                            </div>

                        </div>

                    </div>


                </div>

            </div>
         </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partner.validation');






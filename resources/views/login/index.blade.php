<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 11/1/17
 * Time: 2:36 PM
 */

?>
@include('layouts.header')
@php
    $language_data=new Language();
@endphp
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<div class="container">
    <div class="" id="login-wrapper">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div id="logo-login" class="text-center">
                    <h1>
                        <img style="height: 100px;width: 200px;" src="<?= asset('resources/assets/momom-assets/img/mowomlogo.png') ?>" />
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                
                <div class="account-box">
                    @if(Session::has('msg'))
                        <div class="alert {!!Session::get('alert')!!}">
                            <a class="close" data-dismiss="alert">×</a>
                            <strong>{!!Session::get('msg')!!}</strong> 
                        </div>
                    @endif
                    <form role="form" method="post" action="{{ url('/login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="inputUsernameEmail">{{ $language_data->__('text_login_label_email') }}</label>
                            <input type="text" id="inputUsernameEmail" name="email" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">{{ $language_data->__('text_password') }}</label>
                            <input type="password" id="inputPassword" name="password" value="" class="form-control">
                        </div>
                        <div class="pull-left">
                            
                                <!--<input type="checkbox">Remember me-->
                            <a href="#" class="pull-right label-forgot">{{ $language_data->__('text_forgot_password') }}</a>
                            
                        </div>
                        <button class="btn btn btn-primary pull-right" type="submit">
                            {{ $language_data->__('text_log_in')  }}
                        </button>
                    </form>
                    <!--<a class="forgotLnk" href="index.html"></a>-->
                    <div class="or-box">

                        <!--<center><span class="text-center login-with">Login or <b>Sign Up</b></span></center>-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="test1" class="gmap3"></div>
@include('layouts.footer');
<script type="text/javascript">
    $(function() {

        $("#test1").gmap3({
            marker: {
                latLng: [52.500556, 13.398889],
                options: {
                    draggable: true
                },
                events: {
                    dragend: function(marker) {
                        $(this).gmap3({
                            getaddress: {
                                latLng: marker.getPosition(),
                                callback: function(results) {
                                    var map = $(this).gmap3("get"),
                                        infowindow = $(this).gmap3({
                                            get: "infowindow"
                                        }),
                                        content = results && results[1] ? results && results[1].formatted_address : "no address";
                                    if (infowindow) {
                                        infowindow.open(map, marker);
                                        infowindow.setContent(content);
                                    } else {
                                        $(this).gmap3({
                                            infowindow: {
                                                anchor: marker,
                                                options: {
                                                    content: content
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }
            },
            map: {
                options: {
                    zoom: 15
                }
            }
        });

    });
    </script>
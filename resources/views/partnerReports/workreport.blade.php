@include('layouts.header')
@php
$language_data=new Language();
@endphp
<?php
$fetchMode = DB::getFetchMode();
DB::setFetchMode(\PDO::FETCH_ASSOC);
if ($supervisor_id > 0) {
    $reports_query = "select et.task_id, et.et_id,et.et_duedate,et.task_starttime,t.duration,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_cat, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, e.emp_name,et.emp_id FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and c.companyid=$uid and et.status !=-1 and t.task_delete=0 and et.supervisor_id='$supervisor_id' ";
} else {
    $reports_query = "select et.task_id, et.et_id,et.et_duedate,et.task_starttime,t.duration,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_cat, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours,e.availability, e.emp_name,et.emp_id FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and c.companyid=$uid and et.status !=-1 and t.task_delete=0 ";
}
if (isset($_POST['search'])) {
    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {

//        if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") {
//            $arrFrom = explode("-", $_POST["date_from"]);
//            $arrTo = explode("-", $_POST["date_to"]);
//            $txtDateFrom = jalali_to_gregorian($arrFrom[0], $arrFrom[1], $arrFrom[2], "-");
//            $txtDateTo = jalali_to_gregorian($arrTo[0], $arrTo[1], $arrTo[2], "-");
//        } else {
            $txtDateFrom = $_POST['date_from'];
            $txtDateTo = $_POST['date_to'];
        //}
        $reports_query .= " and ( et.task_starttime >='" . $txtDateFrom . " 00:00:00' and et.task_endtime <= '" . $txtDateTo . " 23:59:59') ";
    }
    if (!empty($_POST['category'])) {
        $reports_query .= " and t.task_cat = " . $_POST['category'];
    }
    if (!empty($_POST['employee'])) {
        $reports_query .= " and et.emp_id = " . $_POST['employee'];
    }
    if (!empty($_POST['client'])) {
        $reports_query .= " and et.clientid= " . $_POST['client'];
    }
    if (!empty($_POST['task'])) {
        $reports_query .= " and et.task_id = " . $_POST['task'];
    }
    if ($_POST['task_status'] != -2 and $_POST['task_status'] != 10) {
        $reports_query .= " and et.status = " . $_POST['task_status'] . " and et.started =  0";
    } else if ($_POST['task_status'] == 10) {
        $reports_query .= " and et.started = " . 1;
    }
}
if (isset($_GET["task"]) && $_GET["task"] == "c") {
    $reports_query .= " and et.status = 2 and et.started =  0";
}

$reports_query .= " order by et.task_id desc ";
$employeelist = DB::select($reports_query);
?>
<style>
    #lightbox .modal-content {
    display: inline-block;
    text-align: center;   
}

#lightbox .close {
    opacity: 1;
    color: rgb(255, 255, 255);
    background-color: rgb(25, 25, 25);
    padding: 5px 10px;
    border-radius: 84px;
    border: 2px solid rgb(255, 255, 255);
    position: absolute;
    top: -15px;
    right: -78px;
    
    z-index:1032;
}
</style>
<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_report') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_work_report') }} </h6>
                            </label>

                            <label class="col-sm-3 pull-right" style="margin-top:10px;">
                                <div class="btn-group pull-right visible-lg">
                                    <div class="btn">
                                      <i class="icon-download"></i>  {{ $language_data->__('text_download_report') }}</div>
                                    <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li>
                                            <a href="javascript:;" id="downloadPDFFile"><i class="fa fa-file-pdf-o"></i> {{ $language_data->__('text_download_pdffile') }}</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="downloadXLSFile"><i class="fa fa-file-excel-o"></i> {{ $language_data->__('text_download_xlsfile') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </label>
                        </div>
                        <div class="body-nest" id="Filtering">

                        </div>

                        <div class="body-nest" id="Footable">
                            <?php
                            $l = new Language();
                            $downloadExcelArray = array();
                            ?>
                            <form name="form1" method="post">
                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                    <tr>
                                        <th width="16%"><?php echo $l->__('text_employee_name'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_task_category'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_tasks_add_screen'); ?></th>
                                        <th width="13%"><?php echo $l->__('text_task_status_filter'); ?></th>
                                        <th width="15%"><?php echo $l->__('text_client'); ?></th>
                                        <th width="11%"><?php echo $l->__('text_date_from'); ?></th>
                                        <th width="15%"><?php echo $l->__('text_date_to'); ?></th>
                                        <th width="17%">&nbsp;</th>
                                    </tr>
                                    <tr>

                                        <td>
                                            <select name="employee" id="employee" class="form-control"  style="width:156px !important;">
                                                <option value=""><?php echo $l->__('text_select_employee'); ?></option>
                                                <?php
                                                $employeelist1 = DB::select("select emp_id, emp_name from tbl_employee where emp_status = 1  and companyid=$uid order by emp_name");
                                                foreach ($employeelist1 as $data) {
                                                    ?>
                                                    <option value="<?php print $data['emp_id'] ?>" <?php
                                                    if (isset($_POST['employee'])) {
                                                        if ($data['emp_id'] == $_POST['employee']) {
                                                            print "selected";
                                                        }
                                                    }
                                                    ?>><?php print $data['emp_name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>

                                        <td>
                                            <select name="category" id="category" class="form-control"  style="width:95% !important">
                                                <option value="0"><?php echo $l->__('text_select_category'); ?></option>
                                                <?php
                                                $employeelist2 = DB::select("select Id, name from task_categories where status = 1 and partner_id = $uid");
                                                foreach ($employeelist2 as $data) {
                                                    ?>
                                                    <option value="<?php print $data['Id'] ?>" <?php
                                                    if (isset($_POST['category'])) {
                                                        if ($data['Id'] == $_POST['category']) {
                                                            print "selected";
                                                        }
                                                    }
                                                    ?>><?php print $data['name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>


                                        <td>
                                            <select name="task" id="task" class="form-control"  style="width:95% !important">
                                                <option value=""><?php echo $l->__('text_select_task'); ?></option>
                                                <?php
                                                $employeelist3 = DB::select("select  distinct task_name,task_id from tbl_task where comp_id=$uid  and  task_status = 1 order by task_name");
                                                foreach ($employeelist3 as $data) {
                                                    ?>
                                                    <option value="<?php print $data['task_id'] ?>" <?php
                                                    if (isset($_POST['task'])) {
                                                        if ($data['task_id'] == $_POST['task']) {
                                                            print "selected";
                                                        }
                                                    }
                                                    ?>><?php print $data['task_name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="task_status" id="task_status" class="form-control"  style="width:132px !important">
                                                <option value="-2"><?php echo $l->__('text_select_status'); ?></option>
                                                <option value="1" <?php
                                                if (isset($_POST['task'])) {
                                                    if ($_POST['task_status'] == 1) {
                                                        print "selected";
                                                    }
                                                }
                                                ?>><?php echo $l->__('text_pending'); ?></option>
                                                <option value="0" <?php
                                                if (isset($_POST['task'])) {
                                                    if ($_POST['task_status'] == 0) {
                                                        print "selected";
                                                    }
                                                }
                                                ?>><?php echo $l->__('text_accept_pending'); ?></option>
                                                <option value="10" <?php
                                                if (isset($_POST['task'])) {
                                                    if ($_POST['task_status'] == 10) {
                                                        print "selected";
                                                    }
                                                }
                                                ?>><?php echo $l->__('text_started'); ?></option>
                                                <option value="2" <?php
                                                if (isset($_POST['task'])) {
                                                    if ($_POST['task_status'] == 2) {
                                                        print "selected";
                                                    }
                                                }
                                                ?>><?php echo $l->__('text_completed'); ?></option>


                                            </select>
                                        </td>
                                        <td>
                                            <select name="client" id="client" class="form-control"  style="width:95% !important">
                                                <option value=""><?php echo $l->__('text_select_client'); ?></option>
                                                <?php
                                                $clientlist = DB::select("select clientid, client_name from tbl_clients where companyid=$uid  and   status = 1 order by client_name");
                                                foreach ($clientlist as $data) {
                                                    ?>
                                                    <option value="<?php print $data['clientid'] ?>" <?php
                                                    if (isset($_POST['client'])) {
                                                        if ($data['clientid'] == $_POST['client']) {
                                                            print "selected";
                                                        }
                                                    }
                                                    ?>><?php print $data['client_name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>

                                            <input type="text" name="date_from" id="txtDateFrom" value="<?php
                                            if (isset($_POST['date_from'])) {
                                                print $_POST['date_from'];
                                            }
                                            ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date_from'); ?>" style="width:107px !important" />
                                        </td>
                                        <td>

                                            <input type="text" name="date_to" id="txtDateTo" value="<?php
                                            if (isset($_POST['date_to'])) {
                                                print $_POST['date_to'];
                                            }
                                            ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date_to'); ?>" style="width: 104px !important;" />
                                        </td>
                                        <td>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-info left filterButton" name="search" type="submit"><?php echo $l->__('text_search'); ?></button>
                                        </td>
                                    </tr>
                                </table>
                                <div style="clear:both"></div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="adv-table">
                                            <table class="display table table-bordered table-striped" id="dynamic-table1" style="margin-top: 20px;">
                                                <thead>
                                                    <tr class="info">
                                                        <th style="text-align: center;">#</th>
                                                        <th><?php echo $l->__('text_employee_name'); ?></th>
                                                        <!--<th><?php //echo $l->__('text_employee_status'); ?></th>-->
                                                        <!--<th><?php echo $l->__('text_duedate'); ?></th>-->
                                                        <th><?php echo $l->__('text_task_start_time'); ?></th>
                                                        <th><?php echo $l->__('text_task_end_time'); ?></th>
                                                        <th><?php echo $l->__('text_task_category'); ?></th>
                                                        <th><?php echo $l->__('text_task_name'); ?></th>
                                                        <th><?php echo $l->__('text_client'); ?></th>
                                                        <th><?php echo $l->__('text_duration_min'); ?></th>
                                                        <th><?php echo $l->__('text_total_time'); ?></th>
                                                        <th class="print-hidden"><?php echo $l->__('text_route_map'); ?></th>
                                                        <th><?php echo $l->__('text_task_status'); ?></th>
                                                        <th><?php echo $l->__('text_download_pdffile'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sno = 1;
                                                    $isBusy =0;
                                                    if (!empty($employeelist)):
                                                        foreach ($employeelist as $data) {

                                                            $date = date("Y-m-d H:i:s");
                                                            $isEmpBusy = DB::select("SELECT is_not_complete,et_duedate,task_id FROM tbl_emp_task where emp_id='" . $data["emp_id"] . "' and task_id = '" . $data["task_id"] . "' and (status !=2 AND status !=-1) order by et_id desc");

                                                            $emp_work_status = DB::select("SELECT emp_work_status FROM tbl_employee where emp_id='" . $data["emp_id"] . "'");
                                                            $emp_work_status = $emp_work_status[0];

                                                            if ($emp_work_status["emp_work_status"] == 1) {
                                                                $isBusy = 1;
                                                            } else if (!empty($isEmpBusy)) {
                                                                $isEmpBusy = $isEmpBusy[0];
                                                                if (strtotime($isEmpBusy["et_duedate"]) <= strtotime($date)) {

                                                                    $durationArr = DB::select("SELECT duration FROM tbl_task where task_id='" . $isEmpBusy["task_id"] . "'");
                                                                    $durationArr = $durationArr[0];

                                                                    $duration = $durationArr["duration"];

                                                                    $dueDt = date("Y-m-d H:i:s", strtotime($isEmpBusy["et_duedate"] . "+$duration minutes"));

                                                                    $et_duedate = strtotime($dueDt);

                                                                    $currDate = strtotime($date);

                                                                    if ($et_duedate > $currDate) {
                                                                        $isBusy = 1;
                                                                    } else if ($isEmpBusy["is_not_complete"] == 1 && $et_duedate < $currDate) {
                                                                        $isBusy = 1;
                                                                    } else if ($isEmpBusy["is_not_complete"] == 3) {
                                                                        $isBusy = 3;
                                                                    }
                                                                }
                                                            } else {
                                                                $isBusy = 0;
                                                            }
                                                            $y= $sno-1;
                                                            ?>
                                                            <tr>
                                                                <td style="text-align: center !important;" data-order="<?php echo $sno; ?>"><?php $downloadExcelArray[$y]['#'] = $language_data->digits($sno);print $l->digits($sno); ?></td>
                                                                <td><?php 
                                                                    $downloadExcelArray[$y][ htmlspecialchars($language_data->__('text_employee_name'), ENT_QUOTES, 'UTF-8')] = $data['emp_name'];
                                                                    print $data['emp_name']; 
                                                                    ?></td>

<!--                                                                <td>						<?php
                                                    //if ($data['availability'] == 2) {
                                                                ?>
                                                                        <a class="btn btn-danger empStatus"> 
                                                                            <?php 
                                                                                //$downloadExcelArray[$y][$language_data->__('text_employee_status')] = $l->__('text_leave');
                                                                                //echo $l->__('text_leave'); 
                                                                            ?>
                                                                        </a>
                                                                    <?php //} else if ($isBusy == 0) { ?>
                                                                        <a class="btn btn-success empStatus">
                                                                            <?php 
                                                                               // $downloadExcelArray[$y][$language_data->__('text_employee_status')] = $l->__('text_available');
                                                                               // echo $l->__('text_available'); 
                                                                            ?>
                                                                        </a>

                                                                    <?php //} else if ($isBusy == 1) { ?>
                                                                        <a class="btn btn-warning empStatus">
                                                                            <?php 
                                                                               // $downloadExcelArray[$y][$language_data->__('text_employee_status')] = $l->__('text_on_duty');
                                                                              //  echo $l->__('text_on_duty'); 
                                                                            ?>
                                                                        </a>

                                                                    <?php //} else if ($isBusy == 3) { ?>
                                                                        <a class="btn btn-info empStatus" >
                                                                            <?php
                                                                               // $downloadExcelArray[$y][$language_data->__('text_employee_status')] = $l->__('text_incomplete');
                                                                               // echo $l->__('text_incomplete'); 
                                                                            ?>
                                                                        </a>

                                                                    <?php //}
                                                                    ?>
                                                                </td>-->
                                                                <td>
                                                                    <?php
                                                                    if ($data['task_starttime'] != '0000-00-00 00:00:00') {
                                                                        $startTime = $l->convertedDates($data['task_starttime'], "fulldatetime");
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_start_time')] = $startTime;
                                                                        echo $startTime;
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_start_time')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    if ($data['task_endtime'] != '0000-00-00 00:00:00') {
                                                                        $endTime = $l->convertedDates($data['task_endtime'], "fulldatetime");
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_end_time')] = $endTime;
                                                                        echo $endTime;
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_end_time')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>

                                                                <td>
                                                                    <?php
                                                                    $task_cat = $data['task_cat'];
                                                                    if ($task_cat != 0) {
                                                                        $task_cat_details = DB::select("select name from task_categories where Id = $task_cat");
                                                                        print $task_cat_details[0]['name'];
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_category')] = $task_cat_details[0]['name'];
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_category')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>


                                                                <td>

                                                                    <a  class="hclass" href="#addteam<?php echo $data['et_id']; ?>"  data-toggle="modal" >
                                                                        <?php 
                                                                            print $data['task_name']; 
                                                                            $downloadExcelArray[$y][$language_data->__('text_task_name')] = $data['task_name'];
                                                                        ?>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                        $downloadExcelArray[$y][$language_data->__('text_client')] = $data['client_name'];
                                                                        print $data['client_name']; 
                                                                    ?>
                                                                </td>
                                                                <td style="text-align: center !important;">
                                                                    <?php 
                                                                        $downloadExcelArray[$y][$language_data->__('text_duration_min')] = $l->digits($data['duration']);
                                                                        echo $l->digits($data['duration']); 
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $tid = $data['task_id'];
                                                                    if ($data['status'] == 2) {

                                                                        if ($data['task_endtime'] != '0000-00-00 00:00:00' && $data['task_starttime'] != '0000-00-00 00:00:00') {

                                                                            $datetime1 = strtotime($data['task_starttime']);
                                                                            $datetime2 = strtotime($data['task_endtime']);
                                                                            $interval = abs($datetime2 - $datetime1);
                                                                            $minutes = round($interval / 60);

                                                                            if ($l->digits($data['duration']) < $l->digits($minutes)) {
                                                                                echo "<span style='color:red;'>" . $l->digits($minutes) . "</span>";
                                                                            } else {
                                                                                echo "<span style='color:green;'>" . $l->digits($minutes) . "</span>";
                                                                            }
                                                                            $downloadExcelArray[$y][$language_data->__('text_total_time')] = $l->digits($minutes);
                                                                        }
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_total_time')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="center" class="print-hidden"><a href="<?php echo url($urlPrifix.'/timesheet/show/' . $tid) ?>" style="font-size:25px;"><i class="fa fa-map-marker"></i></a></td>
                                                                <td>
                                                                    <?php
                                                                    if ($data['started'] == 1) {

                                                                        $downloadExcelArray[$y][$language_data->__('text_task_status')] = $l->__('text_started');
                                                                        print $l->__('text_started');
                                                                    } else if ($data['status'] == 2) {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_status')] = $l->__('text_completed');
                                                                        print $l->__('text_completed');
                                                                    } else if ($data['status'] == 0) {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_status')] = $l->__('text_accept_pending');
                                                                        print $l->__('text_accept_pending');
                                                                    } else if ($data['status'] == 1) {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_status')] = $l->__('text_pending');
                                                                        print $l->__('text_pending');
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <a href="<?php echo url($urlPrifix.'/report/downloadHTML/' . $data['et_id']) ?>" class="btn btn-sm btn-success"> <i class="fa fa-file-pdf-o"></i> <?=$language_data->__('text_download_pdffile')?></a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $sno++;
                                                            ?>
                                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="addteam<?php echo $data['et_id']; ?>" class="modal fade">

                                                            <div class="modal-dialog" style="width: 900px;">
                                                                <?php
                                                                $id = $data['et_id'];

                                                                $task_details = DB::select("select t.task_id, t.description,et.rating,et.clientsign,t.additional_instruction,et.screenshot, t.task_name,et.started, et.task_starttime, et.task_endtime, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, et.report, et.feedback, et.status, c.client_name, c.address, c.contract, c.contactonsite, e.emp_name from tbl_emp_task et, tbl_task t, tbl_clients c, tbl_employee e where et.et_id= $id and et.task_id = t.task_id and et.clientid= c.clientid and e.emp_id = et.emp_id");
                                                                $start_date = $task_details[0]['task_starttime'];
                                                                $end_date = $task_details[0]['task_endtime'];
                                                                ?>
                                                                
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title"><?php echo $l->__('text_task_details'); ?></h4>
                                                                    </div>
                                                                    <div style="clear:both"></div>
                                                                    <div class="modal-body" style="overflow: auto;height: 450px;">
<!--                                                                        <div class="col-sm-12" >
                                                                            <button type="button" style="float: right;" class="btn btn-success" onclick="printDiv('printpage-<?php //echo $data['et_id']; ?>')"><?php //echo $l->__('text_print'); ?></button>
                                                                        </div>-->
                                                                        <div  style="clear:both"></div>
                                                                        
                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_task_name'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">


                                                                            <?php print $task_details[0]['task_name']; ?>
                                                                        </div>
                                                                        <div  style="clear:both"></div>
                                                                        <div style="margin-bottom:30px"></div>
                                                                        <div  style="clear:both"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_additional_instruction'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <?php print $task_details[0]['additional_instruction']; ?>
                                                                        </div>


                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>





                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_employee_name'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <?php print $task_details[0]['emp_name']; ?>
                                                                        </div>


                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_client_name'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <?php print $task_details[0]['client_name']; ?>
                                                                        </div>


                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_phone_number'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <?php print $l->digits($task_details[0]['contactonsite']); ?>
                                                                        </div>


                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_client_address'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <?php print $task_details[0]['address']; ?>
                                                                        </div>


                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_contact_person'); ?></strong>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <?php print $task_details[0]['contract']; ?>
                                                                        </div>


                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>
                                                                        <?php if ($task_details[0]['clientsign'] !== "") { ?>
                                                                            <div class="col-sm-4">
                                                                                <strong><?php echo $l->__('text_client_sign'); ?></strong>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <?php if (!empty($task_details[0]['clientsign'])) { ?>
                                                                                    <img width="80" src="<?=url('public/uploads/taskimages/'.$task_details[0]['clientsign'])?>">

                                                                                <?php } ?>
                                                                            </div>

                                                                            <div  style="clear:both"></div>


                                                                            <div style="margin-bottom:30px"></div>

                                                                        <?php } ?>

                                                                        <?php if ($task_details[0]['rating'] !== "0.0") { ?>
                                                                            <div class="col-sm-4">
                                                                                <strong><?php echo $l->__('text_rating'); ?></strong>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <?php print $l->digits($task_details[0]['rating']); ?>
                                                                            </div>

                                                                            <div  style="clear:both"></div>


                                                                            <div style="margin-bottom:30px"></div>
                                                                        <?php } ?>
                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_task_start_time'); ?></strong>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <?php
                                                                            if ($task_details[0]['task_starttime'] != "0000-00-00 00:00:00") {

                                                                                echo $l->convertedDates($start_date, "fulldatetime");
                                                                            } else {
                                                                                print "-";
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_task_end_time'); ?></strong>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <?php
                                                                            if ($task_details[0]['task_endtime'] != "0000-00-00 00:00:00") {
                                                                                echo $l->convertedDates($end_date, "fulldatetime");
                                                                            } else {
                                                                                print "-";
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">
                                                                            <strong><?php echo $l->__('text_travel_time'); ?></strong>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <?php
                                                                            if ($task_details[0]['status'] == 2) {
                                                                                print $l->digits($task_details[0]['work_hours']) . " " . $l->__('text_hours');
                                                                            } else {
                                                                                print "-";
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>

                                                                        <div class="col-sm-4">  
                                                                            <strong><?php echo $l->__('text_task_images'); ?></strong>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <?php
                                                                            $imglist = $task_details[0]['screenshot'];
                                                                            $showimg = explode(',', $imglist);
                                                                            $ttcount = count($showimg);

                                                                            if (!empty($imglist)) {
                                                                                for ($i = 0; $i < $ttcount; $i++) {
                                                                                    ?>
                                                                                    <div class="col-lg-6 col-md-6 col-xs-6 thumb">
                                                                                        <a class="thumbnail surveAnchorImg" href="javascript:;" data-toggle="modal" data-target="#lightbox">
                                                                                            <img class="img-responsive" src="<?=url('public/uploads/taskimages/'.$showimg[$i])?>" alt="<?php echo $showimg[$i]; ?>">
                                                                                        </a>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div  style="clear:both"></div>


                                                                        <div style="margin-bottom:30px"></div>
                                                                        <?php if ($task_details[0]['report'] !== "") { ?>
                                                                            <div class="col-sm-4">  
                                                                                <strong><?php echo $l->__('text_task_report'); ?></strong>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <?php
                                                                                if (!empty($task_details[0]['report'])) {
                                                                                    print $task_details[0]['report'];
                                                                                } else {
                                                                                    print "-";
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <div  style="clear:both"></div>


                                                                            <div style="margin-bottom:30px"></div>
                                                                            <?php
                                                                        }

                                                                        if ($task_details[0]['feedback'] !== "") {
                                                                            ?>

                                                                            <div class="col-sm-4">  
                                                                                <strong><?php echo $l->__('text_customer_feedback'); ?></strong>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <?php
                                                                                if (!empty($task_details[0]['feedback'])) {
                                                                                    print $task_details[0]['feedback'];
                                                                                } else {
                                                                                    print "-";
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <div  style="clear:both"></div>


                                                                            <div style="margin-bottom:30px"></div>
                                                                        <?php } ?>
                                                                        <div class="col-sm-4">  
                                                                            <strong><?php echo $l->__('text_status'); ?></strong>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <?php
                                                                            if ($task_details[0]['started'] == 1) {
                                                                                print $l->__('text_started');
                                                                            } elseif ($task_details[0]['status'] == 2) {
                                                                                print $l->__('text_completed');
                                                                            } else if ($task_details[0]['status'] == 0) {

                                                                                print $l->__('text_pending');
                                                                            }
                                                                            ?>
                                                                        </div>

                                                                    </div>
                                                                    <div style="clear:both"></div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    <?php } else: ?>
                                                    <tr><td colspan="12" style="text-align:center !important;"><?php echo $l->__('text_no_records'); ?></td></tr>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadXls',"files"=>true,'url' => $urlPrifix.'/report/downloadExcel']) !!}
                        <input type="hidden" name="xlsData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='WorkReport' />
                        {!! Form::close() !!}
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadPDF',"files"=>true,'url' => $urlPrifix.'/report/downloadPDF']) !!}
                        <input type="hidden" name="PDFData" id="PDFData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='WorkReport' />
                        {!! Form::close() !!}
                    </div>


                </div>

            </div>
        </div>

        <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');

<script type="text/javascript">
    $(document).ready(function() {
    var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});
    $(".filterButton").on("click", function (e) {

        var fromDate = $("#txtDateFrom").val();
        var toDate = $("#txtDateTo").val();

        if (fromDate == "" && toDate != "") {
            alert("<?php echo $l->__('text_work_report_filter_date_error_msg2'); ?>");
            return false;
        } else if (fromDate != "" && toDate == "") {
            alert("<?php echo $l->__('text_work_report_filter_date_error_msg3'); ?>");
            return false;
        }

        if (fromDate != "" || toDate != "") {
            if (new Date(fromDate) > new Date(toDate))
            {
                alert("<?php echo $l->__('text_work_report_filter_date_valid_msg'); ?>");
                return false;
            }
        }

    });
    /*
     TODO::Calender js code start
     */
    var language=$("#language").val();
    if(language=='english')
    {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
    }
    else{
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
    }

    /*
     TODO::Calender js code end
     */
</script>
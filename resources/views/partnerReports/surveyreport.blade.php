@include('layouts.header')
@php
$language_data=new Language();
@endphp
<?php
$fetchMode = DB::getFetchMode();
DB::setFetchMode(\PDO::FETCH_ASSOC);
$companyid = $uid;
if ($supervisor_id > 0) {
    $reports_query = "select s.id, sc.client_name,sc.address,sc.phone_number,e.emp_name,cat.name as category_name,s.create_date,s.notes,s.photo FROM tbl_survey s, tbl_survey_client sc, tbl_employee e,task_categories cat where s.client_id =sc.id and cat.id=s.category_id and e.emp_id = s.emp_id and s.companyId='$companyid' and s.supervisor_id='$supervisor_id'";
} else {
    $reports_query = "select s.id, sc.client_name,sc.address,sc.phone_number,e.emp_name,cat.name as category_name,s.create_date,s.notes,s.photo FROM tbl_survey s, tbl_survey_client sc, tbl_employee e,task_categories cat where s.client_id =sc.id and cat.id=s.category_id and e.emp_id = s.emp_id and s.companyId='$companyid'";
}
if (isset($_POST['search'])) {

    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {
//       if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") {
//            $arrFrom = explode("-", $_POST["date_from"]);
//            $arrTo = explode("-", $_POST["date_to"]);
//            $txtDateFrom = jalali_to_gregorian($arrFrom[0], $arrFrom[1], $arrFrom[2], "-");
//            $txtDateTo = jalali_to_gregorian($arrTo[0], $arrTo[1], $arrTo[2], "-");
//        } else {
            $txtDateFrom = $_POST["date_from"];
            $txtDateTo = $_POST["date_to"];
        //}
    }
    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {

        $reports_query .= " and ( s.create_date >='" . $txtDateFrom . " 00:00:00' and s.create_date <= '" . $txtDateTo . " 23:59:59') ";
    }
    if (!empty($_POST['employee'])) {
        $reports_query .= " and s.emp_id = " . $_POST['employee'];
    }
    if (!empty($_POST['client_filter'])) {
        $reports_query .= " and s.client_id = " . $_POST['client_filter'];
    }
    if (!empty($_POST['category'])) {
        $reports_query .= " and s.category_id = " . $_POST['category'];
    }
    
}
$reports_query .= " order by s.id desc";
?>

<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_report') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_survey_report') }} </h6>
                            </label>
                            <label class="col-sm-3 pull-right" style="margin-top:10px;">
                                <div class="btn-group pull-right visible-lg">
                                    <div class="btn">
                                      <i class="icon-download"></i>  {{ $language_data->__('text_download_report') }}</div>
                                    <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li>
                                            <a href="javascript:;" id="downloadPDFFile"><i class="fa fa-file-pdf-o"></i> {{ $language_data->__('text_download_pdffile') }}</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="downloadXLSFile"><i class="fa fa-file-excel-o"></i> {{ $language_data->__('text_download_xlsfile') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </label>

                        </div>
                        <!--<div class="body-nest" id="Filtering">

                        </div>-->

                        <div class="body-nest" id="Footable">
                            <?php
                            $l = new Language();
                            $downloadExcelArray = array();
                            ?>
                            <form name="form1" method="post" action="">
                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                    <tr>
                                        <th width="17%"><?php echo $l->__('text_team'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_client_name'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_category_name'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_date_from'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_date_to'); ?></th>

                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="employee" id="employee" class="form-control"  style="width:95% !important">
                                                <option value=""><?php echo $l->__('text_select_team'); ?></option>
                                                <?php
                                                $employeelist1 = DB::select("select emp_id, emp_name from tbl_employee where emp_status = 1  and companyid=$companyid and department='survey' order by emp_name");
                                                foreach ($employeelist1 as $data) {
                                                    ?>
                                                    <option value="<?php print $data['emp_id'] ?>" <?php
                                                    if (isset($_POST['employee'])) {
                                                        if ($data['emp_id'] == $_POST['employee']) {
                                                            print "selected";
                                                        }
                                                    }
                                                    ?>><?php print $data['emp_name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="client_filter" id="client_filter" class="form-control"  style="width:95% !important">
                                                <option value=""><?php echo $l->__('text_select_client'); ?></option>
                                                <?php
                                                if ($supervisor_id > 0) {
                                                    $clientlist = DB::select("select id, client_name from tbl_survey_client where status = 1 and client_name !='' and companyId='$companyid' and supervisor_id='$supervisor_id'");
                                                } else {
                                                    $clientlist = DB::select("select id, client_name from tbl_survey_client where status = 1 and client_name !='' and companyId='$companyid'");
                                                }
                                                foreach ($clientlist as $data) {
                                                    ?>
                                                    <option value="<?php print $data['id'] ?>" <?php
                                                if (isset($_POST['employee'])) {
                                                    if ($data['id'] == $_POST['client_filter']) {
                                                        print "selected";
                                                    }
                                                }
                                                    ?>><?php print $data['client_name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="category" id="category" class="form-control"  style="width:95% !important">
                                                <option value="0"><?php echo $l->__('text_select_category'); ?></option>
                                                <?php
                                                $employeelist2 = DB::select("select Id, name from task_categories where status = 1 and partner_id = $companyid");
                                                foreach ($employeelist2 as $data) {
                                                    ?>
                                                    <option value="<?php print $data['Id'] ?>" <?php
                                                if (isset($_POST['category'])) {
                                                    if ($data['Id'] == $_POST['category']) {
                                                        print "selected";
                                                    }
                                                }
                                                    ?>><?php print $data['name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>

                                            <input type="text" name="date_from" id="txtDateFrom" value="<?php
                                                        if (isset($_POST['date_from'])) {
                                                            print $_POST['date_from'];
                                                        }
                                                        ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date_from'); ?>" style="width:90% !important" />
                                        </td>
                                        <td>
                                            <input type="text" name="date_to" id="txtDateTo" value="<?php
                                            if (isset($_POST['date_to'])) {
                                                print $_POST['date_to'];
                                            }
                                                        ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date_to'); ?>" style="width:90% !important" />
                                        </td>
                                        <td>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-info left filterButton" name="search" type="submit"><?php echo $l->__('text_search'); ?></button>
                                        </td>
                                    </tr>
                                </table>
                                <div style="clear:both"></div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="adv-table">

                                            <?php
                                            $employeelist = DB::select($reports_query);
                                            $rowCount = count($employeelist);
                                            ?>

                                            <table class="display table table-bordered table-striped" id="dynamic-table1" style="margin-top: 20px;">
                                                <thead>
                                                    <tr class="info">
                                                        <th width="80" style="text-align: center;">#</th>
                                                        <th><?php echo $l->__('text_team'); ?></th>
                                                        <th><?php echo $l->__('text_client_name'); ?></th>
                                                        <th><?php echo $l->__('text_client_address'); ?></th>
                                                        <th><?php echo $l->__('text_categories'); ?></th>
                                                        <th><?php echo $l->__('text_phone_number'); ?></th>
                                                        <th width="18%"><?php echo $l->__('text_notes'); ?></th>
                                                        <th><?php echo $l->__('text_photos'); ?></th>
                                                        <th><?php echo $l->__('text_date'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sno = 1;

                                                    if (!empty($employeelist)) {

                                                        foreach ($employeelist as $data) {
                                                            $y= $sno-1;
                                                            ?>
                                                            <tr>
                                                                <td style="text-align: center !important;"><?php $downloadExcelArray[$y]['#'] = $language_data->digits($sno); print $l->digits($sno); ?></td>
                                                                <td><?php $downloadExcelArray[$y][$language_data->__('text_team')] = $data['emp_name']; print $data['emp_name']; ?></td>
                                                                <td><?php $downloadExcelArray[$y][$language_data->__('text_client_name')] = $data['client_name']; print $data['client_name']; ?></td>
                                                                <td><?php $downloadExcelArray[$y][$language_data->__('text_client_address')] = $data['address']; print $data['address']; ?></td>
                                                                <td><?php $downloadExcelArray[$y][$language_data->__('text_categories')] = $data['category_name']; print $data['category_name']; ?></td>
                                                                <td><?php $downloadExcelArray[$y][$language_data->__('text_phone_number')] = $data['phone_number']; print $data['phone_number']; ?></td>
                                                                <td><?php print (strlen($data['notes']) > 200) ? substr($data['notes'], 0, 200) . '...<a class="hclass" href="#surveNotes' . $data['id'] . '" data-toggle="modal"> ' . $l->__('text_read_more_lable') . '</a>' : $data['notes']; ?></td>
                                                                <td><a class="hclass" href="#photos<?php echo $data['id']; ?>" data-toggle="modal"><?php echo $l->__('text_view_lable'); ?></a></td>
                                                                <td>
                                                                    <?php
                                                                    if ($data['create_date'] != '0000-00-00 00:00:00') {
                                                                        $createdDate = $l->convertedDates($data['create_date'], "fulldatetime");
                                                                        $downloadExcelArray[$y][$language_data->__('text_date')] = $createdDate;
                                                                        echo $createdDate;
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_date')] = '-';
                                                                        echo "-";
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>

                                                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="surveNotes<?php echo $data['id']; ?>" class="modal fade">
                                                                <div class="modal-dialog" style="width: 900px;">
                                                                    <div>
                                                                        <div style="clear:both"></div>
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                <h4 class="modal-title"><?php echo $l->__('text_notes'); ?></h4>
                                                                            </div>
                                                                            <div style="clear:both"></div>
                                                                            <div class="modal-body" style="overflow-y: scroll;">
                                                                                <?php echo $data['notes']; ?>

                                                                            </div>
                                                                            <div style="clear:both"></div>
                                                                        </div>

                                                                        <div style="clear:both"></div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="photos<?php echo $data['id']; ?>" class="modal fade">
                                                            <div class="modal-dialog" style="width: 900px;">
                                                                <div>
                                                                    <div style="clear:both"></div>
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                            <h4 class="modal-title"><?php echo $l->__('text_photos'); ?></h4>
                                                                        </div>
                                                                        <div style="clear:both"></div>
                                                                        <div class="modal-body" style="overflow-y: scroll;">
                                                                            <div class="row">

                                                                                <?php
                                                                                $photoArr = explode(",", $data['photo']);

                                                                                foreach ($photoArr as $val) {
                                                                                    ?>
                                                                                    <?php if (!empty($val)) { ?>
                                                                                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                                                            <a class="thumbnail surveAnchorImg" href="javascript:;">
                                                                                                <img class="img-responsive" src="<?=url('/public/uploads/surveyimages/'.$val) ?>" alt="<?php echo $val; ?>">
                                                                                            </a>
                                                                                        </div>

                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </div>

                                                                        </div>
                                                                        <div style="clear:both"></div>
                                                                    </div>

                                                                    <div style="clear:both"></div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <?php
                                                        $sno++;
                                                        ?>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <tr><td colspan="11" style="text-align:center !important;"><?php echo $l->__('text_no_records'); ?></td></tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadXls',"files"=>true,'url' => $urlPrifix.'/report/downloadExcel']) !!}
                        <input type="hidden" name="xlsData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='SurveyReport' />
                        {!! Form::close() !!}
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadPDF',"files"=>true,'url' => $urlPrifix.'/report/downloadPDF']) !!}
                        <input type="hidden" name="PDFData" id="PDFData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='SurveyReport' />
                        {!! Form::close() !!}
                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');

<script type="text/javascript">
    $(".filterButton").on("click", function (e) {

        var fromDate = $("#txtDateFrom").val();
        var toDate = $("#txtDateTo").val();

        if (fromDate == "" && toDate != "") {
            alert("<?php echo $l->__('text_log_report_filter_date_error_msg1'); ?>");
            return false;
        } else if (fromDate != "" && toDate == "") {
            alert("<?php echo $l->__('text_log_report_filter_date_error_msg2'); ?>");
            return false;
        }

        if (fromDate != "" || toDate != "") {
            if (new Date(fromDate) > new Date(toDate))
            {
                alert("<?php echo $l->__('text_log_report_filter_date_valid_msg'); ?>");
                return false;
            }
        }

    });
    /*
     TODO::Calender js code start
     */
    var language = $("#language").val();
    if (language == 'english')
    {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
    } else {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
    }

    /*
     TODO::Calender js code end
     */
</script>
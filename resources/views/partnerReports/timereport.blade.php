@include('layouts.header')
@php
$language_data=new Language();
@endphp
<?php
$fetchMode = DB::getFetchMode();
DB::setFetchMode(\PDO::FETCH_ASSOC);
$companyid = $uid;
$reports_query12 = "select et.task_endtime, et.task_starttime  from tbl_emp_task as  et inner join tbl_task as te on et.task_id=te.task_id  where  et.comp_id=$companyid and et.status=2 and  te.task_delete=0 group by et.task_id";


$totalcountvalue = "SELECT COUNT( tk.et_id ) AS totjob
FROM tbl_emp_task AS tk
INNER JOIN tbl_task AS te ON tk.task_id = te.task_id
WHERE tk.comp_id =$companyid
AND tk.status =2
AND te.task_delete =0";


if ($supervisor_id > 0) {
    $reports_query = "select et.task_id, et.et_id,et.et_duedate,et.totalhours,et.task_starttime,t.duration,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, e.emp_name FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and c.companyid=$companyid and et.status = '2' and t.task_delete=0 and et.supervisor_id=$supervisor_id ";
} else {
    $reports_query = "select et.task_id, et.et_id,et.totalhours,et.et_duedate,et.task_starttime,t.duration,et.started, et.task_endtime, et.status, et.et_hours, et.et_mins, t.task_name, c.client_name, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours,e.availability, e.emp_name FROM tbl_task t, tbl_clients c, tbl_employee e, tbl_emp_task et where t.clientid =c.clientid  and e.emp_id = et.emp_id and t.task_id = et.task_id and c.companyid=$companyid and et.status=2  and t.task_delete=0";
}


if (isset($_POST['search'])) {

    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {

//        if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") {
//            $arrFrom = explode("-", $_POST["date_from"]);
//            $arrTo = explode("-", $_POST["date_to"]);
//            $txtDateFrom = jalali_to_gregorian($arrFrom[0], $arrFrom[1], $arrFrom[2], "-");
//            $txtDateTo = jalali_to_gregorian($arrTo[0], $arrTo[1], $arrTo[2], "-");
//        } else {
            $txtDateFrom = $_POST["date_from"];
            $txtDateTo = $_POST["date_to"];
        //}
//    convert date .end
    }
    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {

        $reports_query .= " and ( et.task_starttime >='" . $txtDateFrom . " 00:00:00' and et.task_endtime <= '" . $txtDateTo . " 23:59:59') ";
    }
    if (!empty($_POST['employee'])) {
        $reports_query .= " and et.emp_id = " . $_POST['employee'];
    }

    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {


        $totalcountvalue .= " and ( tk.task_starttime >='" . $txtDateFrom . " 00:00:00' and tk.task_endtime <= '" . $txtDateTo . " 23:59:59') ";
    }
    if (!empty($_POST['employee'])) {
        $totalcountvalue .= " and tk.emp_id = " . $_POST['employee'];
    }

    if (!empty($_POST['date_from']) and ! empty($_POST['date_to'])) {

        $reports_query .= " and ( et.task_starttime >='" . $txtDateFrom . " 00:00:00' and et.task_endtime <= '" . $txtDateTo . " 23:59:59') ";
    }
    if (!empty($_POST['employee'])) {
        $reports_query12 .= " and et.emp_id = " . $_POST['employee'];
    }
}
$reports_query .= " order by et.task_id desc";

$totalhours12234234 = DB::select($reports_query12);
$hours1 = 0;
foreach ($totalhours12234234 as $data12) {
    $data12['task_starttime'];
    $t1 = StrToTime($data12['task_starttime']);
    $t2 = StrToTime($data12['task_endtime']);
    $diff = $t2 - $t1;
    $hours12333 = $diff / ( 60 * 60 );
    $hours1 = $hours1 + $hours12333;
}
$daffd = round($hours1, 4);

$total_taskcount = DB::select($totalcountvalue);
?>

<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_report') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_time_reports') }} </h6>
                            </label>
                            <label class="col-sm-3 pull-right" style="margin-top:10px;">
                                <div class="btn-group pull-right visible-lg">
                                    <div class="btn">
                                      <i class="icon-download"></i>  {{ $language_data->__('text_download_report') }}</div>
                                    <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li>
                                            <a href="javascript:;" id="downloadPDFFile"><i class="fa fa-file-pdf-o"></i> {{ $language_data->__('text_download_pdffile') }}</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="downloadXLSFile"><i class="fa fa-file-excel-o"></i> {{ $language_data->__('text_download_xlsfile') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </label>

                        </div>
                        <!--<div class="body-nest" id="Filtering">

                        </div>-->

                        <div class="body-nest" id="Footable">
                            <?php
                            $l = new Language();
                            $downloadExcelArray = array();
                            ?>
                            <form name="form1" method="post" action="">
                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                    <tr>
                                        <th width="17%"><?php echo $l->__('text_employee_name'); ?></th>

                                        <th width="17%"><?php echo $l->__('text_date_from'); ?></th>
                                        <th width="17%"><?php echo $l->__('text_date_to'); ?></th>

                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="employee" id="employee" class="form-control"  style="width:95% !important">
                                                <option value=""><?php echo $l->__('text_select_employee'); ?></option>
                                                <?php
                                                $employeelist1 = DB::select("select emp_id, emp_name from tbl_employee where emp_status = 1  and companyid=$companyid order by emp_name");
                                                foreach ($employeelist1 as $data) {
                                                    ?>
                                                    <option value="<?php print $data['emp_id'] ?>" <?php
                                                if (isset($_POST['employee'])) {
                                                    if ($data['emp_id'] == $_POST['employee']) {
                                                        print "selected";
                                                    }
                                                }
                                                    ?>><?php print $data['emp_name']; ?>  </option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>

                                            <input type="text" name="date_from" id="txtDateFrom" value="<?php
                                                        if (isset($_POST['date_from'])) {
                                                            print $_POST['date_from'];
                                                        }
                                                        ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date_from'); ?>" style="width:90% !important" />
                                        </td>
                                        <td>

                                            <input type="text" name="date_to" id="txtDateTo" value="<?php
                                            if (isset($_POST['date_to'])) {
                                                print $_POST['date_to'];
                                            }
                                                        ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date_to'); ?>" style="width:90% !important" />
                                        </td>
                                        <td>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-info left filterButton" name="search" type="submit"><?php echo $l->__('text_search'); ?></button>
                                        </td>
                                    </tr>
                                </table>
                                <div style="clear:both"></div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="adv-table">

                                            <?php
                                            $employeelist = DB::select($reports_query);
                                            $rowCount = count($employeelist);
                                            ?>

                                            <div class="col-sm-3">

                                                <h3 style="color: #CA1E1E;font-size: 14px;"><?php echo $l->__('text_no_of_task_completed_reports'); ?>:<?php echo $l->digits($rowCount); ?></h3></div>

                                            <div class="col-sm-3">



                                                <h3 style="color: #CA1E1E; font-size: 14px;"><?php echo $l->__('text_time'); ?>: <?php echo $l->digits($daffd); ?> <?php echo $l->__('text_short_min'); ?></h3>



                                            </div>
                                            <table class="display table table-bordered table-striped" id="dynamic-table1" style="margin-top: 20px;">
                                                <thead>
                                                    <tr class="info">
                                                        <th width="80" style="text-align: center;">#</th>
                                                        <th><?php echo $l->__('text_employee_name'); ?></th>

                                                        <th><?php echo $l->__('text_task_start_time_reports'); ?></th>
                                                        <th><?php echo $l->__('text_task_end_time_reports'); ?></th>
                                                        <!--<th><?php //echo $l->__('text_total_working_hours');  ?></th>-->
                                                        <th><?php echo $l->__('text_total_working_min'); ?></th>

                                                        <th><?php echo $l->__('text_allocated_time'); ?></th>
                                                        <th><?php echo $l->__('text_status'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sno = 1;
                                                    if (!empty($employeelist)) {
                                                        
                                                        foreach ($employeelist as $data) {
                                                            $y= $sno-1;
                                                            $date = date_create($data['task_starttime']);
                                                            ?>
                                                            <tr>
                                                                <td style="text-align: center !important;"><?php $downloadExcelArray[$y]['#'] = $language_data->digits($sno);print $l->digits($sno); ?></td>
                                                                <td>
                                                                    <?php
                                                                        $downloadExcelArray[$y][$language_data->__('text_employee_name')] = $data['emp_name'];
                                                                        print $data['emp_name']; 
                                                                    ?>
                                                                </td>


                                                                <td>
                                                                    <?php
                                                                    if (strtotime($data['task_starttime']) != "0" && $data['task_starttime'] != '0000-00-00 00:00:00') {

                                                                        $startTime = $l->convertedDates($data['task_starttime'], "fulldatetime");
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_start_time')] = $startTime;
                                                                        echo $startTime;
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_start_time')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    if (strtotime($data['task_endtime']) != "0" && $data['task_endtime'] != '0000-00-00 00:00:00') {

                                                                        $endTime = $l->convertedDates($data['task_endtime'], "fulldatetime");
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_end_time')] = $endTime;
                                                                        echo $endTime;
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_task_end_time')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>

                                                                <td>
                                                                    <?php
                                                                    if ($data['task_endtime'] != '0000-00-00 00:00:00' && $data['task_starttime'] != '0000-00-00 00:00:00') {
                                                                        $datetime1 = strtotime($data['task_starttime']);
                                                                        $datetime2 = strtotime($data['task_endtime']);
                                                                        $interval = abs($datetime2 - $datetime1);
                                                                        $minutes = round($interval / 60);
                                                                        $duration = round($data['duration']);
                                                                        if ($l->digits($duration) < $l->digits($minutes)) {
                                                                            echo "<span style='color:red;'>" . $l->digits($minutes) . "</span>";
                                                                        } else {
                                                                            echo "<span style='color:green;'>" . $l->digits($minutes) . "</span>";
                                                                        }
                                                                        $downloadExcelArray[$y][$language_data->__('text_total_working_min')] = $l->digits($minutes);
                                                                    } else {
                                                                        $downloadExcelArray[$y][$language_data->__('text_total_working_min')] = '-';
                                                                        print "-";
                                                                    }
                                                                    ?>
                                                                </td>

                                                                <td style="text-align:center !important">
                                                                    <?php
                                                                    $duration = round($data['duration']);
                                                                    $downloadExcelArray[$y][$language_data->__('text_allocated_time')] = $l->digits($duration);
                                                                    echo $l->digits($duration);
                                                                    ?>
                                                                </td>

                                                                <td>
                                                                    <?php
                                                                        $downloadExcelArray[$y][$language_data->__('text_status')] = $l->__('text_completed');
                                                                        echo $l->__('text_completed'); 
                                                                    ?>
                                                                </td>

                                                            </tr>
                                                            <?php
                                                            $sno++;
                                                    }
                                                } else {
                                                    ?>
                                                    <tr><td colspan="11" style="text-align:center !important;"><?php echo $l->__('text_no_records'); ?></td></tr>
                                                <?php }
                                                ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadXls',"files"=>true,'url' => $urlPrifix.'/report/downloadExcel']) !!}
                        <input type="hidden" name="xlsData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='TimeReport' />
                        {!! Form::close() !!}
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadPDF',"files"=>true,'url' => $urlPrifix.'/report/downloadPDF']) !!}
                        <input type="hidden" name="PDFData" id="PDFData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='TimeReport' />
                        {!! Form::close() !!}
                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');

<script type="text/javascript">
    $(".filterButton").on("click", function (e) {

        var fromDate = $("#txtDateFrom").val();
        var toDate = $("#txtDateTo").val();

        if (fromDate == "" && toDate != "") {
            alert("<?php echo $l->__('text_work_report_filter_date_error_msg2'); ?>");
            return false;
        } else if (fromDate != "" && toDate == "") {
            alert("<?php echo $l->__('text_work_report_filter_date_error_msg3'); ?>");
            return false;
        }

        if (fromDate != "" || toDate != "") {
            if (new Date(fromDate) > new Date(toDate))
            {
                alert("<?php echo $l->__('text_work_report_filter_date_valid_msg'); ?>");
                return false;
            }
        }

    });
    /*
     TODO::Calender js code start
     */
    var language = $("#language").val();
    if (language == 'english')
    {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
    } else {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
    }

    /*
     TODO::Calender js code end
     */
</script>
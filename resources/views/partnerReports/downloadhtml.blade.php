<div class="modal-dialog" style="width: 900px;">
<?php
$fetchMode = DB::getFetchMode();
DB::setFetchMode(\PDO::FETCH_ASSOC);
$id = $et_id;
$l = new Language();
$query = "select t.task_id, t.description,et.rating,et.clientsign,t.additional_instruction,et.screenshot, t.task_name,et.started, et.task_starttime, et.task_endtime, TIMEDIFF(et.task_endtime, et.task_starttime) as work_hours, et.report, et.feedback, et.status, c.client_name, c.address, c.contract,c.logo as clientlogo, c.contactonsite, e.emp_name from tbl_emp_task et, tbl_task t, tbl_clients c, tbl_employee e where et.et_id= $id and et.task_id = t.task_id and et.clientid= c.clientid and e.emp_id = et.emp_id";
$task_details = DB::select($query);
$start_date = $task_details[0]['task_starttime'];
$end_date = $task_details[0]['task_endtime'];
?>
    <div style="text-align: center;width:100%;font-size: 50px;">
        <?php echo $l->__('text_task_details'); ?>
    </div>
    <div class="modal-content">
        
        <div class="modal-body">
            
            <div  style="clear:both"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_task_name'); ?></strong>
            </div>
            <div class="col-md-5">

                <?php print $task_details[0]['task_name']; ?>
            </div>
            <div  style="clear:both"></div>
            <div style="margin-bottom:30px"></div>
            <div  style="clear:both"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_additional_instruction'); ?></strong>
            </div>
            <div class="col-md-5">
                <?php print $task_details[0]['additional_instruction']; ?>
            </div>


            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>





            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_employee_name'); ?></strong>
            </div>
            <div class="col-md-5">
                <?php print $task_details[0]['emp_name']; ?>
            </div>


            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_client_name'); ?></strong>
            </div>
            <div class="col-md-5">
                <?php print $task_details[0]['client_name']; ?>
            </div>

            <div  style="clear:both"></div>
            
            <div style="margin-bottom:30px"></div>
            <div class="col-sm-4">
                <strong><?php echo $l->__('text_logo'); ?></strong>
            </div>
            <div class="col-md-5">
                <img style="width: 100px; height: 100px;" src="<?=url('public/uploads/clients/').'/'.$task_details[0]['clientlogo']?>" />
            </div>


            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_phone_number'); ?></strong>
            </div>
            <div class="col-md-5">
                <?php print $l->digits($task_details[0]['contactonsite']); ?>
            </div>


            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_client_address'); ?></strong>
            </div>
            <div class="col-md-5">
                <?php print $task_details[0]['address']; ?>
            </div>


            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_contact_person'); ?></strong>
            </div>
            <div class="col-md-5">
                <?php print $task_details[0]['contract']; ?>
            </div>


            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>
            <?php if ($task_details[0]['clientsign'] !== "") { ?>
                <div class="col-sm-4">
                    <strong><?php echo $l->__('text_client_sign'); ?></strong>
                </div>
                <div class="col-md-5">
                    <?php if (!empty($task_details[0]['clientsign'])) { ?>
                        <img width="80" src="<?= url('public/uploads/taskimages/' . $task_details[0]['clientsign']) ?>">

                    <?php } ?>
                </div>

                <div  style="clear:both"></div>


                <div style="margin-bottom:30px"></div>

            <?php } ?>

            <?php if ($task_details[0]['rating'] !== "0.0") { ?>
                <div class="col-sm-4">
                    <strong><?php echo $l->__('text_rating'); ?></strong>
                </div>
                <div class="col-md-5">
                    <?php print $l->digits($task_details[0]['rating']); ?>
                </div>

                <div  style="clear:both"></div>


                <div style="margin-bottom:30px"></div>
            <?php } ?>
            <div class="col-sm-4">
                <strong><?php echo $l->__('text_task_start_time'); ?></strong>
            </div>
            <div class="col-sm-5">
                <?php
                if ($task_details[0]['task_starttime'] != "0000-00-00 00:00:00") {

                    echo $l->convertedDates($start_date, "fulldatetime");
                } else {
                    print "-";
                }
                ?>
            </div>
            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_task_end_time'); ?></strong>
            </div>
            <div class="col-sm-5">
                <?php
                if ($task_details[0]['task_endtime'] != "0000-00-00 00:00:00") {
                    echo $l->convertedDates($end_date, "fulldatetime");
                } else {
                    print "-";
                }
                ?>
            </div>
            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">
                <strong><?php echo $l->__('text_travel_time'); ?></strong>
            </div>
            <div class="col-sm-5">
                <?php
                if ($task_details[0]['status'] == 2) {
                    print $l->digits($task_details[0]['work_hours']) . " " . $l->__('text_hours');
                } else {
                    print "-";
                }
                ?>
            </div>
            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>

            <div class="col-sm-4">  
                <strong><?php echo $l->__('text_task_images'); ?></strong>
            </div>
            <div class="col-sm-5">
                <?php
                $imglist = $task_details[0]['screenshot'];
                $showimg = explode(',', $imglist);
                $ttcount = count($showimg);

                if (!empty($imglist)) {
                    for ($i = 0; $i < $ttcount; $i++) {
                        ?>
                        <div class="col-lg-6 col-md-6 col-xs-6 thumb">
                            <a class="thumbnail surveAnchorImg" href="javascript:;" data-toggle="modal" data-target="#lightbox">
                                <img style="width: 200px; height: 200px;" src="<?= url('public/uploads/taskimages/' . $showimg[$i]) ?>" alt="<?php echo $showimg[$i]; ?>">
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div  style="clear:both"></div>


            <div style="margin-bottom:30px"></div>
            <?php if ($task_details[0]['report'] !== "") { ?>
                <div class="col-sm-4">  
                    <strong><?php echo $l->__('text_task_report'); ?></strong>
                </div>
                <div class="col-sm-5">
                    <?php
                    if (!empty($task_details[0]['report'])) {
                        print $task_details[0]['report'];
                    } else {
                        print "-";
                    }
                    ?>
                </div>
                <div  style="clear:both"></div>


                <div style="margin-bottom:30px"></div>
                <?php
            }

            if ($task_details[0]['feedback'] !== "") {
                ?>

                <div class="col-sm-4">  
                    <strong><?php echo $l->__('text_customer_feedback'); ?></strong>
                </div>
                <div class="col-sm-5">
                    <?php
                    if (!empty($task_details[0]['feedback'])) {
                        print $task_details[0]['feedback'];
                    } else {
                        print "-";
                    }
                    ?>
                </div>
                <div  style="clear:both"></div>


                <div style="margin-bottom:30px"></div>
            <?php } ?>
            <div class="col-sm-4">  
                <strong><?php echo $l->__('text_status'); ?></strong>
            </div>
            <div class="col-sm-5">
                <?php
                if ($task_details[0]['started'] == 1) {
                    print $l->__('text_started');
                } elseif ($task_details[0]['status'] == 2) {
                    print $l->__('text_completed');
                } else if ($task_details[0]['status'] == 0) {

                    print $l->__('text_pending');
                }
                ?>
            </div>

        </div>
        <div style="clear:both"></div>
    </div>

</div>
<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 12/1/17
 * Time: 7:00 PM
 */
?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>



    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-menu"></span>
                            <span>{{ $language_data->__('text_reports') }}</span>
                        </h2>
                    </div>

                    <div class="col-sm-7"></div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->


            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <label class="col-sm-4">
                                    <h6>{{ $language_data->__('text_active_user_list') }} </h6>
                                </label>
                                <label class="col-sm-3 pull-right" style="margin-top:10px;">
                                    <div class="btn-group pull-right visible-lg">
                                        <div class="btn">
                                          <i class="icon-download"></i>  {{ $language_data->__('text_download_report') }}</div>
                                        <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul role="menu" class="dropdown-menu">
                                            <li>
                                                <a href="javascript:;" id="downloadPDFFile"><i class="fa fa-file-pdf-o"></i> {{ $language_data->__('text_download_pdffile') }}</a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" id="downloadXLSFile"><i class="fa fa-file-excel-o"></i> {{ $language_data->__('text_download_xlsfile') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </label>

                            </div>
                            <div class="body-nest" id="Filtering">

                                <div class="row" style="margin-bottom:10px;">
                                    <div class="col-sm-4">
                                        <input class="form-control" id="filter" placeholder="{{ $language_data->__('text_search') }}..." type="text"/>
                                    </div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-6">
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="body-nest" id="Footable">


                                <table id="activeuserRTable" class="table-striped footable-res footable metro-blue" data-page-size="10" data-filter="#filter" data-filter-text-only="true">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?php echo $language_data->__('text_name'); ?></th>
                                            <th><?php echo $language_data->__('text_profile_image'); ?></th>
                                            <th><?php echo $language_data->__('text_email'); ?></th>
                                            <th><?php echo $language_data->__('text_current_location'); ?></th>
                                            <th><?php echo $language_data->__('text_status'); ?></th>
                                            <th><?php echo $language_data->__('text_work_status'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $fetchMode = DB::getFetchMode();
                                        DB::setFetchMode(\PDO::FETCH_ASSOC);
                                        
                                        $employeelist = DB::select("SELECT * FROM tbl_employee where companyid='$uid' and `delete_employee` = '0' and availability!=2   order by emp_status desc");
                                        $x = 1;
                                        $downloadExcelArray = array();
                                        if(!empty($employeelist)){
                                            foreach ($employeelist as $data) {
                                            $dt12 = $data['emp_id'];
                                            $empcurrentlocation1 = DB::select("SELECT `lan`,`long` FROM `employeesurrentlocation` where employeid='$dt12' order by id desc limit 1");
                                            
                                            $latvalue = "";
                                            $langvalue = "";

                                            if (!empty($empcurrentlocation1)) {
                                                $empcurrentlocation1 = $empcurrentlocation1[0];
                                                $latvalue = $empcurrentlocation1['lan'];
                                                $langvalue = $empcurrentlocation1['long'];
                                            }
                                            $dt = $data['emp_id'];
                                            $empopentask = DB::select("SELECT count(et_id) as totord FROM tbl_emp_task where  emp_id='$dt'");
                                            ?>

                                            <tr style="color:#000">
                                                <?php $y= $x-1; ?>
                                                <td style="text-align:center">
                                                    <?php 
                                                        $downloadExcelArray[$y]['#'] = $language_data->digits($x);
                                                        echo $language_data->digits($x); 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $downloadExcelArray[$y][$language_data->__('text_name')] = $data['emp_name'];
                                                        echo $data['emp_name']; 
                                                    ?>
                                                </td>
                                                <td class="print-hidden">
                                                    <?php if (!empty($data['profileimage']) && file_exists(public_path('uploads/employees/'.$data['profileimage']))) { ?>
                                                        <img src="<?= url('public/uploads/employees/') . '/' . $data['profileimage']?>" height="40px" width="40px"  >
                                                    <?php } else { ?>
                                                        <img src="<?= asset('resources/assets/momom-assets/img/user.png');?>" height="40px" width="40px"  >
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $downloadExcelArray[$y][$language_data->__('text_email')] = $data['emp_email'];
                                                        echo $data['emp_email']; 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $location = getAddressNew($latvalue, $langvalue); 
                                                        if(empty($location)){
                                                            $location = "Not Available";
                                                        }
                                                        $downloadExcelArray[$y][$language_data->__('text_current_location')] = $location;
                                                        echo $location;
                                                    ?>
                                                </td>
                                                <td style="text-align:center !important">
                                                    <?php if ($data['delete_employee'] == 1) { 
                                                        $downloadExcelArray[$y][$language_data->__('text_status')] = $language_data->__('text_deleted_user');
                                                    ?>
                                                    <span class="status-metro status-disabled" title="<?php echo $language_data->__('text_deleted_user'); ?>"><?php echo $language_data->__('text_deleted_user'); ?></span>
                                                    <?php } elseif ($data['emp_status'] == 1) { 
                                                        $downloadExcelArray[$y][$language_data->__('text_status')] = $language_data->__('text_activate_user');
                                                    ?>
                                                        <span class="status-metro status-active" title="<?php echo $language_data->__('text_activate_user'); ?>"><?php echo $language_data->__('text_activate_user'); ?></span>
                                                    <?php } else { 
                                                        $downloadExcelArray[$y][$language_data->__('text_status')] = $language_data->__('text_suspend_user');
                                                    ?>
                                                        <span class="status-metro status-disabled" title="<?php echo $language_data->__('text_suspend_user'); ?>"><?php echo $language_data->__('text_suspend_user'); ?></span>
                                                    <?php } ?>
                                                </td>
                                                <td style="text-align: center !important;">
                                                    <?php
                                                    $empopentask123 = DB::select("SELECT count(et_id) as totord FROM tbl_emp_task where  emp_id='$dt' and started=1 ");
                                                    $empopentask123 = $empopentask123[0];
                                                    ?>

                                                    <?php
                                                    if ($data['availability'] == 2) {
                                                        $downloadExcelArray[$y][$language_data->__('text_work_status')] = "Leave";
                                                        ?>
                                                        <a class="btn btn-danger" style="cursor: default;
                                                           cursor: default;
                                                           font-size: 10px;
                                                           height: 29px;"> Leave</a>
                                                       <?php } else if ($empopentask123['totord'] == 0) { 
                                                           $downloadExcelArray[$y][$language_data->__('text_work_status')] = $language_data->__('text_available');
                                                        ?>
                                                        <a class="btn btn-success" style="cursor: default;
                                                           cursor: default;
                                                           font-size: 10px;
                                                           height: 29px;"><?php echo $language_data->__('text_available'); ?></a>

                                                    <?php } else if ($empopentask123['totord'] >= 1) { 
                                                        $downloadExcelArray[$y][$language_data->__('text_work_status')] = $language_data->__('text_on_duty');
                                                        ?>
                                                        <a class="btn btn-warning"  style="cursor: default;
                                                           cursor: default;
                                                           font-size: 10px;
                                                           height: 29px;"><?php echo $language_data->__('text_on_duty'); ?></a>


                                                    <?php }
                                                    ?>

                                                </td>
                                            </tr>
                                            <?php
                                            $x++;
                                        }
                                        }
                                        DB::setFetchMode($fetchMode);
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <div class="pagination pagination-centered"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                
                            </div>
                            {!! Form::open(['class'=>'form-horizontal bucket-form downloadXls',"files"=>true,'url' => $urlPrifix.'/report/downloadExcel']) !!}
                            <input type="hidden" name="xlsData" value='<?=json_encode($downloadExcelArray)?>' />
                            <input type="hidden" name="filename" value='ActiveuserReport' />
                            {!! Form::close() !!}
                            {!! Form::open(['class'=>'form-horizontal bucket-form downloadPDF',"files"=>true,'url' => $urlPrifix.'/report/downloadPDF']) !!}
                            <input type="hidden" name="PDFData" id="PDFData" value='<?=json_encode($downloadExcelArray)?>' />
                            <input type="hidden" name="filename" value='ActiveuserReport' />
                            {!! Form::close() !!}
                        </div>


                    </div>

                </div>
            </div>


            <!-- /END OF CONTENT -->


            <!-- FOOTER -->

            <!-- / END OF FOOTER -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');

<script type="text/javascript">
    $(document).ready(function () {
        $('#downloadXLSFile').on('click', function () {
            $('.downloadXls').submit();
        });
        $('#downloadPDFFile').on('click', function () {
            $('.downloadPDF').submit();
        });
        //console.log($('#Footable').html());
    });
</script> 
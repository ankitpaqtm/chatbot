@include('layouts.header')
@php
$language_data=new Language();
@endphp
<?php
$fetchMode = DB::getFetchMode();
DB::setFetchMode(\PDO::FETCH_ASSOC);
$companyid = $uid;
$sqry = "";
$qs = "";
if(isset($_POST['filter'])) {
  extract($_POST);
  if($_POST['cmbteam'] != '') {
    $sqry .= " and l.log_name_id = " . $_POST['cmbteam'];
  }
  if(!empty($_POST['txtDateFrom']) and !empty($_POST['txtDateTo'])) {
    
//    if (isset($_SESSION['language']) && $_SESSION['language'] == "persian") {
//        $arrFrom = explode("-", $_POST["txtDateFrom"]);
//        $arrTo = explode("-", $_POST["txtDateTo"]);
//        $txtDateFrom = jalali_to_gregorian($arrFrom[0], $arrFrom[1], $arrFrom[2], "-");
//        $txtDateTo = jalali_to_gregorian($arrTo[0], $arrTo[1], $arrTo[2], "-");
//    } else {
        $txtDateFrom = $_POST["txtDateFrom"];
        $txtDateTo = $_POST["txtDateTo"];
//    }
    $sqry .=  " and (added_date >= '".$txtDateFrom." 00:00:00' and added_date <= '".$txtDateTo." 23:59:59')";
  }
}
$logs_query = "select l.log_name_id, l.log_id, l.user_name, l.desc, l.added_date, ln.name as log_name FROM logs l, log_names ln where ln.Id = l.log_name_id and client_id = $companyid and ln.status = 1 " . $sqry . " order by l.added_date desc" ;

$filterdata = DB::select($logs_query);
?>

<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_report') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_list_of_all_logs_reports') }} </h6>
                            </label>

                            <label class="col-sm-3 pull-right" style="margin-top:10px;">
                                <div class="btn-group pull-right visible-lg">
                                    <div class="btn">
                                      <i class="icon-download"></i>  {{ $language_data->__('text_download_report') }}</div>
                                    <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li>
                                            <a href="javascript:;" id="downloadPDFFile"><i class="fa fa-file-pdf-o"></i> {{ $language_data->__('text_download_pdffile') }}</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" id="downloadXLSFile"><i class="fa fa-file-excel-o"></i> {{ $language_data->__('text_download_xlsfile') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </label>
                        </div>
                        <!--<div class="body-nest" id="Filtering">

                        </div>-->

                        <div class="body-nest" id="Footable">
                            <?php
                            $l = new Language();
                            $downloadExcelArray = array();
                            ?>
                            <form name="form1" method="post" action="">  
                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                  <tr>
                                    <td width="17%"><strong><?php echo $l->__('text_module'); ?></strong></td>
                                    <td width="17%"><strong><?php echo $l->__('text_date_from'); ?></strong></td>
                                    <td width="17%"><strong><?php echo $l->__('text_date_to'); ?></strong></td>
                                    <td width="17%">&nbsp;</td>
                                  </tr>
                                  <tr >
                                    <td width="17%">
                                    <select name="cmbteam" id="cmbteam"  class="form-control"  style="width:95% !important" >
                                      <option value=""><?php echo $l->__('text_select_module'); ?></option>
                                      <?php 
                                      $log_names_list = DB::select("Select * from log_names where status = 1");
                                      foreach($log_names_list as $log_list) {
                                        ?>
                                        <option value="<?php echo $log_list['Id']?>" <?php if(isset($_POST['cmbteam'])) { if($_POST['cmbteam'] == $log_list['Id']) { print "selected='selected'"; } } ?>> <?php echo $l->__('text_' . strtolower($log_list['name'])) ;?>  </option>
                                        <?php
                                      }
                                      ?>
                                    </select>
                                    </td>
                                    <td width="17%"><input type="text" name="txtDateFrom" class="input-large form-control" id="txtDateFrom" style="width:90% !important" value="<?php if(isset($_POST['txtDateFrom'])) { print $_POST['txtDateFrom']; } ?>" /></td>
                                    <td width="17%"><input type="text" name="txtDateTo" class="input-large form-control" id="txtDateTo" style="width:90% !important" value="<?php if(isset($_POST['txtDateTo'])) { print $_POST['txtDateTo']; } ?>" /></td>
                                    <td width="17%">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button class="btn btn-info left filterButton" name="filter" type="submit"><?php echo $l->__('text_filter'); ?></button>
                                    </td>
                                  </tr>
                                </table>
                                <div style="margin-bottom:30px;"></div>
                                <table class="table table-striped clickable" id="dynamic-table1">
                                  <thead>
                                    <tr class="info">
                                    <th width="5%">#</th>
                                    <th width="15%"><?php echo $l->__('text_module_name'); ?></th>
                                    <th width="15%"><?php echo $l->__('text_activity'); ?></th>
                                    <th width="15%"><?php echo $l->__('text_action'); ?></th>
                                    <th width="15%"><?php echo $l->__('text_by_user'); ?></th>
                                    <th width="15%"><?php echo $l->__('text_date_time'); ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php 
                                    $x = 1;
                                    foreach($filterdata as $data) {
                                        $y= $x-1;
                                      ?>
                                      <tr>
                                        <td><?php $downloadExcelArray[$y]['#'] = $language_data->digits($x); echo $l->digits($x);?></td>
                                        <td><?php $downloadExcelArray[$y][$language_data->__('text_module_name')] = $l->__('text_' . strtolower($data['log_name']));print $l->__('text_' . strtolower($data['log_name']));?></td>
                                        <td>
                                          <?php 
                                          $module_id = $data['log_id'];
                                          if ($data['log_name_id'] == 1) {
                                              $downloadExcelArray[$y][$language_data->__('text_activity')] = $l->__('text_admin');
                                            print $l->__('text_admin');
                                          }
                                          elseif ($data['log_name_id'] == 2) {
                                            $client_name_rslt = collect(\DB::select("select client_name from tbl_clients where clientid = $module_id"))->first();
                                            print $client_name_rslt['client_name'];
                                            $downloadExcelArray[$y][$language_data->__('text_activity')] = $client_name_rslt['client_name'];
                                          }
                                          elseif ($data['log_name_id'] == 3) {
                                            $client_name_rslt = collect(\DB::select("select sup_name from tbl_supervisor where supervisor_id = $module_id"))->first();
                                            print $client_name_rslt['sup_name'];
                                            $downloadExcelArray[$y][$language_data->__('text_activity')] = $client_name_rslt['sup_name'];
                                          }
                                          elseif ($data['log_name_id'] == 4) {
                                            $client_name_rslt = collect(\DB::select("select emp_name from tbl_employee where emp_id = $module_id"))->first();
                                            print $client_name_rslt['emp_name'];
                                            $downloadExcelArray[$y][$language_data->__('text_activity')] = $client_name_rslt['emp_name'];
                                          }
                                          elseif ($data['log_name_id'] == 5) {
                                            $client_name_rslt = collect(\DB::select("select task_name from tbl_task where task_id = $module_id"))->first();
                                            print $client_name_rslt['task_name'];
                                            $downloadExcelArray[$y][$language_data->__('text_activity')] = $client_name_rslt['task_name'];
                                          }
                                          elseif ($data['log_name_id'] == 6) {
                                            $client_name_rslt = collect(\DB::select("select name from task_categories where Id = $module_id"))->first();
                                            print $client_name_rslt['name'];
                                            $downloadExcelArray[$y][$language_data->__('text_activity')] = $client_name_rslt['name'];
                                          }
                                          elseif ($data['log_name_id'] == 7) {
                                            $client_name_rslt = collect(\DB::select("select company_name from partnerslist where partnerid = $module_id"))->first();
                                            print $client_name_rslt['company_name'];
                                            $downloadExcelArray[$y][$language_data->__('text_activity')] = $client_name_rslt['company_name'];
                                          }
                                          ?>
                                        </td>
                                        <td>
                                          <?php $downloadExcelArray[$y][$language_data->__('text_action')] = $l->__('text_' . strtolower(str_replace(" ", "_", $data['desc'])));print $l->__('text_' . strtolower(str_replace(" ", "_", $data['desc'])));?>
                                        </td>
                                        <td>
                                          <?php $downloadExcelArray[$y][$language_data->__('text_by_user')] = $data['user_name']; print $data['user_name'];?>
                                        </td>
                                        <td>
                                          <?php 
                                          if($data['added_date'] !='0000-00-00 00:00:00') {
                                              echo $l->convertedDates($data['added_date'],"fulldatetime");
                                              $downloadExcelArray[$y][$language_data->__('text_date_time')] = $l->convertedDates($data['added_date'],"fulldatetime");
                                          }
                                          ?>
                                        </td>
                                      </tr>   
                                      <?php 
                                      $x++;  
                                    }   
                                    ?>
                                    <tr>
                                      <td colspan="7" style="text-align:center">
                                        <?php 
                                        if (empty($filterdata)) {
                                          echo $l->__('text_no_data');
                                        }
                                        ?>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </form>
                        </div>
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadXls',"files"=>true,'url' => $urlPrifix.'/report/downloadExcel']) !!}
                        <input type="hidden" name="xlsData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='LogReport' />
                        {!! Form::close() !!}
                        {!! Form::open(['class'=>'form-horizontal bucket-form downloadPDF',"files"=>true,'url' => $urlPrifix.'/report/downloadPDF']) !!}
                        <input type="hidden" name="PDFData" id="PDFData" value='<?=json_encode($downloadExcelArray)?>' />
                        <input type="hidden" name="filename" value='LogReport' />
                        {!! Form::close() !!}
                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');

<script type="text/javascript">
    $(".filterButton").on("click", function (e) {

        var fromDate = $("#txtDateFrom").val();
        var toDate = $("#txtDateTo").val();

        if (fromDate == "" && toDate != "") {
            alert("<?php echo $l->__('text_log_report_filter_date_error_msg1'); ?>");
            return false;
        } else if (fromDate != "" && toDate == "") {
            alert("<?php echo $l->__('text_log_report_filter_date_error_msg2'); ?>");
            return false;
        }

        if (fromDate != "" || toDate != "") {
            if (new Date(fromDate) > new Date(toDate))
            {
                alert("<?php echo $l->__('text_log_report_filter_date_valid_msg'); ?>");
                return false;
            }
        }

    });
    /*
     TODO::Calender js code start
     */
    var language = $("#language").val();
    if (language == 'english')
    {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            langNumbers: true,
            weekNumbers: false
        });
    } else {
        Calendar.setup({
            inputField: "txtDateFrom", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
        Calendar.setup({
            inputField: "txtDateTo", // id of the input field
            button: "date_btn_9", // trigger for the calendar (button ID)
            ifFormat: "%Y-%m-%d", // format of the input field
            showsTime: false,
            dateType: 'jalali',
            langNumbers: true,
            weekNumbers: false
        });
    }

    /*
     TODO::Calender js code end
     */
</script>
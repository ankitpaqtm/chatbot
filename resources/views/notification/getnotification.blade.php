<?php
$language_data = new Language();
?>
<a data-toggle="dropdown" id="notificationCountBellId" class="dropdown-toggle" href="#" title="<?=sprintf($language_data->__('text_notifications'), $language_data->digits($nCount))?>">
    <i style="font-size:20px;" class="entypo-bell"></i>
    <div class="noft-red" id="NotiCount">
        <?=$language_data->digits($nCount)?>
    </div>
</a>
<ul style="margin: 11px 0 0 9px;height: 342px;overflow-y: scroll;" role="menu" class="dropdown-menu dropdown-wrap">

    <!--<li>
        <div style="width: 100%;" id="liNotiCount"><?=sprintf($language_data->__('text_notifications'), $language_data->digits($nCount))?></div>
    </li>-->
    <li class="divider"></li>
    <?php
    if(!empty($notiDatas)){
        foreach ($notiDatas as $noti){
    ?>
        <li>
            <a href="javascript:;">
                <?php if (!empty($noti->profileimage) && file_exists(public_path('uploads/employees/'.$noti->profileimage))) { ?>
                    <img style="top:10px;width: 30px;" class="img-msg img-circle" src="<?= url('public/uploads/employees/') . '/' . $noti->profileimage?>" />
                <?php } else { ?>
                    <img style="top:10px;width: 30px;" class="img-msg img-circle" src="<?= asset('resources/assets/momom-assets/img/user.png');?>" /> 
                <?php } ?>
                    <strong>    
                    <?= $noti->emp_name?> 
                    <?php
                    if($noti->is_accepted=='1'){
                        echo $language_data->__('text_accepted_task');
                    } else if($noti->is_accepted=='2'){
                        echo $language_data->__('text_rejected_task');
                    } else if($noti->is_accepted=='3'){
                        echo $language_data->__('text_completed_task');
                    } else if($noti->is_accepted=='4'){
                        echo $language_data->__('text_started_task');
                    }
                    echo ' "'.$noti->task_name.'"';
                    ?>
                    </strong>
                <div style="visibility: hidden;"></div> 
                 
                <b style="left:60px;margin-top: -10px;width: 34%;">
                    <?php
                        $newDate = date("d-m-Y H:i:s", strtotime($noti->updated_date));
                        echo "AT: ".$language_data->digits($newDate);
                    ?>
                </b>
                <div style="visibility: hidden;"></div>    
            </a>
        </li>
        <li class="divider"></li>
    <?php
        }
    }
    ?>

</ul>
<script type="text/javascript">
    $('#notificationCountBellId').click(function(){
        $.ajax({
            url: "<?= url("clearnotificationcount") ?>",
            type: "POST",
            data: "_token=<?=csrf_token()?>",
            success: function (msg) {
                $("#NotiCount").text('0');
                //$("#liNotiCount").text('<?=sprintf($language_data->__('text_notifications'), 0)?>');
            }
        });
    });
</script>                
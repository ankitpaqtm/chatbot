<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 24/1/17
 * Time: 3:02 PM
 */
?>
@php
    $language_data=new Language();
@endphp
<script type="text/javascript">
    $(document).ready(function(){
        $(".submitPartner").click(function () {
            $(".error").html("");
            $(".employeecount").css("border","1px solid #e2e2e4");


            if ($("#company_name").val() == "") {
                $("#company_name_error").html('<?php echo $language_data->__('text_name_validation_msg'); ?>');
                return false;
            }
            if ($("#address").val() == "") {
                $("#address_error").html('<?php echo $language_data->__('text_location_add_validation_msg'); ?>');
                return false;
            }

            if($('#clientlogo').val() != ""){
                var ext = $('#clientlogo').val().split('.').pop().toLowerCase();
                if ($('#clientlogo').val() != "" && $.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                    $("#clientlogo_error").html('<?php echo $language_data->__('text_images_ext_allowed'); ?>');
                    return false;
                }
            }
            if ($("#email_id").val() == "") {
                $("#email_id_error").html('<?php echo $language_data->__('text_email_validation_msg'); ?>');
                return false;
            }

            var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            if (!filter.test($("#email_id").val())) {
                $("#email_id_error").html('<?php echo $language_data->__('error_invalid_email'); ?>');
                return false;
            }

            var language=$("#language").val();
            if (language != 'persian') {
                if ($("#email_id").val().charAt(0) == ".") {
                    $("#email_id_error").html('<?php echo $language_data->__('error_invalid_email'); ?>');
                    return false;
                }
            }
            if ($("#admin_username").val() == "") {
                $("#admin_username_error").html('<?php echo $language_data->__('text_admin_username_add'); ?>');
                return false;
            }
            

            if ($("#mobile").val() == "") {
                $("#contact_number_error").html('<?php echo $language_data->__('text_valid_contact_msg'); ?>');
                return false;
            }

            if ($("#mobile").val() != "" && $("#mobile").val().length < 8) {
                $("#contact_number_error").html('<?php echo $language_data->__('text_delete_partner_valid_contact_msg'); ?>');
                return false;
            }
            

        });
        $(".changespass").click(function () {
            $("#admin_password_error").html('');
            $("#new_password_error").html('');
            $("#reenter_password_error").html('');
            if ($(".admin_password").val() == "") {
                $("#admin_password_error").html('<?php echo $language_data->__('text_old_password_validation_msg'); ?>');
                return false;
            }
            if ($(".new_password").val() == "") {
                $("#new_password_error").html('<?php echo $language_data->__('text_new_password_validation_msg'); ?>');
                return false;
            }
            if ($(".reenter_password").val() == "") {
                $("#reenter_password_error").html('<?php echo $language_data->__('text_reenter_password_validation_msg'); ?>');
                return false;
            }
            if ($(".new_password").val() != $(".reenter_password").val()) {
                $("#reenter_password_error").html('<?php echo $language_data->__('text_new_pwd_reenter_not_match'); ?>');
                return false;
            }


        });
    })
</script>

<?php
/**
 * Created by PhpStorm.
 * User: avadh-latitude
 * Date: 17/1/17
 * Time: 12:26 PM
 */

?>

@include('layouts.header')
@php
$language_data=new Language();
@endphp
<body>


<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_partners') }}
                            </span>
                    </h2>

                </div>

                <div class="col-sm-7">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <h6>
                                {{ $language_data->__('text_settings') }} </h6>
                            <div class="titleClose">
                                <a class="gone" href="#FootableClose">
                                    <span class="entypo-cancel"></span>
                                </a>
                            </div>
                            <div class="titleToggle">
                                <a class="nav-toggle-alt" href="#Footable">
                                    <span class="entypo-up-open"></span>
                                </a>
                            </div>

                        </div>


                        <div class="body-nest" id="element">
                            <div class="panel-body">
                                    @if(Session::has('msg'))
                                        <div class="alert {!!Session::get('alert')!!}">
                                            <a class="close" data-dismiss="alert">×</a>
                                            <strong>{!!Session::get('msg')!!}</strong> 
                                        </div>
                                    @endif
                                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/setting/store']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_name') }} <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                        <input type="hidden" name="partnerid" value="{{ $partner_data[0]->partnerid }}" />
                                            <input type="text" class="form-control" id="company_name" name="company_name" novalidate required autocomplete="off" class="form-control placeholder-no-fix p_name alpha-only" value="{{ $partner_data[0]->company_name }}">
                                            <label for="company_name" id="company_name_error" generated="true" class="error">{{ $errors->first('company_name') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_location_address') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <textarea name="address" id="address" required    style="margin-bottom:10px;" class="form-control address" >{{ $partner_data[0]->address }}</textarea>
                                            <label for="address" id="address_error" generated="true" class="error">{{ $errors->first('address') }}</label>
                                            <input type="hidden" id="latitude" name="latitude" value="{{ $partner_data[0]->latitude }}" >
                                            <input type="hidden" id="longitude" name="longitude" value="{{ $partner_data[0]->longitude }}" >

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 pull-left">
                                        <div id="myMap1"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_logo') }} </label>
                                        <div class="col-sm-6">
                                            <img src="{{ url('public/uploads/logos/').'/'.$partner_data[0]->logo}}" alt="" height="100" width="100" />
                                            <input type="file" style="margin-bottom:10px;height:auto;"    placeholder="" autocomplete="off" name="clientlogo" id="clientlogo"  class="form-control round-input imgLogo" >
                                            <label for="address" id="clientlogo_error" generated="true" class="error">{{ $errors->first('clientlogo') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_email') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" required placeholder="" value="{{$partner_data[0]->email_id}}" id="email_id" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,3}$"   name="email_id"  class="form-control placeholder-no-fix emailId">
                                            <label for="email_id" id="email_id_error" generated="true" class="error">{{ $errors->first('email_id') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_admin_username') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" style="margin-bottom:10px;" required  value="{{$partner_data[0]->admin_usernmae}}"    id="admin_username"   name="admin_username"   placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
                                            <label for="address" id="admin_username_error" generated="true" class="error">{{ $errors->first('admin_username') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_admin_password') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <button class="btn btn-success" href="#changepassword" id="changepassbtn" data-toggle="modal" type="button">{{ $language_data->__('text_change_pwd') }}</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{ $language_data->__('text_contact_number') }} <span class="required">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" maxlength="16" required    novalidate  name="mobile" id="mobile"  value="{{$partner_data[0]->mobile}}"  autocomplete="off" class="form-control placeholder-no-fix contactNumber allowNumber">
                                            <label for="contact_number" id="contact_number_error" generated="true" class="error"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_employees_allowed') }}</span></label>
                                        <div class="col-sm-4">
                                            <input type="number" readonly="" value="{{$partner_data[0]->employeecount}}"  class="form-control placeholder-no-fix ">
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            <span class="required">
                                                <?php 
                                                    $empCount = DB::table("tbl_employee")
                                                    ->where("companyid", $partner_data[0]->partnerid)
                                                    ->where("delete_employee", '0') 
                                                    ->get()->count();
                                                    echo $language_data->__('text_used').': '.$empCount; 
                                                ?>
                                            </span>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_supervisors_allowed') }}</span></label>
                                        <div class="col-sm-4">
                                            <input type="number" readonly="" value="{{$partner_data[0]->supervisorcount}}"  class="form-control placeholder-no-fix">
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            <span class="required">
                                                <?php 
                                                    $supCount = DB::table("tbl_supervisor")
                                                    ->where("companyid", $partner_data[0]->partnerid)
                                                    ->where("deleteval", '0') 
                                                    ->get()->count();
                                                    echo $language_data->__('text_used').': '.$supCount; 
                                                ?>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_clients_allowed') }}</span></label>
                                        <div class="col-sm-4">
                                            <input type="number" readonly="" value="{{$partner_data[0]->clinetscount}}"  class="form-control placeholder-no-fix">
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            <span class="required">
                                                <?php 
                                                    $clntCount = DB::table("tbl_clients")
                                                    ->where("companyid", $partner_data[0]->partnerid)
                                                    ->get()->count();
                                                    echo $language_data->__('text_used').': '.$clntCount; 
                                                ?>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_no_of_tasks_allowed') }}</span></label>
                                        <div class="col-sm-4">
                                            <input type="number" readonly="" value="{{$partner_data[0]->taskcount}}"  class="form-control placeholder-no-fix">
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            <span class="required">
                                                <?php 
                                                    $taskCount = DB::table("tbl_task")
                                                    ->where("comp_id", $partner_data[0]->partnerid)
                                                    ->where("task_delete", '0') 
                                                    ->get()->count();
                                                    echo $language_data->__('text_used').': '.$taskCount; 
                                                ?>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><span class="required">{{ $language_data->__('text_duedate') }} *</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" readonly=""  value="{{$partner_data[0]->due_date}}" class="form-control placeholder-no-fix">
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center">
                                    <button class="btn btn-info submitPartner" name="add_partner" id="add_partner" type="submit">{{ $language_data->__('text_update') }}</button>
                                    </div>
                                {!! Form::close() !!}
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>

         <div aria-hidden="true" aria-labelledby="myModalLabel" style="overflow:auto;" role="dialog" tabindex="-1" id="changepassword" class="modal fade">

                <div class="modal-dialog">
                    {!! Form::open(['class'=>'form-horizontal bucket-form',"files"=>true,'url' => 'partner/setting/changepassword']) !!}
                        <div class="modal-content">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>


                                <h4 class="modal-title"><?php echo $language_data->__('text_change_pwd'); ?></h4>

                            </div>

                            <div class="modal-body">

                                <div class="col-sm-4">

                                    <label><?php echo $language_data->__('text_old_pwd'); ?></label></div>

                                <div class="col-sm-8">

                                    <input type="password" style="margin-bottom:10px;" tabindex="1" required  name="admin_password"   value="{{ old('admin_password') }}"      placeholder="<?php echo $language_data->__('text_old_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix admin_password">
                                    <label for="admin_password" id="admin_password_error" generated="true" class="error">{{ $errors->first('admin_password') }}</label>
                                </div>  <div class="clearfix"></div>


                                <div class="col-sm-4">

                                    <label><?php echo $language_data->__('text_new_pwd'); ?></label>
                                </div>

                                <div class="col-sm-8">
                                    <input type="password" style="margin-bottom:10px;" required  tabindex="2" name="new_password"   value="{{ old('new_password') }}"  placeholder="<?php echo $language_data->__('text_new_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix new_password">
                                    <label for="new_password" id="new_password_error" generated="true" class="error">{{ $errors->first('new_password') }}</label>
                                </div>  

                                <div class="clearfix"></div>


                                <div class="col-sm-4">

                                    <label><?php echo $language_data->__('text_re_enter_pwd'); ?></label>
                                </div>

                                <div class="col-sm-8">
                                    <input type="password" style="margin-bottom:10px;" required  tabindex="3" name="reenter_password"   value="{{ old('reenter_password') }}"      placeholder="<?php echo $language_data->__('text_re_enter_pwd'); ?>" autocomplete="off" class="form-control placeholder-no-fix reenter_password">
                                    <label for="reenter_password" id="reenter_password_error" generated="true" class="error">{{ $errors->first('reenter_password') }}</label>
                                </div>  

                                <div class="clearfix"></div>
                                <div class="clear"></div>


                                <div class="modal-footer">



                                    <button tabindex="4" class="btn btn-success changespass" name="changepasswordsubmit"
                                            type="submit"><?php echo $language_data->__('text_submit'); ?></button>
                                     <button tabindex="5" data-dismiss="modal" class="btn btn-default"
                                            type="button"><?php echo $language_data->__('text_cancel'); ?></button>

                                </div>

                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>

        </div>
        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>
@include('layouts.footer');
@include('partnerSetting.validation');
<script type="text/javascript">
    $(document).ready(function () {
        @if(Session::has('passError'))
            $('#changepassbtn').trigger('click');
        @endif    
        $('#changepassword').on('show.bs.modal', function () {


        });
    });
</script>  





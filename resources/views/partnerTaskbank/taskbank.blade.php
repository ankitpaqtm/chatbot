@include('layouts.header')
@php
$language_data=new Language();
@endphp
<?php
$fetchMode = DB::getFetchMode();
DB::setFetchMode(\PDO::FETCH_ASSOC);
$companyid = $uid;
$date = date('Y-m-d');
//$date = "2017-02-22";
$title = $language_data->__('text_today');
if (isset($_POST['date_to'])):
    $date = date('Y-m-d', strtotime($_POST['date_to']));
    $title = date('l,d F Y', strtotime($date));
endif;

if($supervisor_id > 0){
$totalemp = "SELECT emp_id,emp_name from tbl_employee WHERE companyid = " . $uid . " AND supervisor_id = ".$supervisor_id." AND delete_employee = 0";    
}  else {
$totalemp = "SELECT emp_id,emp_name from tbl_employee WHERE companyid = " . $uid . " AND delete_employee = 0";    
}

$aEmployees = DB::select($totalemp);


$sQuery = "SELECT t.et_id,ta.task_id,ta.duration,ta.task_name,ta.is_recurring,t.emp_id,t.totalhours,t.et_hours,t.task_starttime,task_endtime,t.et_mins,ta.tasktype,t.started,t.status FROM tbl_emp_task t";
//$sQuery .= " INNER JOIN tbl_task ta ON ta.task_id = t.task_id AND ta.task_delete = 0 AND ta.is_recurring = 0";
$sQuery .= " INNER JOIN tbl_task ta ON ta.task_id = t.task_id AND ta.task_delete = 0";

if($supervisor_id > 0){
$sQuery .= " WHERE t.supervisor_id = ".$supervisor_id." AND DATE(t.et_duedate) LIKE '%" . $date . "%' AND t.emp_id = {emp} AND t.et_hours = {hour}";    
}  else {
$sQuery .= " WHERE DATE(t.et_duedate) LIKE '%" . $date . "%' AND t.emp_id = {emp} AND t.et_hours = {hour}";    
}


//echo $sQuery;
?>
<script type="text/javascript" src="{{ asset('resources/assets/momom-assets/js/redipsdrag/redips-drag-min.js') }}"></script>
<!--  PAPER WRAP -->
<div class="wrap-fluid">
    <div class="container-fluid paper-wrap bevel tlbr">


        <!-- CONTENT -->
        <!--TITLE -->
        <div class="row">
            <div id="paper-top">
                <div class="col-sm-3">
                    <h2 class="tittle-content-header">
                        <span class="entypo-menu"></span>
                        <span>{{ $language_data->__('text_tasks') }}</span>
                    </h2>
                </div>

                <div class="col-sm-7"></div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <!--/ TITLE -->

        <!-- BREADCRUMB -->


        <!-- END OF BREADCRUMB -->


        <div class="content-wrap">
            <div class="row">


                <div class="col-sm-12">

                    <div class="nest" id="FootableClose">
                        <div class="title-alt">
                            <label class="col-sm-3">
                                <h6>{{ $language_data->__('text_task_bank') }} </h6>
                            </label>

                        </div>
                        <!--<div class="body-nest" id="Filtering">

                        </div>-->

                        <div class="body-nest" id="Footable">
                            <?php
                            $l = new Language();
                            $downloadExcelArray = array();
                            ?>
                            <div class="col-md-4 pull-right">
                                <form name="form1" method="post" action="">
                                <table width="100%" border="0">
                                    <tr>
                                        <th><?php echo $l->__('text_date'); ?></th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" name="date_to" id="txtDateTo" value="<?php if (isset($_POST['date_to'])) { print $_POST['date_to']; } ?>" class="form-control input-large" placeHolder="<?php echo $l->__('text_date'); ?>" />
                                        </td>
                                        <td class="text-right">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-info left filterButton" name="search" type="submit"><?php echo $l->__('text_search'); ?></button>
                                        </td>
                                    </tr>
                                </table>
                                </form> 
                            </div>
                                <div style="clear:both"></div>

                                <div class="row" id="right">
                                    <div class="col-sm-12 text-center" style="color: #1462a6;line-height: 50px;">			  		
                                        <strong style="font-size: 16px;"><?php echo $title; ?></strong>  		
                                    </div>
                                    <div class="clear"></div>
                                    <div class="col-sm-12">
                                        <div class="adv-table">
                                            <div class="col-sm-12" id="redips-drag" style="padding: 0;">
                                                <table class="tableClass table" id="table2">
                                                    <tbody>
                                                        <tr style="background: #26a637;color:#fff !important;">
                                                            <td class="redips-mark blank" style="height:40px;"><?php echo $l->__('text_staff'); ?></td>
                                                            <?php for ($i = 0; $i < 24; $i++): ?>
                                                                <td class="redips-mark"><?php echo $l->digits(number_format($i, 2)); ?></td>
                                                            <?php endfor; ?>
                                                        </tr>
                                                        <?php foreach ($aEmployees as $aEmployee): ?>
                                                            <tr style="height: 40px;">
                                                                <td class="redips-mark" style="width:100px;background-color:#1462a6;color:#fff;"><?php echo $aEmployee['emp_name']; ?></td>
                                                                <?php
                                                                for ($i = 0; $i < 24; $i++):
                                                                    //echo str_replace(array('{emp}','{hour}'), array($aEmployee['emp_id'],$iHour), $sQuery);
                                                                    $iHour = $i;
                                                                    $flt_Q = str_replace(array('{emp}', '{hour}'), array($aEmployee['emp_id'], $iHour), $sQuery);
                                                                    $aTaskData = DB::select($flt_Q);
                                                                    $iColSpan = 1;
                                                                    $sClass = "";
                                                                    $bOld = 1;
                                                                    $sColor = '';
                                                                    $taskName = '';
                                                                    $duration = '';
                                                                    if ($aTaskData && !empty($aTaskData)) {
                                                                        
                                                                        $aTask = $aTaskData[0];
                                                                        //print_r($aTask);exit;
                                                                        $taskName = $aTask['task_name'];
                                                                        $aTask['totalhours'] = 1;
                                                                        $datetime1 = strtotime($aTask['task_starttime']);
                                                                        $datetime2 = strtotime($aTask['task_endtime']);
                                                                        $intervalCount = abs($datetime2 - $datetime1);
//                                                                        $hoursTotal = round($intervalCount / 3600);
                                                                        
                                                                        $duration =  $aTask['duration'];
                                                                        
                                                                        $hoursTotal =  $duration / 60;
                                                                        
                                                                        $hoursTotal =  round($hoursTotal);
                                                                        
                                                                        
                                                                        
                                                                        if($hoursTotal > 1){
                                                                            $aTask['totalhours'] = $hoursTotal;
                                                                        }
                                                                        $iColSpan = $aTask['totalhours'];
                                                                        $i += $aTask['totalhours'] - 1;
                                                                        $sClass = "drop";
                                                                        $bOld = 0;
                                                                        $sColor = "grey";
                                                                        if (!$aTask['started']) {
                                                                            //$sColor = "blue";
                                                                            $iStatus = $aTask['status'];
                                                                            if ($iStatus == 2)
                                                                                $sColor = "green";
                                                                            else if ($iStatus == -1)
                                                                                $sColor = "red";
                                                                        }
                                                                        if ($aTask['tasktype'] == 2)
                                                                            $sColor .= ' special';
                                                                    }
                                                                    ?>
                                                                    <td class="<?php echo $sClass . ' ' . $sColor; ?>" colspan="<?php echo $iColSpan; ?>" data-task="<?php echo $iHour . '_' . $aEmployee['emp_id'] . '_' . $iColSpan . '_' . $bOld; ?>" data-emp-task="<?php echo (isset($aTask['et_id']) ? ($aTask['et_id'] . ' ' . $aTask['task_id'].' '.$aTask['emp_id'].' '.$aTask['is_recurring']) : ''); ?>">
                                                                        <div class="<?php echo $bOld == 0 ? 'redips-drag' : ''; ?> drag-box" title="<?php echo $language_data->__('text_task_name').": " .$taskName.", ". $language_data->__('text_duration_min').": ".$duration; ?>"><?php echo $taskName ?></div>
                                                                    </td>
                                                                <?php endfor; ?>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>			
                                            <div id="redips-clone"></div>
                                        </div>
                                    </div>
                                    <div class="body-nest col-sm-12">
                                        <div class="text-center">
                                            <span style="padding:5px 15px 5px 15px;background:#999;">&nbsp;</span> <span style="font-weight:bold;"><?php echo $language_data->__('text_pending'); ?></span>
                                            <span style="margin:2px;"></span>
                                            <span style="padding:5px 15px 5px 15px;background:#4AAB07;">&nbsp;</span> <span style="font-weight:bold;"><?php echo $language_data->__('text_completed'); ?></span>
<!--                                            <span style="margin:2px;"></span>
                                            <span style="padding:5px 15px 5px 15px;background:#C2171A;">&nbsp;</span> <span style="font-weight:bold;"><?php //echo $language_data->__('text_rejected'); ?></span>-->
                                        </div>
                                    </div>
                                </div>
                            

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <!-- /END OF CONTENT -->


        <!-- FOOTER -->

        <!-- / END OF FOOTER -->


    </div>
</div>
<!--  END OF PAPER WRAP -->

<!-- RIGHT SLIDER CONTENT -->
</body>

@include('layouts.footer');

<script type="text/javascript">
    $(".filterButton").on("click", function (e) {
        var toDate = $("#txtDateTo").val();
        if (toDate == "") {
            alert("<?php echo $l->__('text_work_report_filter_date_error_msg3'); ?>");
            return false;
        }
    });
    /*
     TODO::Calender js code start
     */
    
    $('#txtDateTo').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });
    /*
     TODO::Calender js code end
     */
     var sMsg = '';
    $(document).ready(function() {
        var rd = REDIPS.drag;
        rd.init();
        rd.dropMode = 'single';
        rd.event.droppedBefore = function(td) {
                var bValid = true;
                $('.alert').remove();
                try {
                        var source = $(rd.td.source);
                        //if(!source.hasClass('blue'))
                        if(!source.hasClass('grey'))
                                throw "<?php echo $language_data->__('error_reassign'); ?>";
                        var target = $(rd.td.target);
                        if(source.is(target))
                                return bValid;
                        var iCol = source.attr('colspan');
                        var selector = 'td:lt(' + (iCol - 1) + ')';
                        var aNext = target.nextAll(selector);
                        if((aNext.length + 1) < iCol)
                                throw "<?php echo $language_data->__('error_duration'); ?>";
                        $(aNext).each(function() {
                                if($(this).not(source).hasClass('drop')) {
                                        throw "<?php echo $language_data->__('error_timeslot'); ?>";
                                        return;
                                }
                        });
                        var aSource = source.attr('data-task').split('_').ToInt();
                        var aTarget = target.attr('data-task').split('_').ToInt();
                        var sEmp = source.attr('data-emp-task');
                        var aTask = sEmp.split(' ').ToInt();
                        var data = {
                                CurrentEmpId: aTask[2],
                                taskId: aTask[1],
                                taskEmpId: aTask[0],
                                EmpId: aTarget[1],
                                iHour: aTarget[0],
                                isRecurring: aTask[3],
                        };
                        if(!relocateTask(data))
                                throw sMsg;
                        var html = ''
                        for(var i = aSource[0] + 1;i < aSource[0] + aSource[2];i++) {
                                html += '<td colspan="1" data-task="' + i + '_' + aSource[1] + '_1_1" data-emp-task=""><div class="drag-box"></div></td>';
                        }
                        source.after(html);
                        var sClass = source.attr('class');
                        source.attr({colspan:1,'data-task': aSource[0] + '_' + aSource[1] + '_1_1','data-emp-task':''}).removeClass();
                        target.nextAll(selector).remove();
                        target.attr({colspan: iCol,'data-task': aTarget[0] + '_' + aTarget[1] + '_' + iCol + '_0','data-emp-task':sEmp}).addClass(sClass);
                        $('#right').before('<div class="alert alert-success">' + sMsg + '</div>');
                }
                catch(ex) {
                        bValid = false;
                        $('#right').before('<div class="alert alert-danger">' + ex + '</div>');
                }
                setTimeout(function() {
                        $('.alert').fadeOut();
                },2000);
                return bValid;
        }
    });

    function relocateTask(data) {
            if(data === null || data === undefined)
                    return;
            var EmpId = data.EmpId;    
            var iHour = data.iHour;    
            var taskId = data.taskId;    
            var taskEmpId = data.taskEmpId;    
            var CurrentEmpId = data.CurrentEmpId;    
            var isRecurring = data.isRecurring;    
            var bValid = true;
            //console.log(data);
            $.ajax({
                    url: '<?= url($urlPrifix."/taskbank/insertdragtask") ?>',
                    data: "_token=<?=csrf_token()?>&EmpId="+EmpId+"&iHour="+iHour+"&taskId="+taskId+"&taskEmpId="+taskEmpId+"&CurrentEmpId="+CurrentEmpId+"&isRecurring="+isRecurring,
                    type: 'POST',
                    dataType: 'json',
                    async: false,
                    beforeSend: function() {

                    },
                    success: function(res) {
                            if(res.hasOwnProperty('error'))
                                    bValid = false;
                            sMsg = "<?php echo $language_data->__('text_task_reassigned'); ?>";
                            if(isRecurring==1){
                                window.location.reload();
                            }
                    },
                    complete: function() {

                    }
            });
            return bValid;
    }

    Array.prototype.ToInt = function() {
            return $.map(this,function(val) {
                    return parseInt(val);
            })
    }
</script>